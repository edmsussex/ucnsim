This repository holds the Sussex fork of STARucn - a Monte Carlo simulation program for ultra-cold neutron experiments. This version adds spin tracking, multi-threading and a number of enhancements tailored to simulating neutron EDM experiments.



Contributors:

* Nick Ayres <<na280@sussex.ac.uk>>
* Philip Harris <<P.G.Harris@sussex.ac.uk>>
* Oliver Winston

This repository is primarily maintained by Nicholas (Nick) Ayres - <<na280@sussex.ac.uk>>. If you encounter any bugs or have any questions, please email Nick or use the bug tracking system. Any feedback is welcomed.

The original project was developed by Benoit Clement of LPSC Grenoble, and is available at http://sourceforge.net/projects/starucn/ .