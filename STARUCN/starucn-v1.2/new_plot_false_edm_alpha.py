#!/usr/bin/env python

import glob
import numpy
import matplotlib
from rootpy.tree import Tree
from rootpy.io import root_open
from root_numpy import tree2array
from matplotlib import pyplot
import sys

epsilon = 0.01

def compare (float_1, float_2):
	return abs(float_1 - float_2) <= epsilon

plotfile = sys.argv[1]

#fedm = 12
#err = 13
#alpha = 23

def getresultsarray(i,outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	bigarray = tree2array(resultstree)
	
	resultsarray = []
	
	for i in len(bigarray):
		result = (bigarray[i][12], bigarray[i][13], bigarray[i][23])
		resultsarray.append(result)
	print alpha, " " , falseedm, "+/- " , error
	return resultsarray




positivealphalist = []
positivefedmlist = []
positivefedmerrorlist = []
negativealphalist = []
negativefedmlist = []
negativefedmerrorlist = []

for i in range (alphalist):
	if alphalist[i] >= 0:
		positivealphalist.append(alphalist[i])
		
	


pyplot.errorbar(alphalist,falseedmlist,yerr=falseedmerrorlist,fmt='+')
pyplot.show()
