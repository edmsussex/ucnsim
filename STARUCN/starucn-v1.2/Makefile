#======================================================================
#STARucn MC software
#File : Makefile
#Copyright 2013 Benoit Cl�ment
#mail : bclement_AT_lpsc.in2p3.fr
#======================================================================
#This file is part of STARucn. 
#STARucn is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================

ROOTCFLAGS    = $(shell root-config --cflags)
ROOTLIBS      = $(shell root-config --libs)
ROOTGLIBS     = $(shell root-config --glibs)

GSLCFLAGS     = $(shell gsl-config --cflags)
GSLLIBS       = $(shell gsl-config --libs)

#MPFRCFLAGS  =
#MPFRLIBS = -lmpfr -lgmp
#USE_MPFR = -DUSE_MPFR
CC = gcc
CXX	   = g++
CXXFLAGS      = -g -Wall -Werror $(ROOTCFLAGS) $(GSLCFLAGS) $(USE_MPFR) -O3 -std=c++11 -march=native
CINT	  = rootcint

LIBS	  = $(GSLLIBS) $(ROOTLIBS) -lTreePlayer -lMathMore -lGeom  -lRGL -lGui -lGed -lGeomPainter -lGeomBuilder  $(SYSLIBS) $(MPFRLIBS) -rdynamic

BUILD	 = ./build
SRC	   = ./src
BIN	   = ./bin
INC    = ./inc
LIB	   = ./lib
DICT	 = ./dict

PLUGINS	  = plugins
CORE      = core
SHADOW    = shadow
EDM = edm
TOOLS = tools
TESTS = tests
INCFLAGS      = -I$(INC)/$(CORE) -I$(INC)/$(PLUGINS) -I$(INC)/$(SHADOW)  -I$(INC)/$(EDM)
CXXFLAGS     += $(INCFLAGS)
LOCALLIBS     = -L$(LIB) -lSTARucn

PLUGINSSRC = $(wildcard $(SRC)/$(PLUGINS)/*.cpp)
PLUGINSOBJ = $(subst $(SRC)/$(PLUGINS),$(BUILD)/$(PLUGINS),$(patsubst %.cpp,%.o,$(PLUGINSSRC)))

CORESRC = $(wildcard $(SRC)/$(CORE)/*.cpp)
COREOBJ = $(subst $(SRC)/$(CORE),$(BUILD)/$(CORE),$(patsubst %.cpp,%.o,$(CORESRC)))

SHADOWSRC = $(wildcard $(SRC)/$(SHADOW)/*.cpp)
SHADOWOBJ = $(subst $(SRC)/$(SHADOW),$(BUILD)/$(SHADOW),$(patsubst %.cpp,%.o,$(SHADOWSRC)))

EXESRC = $(wildcard $(SRC)/exe/*.cxx)
EXEOBJ = $(subst $(SRC)/exe/,$(BIN)/,$(patsubst %.cxx,%_x,$(EXESRC)))

EDMSRC = $(wildcard $(SRC)/$(EDM)/*.cpp)
EDMOBJ = $(subst $(SRC)/$(EDM),$(BUILD)/$(EDM),$(patsubst %.cpp,%.o,$(EDMSRC)))

TOOLSSRC = $(wildcard $(SRC)/tools/*.cxx)
TOOLSOBJ = $(subst $(SRC)/tools/,$(BIN)/,$(patsubst %.cxx,%_x,$(TOOLSSRC)))

TESTSSRC = $(wildcard $(SRC)/tests/*.cxx)
TESTSOBJ = $(subst $(SRC)/tests/,$(BIN)/,$(patsubst %.cxx,%_x,$(TESTSSRC)))

#echo "Compiling with 64bits architecture"

default: all

all: exe tools tests docs
	@echo "linking executable  bin/STARucn_x to starucn"
	@ln -f -s bin/STARucn_x ./starucn
	@ln -f -s bin/STARucn_threaded_x ./starucn_threaded

#this creates the needed folders
folders:
	@mkdir -p $(LIB) $(BUILD)/$(DICT) $(BUILD)/$(CORE) $(BUILD)/$(PLUGINS) $(BUILD)/$(SHADOW) $(BUILD)/$(EDM) anim

core:    $(BUILD)/$(DICT)/$(CORE)Dictionary.cpp    $(COREOBJ)
plugins: $(BUILD)/$(DICT)/$(PLUGINS)Dictionary.cpp $(PLUGINSOBJ)
shadow:  $(BUILD)/$(DICT)/$(SHADOW)Dictionary.cpp $(SHADOWOBJ)
edm:    $(BUILD)/$(DICT)/$(EDM)Dictionary.cpp    $(EDMOBJ)
exe:    folders library $(EXEOBJ)
tools:     $(TOOLSOBJ) folders
tests:     $(TESTSOBJ) folders

library: core plugins shadow edm
	@echo "Building library libSTARucn.so"
	$(CXX) -shared -o $(LIB)/libSTARucn.so $(BUILD)/*/*.o

$(BUILD)/$(DICT)/%Dictionary.cpp: $(wildcard $(INC)/%/*.h)
	@echo "in ["$*"] -> Generating include file and linkdef"
	@python build/buildlinkdef $(INC)/$* $(BUILD)/$(DICT)/$*Includes.h $(BUILD)/$(DICT)/$*Linkdef.h
	@echo "in ["$*"] -> Building ROOT Dictionary."
	@$(CINT) -f $@ -c $(INCFLAGS) $(BUILD)/$(DICT)/$*Includes.h $(BUILD)/$(DICT)/$*Linkdef.h
	@$(CXX) $(CXXFLAGS) -I. -fPIC -o $(BUILD)/$(DICT)/$*Dictionary.o -c $(BUILD)/$(DICT)/$*Dictionary.cpp

$(BUILD)/$(CORE)/%.o: $(SRC)/$(CORE)/%.cpp $(INC)/$(CORE)/%.h
	@echo "in ["$(CORE)"] -> Building object : " $@
	@$(CXX) $(CXXFLAGS) -fPIC -o $@ -c $<

$(BUILD)/$(SHADOW)/%.o: $(SRC)/$(SHADOW)/%.cpp $(INC)/$(SHADOW)/%.h
	@echo "in ["$(SHADOW)"] -> Building object : " $@
	@$(CXX) $(CXXFLAGS) -fPIC -o $@ -c $<

$(BUILD)/$(PLUGINS)/%.o: $(SRC)/$(PLUGINS)/%.cpp $(INC)/$(PLUGINS)/%.h
	@echo "in ["$(PLUGINS)"] -> Building object : " $@
	@$(CXX) $(CXXFLAGS) -fPIC -o $@ -c $<

$(BUILD)/$(EDM)/%.o: $(SRC)/$(EDM)/%.cpp $(INC)/$(EDM)/%.h
	@echo "in ["$(EDM)"] -> Building object : " $@
	@$(CXX) $(CXXFLAGS) -fPIC -o $@ -c $<
	
$(BIN)/%_x: $(SRC)/exe/%.cxx library
	@echo "Building executable : " $@
	$(CXX) $(CXXFLAGS) -o $@  $< $(LOCALLIBS) $(LIBS)

$(BIN)/%_x: $(SRC)/tools/%.cxx library
	@echo "Building executable : " $@
	$(CXX) $(CXXFLAGS) -o $@ $<  $(LOCALLIBS) $(LIBS)
	
$(BIN)/%_x: $(SRC)/tests/%.cxx library
	@echo "Building executable : " $@
	$(CXX) $(CXXFLAGS) -o $@ $<  $(LOCALLIBS) $(LIBS)

docs:
	doxygen Doxygen.cfg

clean:
	@echo "Deleting compiled files"
	@rm -f $(BUILD)/*/*.*
	@rm -f $(BIN)/*_x
	@rm -f $(LIB)/libSTARucn.so
	@rm -f ./starucn
	@rm -f ./starucn_threaded
	@rm -f *.pyc
    
    
