#!/usr/bin/env python

import glob
import numpy
import math
import matplotlib
from rootpy.tree import Tree
from rootpy.io import root_open
from root_numpy import tree2array
from matplotlib import pyplot

filename = "fedm_vs_vxy.root"

hbar = 1.05457173e-34
B0z = 1e-6 #T
dBdz = 1e-9 #T/m
R = 0.235 #m
gyro = -1.83247179e8
J = 0.5
c = 2.998e8
w0 = -gyro * B0z
e_charge = 1.602e-19


def eq29(V):
	Vxy = numpy.sqrt(V**2 * 2.0 /3.0)
	wr_star_squared = math.pi**2 /6 * (Vxy/R)**2
	bracket = 1/(1-(wr_star_squared/w0**2))
	falseedm = - (J*hbar/2) * (dBdz/B0z**2) * (Vxy**2/c**2) * bracket
	falseedm_ecm = falseedm * 100 / e_charge #convert to ecm
	#print falseedm_ecm
	return falseedm_ecm


def getresults(outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	#for row in resultstree:		
		#print row
		#pass
	resultsarray = tree2array(resultstree)
	falseedm = []
	error = []
	Vxy = []
	parfile=[]
	
	for row in resultsarray:
		falseedm.append(row[14])
		error.append(row[15])
		Vxy.append(row[25])
		parfile.append(row[-1])
	return falseedm,error,Vxy,parfile

fedm_mc, fedm_error_mc, Vxy_mc,parfiles = getresults(filename)

Vxy_analytic = numpy.linspace(0.00001,7,10000)
fedm_analytic  = eq29(Vxy_analytic)

pyplot.plot(Vxy_analytic,fedm_analytic,label="eq29")

pyplot.errorbar(Vxy_mc,fedm_mc,yerr=fedm_error_mc,fmt='+',label="STARucn")

pyplot.legend()
pyplot.xlabel("V (m/s)")
pyplot.ylabel("False EDM (ecm)")

print parfiles[0]

pyplot.show()

