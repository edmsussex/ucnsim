#!/usr/bin/env python

#before running, do pip install --user rootpy
#pip install --user root_numpy

import os
from scipy import optimize
import numpy
import shutil
import random
import math
from rootpy.tree import Tree
from rootpy.io import root_open
from root_numpy import tree2array
from matplotlib import pyplot


def getalpha(i,outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	resultsarray = tree2array(resultstree)
	alpha = resultsarray[i][6]
	alphaerror = resultsarray[i][7]
	return alpha,alphaerror
	


print "STARucn T2 Script - Nicholas Ayres 2014 na280@sussex.ac.uk"
print "This generates a run file, runs STARucn on it, reads alpha from file, runs until alpha < endalpha then calculates T2"

endalpha = 0.999 #finish when alpha gets below this number
timestep = 1 #timestep in seconds
NbParticles = 100 #number of neutrons to start with
random.seed(1) #seed for RNG, in turn seeds STARucn RNGs
runid = "T2" #so we know how to name everything...
runsection = 0 #not currently implemented

#base script 
basescriptpath = os.getcwd() + '/scripts/T2BaseScript.par'

#folder in which to place automatically generated starucn scripts
outscriptfolder = os.getcwd() + '/scripts/autogenscripts'

#output file
outputfile = "output_" + runid + "_" + str(runsection) + ".root"

os.system("rm " +outputfile)

run_command = './starucn_threaded'

#for starting parameters alpha = 1 exactly at t = 0
alphalist = numpy.array([1])
alphaerrorlist = numpy.array([0])
storagetimelist = numpy.array([0])



i = 0
while (i<5):
	scriptpath = outscriptfolder + '/' + runid + "_" + str(i) + '.par'
	shutil.copy(basescriptpath,scriptpath) #copy base script
	print "Opening File"
	f = open(scriptpath,'a') #open for appending
	#first time use EDMVolumeGenerator, then use EDMRootFileGenerator
	print "Writing to File"
	if (i == 0):
		f.write("mcsetup Generator VolumeGenerator  @{this} \n")
	else:
		f.write("mcsetup Generator RootFileGenerator  @{this} \n")
	#write lines to file
	f.write("int NbParticles " + str(NbParticles) + "\n")
	f.write("string RootFileGenerator.inputrunid " + runid + "\n")
	f.write("string RootFileGenerator.inputruntimeslice " + str(i-1)+ "\n")
	f.write("string SpinRootOutputInterface.RunID " + runid+ "\n")
	f.write("int SpinRootOutputInterface.TimeSlice " + str(i)+ "\n")
	f.write("double SpinRootOutputInterface.StorageTime " + str ((i+1) * timestep)+ "\n")
	f.write("int GenerationSeed "  + str(random.randint(0,4294967295))+ "\n") #scale random number in (0,1) to largest unsigned integer
	print "closing file"
	f.close()
	
	print "running starucn with command \"./starucn " + scriptpath + "\" "
	os.system("./starucn " + scriptpath)
	
	alpha,alphaerror = getalpha(i,outputfile)
	
	storagetimelist = numpy.append(storagetimelist,(i+1)*timestep)
	alphalist = numpy.append(alphalist,alpha)
	alphaerrorlist = numpy.append(alphaerrorlist,alphaerror)
	i=i+1
	print "alpha = ", alpha

print alphalist
print alphaerrorlist
	
#function to fit
def expdecay(t,A,T):
	return A*numpy.exp(-t/T)

popt, pcov = optimize.curve_fit(expdecay,storagetimelist,alphalist)#,None,alphaerrorlist,True)
print popt, pcov

alphaFittedData = expdecay(storagetimelist, popt[0], popt[1])

pyplot.plot(storagetimelist,alphalist,'r+',label="MC data")
pyplot.plot(storagetimelist,alphaFittedData,'-',label="fit")
pyplot.show()
