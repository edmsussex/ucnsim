/*
======================================================================
STARucn MC software
File : LoopCounter.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class LoopCounter                                        //
//                                                          //
// Display percentage of procesed events                    //
//                                                          //
// Author : B. Clement (bclement@lpsc.in2p3.fr)             //
//                                                          //
//////////////////////////////////////////////////////////////


#ifndef LOOPCOUNTER_H
#define LOOPCOUNTER_H

#include <string>
#include "TString.h"

class LoopCounter
{
public:
	LoopCounter(int, int, std::string="Processing : ", std::string="Processing done.", int=2, int=4);
	~LoopCounter();
  void Init(int max, int step);
	void SetColors(int, int);
  void SetText(std::string, std::string);
	void Update();

#ifdef _WIN32
  static char* TxtForm(int u=-1, int b=0, int f=0) { return Form(""); }
  static char* TxtFormNoBkg(int c=-1)  { return Form(""); }
#else
  static char* TxtForm(int u=-1, int b=0, int f=0)  { return u>=0 ? Form("\033[%d;3%d;4%dm",u,b,f) : Form("\033[0m"); }
  static char* TxtFormNoBkg(int c=-1)  { return c>=0 ? Form("\033[0;3%d",c) : Form("\033[0m"); }
#endif

private: 
    int kMax;
  	int fCurr;
    int fStep;
	std::string fColor;
	std::string fRestore;
	std::string fText;
	std::string fFinal;
};
#endif
