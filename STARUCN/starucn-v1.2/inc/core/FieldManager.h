/**
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 */

#ifndef FIELDMANAGER_H
#define FIELDMANAGER_H

#include "Configurable.h"
#include "Vector3L.h"
#include "FieldCollection.h"

 /** 
 @class FieldManager
 * @brief The FieldManager deals with three different types of field: B fields,
 * electric fields and the electric fields when the polarity is reversed.
 * These are set in the script file with as such string FieldManager.BFields B0,B1,B2.
 * This defines the collections of B fields to be managed. The property of the individual of B fields
 * can be set using the name just given i.e: string FieldManager.Fields.B0 FixedB0:UniformField.
 * This tells the field manager that B0 to create an instance of a uniform field named FixedB0.
 * The properties of these instances are depend upon the type of field implemented (see UniformField, GridMap).
 * 
 */
class FieldManager : public Configurable
{
public:
	FieldManager();
	~FieldManager();
	void Configure(const char *file);
		/**
	 * @return the electric field vector at given position.
	 */
	Vector3L GetEField(Vector3L& point, double time);
	/**
	 * @return the electric field vector at given position when the polarity is reversed.
	 */
	Vector3L GetERevField(Vector3L& point, double time);
	/**
	 * @return the electric field vector at given position (time independant, depricated).
	 */
	Vector3L GetEField(Vector3L& point);
	/**
	 * @return the electric field vector at given position when the polarity is reversed (time independent, deprecated.
	 */
	Vector3L GetERevField(Vector3L& point);
	/**
	 * @return the B field vector (time independant, depricated).
	 */	
	Vector3L GetBField(Vector3L& point);
	
	/**
	 * @return B field Vector for a time-dependant B field
	 * time should be time since start of run in seconds
	 * 
	 * */
	
	Vector3L GetBField(Vector3L& point, double time);
	
private:
	void ConfigureB(const char *file);
	void ConfigureE(const char *file);
	void ConfigureERev(const char *file);
	FieldCollection EFields;
	FieldCollection ERevFields;
	FieldCollection BFields;
	int verbosity;
};
#endif
