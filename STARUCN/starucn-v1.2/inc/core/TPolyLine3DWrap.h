/*
======================================================================
STARucn MC software
File : TPolyLine3D.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef TPOLYLINE3DWRAP_H
#define TPOLYLINE3DWRAP_H

#include <iostream>
#include "TPolyLine3D.h"
#include "TNamed.h"

class TPolyLine3DWrap : public TNamed
{
public:
	TPolyLine3DWrap();
	~TPolyLine3DWrap();
  void Reset()     {pl.SetPolyLine(0);}
  void SetNextPoint(double* d)  {pl.SetNextPoint(d[0],d[1],d[2]);}
  TPolyLine3D pl;
ClassDef(TPolyLine3DWrap, 1);
};

#endif
