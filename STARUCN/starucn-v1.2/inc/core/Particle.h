/*
======================================================================
STARucn MC software
File : Particle.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef PARTICLE_H
#define PARTICLE_H

#include "TF1.h"
#include <iostream>

#include "Spin.h"
#include "TRandom3.h"


class Particle : public TObject
{
public:
  Particle();
  Particle(double e, double* pos);
  ~Particle() {;}

  static bool compare(const Particle* a, const Particle* b) 
    {
    if(a->time == b->time) return a->speed < b->speed;
    return a->time < b->time;
    }

  Particle & operator=(Particle&);
 
  double GetSpeed()          {return speed;}
  double GetDirection(int i) {return direction[i];}
  double GetPosition(int i)  {return position[i];}
  double* GetPosition()      {return position;} 
  double* GetDirection()     {return direction;}   
  double GetTime()           {return time;}
  Vector3L GetPositionV();
  Vector3L GetVelocityV();
  
  void SetSpeed(double e)  {speed = e;}
  void SetDirection(double* dir);
  void SetPosition(int i, double x) {position[i] = x;}
  void SetTime(double t)  {time=t;}
  void AddTime(double t)  {time+=t;}
  void Reset(double* pos, double* dir)  {position[0]=pos[0];position[1]=pos[1];position[2]=pos[2];SetDirection(dir);time=0; dead=false; speed=0; active=true; spin.Reset(); spinRevE.Reset();}

  double GetCosTh()     {return cth;} 
  double GetSinTh()     {return sth;}
  double GetCosPhi()    {return cph;}
  double GetSinPhi()    {return sph;}

  void PrintPosition(std::ostream& st)  {st << "Px = " << position [0] << " , Py = " <<position[1]  << " , Pz = " << position[2] ;}
  void PrintDirection(std::ostream& st) {st << "Dx = " << direction[0] << " , Dy = " <<direction[1] << " , Dz = " << direction[2];}
  void PrintSpinVector (std::ostream& st) {st << "Sx = " << spin.GetSigma().x() << " , Sy = " << spin.GetSigma().y() << " , Sz = " << spin.GetSigma().z();}
  void PrintSpinRevEVector (std::ostream& st) {st << "SRevEx = " << spinRevE.GetSigma().x() << " , SRevEy = " << spinRevE.GetSigma().y() << " , SRevEz = " << spinRevE.GetSigma().z();}
  
  void   Kill()         {dead = true;}
  bool   Dead()         {return dead;}
  void   Switch()         {active = !active;}
  bool   Active()         {return active;}

  void SetInit();

  //holds initial conditions for ThreadSafeEDMOutputInterface
  Spin spin, spinRevE;
  Spin spinInit, spinRevEInit;
  double kV_init;
  double kVx_init;
  double kVy_init;
  double kVz_init;
  double kX_init;
  double kY_init;
  double kZ_init;
  double kT_init;
  
  //counts interactions
  int kDiff;
  int kTran;
  int kSpec;
  
  //counts steps
  int kStep;
  
  int id; //set by generator, random generators should increment this while filegenerators will want to read this from a file.
  
  TRandom3 Gen;
  
  void SetParticleSeed(unsigned int seed = 0);
  unsigned int ParticleSeed;
  
private:
  void Scatter(double, double); 
  double position[3];
  double direction[3];   

  double speed;
  double time;
  double cth, sth;
  double cph, sph;
  bool   dead;
  bool active;
ClassDef(Particle,1);
};
#endif


