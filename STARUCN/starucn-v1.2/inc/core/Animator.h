/*
======================================================================
STARucn MC software
File : Animator.h
Copyright 2013 Benoit Clement
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/

#ifndef ANIMATOR_H
#define ANIMATOR_H

#include <iostream>
#include <string>

#include "TPolyMarker3D.h"
#include "Geometry.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TView.h"

class Animator : public TApplication {
public:
	Animator( std::string conf);
	~Animator();
	void SetDebug(int status=0) {kDebug=status;}
  void MakeMovie();

private:
	int kDebug;
  void DrawAnimationMarkers(std::string file);
  void DrawAnimationGeometry(std::string file);
  void MakeCut(std::string& cut);
  void ReadCuts(ParameterReader& pr);
  void MakeCanvas();

  Geometry* fGeo;
  TPolyMarker3D** fMark;
  
  std::vector<std::pair<TFormula*, int> > fSets;
  std::vector<std::pair<TFormula*, int> >::iterator fItr; 
  std::vector<std::pair<TFormula*, int> >::iterator fItrE;
  
  int kFrames;
  double fStep;
  std::string fGeofile;
  std::string fName;
  std::string fOut;
  std::string fPhimode;
  std::string fPhidef;
  std::string fThmode;
  std::string fThdef;
  double fFactor;
  std::string fMode;
  int    kStart;
  int    kStop;
  double fSize;
  std::string fOpenGLCamera[5];
  std::string fOpenGLCameraMode[5];
  TCanvas* fCanvas;
  TView* fView;
ClassDef(Animator,0);
};

#endif
