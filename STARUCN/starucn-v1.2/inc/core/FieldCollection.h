/**
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 */
#ifndef FIELDCOLLECTION_H
#define FIELDCOLLECTION_H

#include <vector>
#include "Vector3L.h"
#include "Field.h"

/**
 * @class FieldCollection
 * @brief Stores a collection of fields.
 * 
 */
class FieldCollection 
{
public:
	FieldCollection() {;};
	~FieldCollection();
	/**
	 * @param The point at which the field is to be found.
	 * @return Sum of the fields that are defined at the given point.
	 */
	Vector3L GetField(Vector3L& point);
	
	/**
	 * @param New field that is to be added to the collection.
	 */
	void AddField(Field *F);
	
		/**
	 * @param The point and time at which the field is to be found.
	 * @return Sum of the fields that are defined at the given point.
	 */
	Vector3L GetField(Vector3L& point, double time);
	
	
private:
	std::vector<Field*> Fields;
};

#endif
