/*
======================================================================
STARucn MC software
File : Geometry.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class Geometry                                           //
//                                                          //
// Descritpion of detector geometry and materials           //
// based on ROOT TGeoVolume/TGeoManager                     //
//                                                          //
// Author : B. Clement (bclement_AT_lpsc.in2p3.fr)          //
//                                                          //
//////////////////////////////////////////////////////////////

#ifndef Geometry_H
#define Geometry_H
#include "TGeoManager.h"
#include "TGeoMaterial.h"
#include "TGeoMedium.h"
#include "TGeoMatrix.h"
#include "TGeoVolume.h"
#include "InteractingMaterial.h"
#include "Particle.h"
#include <vector>
#include <map>
#include <cmath>
#include <iostream>

class Geometry : public TGeoManager  // Master frame
{
public:
  Geometry(const char* script);
  Geometry(std::istream& geometryfile) ;
  ~Geometry();
  void AddVolume(std::string name, std::string& type, double* pars, TGeoMatrix* mov, InteractingMaterial* mat, int active, int color, bool declare);
  void AddCompositeVolume(std::string name, std::string& definition, TGeoMatrix* mov, InteractingMaterial* mat, int active, int color, bool declare);
//   double DistanceToNext(Particle&, bool&);
  int SetCurrent(double* pos)  ;
  InteractingMaterial* GetMaterial()   ;
  InteractingMaterial* GetMaterial(TGeoVolume*)   ;
  void Draw(Option_t* opt = "")  {fWorld->Draw(opt);/*fWorld->Raytrace();*/}
  TGeoVolume* GetWorld()   {return fWorld;}
  TObjArray*   GetNodes()    {return fWorld->GetNodes();}

  TGeoVolume* FindVolume(double* pos);
  std::string GetInput()    {return fInput;}

  int GetVolumeId(TGeoVolume* v)  {return fActive[v];}  
private:

  void Configure(ParameterReader& pr);
 
  std::vector<TGeoVolume*>  fVolumes;
  TGeoNode*                 fCurrent;
  TObjArray*                fNodes;
  int                       kNodes;
  TGeoVolume* fWorld;
  std::map<TGeoVolume*, InteractingMaterial*>   fMaterials;

  std::map<TGeoVolume*, int>        fActive; 
  TGeoMedium*   fMed;
  TGeoMaterial* fEmpty; 
  int fCount;

  std::map<std::string, InteractingMaterial*> materials;
  std::map<std::string, TGeoMatrix*> matrices;  

  std::vector<TGeoVolume*>::iterator itr, itrE;
  std::string fInput;
  
  ClassDef(Geometry,1)
};

#endif
