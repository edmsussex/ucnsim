/**
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 */
#ifndef SPIN_H
#define SPIN_H

#include "TRandom3.h"
#include "TRandom.h"

#include "Vector3L.h"

/** @class Spin
 * @brief Classical spin tracking
 * @section Overview
 * The spin vector is modelled classically as
 *  \f{align*}{
 * \vec{\sigma}(t) &= \left(\vec{\sigma}_0-\frac{\left(\vec{\sigma}_0\cdot \vec{B}\right)\vec{B}}{B^2}\right)\cos\left(\omega t\right) \\
 *                        &+ \frac{\vec{\sigma}_0\times\vec{B}}{B}\sin\left(\omega t\right) \\
 *                         &+ \frac{\left(\vec{\sigma}_0\cdot\vec{B}\right)
 *                         \vec{B}}{B^2}
 * \f} \cite phil13 
 * since the expectation value of the spin vector of a Particle evolves classicaly in time.
 * 
 * @section Tests
 * So far, the spin class has been test to give a 10^{-11,-12} error on frequency shift
 * in the cases of 1) v=(5,0,0),B=(0,0,1e-6),E=(0,0,0) and 2) v=(5,0,0),B=(0,0,1e-6),E=(0,0,5e-6).
 */
class Spin : public TObject
{
public:
	Spin();
	/**
	 * @param Gyromagnetic ratio of the particle with this spin vector.
	 * @param Initial spin vector
	 */
	Spin(double gyro, Vector3L sigma0);
	
	/**
	 * @return The total time which the spin vector has been precessing.
	 */
	long double GetTime(void);
	/**
	 * @return The phase difference between the spin vector and the reference phase.
	 */
	double GetPhaseDiff();
	
	/**
	 * @return The frequency shift over the entire time for which the spin vector has
	 * been precessing.
	 */
	double GetFreqDiff();
	
	/**
	 * Set the gyromagnetic ratio.
	 * @param Gyromagnetic ratio.
	 */
	void SetGyro(double g);
	
	/**
	 * Reset the properties of the neutron spin back to time = 0.
	 */
	void Reset();
	/**
	 * Sets sigma to given value
	 */
	void SetSpin(Vector3L newspin);
	/**
	 * Sets spin in random direction in xy plane
	 */
	void SetRandomSxy();
	/**
	 * sets spin in random direction
	 */
	void SetRandomSpin();
	/**
	 * lets sigma0 = sigma
	 */
	void SetStartSpin();
	/**
	 * Sets spin vector to be in the plane containing u,v, with random direction
	 * 
	 */
	void SetRandomSpinPlane(Vector3L u, Vector3L v);
	/**
	 * Allow the spin vector to evolve in a magnetic field B for time dt.
	 * 
	 * @param Time increment dt for which the B field is applied.
	 * @param The B average field vector experienced by the Particle with spin in the time dt.
	 */
	void Evolve(long double dt, Vector3L& B, Vector3L& t, Vector3L& u);
	Vector3L EvolveSpinVector(long double dtheta, Vector3L& B);
	~Spin();
	Spin & operator=(Spin&);
	Vector3L& GetSigma0();
	Vector3L& GetSigma();
	long double GetTheta();
	long double GetPhase();
	/**
	 * I have no idea what RefPhase is...
	 */
	long double GetRefphase();
	double GetGyro();
	long double GetError();
	void AccumulateError(long double);
	bool IsDepolarised();
	
	void SetSigma(Vector3L&);
	void SetTheta(long double);
	void SetPhase(long double);
	void SetThetaError(long double);
	void SetSigma0(Vector3L&);
	void SetRefPhase(long double);
	void SetTime(long double);
	
	void SeedRNG(long double);

private:
  /*
   * Gyromagnetic ratio reduced values for Particles, Hg: 7.5901309E+6, -29.1646870E+6
   */
  double gyro; 
  bool depolarised;
  Vector3L sigma0; /*!< Initial spin vector  */
  Vector3L sigma; /*!< Spin vector at current time. */
  long double refphase; /*!< Reference phase */
  long double phase; /*!< Current phase in of spin in reference plane */
  long double time; /*!< Current time */
  long double theta; /*!< total phase in the plane perpendicular to the overall B field */
  long double thetaerror;
  TRandom3 r;
ClassDef(Spin,1);
};

#endif

