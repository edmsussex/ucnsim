/*
======================================================================
STARucn MC software
File : BasePropagator.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef BasePropagator_H
#define BasePropagator_H
#include "Configurable.h"
#include "Particle.h"
#include "TPolyLine3D.h"
#include "TGeoVolume.h"
#include "CoreProcess.h"

#include "Geometry.h"
#include "BaseGenerator.h"
#include "BaseInteraction.h"
#include "BaseOutputInterface.h"

class BasePropagator : public Configurable
{
public:
  BasePropagator();
  virtual ~BasePropagator();
  virtual void Configure(const char*) = 0;

  virtual void SetCurrentVolume(TGeoVolume* );

  virtual bool   Propagate(double dt, Particle&)            = 0;
  virtual double PropagateToSurface(Particle&, double& Dt, double* Norm, TGeoVolume* &next, TPolyLine3D* u = 0)  = 0;
   virtual void   Evolve(double dt, Particle&) = 0;

protected:

  CoreProcess*fCore;
  TObjArray * fNodes;
  int         kNodes; 
  TGeoVolume* fCurrentVolume;
  TGeoShape * fCurrentShape;
  TGeoNode  * fCurrentNode;
};

#endif

