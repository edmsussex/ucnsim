/**
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
*/
#ifndef FIELD_H
#define FIELD_H

#include <string>

#include "Configurable.h"
#include "Vector3L.h"

/**
 * @class Field
 * @brief Abstract class defining the structure of a Field which
 * is just a class that must associate a vector with a given position.
 * Each field has a name so that the derived class field properties 
 * can be set in the configuration script files.
 */
class Field : public Configurable
{
public:
	Field();
	virtual ~Field();
	virtual void Configure(const char*);	
	virtual Vector3L GetField(Vector3L& pos) = 0;
	/**
	 * @param position , time
	 * @returns returns field value at given position + time
	 * If not defined as time varying then returns GetField(Vector3L& pos)
	 * for backwards compatibility.
	 * */
	virtual Vector3L GetField(Vector3L& pos, double time) {
		return GetField(pos);
	}
	/**
	 * @param position
	 * @returns not implemented, always true
	 * Should return whether point is within region within which field applies
	 */
	bool Contains(Vector3L point);
	/**
	 * @param position , time 
	 * @returns semi implemented - returns false if UseFieldBoundaries is true and
	 * time > EndTime or time < StartTime, else returns true
	 * 
	 * Should return whether point is within region within which field applies
	 */
	bool Contains(Vector3L point, double time);
	void SetFieldName(std::string& s);
//protected:
	Vector3L origin;
	long double halfX, halfY, halfZ;
	std::string fieldName;
	double StartTime, EndTime;
	bool UseFieldBoundaries;
};
#endif
