/*
======================================================================
STARucn MC software
File : InteractingMaterial.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class InteractingMaterial                                //
//                                                          //
// Simple container class for material properties           //
//   store :                                                //
//      * Decay constant     : double Lambda()              //
//      * FermiPotential     : double Fermi()               //
//      * Absorbtion coeff   : double Eta()                 //
//      * Diffusion fraction : double D()                   //
//                                                          //
// Author : B. Clement (bclement_AT_lpsc.in2p3.fr)          //
//                                                          //
//////////////////////////////////////////////////////////////

#ifndef INTERACTINGMATERIAL_H
#define INTERACTINGMATERIAL_H

#include "ParameterReader.h"
#include <map>
#include <set>

struct materialunsorter
{
  static table* fTable ; // unsorting table
  bool operator()(std::string s1, std::string s2) const
  {
    if(! fTable->second[s2]) fTable->second[s2] = ++(fTable->first);
    if(! fTable->second[s1]) fTable->second[s1] = ++(fTable->first);
    return fTable->second[s1]<fTable->second[s2];
  }
};

class InteractingMaterial 
{
friend class Geometry;

public :
  InteractingMaterial(double, double, double, double);
  InteractingMaterial();
  ~InteractingMaterial() {delete[] fProperties;}  

  double Lambda() const  {return fProperties[0];}
  double Fermi()  const  {return fProperties[1];}
  double Eta()    const  {return fProperties[2];}
  double D()      const  {return fProperties[3];}

  void       SetProperty(std::string name, double value) ;
  double     GetProperty(std::string name)                {return fPropertiesMap[name]; }
  double     GetProperty(int i)                           {return fProperties[i];}
  static int GetPropertyIndex(std::string name)           {return materialunsorter::fTable->second[name];}

private:
  int kLambda;  // s^-1
  int kFermi;   // neV
  int kEta;     // no dim
  int kD;       // no dim

  double* fProperties;
  std::map<std::string, double> fPropertiesMap;
  static std::set<std::string, materialunsorter> fPropertiesCatalog;

  void CopyMap();

};

#endif

