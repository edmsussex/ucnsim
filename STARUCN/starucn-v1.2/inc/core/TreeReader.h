/*
======================================================================
STARucn MC software
File : TreeReader.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef TREEREADER_H
#define TREEREADER_H

//////////////////////////////////////////////////
//
//		Class TreeReader
//		TreeReader.h
//
// Class for Tree reading through TFomula
//////////////////////////////////////////////////

#include "TTree.h"
#include "TTreeFormula.h"
#include "TTreeFormulaManager.h"
#include "TString.h"
#include <map>
#include <vector>

//////////////////////////////////////////////////
class TreeReader //: public TTreeFormulaManager
{
 private:

  TTree* fTree;
  int              fCurrentEntry; 			// current ntuple entry stored in buffer
  int              fEntries;      			// total number of entries
  bool             fIsChain;
  int              fCurrentTree;
  std::map<std::string, TTreeFormula*>     fFormulae;	// known formulae

 public:

  TreeReader();               // Default ctor
  TreeReader(TTree* n);       // ctor with ntuple
  virtual ~TreeReader();      // dtor

  void SetTree(TTree* n);       //
  double GetVariable(const char* c, int entry=-2); // return variable s for a given entry (<0 -> current entry)
  int    GetEntry(int entry=-1);     // Read a given entry in the buffer (-1 -> next entry);
  int    GetCurrentEntry()    {return fCurrentEntry;}
  int      GetEntries()             { return fEntries ; }
  TTree*   GetTree()                { return fTree    ; }
  void Restart()                    {fCurrentEntry = -1;}
//   ClassDef(TreeReader,0);  // Integrate this class into ROOT (must be the last member)
};

#endif
