/*
======================================================================
STARucn MC software
File : BaseOutputInterface.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef BaseOutputInterface_H
#define BaseOutputInterface_H

#include "Particle.h"
#include "Configurable.h"
#include "TPolyLine3DWrap.h"
#include "TGeoVolume.h"
#include "CoreProcess.h"
#include "Geometry.h"
#include "BasePropagator.h"
#include "BaseInteraction.h"
#include "BaseGenerator.h"
#include "TRandom3.h"

class BaseOutputInterface : public Configurable
{
public:
	BaseOutputInterface();
	virtual ~BaseOutputInterface();

  virtual void Configure(const char*)              = 0;

  virtual void BeginParticle(Particle&)                                                                          = 0;
  virtual void SaveStep  (Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)              = 0;
  virtual void EndParticle(Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)             = 0;
  virtual void Finalize()                          = 0;

  TPolyLine3DWrap*  Line3D;

  TRandom3  Gen;

protected:
  CoreProcess* fCore;
  int kId  ;
  int kStep;  
};

#endif


