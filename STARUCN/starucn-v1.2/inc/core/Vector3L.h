/**
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 *
 */
#ifndef VECTOR3L_H
#define VECTOR3L_H

#include <iostream>
#include "EDM.h"

//for mac compatibility, M_PI_2l is not defined in apple math.h
#ifndef M_PI_2l
	#define M_PI_2l 1.570796326794896619231321691639751442L
#endif

/**
 * @class Vector3L
 * @brief Long double vector class.
 * The Vector3L class is meant to match the behavoir of TVector3 as much as possible. 
 * However, much of the functionality of TVector3 has been left unimplemented.
 */
class Vector3L
{
public:
	Vector3L();
	Vector3L(long double x, long double y, long double z);
	~Vector3L();
	long double x() const;
	long double y() const;
	long double z() const;
	
	/**
	 * @return Magnitude of vector.
	 */
	long double Mag() const;
	/**
	 * @return Square of the magnitude of the vector.
	 */
	long double Mag2() const;
	/**
	 * @return Dot product with vector v.
	 */
	long double Dot(const Vector3L& v) const;
	/**
	 * @return Cross product with vector v. The order of the operation
	 * is this vector x v.
	 */
	Vector3L Cross(const Vector3L& v) const;
	
	/**
	 * @return Angle between the two vectors.
	 */
	long double AngleBetween(const Vector3L &v) const;
	/**
	 * @param Unit vector  of plane.
	 * @param Second unit vector that prescribes the entire plane.
	 * @return The projection of the vector onto the plane.
	 */
	Vector3L ProjectOnPlane(const Vector3L& t, const Vector3L& u) const;
	
	Vector3L&	operator*=(const long double a);
	//Vector3L	operator*(long double a);
	Vector3L&	operator/=(const long double a);
	//Vector3L	operator/(long double a);
	
	Vector3L&	operator+=(const Vector3L&);
	Vector3L&	operator-=(const Vector3L&);
	//Vector3L	operator+(Vector3L&);
	//Vector3L	operator-(Vector3L&);

	/**
	 * Print this vector to given ostream (used for overloading <<).
	 */
	void print(std::ostream&);
private:
	long double elements[3];
};

Vector3L	operator+(const Vector3L& a, const Vector3L& b);
Vector3L	operator-(const Vector3L& a, const Vector3L& b);
Vector3L	operator*(const long double a, const Vector3L& b);
Vector3L	operator*(const Vector3L& b, const long double a);
Vector3L	operator/(const Vector3L& a, const long double b);

std::ostream& operator<< (std::ostream &out, Vector3L &v);
#endif 
