/*
======================================================================
STARucn MC software
File : BaseGenerator.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef BaseGENERATOR_H
#define BaseGENERATOR_H

#include "Particle.h"
#include "Configurable.h"
#include "TRandom3.h"
#include "CoreProcess.h"

#include "Geometry.h"
#include "BasePropagator.h"
#include "BaseInteraction.h"
#include "BaseOutputInterface.h"

class BaseGenerator : public Configurable
{
public:
	BaseGenerator();
	virtual ~BaseGenerator();

	virtual void Configure(const char*)              = 0;
	virtual void Generate(Particle&)                 = 0;
	virtual bool Good()   {return true;}

	static TRandom3  Gen;
protected:
	CoreProcess* fCore;
};

#endif


