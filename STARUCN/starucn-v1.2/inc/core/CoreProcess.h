/*
======================================================================
STARucn MC software
File : CoreProcess.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef COREPROCESS_H
#define COREPROCESS_H

#include <string>
#include "TFile.h"

class Geometry;
class BasePropagator;
class BaseGenerator;
class BaseInteraction;
class BaseOutputInterface;

class CoreProcess{

public:
  CoreProcess();
	~CoreProcess();
	void SetDebug(int status=0) {kDebug=status;}
  static CoreProcess* GetInstance();

  void SetGeometry       (Geometry* geo)  {fGeo=geo;}
  void SetGeometry       (std::string& geofile);
  void SetGenerator      (BaseGenerator* gene)        {fGenerator      =gene;}
  void SetGenerator      (std::string& type, std::string& file);
  void SetPropagator     (BasePropagator* prop)       {fPropagator     =prop;}
  void SetPropagator     (std::string& type, std::string& file);
  void SetInteraction    (BaseInteraction* inte)      {fInteraction    =inte;}
  void SetInteraction    (std::string& type, std::string& file);
  void SetOutputInterface(BaseOutputInterface* outi)  {fOutputInterface=outi;}
  void SetOutputInterface(std::string& type, std::string& file);
  void OpenRootFile      (std::string filename);

  TFile*               RootFile;
  Geometry*            fGeo;
  BaseGenerator*       fGenerator;
  BasePropagator*      fPropagator;
  BaseInteraction*     fInteraction;
  BaseOutputInterface* fOutputInterface;

private:
	
	int kDebug;
  static CoreProcess* fInstance;  
};

#endif
