/*
======================================================================
STARucn MC software
File : BaseInteraction.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef BaseINTERACTION_H
#define BaseINTERACTION_H

#include <iostream>
#include "Particle.h"
#include "Configurable.h"
#include "TRandom3.h"
#include "TGeoVolume.h"
#include "CoreProcess.h"

#include "Geometry.h"
#include "BasePropagator.h"
#include "BaseGenerator.h"
#include "BaseOutputInterface.h"

class BaseInteraction : public Configurable{
public:
	BaseInteraction();
	virtual ~BaseInteraction();
	virtual void Configure(const char*)              = 0;
	virtual int SurfaceInteraction(Particle&, TGeoVolume* vin, TGeoVolume* vnext, double dt, double* norm, TGeoVolume*& newvol) = 0;
  virtual int VolumeInteraction (Particle&, TGeoVolume* vin, double Dt) = 0;

protected:
  CoreProcess* fCore;
};

#endif
