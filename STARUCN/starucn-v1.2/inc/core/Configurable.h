/*
======================================================================
STARucn MC software
File : Configurable.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class Configurable                                       //
//                                                          //
// Abstract class for configurable plugins                  //
//                                                          //
// Author : B. Clement (bclement_AT_lpsc.in2p3.fr)          // 
//                                                          //
//////////////////////////////////////////////////////////////


#ifndef CONFIGURABLE_H
#define CONFIGURABLE_H

#include <iostream>
#include <string>
#include "ParameterReader.h"

 
class Configurable{
public:
	Configurable() {kDebug=false;fExampleScript="";kConfigured = false;}
	virtual ~Configurable() {;}
  virtual void Configure(const char*)              = 0;
  
  void SetParameterFile(const char* file  )     {fParam.AddTypeFile(TypeFile.c_str()); fParam.AddParameterFile(file);}
  void SetParameterFile(std::istream& file)     {fParam.AddTypeFile(TypeFile.c_str()); fParam.AddParameterFile(file);}
  
  void SetDebug(int status=0) {kDebug=status;}
  
  static void GetDefaultScript(const char* c=0) {DefaultScript(fExampleScript , c);};
  static std::string TypeFile;
  
protected:
	int kDebug;
  ParameterReader fParam;
  bool kConfigured;
  // defaul script generator
  static std::string fExampleScript; 
  static void DefaultScript(std::string& , const char* =0);

};

#endif
