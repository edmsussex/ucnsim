/*
======================================================================
STARucn MC software
File : ParameterReader.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef PARAMETERREADER_H
#define PARAMETERREADER_H

//////////////////////////////////////////////////
//
//              class ParameterReader
//		struct unsorter
//		class Parameter
//
//              Parameterreader.h
//
//////////////////////////////////////////////////

# include "TObject.h"
#include <vector>
#include <map>
#include <string>
#include <istream>
#include <cstdlib>
#include <iostream>

struct unsorter;
class Parameter;
typedef std::map<std::string, Parameter, unsorter> prtype;
typedef std::pair<int ,std::map<std::string, int> > table;

//////////////////////////////////////////////////
// struct unsorter
// use this structure to sort objects by the time
// of their creation (using static table* tab)
struct unsorter
{
  static table* tab;  // Static index
  bool operator()(std::string s1, std::string s2) const
  {
    if(! tab->second[s1]) tab->second[s1] = ++(tab->first);
    if(! tab->second[s2]) tab->second[s2] = ++(tab->first);
    return tab->second[s1]<tab->second[s2];
  }
};
   
//////////////////////////////////////////////////
// class Parameter
// a general type for int, string, double and bool
class Parameter // mutli type wrapper
{
 private:

  int type;	// Type : integer (0), string(1), double(2) or boolean(3)
  void* val;	// Pointer on the parameter value
  void Clear();
 
 public: 

  Parameter()              {type=-1; val=0;}
  Parameter(int t, std::string& name)  {setValue(t,name);}
  Parameter(int i)         {setValue(i);}
  Parameter(std::string i) {setValue(i);}
  Parameter(double i)      {setValue(i);}
  Parameter(bool i)        {setValue(i);}
  Parameter(const Parameter&);
  virtual ~Parameter()             {this->Clear();}

  void setValue(int i);
  void setValue(std::string i);
  void setValue(double i);
  void setValue(bool i);
  void setValue(int t, std::string& name);
  void getValue(int& i)         {if(type==0) i = *(static_cast<int*>(val));}
  void getValue(std::string& i) {if(type==1) i = *(static_cast<std::string*>(val));}
  void getValue(double& i)      {if(type==2) i = *(static_cast<double*>(val));}
  void getValue(bool& i)        {if(type==3) i = *(static_cast<bool*>(val));}

  int getType() {return type;}  
 
  Parameter operator= (Parameter);

};

//////////////////////////////////////////////////
// class ParameterReader
// read parameters files
class ParameterReader
{
 public:

  ParameterReader();			// creators, destructors
  virtual ~ParameterReader();

  void AddTypeFile(const char*);	// Add files
  void AddParameterFile(const char*);
  void AddParameterFile(std::istream&);
  // Get parameters
  int         GetParameter(int&, std::string type, std::string name="", std::string key="");
  std::string GetParameter(std::string&, std::string type, std::string name="", std::string key="");
  double      GetParameter(double&, std::string type, std::string name="", std::string key="");
  bool        GetParameter(bool&, std::string type, std::string name="", std::string key="");

  template <class T> T GetParameter( std::string type, std::string name="", std::string key="")
    {
    T toto;
    GetParameter(toto,type,name,key);
    return toto;
    }
  int         GetInt(std::string name="")  {int u=0; GetParameter(u,"int",name); return u;}
  std::string GetString(std::string name=""){std::string u=""; GetParameter(u,"string",name); return u;}  
  double      GetDouble(std::string name="")  {double u=0; GetParameter(u,"double",name); return u;}  
  bool        GetBool(std::string name="")  {bool u=0; GetParameter(u,"bool",name); return u;}
  
  int         GetSize(std::string name)  { return m_Parameters[name].size(); }
  // Iterators
  bool        itrEnd(std::string);
  void        itrInit(std::string);
  void        itrNext(std::string);
  std::string itrName(std::string);
  void        SubstituteConstant(std::string& line);
  // string methods
  // - decompose a string into substrings, eg. "a|b|c" -> {"a","b","c"}
  static void ReadString(std::vector<std::string>&, std::string, char = '|');
  // - substitute a char by another in string
  static std::string Substitute(std::string, char, char);
  static std::string GetPath(std::string file);
  
  template<class T> static T Parse(std::string &, bool rem=true); 
  // string to parameter converter
  // Display
  void DisplayCatalog();
  void DisplayVariables();
  void DisplayConstants();
 
 private:
  std::map< std::string, std::string>                   m_Constants;
  std::map< std::string, std::pair<table, prtype> >			m_Catalog;	// Stores structure of defined types
  std::map< std::string, std::map<std::string, prtype > >		m_Parameters;	// indices : type , name
  std::map< std::string, std::map<std::string, prtype>::iterator >	m_Itr;		// Iterator
  ClassDef(ParameterReader,0)   
};

#endif


