/**
 * @file 
 * @author  Nick Ayres <na280@gmail.com>
 * @version 1.2
 */
 
 /** @class DoubleWithError
 * @brief Holds a double and an associated error.
 */
 
 #ifndef DOUBLEWITHERROR_H
 #define DOUBLEWITHERROR_H
 #include "TObject.h"
 #include <iostream>
 
class DoubleWithError{
public:
	DoubleWithError();
	double number;
	double error;
	void print(std::ostream&);
	
};

std::ostream& operator<< (std::ostream &out, DoubleWithError &d);
#endif
