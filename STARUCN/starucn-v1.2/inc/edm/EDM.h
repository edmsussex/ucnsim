/**
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 * @brief Constants etc. that are defined in the EDM namespace.
 *
 */
#ifndef EDM_H
#define EDM_H

#ifdef USE_MPFR
#include <mpreal.h>
#else
#include <cmath>
#endif

namespace EDM {
	
	
inline double rgamma_N()
{
	return -29.1646870E+6;
}


inline double echarge()
{
	return 1.60217646e-19;
}


inline double csquared()
{
	return 299792458.*299792458.;
}

inline double hbar()
{
	return 1.054571596e-34;
}

inline double rgamma_Hg()
{
	return -29.1646870E+6;
}

inline double g_acc()
{
	return -9.80665;
}


inline double g_squared()
{
	return 96.1703842225;
}

inline double mu0()
{
	return 4*M_PI * 10e-7;
}

#ifdef USE_MPFR


typedef mpfr::mpreal real;


inline real Rcos(real v)
{
	return cos(v);
}

inline real Rsin(real v)
{
	return sin(v);
}


inline real Racos(real v)
{
	return acos(v);
}

inline real Rasin(real v)
{
	return asin(v);
}



inline real Rsqrt(real v)
{
	return sqrt(v);
}

inline real Rfabs(real v)
{
	return fabs(v);
}

inline double toDouble(real v)
{
	return v.toDouble();
}

inline long double toLDouble(real v)
{
	return v.toLDouble();
}

#else

typedef long double real;

inline real Rcos(real v)
{
	return cosl(v);
}

inline real Rsin(real v)
{
	return sinl(v);
}

inline real Racos(real v)
{
	return acosl(v);
}

inline real Rasin(real v)
{
	return asinl(v);
}


inline real Rfabs(real v)
{
	return fabsl(v);
}

inline real Rsqrt(real v)
{
	return sqrtl(v);
}

inline double toDouble(real v)
{
	return v;
}

inline long double toLDouble(real v)
{
	return v;
}

#endif

}

#endif
