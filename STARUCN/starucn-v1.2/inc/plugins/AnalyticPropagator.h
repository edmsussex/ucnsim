/*
======================================================================
STARucn MC software
File : AnalyticPropagator.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef AnalyticPropagator_H
#define AnalyticPropagator_H

#include "StepPropagator.h"
#include "ShadowGeometry.h"

class AnalyticPropagator : public StepPropagator
{
public:
	AnalyticPropagator();
	virtual ~AnalyticPropagator();
	virtual void Configure(const char*);

  double PropagateToSurface(Particle&, double& Dt, double* Norm, TGeoVolume* &next, TPolyLine3D* u=0); // no polyline implementation

private:
    ShadowGeometry* fShadow;   // analytic copy af a part of the geometry

	double fX[5];
	double fY[5];
	double fZ[5];
};

#endif
