/*
======================================================================
STARucn MC software
File : MultiOutputInterface.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class MultiOutputInterface		                            //
//                                                          //
// OutputInterface plugin for combined outputs              //
//                                                          //
// Author : B. Clement (bclement_AT_lpsc.in2p3.fr)          //
//                                                          //
//////////////////////////////////////////////////////////////

#ifndef MultiOutputInterface_H
#define MultiOutputInterface_H

#include "BaseOutputInterface.h"
#include "TFile.h"
#include "TH1F.h"

class MultiOutputInterface : public BaseOutputInterface
{
public:
	MultiOutputInterface();
	virtual ~MultiOutputInterface();

  virtual void Configure(const char*);
	virtual void SaveStep(Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
	virtual void EndParticle (Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
	virtual void BeginParticle (Particle&);
  virtual void Finalize();

private:

  int kSize;
  BaseOutputInterface** fInterfaces;
};

#endif


