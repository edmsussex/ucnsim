/**
 * @file 
 * @author  Nick Ayres
 * @version 1.2
 * @brief Saves run data as ROOT files
 * This saves both the raw data from each particle as well as information about the overall run e.g. alpha, false edm measurement etc.
 */

#ifndef SpinRootOutputInterface_H
#define SpinRootOutputInterface_H


#include "BaseOutputInterface.h"
#include "TFile.h"
#include "TNtupleD.h"
#include "TKey.h"
#include "TTree.h"
#include "FieldManager.h"
#include "DoubleWithError.h"
#include "Vector3L.h"
#include <vector>
#include <string>

class SpinRootOutputInterface : public BaseOutputInterface
{
public:
	SpinRootOutputInterface();
	virtual ~SpinRootOutputInterface();

	virtual void Configure(const char*);
	virtual void EndParticle (Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
	virtual void BeginParticle (Particle&);
  
	virtual void SaveStep(Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
	virtual void Finalize();
protected:
	
	//holds address of config file so we can save its contents later
	const char* cfgpath;
	
	//calculates the false edm (due to vxE etc.) of a single neutron
	double GetTrueFalseEDM(Particle &n);

	//these routines calculate ensemble parameters for the run
	double CalculateReferencePhase(std::vector<std::vector<double>>);
	double CalculateReferencePhaseRevE(std::vector<std::vector<double>>);
	double GetAlpha(std::vector<std::vector<double>>,double referencephase);
	double GetAlphaRevE(std::vector<std::vector<double>>,double referencephase);
	double GetMeasuredAngFreq(double TrueAvgFreq, double referencephase);
	double GetTrueAvgAngFreq(std::vector<std::vector<double>>);
	double GetTrueAvgAngFreqRevE(std::vector<std::vector<double>>);
	double GetEnsembleFalseEDM(double AvgAngFreqDiff);
	double GetAvgAngFreqDiff(std::vector<std::vector<double>>);
	
	TPolyLine3DWrap fLine3D;
	int LineRecord;
	TFile* fOutput;
	TNtupleD* fTree;
  
	std::vector<std::string> fFinalVolumes;
	
	std::string runid;
	std::string OuterLoopVariableName;
	double OuterLoopVariableValue;
	int runsection;
	int runtimeslice;

	void SaveParticle(Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface, bool end);

	FieldManager fieldManager; //to find E, B for False EDM Calculations

	double  MNEUTRON;
	double  fStopTime;
	Vector3L AverageEField;
	
	//bootstrap stuff
	bool UseBootstrapErrors;
	int nBootstrap;
	std::vector<std::vector<double>> Bootstrap(std::vector<std::vector<double>>);

};

#endif
