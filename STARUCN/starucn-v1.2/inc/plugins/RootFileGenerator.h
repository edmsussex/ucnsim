#ifndef ROOT_FILE_GENERATOR_H
#define ROOT_FILE_GENERATOR_H

#include <string>
#include "Vector3L.h"
#include "BaseGenerator.h"
#include <fstream>
#include "ParameterReader.h"
#include "TFile.h"
#include "TNtupleD.h"
#include "Particle.h"
#include "Spin.h"

class RootFileGenerator : public BaseGenerator {
public:
	RootFileGenerator();
	~RootFileGenerator();
	void Configure(const char*);
	void Generate(Particle&);
	
private:

	TNtupleD* InputTree;
	//TFile* InputFile;
	int particleid;
	int entries;
	
	double ParticleGyro;
};

#endif
