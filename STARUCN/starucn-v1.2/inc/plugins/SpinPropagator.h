/**
 * @file 
 * @author  Nick Ayres <na280@sussex.ac.uk>
 * @version 1.2
 */


#ifndef SpinPropagator_H
#define SpinPropagator_H

#include <cmath>
#include "Vector3L.h"
#include "StepPropagator.h"
#include "FieldManager.h"
#include <iomanip>
#include <algorithm>
#include <string>


/** @class SpinPropagator 
 * @brief Spin Propagator with adaptive step size
 * 
 * This class is propagator for UCN Spins. It acts like StepPropagator, but it also averages the magnetic
 * field over the step and applies it to the spin using spin.Evolve.
 * 
 * Several algorithms are incorparated, but only RungeKuttaCashKarpPropagate is well tested.
*/

class SpinPropagator : StepPropagator{
	public:
	SpinPropagator();
	~SpinPropagator();
	void Configure(const char* file);
	bool Propagate(double dt, Particle&);
	double PropagateToSurface(Particle&, double& Dt, double* Norm, TGeoVolume* &next, TPolyLine3D* u=0);
	
	protected:

	bool kUseGravity;
	double fStep; // the smaller the slower... but more precise, in mm
	double fTolerance;

	bool NonAdaptiveRungeKuttaCashKarpPropagate(double dt,Particle &n);
	bool SingleStepNonAdaptiveRungeKuttaCashKarpPropagate(double dt,Particle &n);
	bool MinimalTestPropagate(double dt,Particle &n);
	bool RungeKuttaCashKarpPropagate(double dt, Particle&);
	
	bool IsInVolume(double dt, Particle& n);
	void UpdateNeutron(double dt, Particle &n);
	
	
	Vector3L PreviousBField; //this holds the B field (previously calculated) at the current position
	
	std::string PropagatorMode;

	bool DisablevxE;

	//applies vxE effect
	Vector3L GetEffectiveB(double t, Particle &n, Vector3L& u, Vector3L &v);
	//Effective B field with E reversed
	Vector3L GetEffectiveBRevE(double t, Particle &n, Vector3L& u, Vector3L &v);

	//gets field at neutron position time t after neutron current time
	Vector3L GetBField(double t, Particle& n, Vector3L &u, Vector3L &v);
	Vector3L GetEField(double t, Particle& n);
	Vector3L GetERevField(double t, Particle &n);
	Vector3L GetVelocityV(double t, Particle &n); //calculates v(t) from gravitational interaction/SUVAT
	Vector3L GetPositionV(double t, Particle &n); //calculates r(t) from gravitational interaction/SUVAT


	double MaxTimeStep; //Timestep to start with
	double MinTimeStep; //Stops us ending up trying infinitely smaller timesteps if we have a problem

	double TargetError; //target error in precession angle per unit time 
	
	Vector3L AddVector3L(Vector3L A, Vector3L B);

	FieldManager fieldManager; /*! Manages the fields that are specified in the configuration script */
	Vector3L plane_u, plane_v; /*! Plane in which the we track the accumulated phase of the spin vector */
	double backwardstime; //stores time when we are going backwards.
};

#endif
