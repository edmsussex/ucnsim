/**
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 */
#ifndef FIELDMAP_H
#define FIELDMAP_H

#include "Vector3L.h"
#include "Field.h"


 /**
  * @class FieldMapPoint
  * Class defining point in a field (position associated with a value).
  */
class FieldMapPoint
{
	public:
		Vector3L pos; /*! The position of field map point */
		Vector3L val; /*! The field value of the field map point. */
};

/**
 * @class FieldMap
 * @brief Abstract class defining the structure of fieldmap implementations.
 * 
 * The point of the class was to abstract away searching nearest neighbours
 * and interpolation. However, this now seems a redundant idea because
 * the k-d tree is not as fast as the grid search for rectangular grids
 * and the linear interpolation does not work on non rectangular grids.
 */
class FieldMap :  public Field
{
public:
  FieldMap() {;}
  virtual ~FieldMap(){;}
  virtual void Configure(const char*) = 0;	
  virtual int Init(Vector3L origin) = 0;
  /**
   * Calls GetPointPoints() to get the points surrounding the given position
   * vector and then does a linear interpolation to get the field value.
   * @param Location of interest.
   * @return Field value at given location.
   */
  Vector3L GetField(Vector3L& pos);
  /**
   * Get the points surrounding a position vector.
   */
  virtual int GetBoundingPoints(Vector3L& pos, FieldMapPoint **points) = 0;
};

#endif
