/*
======================================================================
STARucn MC software
File : HistogramOutputInterface.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class HistogramOutputInterface		                        //
//                                                          //
// OutputInterface plugin for root file event output        //
//                                                          //
// Author : B. Clement (bclement@lpsc.in2p3.fr)             //
//                                                          //
//////////////////////////////////////////////////////////////

#ifndef HistogramOutputInterface_H
#define HistogramOutputInterface_H

#include "BaseOutputInterface.h"
#include "TFile.h"
#include "TH1F.h"

class HistogramOutputInterface : public BaseOutputInterface
{
public:
	HistogramOutputInterface();
	virtual ~HistogramOutputInterface();

  virtual void Configure(const char*);
	virtual void SaveStep(Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
	virtual void EndParticle (Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
	virtual void BeginParticle (Particle&);
  virtual void Finalize();
  
private:

  TFile* fOutput;
  std::string fVol;
  double fTstep;
  int fNstep;
  TH1* fHistTime; 
  int fBin;
};

#endif


