/*
======================================================================
STARucn MC software
File : CompleteRootOutputInterface.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class CompleteRootOutputInterface                        //
//                                                          //
// OutputInterface plugin for root file event output        //
//                                                          //
// Author : B. Clement (bclement_AT_lpsc.in2p3.fr)          //
//                                                          //
//////////////////////////////////////////////////////////////

#ifndef CompleteRootOutputInterface_H
#define CompleteRootOutputInterface_H

#include "BaseOutputInterface.h"
#include "TFile.h"
#include "TNtupleD.h"
#include <vector>
#include <string>

class CompleteRootOutputInterface : public BaseOutputInterface
{
public:
	CompleteRootOutputInterface();
	virtual ~CompleteRootOutputInterface();

  virtual void Configure(const char*);
	virtual void SaveStep(Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
	virtual void EndParticle (Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
	virtual void BeginParticle (Particle&);
  virtual void Finalize();
  
protected:
  TPolyLine3DWrap fLine3D;
  int LineRecord;
  TFile* fOutput;
  TNtupleD* fTree;
  
  std::vector<std::string> fFinalVolumes;
  int kDiff;
  int kTran;
  int kSpec;

  double kV_init;
  double kVx_init;
  double kVy_init;
  double kVz_init;
  double kX_init;
  double kY_init;
  double kZ_init;
  double kT_init;

  double  MNEUTRON;
};

#endif


