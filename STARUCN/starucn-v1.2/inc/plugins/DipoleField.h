/**
 * @class DipoleField
 * @file 
 * @author  Nick Ayres <na280@sussex.ac.uk>
 * @version 1.2
 * @brief This field describes a pure dipole at position x,y,z with m the (vector) magnetic dipole moment
 * 
 */
#ifndef DIPOLE_FIELD_H
#define DIPOLE_FIELD_H

#include "Field.h"
#include "Vector3L.h"


class DipoleField : Field
{
public:
	DipoleField();
	~DipoleField();
	void Configure(const char *f);
	/**
	 * @param Field value at location pos -- irrelevant but there 
	 * for consistency with other field implementations.
	 * @return Field value (same for all position vectors given).
	 */
	Vector3L GetField(Vector3L& r);
private:
	Vector3L pos; /*! Position of Dipole Moment */
	Vector3L mdm; /*! Magnetic Dipole Moment */
};

#endif
