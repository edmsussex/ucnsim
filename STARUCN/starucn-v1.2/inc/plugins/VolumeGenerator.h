/*
======================================================================
STARucn MC software
File : VolumeGenerator.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#pragma once
#include "BaseGenerator.h"
#include "TGeoVolume.h"
#include "TGeoBBox.h"
#include "TF1.h"
#include "TMath.h"

#include <fstream>

class VolumeGenerator : public BaseGenerator
{
public:
  VolumeGenerator();
  virtual ~VolumeGenerator();
  void Configure(const char*);
  void Generate(Particle&);

private:
  unsigned int id;
  int NbParticles;
  double fMin[3];
  double fMax[3];
  TF1*   fSpeedDensity[3];
  std::string fMode;

  double fRate;
  double fCurrTime;

  TGeoVolume* fActiveVolume;
  TGeoBBox  * fActiveBox;
  TGeoNode  * fActiveNode;

  double fDx;
  double fDy;
  double fDz;
  double fOx;
  double fOy;
  double fOz;
  
  //Velocity Histogram
  bool UseVelocityHistogram;
  std::string VelocityHistogramPath;
  std::vector<double> hist_velocitylist;
  std::vector<double> hist_velocityprob;
  
  bool UseProperPositionDist;
  
  //For Alpha mode
  bool UseAlphaMode;
  double Alpha;
  double TrapRadius;
  Vector3L alpha_spin_plane_u;
  Vector3L alpha_spin_plane_v; 
  
  double ParticleGyro;
  
  std::string SpinGeneratorMode;
  Vector3L plane_u, plane_v; //holds plane to start neutrons in if SpinGeneratorMode == plane
  Vector3L StartPhase; //holds starting phase if mode is SpinGeneratorMode == identical
};
