/*
======================================================================
STARucn MC software
File : NeutronInteraction.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef NeutronINTERACTION_H
#define NeutronINTERACTION_H

#include <iostream>
#include "Particle.h"
#include "BaseInteraction.h"
#include "TRandom3.h"

class NeutronInteraction : public BaseInteraction{
public:
	NeutronInteraction();
	virtual ~NeutronInteraction();
  void Configure(const char* file);

  int SurfaceInteraction(Particle&, TGeoVolume* vin, TGeoVolume* vnext, double dt, double* norm, TGeoVolume*& newvol);
  int VolumeInteraction (Particle&, TGeoVolume* vin, double Dt);

  static double  EnergyToVelocity(double df)  {return 1e3*sqrt(180.*fabs(df)/939.56536)*fabs(df)/df;} // v in mm/s
  static double  VelocityToEnergy(double v)   {return v*v*1e-6*939.56536/180.;} // E in neV
    
private:
  bool UseSurfaceDepolarisation;
  double DepolarisationProbability;

  double GetTransmissionProbability();
  double GetReflectionProbability();
  double GetAbsorbtionProbability();
  bool NoDecay;
};

#endif
