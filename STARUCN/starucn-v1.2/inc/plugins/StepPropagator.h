/*
======================================================================
STARucn MC software
File : StepPropagator.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef StepPropagator_H
#define StepPropagator_H

#include "BasePropagator.h"

class StepPropagator : public BasePropagator
{
public:
	StepPropagator();
	virtual ~StepPropagator();
	virtual void Configure(const char*);

	bool   Propagate(double dt, Particle&);
  double PropagateToSurface(Particle&, double& Dt, double* Norm, TGeoVolume* &next, TPolyLine3D* u=0);
  void Evolve(double dt, Particle&){;}
protected:
	bool kUseGravity;
	double fStep; // the smaller the slower... but more precise, in mm
	double fTolerance;
};

#endif
