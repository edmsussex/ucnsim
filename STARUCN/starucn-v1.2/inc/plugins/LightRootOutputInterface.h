/*
======================================================================
STARucn MC software
File : LightRootOutputInterface.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class LightRootOutputInterface                           //
//                                                          //
// OutputInterface plugin for root file event output        //
//                                                          //
// Author : B. Clement (bclement@lpsc.in2p3.fr)             //
//                                                          //
//////////////////////////////////////////////////////////////

#ifndef LightRootOutputInterface_H
#define LightRootOutputInterface_H

#include "BaseOutputInterface.h"
#include "TFile.h"
#include "TNtupleD.h"
#include <vector>
#include <string>
#include <fstream>

class LightRootOutputInterface : public BaseOutputInterface
{
public:
	LightRootOutputInterface();
	virtual ~LightRootOutputInterface();

  virtual void Configure(const char*);
  virtual void BeginParticle(Particle&) {;}
	virtual void SaveStep(Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
	virtual void EndParticle (Particle&, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface);
  virtual void Finalize();
  
private:
  TPolyLine3DWrap fLine3D;
  int LineRecord;
  std::vector<std::string> fFinalVolumes;
  TFile* fOutput;
  TNtupleD* fTree;
  
  int kDiff;
  int kTran;
  int kSpec;

};

#endif


