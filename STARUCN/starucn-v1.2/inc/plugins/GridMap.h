/**
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 */
#ifndef GRIDMAP_H
#define GRIDMAP_H

#include <string>
#include <vector>
#include <map>

#include "Vector3L.h"
#include "Particle.h"
#include "FieldMap.h"


class GridMapPointCache
{
	public:
		unsigned int i,j,k;
};

/**
 * @class GridMap
 * @brief Regular grid map implementation of a Field.
 * 
 * GridMap implementation from Phil's monte carlo. The input file
 * for the grid can be set with fieldname.SourceFile.
 * @section sec File Format
 * All the values are tab seperated.
 * The first line of the file takes three values: 
 * number of x points, number of y points, number of z points.
 * 
 * The subsequent lines are all define a position and field values associated
 * with it. The format for this is %lf\t%\lf\t%lf\t%le\t%\le\t%le
 * with the first numbers values corresponding to the x, y,z position
 * cooridantes.
 * 
 * The z coordinate is varied first, then y and then x. Each of the
 * coordinates are ticked over in ascending order.
 * 
 */
class GridMap : public FieldMap
{
public:
	GridMap();
	~GridMap();
	void Configure(const char *file);
	/**
	 * Read in field map.
	 * @param The origin of the field map (adds the value onto everypoint). 
	 * The origin should be set with fieldname.origin but at the moment
	 * just defaults to zero.
	 */
	int Init(Vector3L origin);
	/**
	 * Searches the grid for the 8 surrounding points.
	 */
	int GetBoundingPoints(Vector3L& point, FieldMapPoint **points);
	
protected:
	std::string sourceFile; /*! The grid map file name that is to be read in.*/
private:
	std::vector<std::vector<std::vector<FieldMapPoint> > > grid;
	
	/**
	 * Probably change the Field system so particles are passed instead of
	 * points that way the position of the given particle can be cached.
	 */
	std::map<Particle*,GridMapPointCache> cache;
	
};

#endif
