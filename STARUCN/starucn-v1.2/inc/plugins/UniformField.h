/**
 * @class UniformField
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 * @brief Uniform field implementation. The values of the field can
 * be set in the appropriate script file with fieldname.{x,y,y}.
 * (See usage of FieldManager).
 * 
 */
#ifndef UNIFORM_FIELD_H
#define UNIFORM_FIELD_H

#include "Field.h"
#include "Vector3L.h"


class UniformField : Field
{
public:
	UniformField();
	~UniformField();
	void Configure(const char *f);
	/**
	 * @param Field value at location pos -- irrelevant but there 
	 * for consistency with other field implementations.
	 * @return Field value (same for all position vectors given).
	 */
	Vector3L GetField(Vector3L& pos);
private:
	Vector3L fieldValue; /*! Field value everywhere */
};
#endif
