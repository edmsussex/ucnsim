/**
 * @class UniformField
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 * @brief Uniform field implementation. The values of the field can
 * be set in the appropriate script file with fieldname.{x,y,y}.
 * (See usage of FieldManager).
 * 
 */
#ifndef TIME_VARYING_FIELD_H
#define TIME_VARYING_FIELD_H

#include <string>
#include "Field.h"
#include "Vector3L.h"
#include "TFormula.h"


class TimeVaryingField : Field
{
public:
	TimeVaryingField();
	~TimeVaryingField();
	void Configure(const char *f);
	/**
	 * @param position, time from start
	 * @return Field value at time/position specified.
	 */
	Vector3L GetField(Vector3L& pos,double time);
	Vector3L GetField(Vector3L& pos);
private:
	Vector3L fieldValue; /*! Field value everywhere */
	
	TFormula *xformula, *yformula, *zformula;
	std::string xformulastring, yformulastring, zformulastring;
	
};
#endif
