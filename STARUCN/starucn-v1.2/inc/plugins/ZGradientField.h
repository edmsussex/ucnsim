/**
 * @file 
 * @author  Nick Ayres <na280@sussex.ac.uk>
 * @version 1.2
 */


 /** 
 @class ZGradientField
 * @brief The ZGradientField class describes a field given by the field at the origin and
 * by dF/dz - the vertical field gradient.
 * 
 * 
 * Assuming a divergenceless field
 * we know that, for a cylinder, Flux through top + Flux through base + 
 * Flux through sides = 0. By assuming Bz is independant of x and y, we get
 * 
 * Fx = F0x - x/2 dF/dz
 * Fy = F0y - y/2 dF/dz
 * Fz = F0z + z dF/dz
 * 
 * This is the same as used in Phil's code.
 * 
 */
#ifndef ZGRADIENT_FIELD_H
#define ZGRADIENT_FIELD_H

#include "Field.h"
#include "Vector3L.h"


class ZGradientField : Field
{
public:
	ZGradientField();
	~ZGradientField();
	void Configure(const char *f);
	Vector3L GetField(Vector3L& pos);
private:
	Vector3L F0;
	long double dFdz;
};
#endif
