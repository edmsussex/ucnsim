/*
======================================================================
STARucn MC software
File : ShadowQuadric.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef SHADOWQUADRIC_H
#define SHADOWQUADRIC_H

#include <iostream>
#include <vector>
#include "ShadowSurface.h"

class ShadowQuadric : public ShadowSurface {
public:
	ShadowQuadric(double* pars=0);
	virtual ~ShadowQuadric();
	void SetDebug(int status=0) {kDebug=status;}

	void SetSphere(double r)   {SetEllipsoide(r,r,r);}
	void SetEllipsoide(double r1, double r2, double r3);
	void SetCylinder(double r);
  void SetPlane(double phi); // horzontal plane at z=0
	void SetTwistedPlane(double a, double dz);

	virtual void Transform();   // Change coordinates 
	virtual void EvalIJ(int u, double a, double b, std::vector<double>& r);
	virtual int Side(double x, double y, double z)  ;
	virtual double ClosestIntersect(double* x, double* y, double* z, double* n=0); // x=x[0]*t*t+x[1]*t+x[2]*t  x[3]=result;

protected:
	int kDebug;
  bool IsPlane;
	//  fQuad[0]*x^2+fQuad[1]*y^2+fQuad[2]*z^2+fCross[0]*xy+fCross[1]*yz+fCross[2]*zx+fLin[0]*x+fLin[1]*y+fLin[2]*z+fConst
	double fQuad[3];
	double fCross[3];
	double fLin[3];
	double fConst;
	std::vector<double> fTmp;
	virtual void Normal(double x, double y, double z, double vx, double vy, double vz, double* n);
};

#endif
