/*
======================================================================
STARucn MC software
File : ShadowSurface.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef SHADOWSURFACE_H
#define SHADOWSURFACE_H

#include <iostream>
#include <vector>
#include "ShadowTransform.h" 

class ShadowSurface{
public:
	ShadowSurface();
	virtual ~ShadowSurface();
	void SetDebug(int status=0) {kDebug=status;}

	void SetTransform(ShadowTransform* t)    {fT = t; if(fT) Transform();}
	virtual void Transform() = 0;   // Change coordinates 
	void EvalXY(double x, double y, std::vector<double>& r) {EvalIJ(2,x,y,r);}
	void EvalYZ(double y, double z, std::vector<double>& r) {EvalIJ(0,y,z,r);}
	void EvalZX(double z, double x, std::vector<double>& r) {EvalIJ(1,z,x,r);}
	virtual void EvalIJ(int u, double a, double b, std::vector<double>& r) = 0;
	
	virtual int Side(double x, double y, double z) = 0;
	void DrawRandoms(int n, double bX, double bY, double bZ,  std::vector<double>& x,  std::vector<double>& y,  std::vector<double>& z);

	virtual double ClosestIntersect(double* x, double* y, double* z, double* n=0) = 0; // x=x[0]*t*t+x[1]*t+x[2]*t  x[3]=result;

protected:
	virtual void Normal(double x, double y, double z, double vx, double vy, double vz, double* n);
	int kDebug;
	ShadowTransform* fT;
};

#endif
