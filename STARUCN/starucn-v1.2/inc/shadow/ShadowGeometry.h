/*
======================================================================
STARucn MC software
File : ShadowGeometry.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef SHADOWGEOMETRY_H
#define SHADOWGEOMETRY_H

#include <iostream>
#include "ShadowVolume.h"
#include "Geometry.h"
#include <vector>

class ShadowGeometry{
public:
	ShadowGeometry(Geometry* geo);
	~ShadowGeometry();
	void SetDebug(int status=0) {kDebug=status;}

	ShadowVolume* FindVolume(double*)     {return 0;}
	ShadowVolume* GetShadow(TGeoVolume* gv)   {return fShadows[gv];};

// Shapes shadowing the root ones
	ShadowVolume* MakeBox (double dx, double dy, double dz, ShadowTransform* sdw) ;
	ShadowVolume* MakeTube(double rmin, double rmax, double dz, ShadowTransform* sdw);
    ShadowVolume* MakeTubs  (double rmin, double rmax, double dz, double phi1, double phi2, ShadowTransform* sdw);
    ShadowVolume* MakeTorus (double R, double rmin, double rmax, double phi1, double phi2, ShadowTransform* sdw);

// many more to implement...
	bool Contains(double* x);

private:
  int kDebug;
  Geometry* fGeometry;

  std::map<TGeoVolume*, ShadowVolume*>    fShadows;
  std::map<std::string, ShadowTransform*> fTransforms;
  std::map<std::string, ShadowVolume*>    fVolumes;
  std::vector<ShadowSurface*>            fSurfaces;  // keep record for cleaning // NOT YET IMPLEMENTED
};

#endif
