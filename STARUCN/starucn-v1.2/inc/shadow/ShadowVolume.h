/*
======================================================================
STARucn MC software
File : ShadowVolume.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef SHADOWVOLUME_H
#define SHADOWVOLUME_H

#include <iostream>
#include <ShadowSurface.h>
#include <vector>
#include <map>

class ShadowVolume{
public:
	ShadowVolume();
	~ShadowVolume();
	void SetDebug(int status=0) {kDebug=status;}

	void AddSurface(ShadowSurface* f, int i)   {fSurfaces.push_back(std::pair<ShadowSurface*,int>(f,i)); kN++;}

	bool Contains(double*)  ;
    double ClosestIntersect(double* x, double* y, double* z);
  
private:
	int kDebug;
	int kN; 
    std::vector <std::pair<ShadowSurface*,int> > fSurfaces;  
};

#endif
