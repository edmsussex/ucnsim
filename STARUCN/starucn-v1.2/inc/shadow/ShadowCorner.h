/*
======================================================================
STARucn MC software
File : ShadowSurface.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef SHADOWCORNER_H
#define SHADOWCORNER_H

#include <iostream>
#include "ShadowSurface.h"

class ShadowCorner : public ShadowSurface {
public:
	ShadowCorner(double phi1, double phi2);
	~ShadowCorner();
	void SetDebug(int status=0) {kDebug=status;}

  virtual void Transform();   // Change coordinates 
  virtual void EvalIJ(int u, double a, double b, std::vector<double>& r);
  virtual int Side(double x, double y, double z)  ;
  virtual double ClosestIntersect(double* x, double* y, double* z, double* n=0); // x=x[0]*t*t+x[1]*t+x[2]*t  x[3]=result;

private:
  virtual void Normal(double x, double y, double z, double vx, double vy, double vz, double* n);   	    
  int kDebug;
  double fCoeffs[3][4]; // 0,1 borders, 2 orth plane
  int fPlaneSide[3];    // 0,1 borders, 2 orth plane
  std::vector<double> fTmp;
};

#endif
