/*
======================================================================
STARucn MC software
File : ShadowTransform.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef SHADOWTRANSFORM_H
#define SHADOWTRANSFORM_H

#include <iostream>

class ShadowTransform{
public :
	ShadowTransform(double*, double*);
	virtual ~ShadowTransform();
	void SetTranslation(double *);
	void SetTranslation(double,double,double);
	void SetRotation(double *);
	void SetRotation(double,double,double);

	void Transform(double*, double*);
	void Transform(double&,double&,double&);
	void InvTransform(double*, double*);
	void InvTransform(double&,double&,double&);

	double M(int i, int j) {return  fMatrix[i][j];}
	double T(int i)        {return i==0 ? fDx : i==1 ? fDy : fDz;}

private :
	double fCa, fSa;
    double fCb, fSb;
    double fCc, fSc;
	double fDx, fDy, fDz;
	double fMatrix[3][3];

};

#endif
