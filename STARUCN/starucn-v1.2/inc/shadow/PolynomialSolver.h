/*
======================================================================
STARucn MC software
File : PolynomialSolver.h
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#ifndef POLYNOMIALSOLVER_H
#define POLYNOMIALSOLVER_H
// analytic solver of polynomial equation... up to the fourth order
#include <vector>

class PolynomialSolver
{
public:
	PolynomialSolver();
	~PolynomialSolver();

	static void Solve(int order, double* par, std::vector <double>& sol, bool clear=true, bool scale=true);
};

#endif
