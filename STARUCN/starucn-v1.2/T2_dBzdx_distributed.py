#!/usr/bin/env python

import os
import numpy
import shutil
import random
import math

print "STARucn T2 Script - Nicholas Ayres 2014 na280@sussex.ac.uk"
print "This generates several base scripts, then qsubs several to run starucn for each value"

NbParticles = 1024 #number of neutrons to start with for each dBz/dz value
random.seed(12334) #seed for RNG, in turn seeds STARucn RNGs
runid = "T2_dBzdx" #so we know how to name everything...
endalpha = 0.70
killtime = 30
timestep = 1

runscriptpath = os.getcwd() + "/T2_dBdz_distributed_submit.sh"

dBzdxArray = numpy.linspace(0,50e-12,21)#units of T/mm
#dBzdxArray = numpy.array([10e-12])
#shutil.rmtree("/home/na280/out")
#os.mkdir("/home/na280/out")


def starucnthread(runsection):
	#base script 
	basescriptpath = os.getcwd() + '/scripts/T2_dBzdx_BaseScript.par'
	
	#folder in which to place automatically generated starucn scripts
	outscriptfolder = os.getcwd() + '/scripts/autogenscripts'
	
	#output file
	outputfile = "output_" + runid + "_" + str(runsection) + ".root"
	
	os.system("rm " +outputfile)
	
	scriptpath = outscriptfolder + '/' + runid + "_" + str(runsection) + "_basescript.par"
	print "copying ", basescriptpath, " to " , scriptpath
	shutil.copy(basescriptpath,scriptpath) #copy base script
	print "Opening File ", scriptpath
	
	f = open(scriptpath,'a') #open for appending
	
	print "Writing to File"
	#write lines to file
	
	f.write("int NbParticles " + str(NbParticles) + "\n")
	f.write("string RootFileGenerator.inputrunid " + runid + "\n")
	f.write("string RootFileGenerator.inputrunsection " + str(runsection) + "\n")
	f.write("string SpinRootOutputInterface.RunID " + runid+ "\n")
	f.write("int SpinRootOutputInterface.RunSection " + str(runsection) + "\n")
	#f.write("double FieldManager.BGrad.dFdz " + str(dBdzArray[runsection])+ "\n")
	f.write("string FieldManager.BGrad.zformula 1e-6+x*"+str(dBzdxArray[runsection])+"\n")
	print "closing file"
	f.close()
	
	starucn_args = runid + " " + str(runsection) + " " + str(dBzdxArray[runsection]) + " " + str(random.randint(0,2147483647))  \
		+ " " + str(endalpha) + " " + str(timestep) + " " + str(killtime) + " " + scriptpath
	
	command = "qsub -q mps.q -pe openmp 64-1 -N " + runid + "_" + str(runsection) + " -o $HOME/out/out_"+ runid + "_" + str(runsection)+ " " + runscriptpath + " " + starucn_args
	print "queueing T2_dBzdx with command " , command
	os.system(command)
	#os.system("source " + runscriptpath + " " + starucn_args) #alternative if you don't want to qsub

for runsection in range (len (dBzdxArray)):
	jobno = starucnthread(runsection)
	
