#!/usr/bin/env python

import os
import numpy
import shutil
import random
import math

print "STARucn T2 Script - Nicholas Ayres 2014 na280@sussex.ac.uk"
print "This generates several base scripts, then qsubs several to run starucn for each value"

NbParticles = 100 #number of neutrons to start with for each dBz/dz value
random.seed(23422342) #seed for RNG, in turn seeds STARucn RNGs
runid = "fedm_vs_vxy" #so we know how to name everything...
killtime = 15

runscriptpath = os.getcwd() + "/alpha_distributed_submit.sh"

vArray = numpy.linspace(0.1,7,20)
#alphaArray = numpy.array([1,2])


def starucnthread(runsection):
	#base script 
	basescriptpath = os.getcwd() + '/scripts/false_edm_vs_vxy.par'
	
	#folder in which to place automatically generated starucn scripts
	outscriptfolder = os.getcwd() + '/scripts/autogenscripts'
	
	#output file
	outputfile = "output_" + runid + "_" + str(runsection) + ".root"
	
	os.system("rm " +outputfile)
	
	scriptpath = outscriptfolder + '/' + runid + "_" + str(runsection) + ".par"
	print "copying ", basescriptpath, " to " , scriptpath
	shutil.copy(basescriptpath,scriptpath) #copy base script
	print "Opening File ", scriptpath
	
	f = open(scriptpath,'a') #open for appending
	
	print "Writing to File"
	#write lines to file
	
	f.write("int NbParticles " + str(NbParticles) + "\n")
	f.write("string SpinRootOutputInterface.RunID " + runid + "\n")
	f.write("int SpinRootOutputInterface.RunSection " + str(runsection) + "\n")
	f.write("double SpinRootOutputInterface.StorageTime " + str(killtime) + "\n")
	f.write("density VolumeGenerator.Density_V Uniform " + str(vArray[runsection]*1000) + " " + str(vArray[runsection]*1000) + "\n")
	f.write("double SpinRootOutputInterface.OuterLoopVariableValue " + str(vArray[runsection]) + "\n")
	f.write("int GenerationSeed " + str(random.randint(0,2**31)) + "\n")
	print "closing file"
	f.close()
	
	command = "./starucn_threaded " + scriptpath
	#command = "qsub -q mps.q -N " + runid + "_" + str(runsection) + " -o $HOME/out/out_"+ runid + "_" + str(runsection)+ " " + runscriptpath + " " + scriptpath
	print "queueing alpha with command " , command
	os.system(command)
	


for runsection in range (len (vArray)):
	starucnthread(runsection)

	
