#!/usr/bin/env python

import csv
from matplotlib import pyplot
import numpy
import math

###############
###Constants###
###############
v=5. #ucn velocity, m/s
n_gyro = 1.83247179e8 #ucn gyro T-1 s-1
R = 0.25 #Trap Radius, m

###############
###Functions###
###############
#centroid of semicircle = 4r/3pi above centre
def xgrad_to_delta_B(xgrad):
	return 4*R/(3*math.pi)*xgrad

#analytic prediction by Pendelbury
def T2_analytic(delta_B):
	return 2*v/(math.pi * R * n_gyro**2 *delta_B**2)
	
	
csvfile = open("T2_T2_dBzdx.txt",'rb')
reader = csv.reader(csvfile,delimiter='\t')
dBzdx = []
T2 = []
T2_err = []
for row in reader:
	#print row
	dBzdx.append(float(row[1]))
	T2.append(float(row[2]))
	T2_err.append(float(row[3]))

dBzdx = numpy.array(dBzdx)
dBzdx *= 1000 #convert T/mm to T/m
T2 = numpy.array(T2)
T2_err = numpy.array(T2_err)

#print dBzdx, T2, T2_err

pyplot.errorbar(dBzdx,T2,yerr=T2_err,label="STARucn MC",fmt='+')

grad_array=numpy.linspace(1e-10,dBzdx.max(),5000)
delta_B_array=numpy.zeros(len(grad_array))
T2analytic=numpy.zeros(len(grad_array))
for i in range(len(grad_array)):
	delta_B_array[i] = xgrad_to_delta_B(grad_array[i])
	T2analytic[i]=T2_analytic(delta_B_array[i])


#print grad_array,delta_B_array, T2analytic
pyplot.plot(grad_array,T2analytic,label="Analytic Prediction")

pyplot.ylim(0,1.3*T2[1])
pyplot.legend()
pyplot.xlabel("dBzdx - T/m")
pyplot.ylabel("T2 - s")
pyplot.savefig("T2_dBzdx.png")

pyplot.figure(2)
#simulated/analytic
ratioarray = T2/T2_analytic(xgrad_to_delta_B(dBzdx))
pyplot.scatter(dBzdx,ratioarray)
pyplot.xlim(0,1.3*max(dBzdx))
pyplot.xlabel("dBz/dx")
pyplot.ylabel("simulated value / analytic value")
pyplot.show()
