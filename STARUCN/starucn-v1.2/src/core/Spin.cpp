/**
 * @file 
 * @author  Oliver Winston <ollywinston@gmail.com>
 * @version 1.2
 * @brief Classical spin tracking
 */

#include <cmath>
#include <ctime>

#include <iostream>

#include "Spin.h"

#undef DEBUG

Spin::Spin()
{
	gyro = 0;
	time = 0;
	phase= 0;
	theta = 0;
	refphase= 0;
	sigma0 = Vector3L(1,0,0);
	sigma = sigma0;
	thetaerror = 0;
	depolarised = 0;
}

Spin::Spin(double g, Vector3L sigma0)
{
	time = 0;
	phase= 0;
	theta = 0;
	refphase= 0;
	sigma0 = sigma0;
	sigma = sigma0;
	gyro = g;
	thetaerror = 0;
	depolarised = 0;
#ifdef DEBUG
	std::cout << "Setting gyro = " << gyro << std::endl;
#endif
}

Spin::~Spin()
{
}

void Spin::Reset()
{
	time = 0;
	phase= 0;
	theta = 0;
	refphase= 0;
	sigma = sigma0;
	depolarised = 0;
}


void Spin::SetRandomSxy()
{
	long double x, y; 
	x = r.Uniform() * 2 - 1;
	y = std::sqrt(1 - x*x); 
	sigma0 = Vector3L(x,y,0); 
}

void Spin::SetSpin(Vector3L newspin){
	sigma = newspin;
}


void Spin::SetStartSpin(){
	sigma0 = sigma;
}

void Spin::SetRandomSpinPlane(Vector3L u, Vector3L v){
	long double x,y,z, phi,theta;
	//Generate random vector in 3D spherical polar coords (r=1)
	phi = 2 * M_PI * r.Uniform();
	theta = std::acos( (r.Uniform() * 2) - 1);
	//convert to cartesian
	x = std::sin(theta) * std::cos(phi);
	y = std::sin (theta) * std::sin (phi);
	z = std::cos(theta);
	Vector3L s(x,y,z);
	//project onto flat plane
	Vector3L s2 = s.ProjectOnPlane(u,v);
	s2 = s2 / s2.Mag();
	sigma = s2;
}

void Spin::SetRandomSpin() {
	long double x,y,z, phi,theta;
	//Generate random vector in 3D spherical polar coords (r=1)
	phi = 2 * M_PI * r.Uniform();
	theta = std::acos( (r.Uniform() * 2) - 1);
	//convert to cartesian
	x = std::sin(theta) * std::cos(phi);
	y = std::sin (theta) * std::sin (phi);
	z = std::cos(theta);
	sigma = Vector3L (x,y,z);
	depolarised = 1;
}

Vector3L Spin::EvolveSpinVector(long double dtheta, Vector3L& B)
{
	Vector3L c,d, sigma2;

	d = B*(sigma.Dot(B)/B.Mag2());
	sigma2 = (sigma - d)* std::cos(dtheta);
		
	c = sigma.Cross(B)*std::sin(dtheta)/B.Mag();
	sigma2 += c;
	sigma2 += d;
	
	return sigma2;
}

//#undef DEBUG
#define GET_SIGN(a) ((a > 0) - (a < 0))
void Spin::Evolve(long double dt, Vector3L& B, Vector3L& t, Vector3L& u)
{
#ifdef DEBUG	
	std::cout << "spin.evolve called with dt = " << dt << ", B = (" << B.x() << ", " << B.y() << ", " << B.z() << ")" << std::endl;
	std::cout << "Initial spin vector = (" << sigma.x() << ", " << sigma.y() << ", " << sigma.z() << ")" << std::endl;
#endif
	Vector3L  sigma2;
	long double afreq = -this->gyro * B.Mag();
	long double dtheta = afreq * dt;
	
#ifdef DEBUG

	std::cout << "gyro = " << -this->gyro << ", mag of B = " << B.Mag() << "afreq = " << afreq << ", dtheta = " << dtheta << std::endl;

#endif
	
	if (std::abs(dtheta) >= M_PI_2l)
	{
#ifdef DEBUG
		std::cout << "Splitting " << std::endl;
#endif
		Evolve(dt/2, B, t, u);
		Evolve(dt/2, B, t, u);
		return;
	}
		
	sigma2 = EvolveSpinVector(dtheta, B);
	
	/* track increase in reference phase */
	Vector3L v;
	long double drefphase;
	long double B0;
	v = t.Cross(u);
	B0 = B.Dot(v);
	drefphase = - B0 * gyro * dt;
	
	/* track increase in phase in plane t, u */
	Vector3L sig0, sig1;
	sig0 = sigma.ProjectOnPlane(t,u);
	sig1 = sigma2.ProjectOnPlane(t,u);
	long double dphase;
	dphase = sig1.AngleBetween(sig0) * GET_SIGN(drefphase);
	
	//test if dphase = nan
	if (dphase != dphase){
		std::cout << "dphase = nan  - sig0 = " << sig0 << "sig1 = " << sig1 << "angle between is " << sig1.AngleBetween(sig0) << std::endl;
		std::cout << "setting dphase = 0" << std::endl;
		dphase = 0;
	}
	
	/* update spin vector */
	phase += dphase;
	time += dt;
	theta += dtheta;
	sigma = sigma2;
	refphase += drefphase;	
	
#ifdef DEBUG
#define COUTVEC(v) "(" << (v).x() << "," << (v).y() << ',' << (v).z() << ')'

	Vector3L expected0(std::cos(theta), -std::sin(theta),0);
	Vector3L expected(std::cos(theta), -B.z() * std::sin(theta)/std::sqrt(B.z()*B.z() + B.y() * B.y()),0);
	Vector3L diff = sig1 - expected;
	std::cout << "t=" << time  << std::endl; 
	std::cout << "\tdt=" << dt << std::endl;
	std::cout << "\tafreq=" << afreq << std::endl;
	std::cout << "\tdirection=" << GET_SIGN(dtheta) << std::endl;
	std::cout << "\tphase=" << phase << std::endl;
	std::cout << "\tdphase=" << dphase << std::endl;
	std::cout << "\tdrefphase=" << drefphase << std::endl;
	std::cout << "\trefphase=" << refphase << std::endl;
	std::cout << "\ttheta=" << theta << std::endl;
	std::cout << "\tdFreqdiff=" << (drefphase - dphase)/dt << std::endl;
	std::cout << "\tFreqdiff=" << (refphase - phase)/time << std::endl;
	std::cout << "\tsig1 - expected=" << diff << std::endl;
	std::cout << "\tB=" << B << std::endl;
	std::cout << "\tB0=" << B0 << std::endl;
	Vector3L d = sigma2 - sigma;
	std::cout << "\tt=" << t << std::endl;
	std::cout << "\tu=" << u << std::endl;
	std::cout << "\tv=" << v << std::endl;
	std::cout << "\td=" << d << std::endl;
	std::cout << "\tsig0=" << sig0 << std::endl;
	std::cout << "\tsig1=" << sig1 << std::endl;
#endif

if (sigma.x() != sigma.x() || sigma.y() != sigma.y() || sigma.z() != sigma.z())
	std::cout << "sigma is nan!, last B = " << B.x() << " " << B.y() << " " << B.z() << " " << std::endl;

//std::cout << "evolved spin by dphase = " << dphase << ", dtheta = " << dtheta << std::endl;

}


void Spin::SetGyro(double g)
{
	gyro = g;

#ifdef DEBUG
	std::cout << "Setting gyro = " << g << std::endl;
#endif
}

double Spin::GetPhaseDiff()
{
	return phase - refphase;
}

double Spin::GetFreqDiff()
{
	return (phase - refphase)/time;
}


long double Spin::GetTime(void)
{
	return time;
}

double Spin::GetGyro()
{
	return gyro;
}

long double Spin::GetPhase()
{
	return phase;
}

long double Spin::GetTheta()
{
	return theta;
}

long double Spin::GetRefphase()
{
	return refphase;
}

Vector3L& Spin::GetSigma0()
{
	return sigma0;
}

Vector3L& Spin::GetSigma()
{
	return sigma;
}

Spin & Spin::operator=(Spin& s)
{
	refphase = s.GetRefphase();
	sigma0 = s.GetSigma0();
	sigma = s.GetSigma();
	time = s.GetTime();
	theta = s.GetTheta();
	gyro = GetGyro();
	phase = s.GetPhase();
	return *this;
}


long double Spin::GetError(){
	return thetaerror;
}

void Spin::AccumulateError(long double newerr) {
	thetaerror= std::sqrt((thetaerror*thetaerror) + (newerr*newerr));
}

bool Spin::IsDepolarised(){
	return depolarised;
}

void Spin::SetSigma(Vector3L &newsigma){
	sigma =  newsigma;
	return;
}

void Spin::SetTheta(long double newtheta){
	theta = newtheta;
}

void Spin::SetPhase(long double newphase){
	phase = newphase;
}

void Spin::SetThetaError(long double newerror){
	thetaerror = newerror;
}
	
void Spin::SetSigma0(Vector3L &newsigma0){
	sigma0 = newsigma0;
	return;
}

void Spin::SetRefPhase(long double newrefphase){
	refphase = newrefphase;
}

void Spin::SetTime(long double newtime){
	time = newtime;
}

void Spin::SeedRNG(long double newseed){
	r.SetSeed(newseed);
}
