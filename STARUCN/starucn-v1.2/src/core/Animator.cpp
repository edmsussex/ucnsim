/*
======================================================================
STARucn MC software
File : Animator.cpp
Copyright 2013 Benoit Clement
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/

#include "Animator.h"
#include "Configurable.h"
#include "LoopCounter.h"
#include "TStyle.h"
#include "TMath.h"
#include "TF1.h"
#include "TView3D.h"
#include "TText.h"
#include "TGLViewer.h"

#include <fstream>
#include <sstream>

///////////////////////////////////////////////////////
Animator::Animator(std::string conf) : TApplication("Animator",0,0)
///////////////////////////////////////////////////////
{
ParameterReader pr;
pr.AddTypeFile(Configurable::TypeFile.c_str());
pr.AddParameterFile(conf.c_str());

fGeofile = pr.GetString("Geometry");
fGeo = 0;
fCanvas = 0;
kFrames = pr.GetInt("Animate.NbFrames");
fStep = pr.GetDouble("Animate.TimeStep");
fName = pr.GetString("Animate.BaseName");
fOut  = pr.GetString("Animate.OutputDir");
fPhimode = pr.GetParameter<std::string>("camera","Animate.PhiAngle","type");
fPhidef  = pr.GetParameter<std::string>("camera","Animate.PhiAngle","value");
fThmode  = pr.GetParameter<std::string>("camera","Animate.ThetaAngle","type");
fThdef   = pr.GetParameter<std::string>("camera","Animate.ThetaAngle","value");

fFactor    = pr.GetDouble("Animate.Timefactor");
fMode = pr.GetString("Animate.Display");
kStart = pr.GetInt("Animate.StartFrame");
kStop  = pr.GetInt("Animate.StopFrame");

if(fMode=="StdROOT") MakeBatch();
fSize  =  pr.GetDouble("Animate.MarkerSize");
if(fSize<=0) fSize = fMode=="StdROOT" ? 0.4 : 3;

if(fMode=="OpenGL")
  {
  fOpenGLCamera[0] = pr.GetParameter<std::string>("camera","Animate.OpenGLFov","value");
  fOpenGLCamera[1] = pr.GetParameter<std::string>("camera","Animate.OpenGLDolly","value");
  fOpenGLCamera[2] = pr.GetParameter<std::string>("camera","Animate.OpenGLCenterX","value");
  fOpenGLCamera[3] = pr.GetParameter<std::string>("camera","Animate.OpenGLCenterY","value");
  fOpenGLCamera[4] = pr.GetParameter<std::string>("camera","Animate.OpenGLCenterZ","value");
  fOpenGLCameraMode[0] = pr.GetParameter<std::string>("camera","Animate.OpenGLFov","type");
  fOpenGLCameraMode[1] = pr.GetParameter<std::string>("camera","Animate.OpenGLDolly","type");
  fOpenGLCameraMode[2] = pr.GetParameter<std::string>("camera","Animate.OpenGLCenterX","type");
  fOpenGLCameraMode[3] = pr.GetParameter<std::string>("camera","Animate.OpenGLCenterY","type");
  fOpenGLCameraMode[4] = pr.GetParameter<std::string>("camera","Animate.OpenGLCenterZ","type");
  }


ReadCuts(pr);

// correct out of bounds start and stop
if(kStop<1) kStop=kFrames;
if(kStart<1)    kStart = 1;
if(kStop>kFrames) kStop=kFrames;
if(kStart>kStop) kStart = kStop;

}

///////////////////////////////////////////////////////
Animator::~Animator()
///////////////////////////////////////////////////////
{;}


///////////////////////////////////////////////////////
void Animator::MakeMovie()
///////////////////////////////////////////////////////
{
// init angles
TF1* funph = 0;
TF1* funth = 0;
if(fPhimode=="function") funph = new TF1("funph",fPhidef.c_str(),0,kFrames*fStep);
if(fThmode =="function") funth = new TF1("funth",fThdef .c_str(),0,kFrames*fStep);

double valph = funph==0 ? atof(fPhidef.c_str()) : 0;
double valth = funth==0 ? atof(fThdef .c_str()) : 0;

// init ogl parameters
TF1*   funogl[5];
double valogl[5];

for(int i=0; i<5;i++)
  {
  funogl[i] = fOpenGLCameraMode[i]=="function" ? new TF1(Form("funogl%d",i), fOpenGLCamera[i].c_str(),0,kFrames*fStep) : 0;
  valogl[i] = funogl[i]==0 ? atof(fOpenGLCamera[i].c_str()) : 0;
  }
if(valogl[0]==0) valogl[0] = 50; 
gStyle->SetLineWidth(2);
TText text;

int timeoffset = int(fStep*100*fFactor);
LoopCounter counter(kStop-kStart+1, kStop-kStart+1>100 ? 100 : kStop-kStart+1);

if(fMode=="StdROOT") fGeo = new Geometry(fGeofile.c_str());
MakeCanvas();
for(int k=kStart; k<=kStop; k++)
  {
  double time = k*fStep;
  double th = funth==0 ? valth : funth->Eval(time);
  double ph = funph==0 ? valph : funph->Eval(time);

  if(fMode=="StdROOT")
    {
    fCanvas->SetTheta(th);
    fCanvas->SetPhi  (ph);
    fGeo->Draw();
    DrawAnimationMarkers(Form("%s_%d.frame",(fOut+"/"+fName).c_str(),k)) ;
//     DrawAnimationGeometry(Form("%s_%d.frame",(fOut+"/"+fName).c_str(),k));

    TText* txt = text.DrawText(0.01,0.90,Form("Time = %.2lf s",time));
    if(k==kStart) fCanvas->SaveAs(Form("%s.gif",(fOut+"/"+fName).c_str()));
    else if(k==kStop) fCanvas->SaveAs(Form("%s.gif++",(fOut+"/"+fName).c_str()));
    else          fCanvas->SaveAs(Form("%s.gif+%d",(fOut+"/"+fName).c_str(),timeoffset));
    delete txt;
    }
  else if(fMode=="OpenGL")
    {
    for(int i=0; i<5;i++) valogl[i] = funogl[i] ? funogl[i]->Eval(time) : valogl[i];
    DrawAnimationGeometry(Form("%s_%d.frame",(fOut+"/"+fName).c_str(),k));
    TGLViewer* v= static_cast<TGLViewer*>(fCanvas->GetViewer3D("ogl"));
    v->SetCurrentCamera(TGLViewer::kCameraPerspXOY);
//     double cen[]={210,0,0};
    v->SetClearColor(1);
    v->SetPerspectiveCamera(TGLViewer::kCameraPerspXOY,valogl[0],valogl[1],valogl+2,-th*TMath::Pi()/180.,(-ph-90)*TMath::Pi()/180.);
    if(k==kStart) v->SavePictureUsingFBO(Form("%s.gif",(fOut+"/"+fName).c_str()),600,600);
    else if(k==kStop) v->SavePictureUsingFBO(Form("%s.gif++",(fOut+"/"+fName).c_str()),600,600);
    else         v->SavePictureUsingFBO(Form("%s.gif+%d",(fOut+"/"+fName).c_str(),timeoffset),600,600);
    delete v; v=0;
    }
  counter.Update();
  }
counter.Update();
}


/////////////////////////////////////////////////////////////////
void Animator::MakeCanvas()
/////////////////////////////////////////////////////////////////
{
if(fCanvas) delete fCanvas;
fCanvas = new TCanvas("c", "c",4,20,600,600);
fCanvas->SetHighLightColor(2);
fCanvas->Range(-1,-1,1,1);
fCanvas->SetFillColor(0);
fCanvas->SetBorderMode(0);
fCanvas->SetBorderSize(2);
fCanvas->SetFrameBorderMode(0);
}


/////////////////////////////////////////////////////////////////
void Animator::DrawAnimationMarkers(std::string file)
/////////////////////////////////////////////////////////////////
{
std::ifstream f(file.c_str());

int u=0;
for(fItr = fSets.begin(); fItr!=fItrE; fItr++) 
  {
  if(fMark[u]) delete fMark[u];
  fMark[u] = new TPolyMarker3D;
  fMark[u]->SetMarkerColor(fItr->second);
  fMark[u]->SetMarkerStyle(20);
  fMark[u]->SetMarkerSize(fSize);
  u++;
  }
std::string line;
double var[7];
double active;
while(f.good() && !f.eof())
  {
  getline(f,line);
  if(line=="") continue;
  std::istringstream str(line);
  for(int i=0;i <7;i++) str >> var[i];
  str >> active;
  if(active<0.5) continue;
  var[6] = TMath::Sqrt(var[3]*var[3]+var[4]*var[4]+var[5]*var[5]);
  u=0;
  for(fItr = fSets.begin(); fItr!=fItrE; fItr++)
    {
    double test = fItr->first->EvalPar(0,var);
    if(test>0.5)  {fMark[u]->SetNextPoint(var[0],var[1],var[2]); break;}
    u++;
    }
  }
f.close();  
u=0;
for(fItr = fSets.begin(); fItr!=fItrE; fItr++) fMark[u++]->Draw("");
}

/////////////////////////////////////////////////////////////////
void Animator::DrawAnimationGeometry(std::string file)
/////////////////////////////////////////////////////////////////
{
ParameterReader pr;
pr.AddTypeFile(Configurable::TypeFile.c_str());
pr.AddParameterFile( fGeofile.c_str() );
pr.itrInit("geometry");
std::string vlist = pr.GetParameter<std::string>("geometry","","volumes");
double X  = pr.GetParameter<double>("geometry","","sizeX"); 
double Y  = pr.GetParameter<double>("geometry","","sizeY"); 
double Z  = pr.GetParameter<double>("geometry","","sizeZ"); 

std::ifstream f(file.c_str());
std::string NewGeo = "";
NewGeo+= "include " + fGeofile + "\n";
NewGeo+= "int Transparency 50\n";
int u=0;
for(fItr = fSets.begin(); fItr!=fItrE; fItr++) NewGeo+= Form("material m%d 0 0 0 %d\n",u++,fItr->second);

std::string line;
double var[7];
double active;

int cnt = 0;
while(f.good() && !f.eof())
  {
  getline(f,line);
  std::istringstream str(line);
  for(int i=0;i <7;i++) str >> var[i];
  str >> active;
  if(active<0.5) continue;
  var[6] = TMath::Sqrt(var[3]*var[3]+var[4]*var[4]+var[5]*var[5]);
  u=0;
  for(fItr = fSets.begin(); fItr!=fItrE; fItr++)
    {
    double test = fItr->first->EvalPar(0,var);
    if(test>0.5)  
      {
      cnt++;
      NewGeo+= Form("matrix sphM%d  %lf  %lf %lf 1000 1000 1000\n", cnt, var[0],var[1],var[2]); 
      NewGeo+= Form("volume sphV%d  m%d   Sphere 0|%lf|0|180|0|360 sphM%d  0\n",  cnt, u, fSize, cnt);
      vlist+= Form("|sphV%d",cnt);
      break;
      }
    u++;
    }
  }
NewGeo+= Form("geometry Geo  %lf %lf %lf  %s\n", X, Y, Z, vlist.c_str());
f.close();
std::istringstream g(NewGeo.c_str());
if(fGeo) delete fGeo;
fGeo = new Geometry(g);
fGeo->Draw();
}

/////////////////////////////////////////////////////////////////
void Animator::MakeCut(std::string& cut)
/////////////////////////////////////////////////////////////////
{
std::string test[7]={"[x]","[y]","[z]","[vz]","[vy]","[vx]","[v]"};
std::string sbst[7]={"[0]","[1]","[2]","[3]","[4]","[5]","[6]"};
for(int i=0; i<7;i++)
  {
  int pos = 0;
  int n   = cut.length();
  while(pos<n&& pos>=0)
    {
    pos = cut.find(test[i],pos);
    if(pos<n && pos>=0) cut.replace(pos,test[i].length(),sbst[i]);
    n = cut.length();
    }
  }
}

///////////////////////////////////////////////////////
void Animator::ReadCuts(ParameterReader& pr)
///////////////////////////////////////////////////////
{
for(pr.itrInit("animset");!pr.itrEnd("animset");pr.itrNext("animset"))
  {
  TString name = pr.itrName("animset").c_str();
  if(!name.Contains("Animate.")) continue;
  std::pair<TFormula*, int> tmp;

  std::string def = pr.GetParameter<std::string>("animset","","def");
  tmp.second      = pr.GetParameter<int>("animset","","color");
  MakeCut(def);
  tmp.first = new TFormula(name.Data(), def.c_str());
  fSets.push_back(tmp);
  }
fItrE =  fSets.end();
fMark = new TPolyMarker3D*[fSets.size()];
for(unsigned int i=0; i<fSets.size();i++) fMark[i]=0;
}
