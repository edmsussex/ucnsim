/*
======================================================================
STARucn MC software
File : Configurable.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class Configurable                                       //
//                                                          //
// Abstract class for configurable plugins                  //
//                                                          //
//                                                          //
//////////////////////////////////////////////////////////////

#include "Configurable.h"
#include <iostream>
#include <fstream>

std::string Configurable::TypeFile = "scripts/parameters.typ";
std::string Configurable::fExampleScript = "";

////////////////////////////////////////////////
void Configurable::DefaultScript(std::string& str, const char* file)
////////////////////////////////////////////////
{
if(str=="") return;
if(file)
  {
  std::ofstream F(file);
  F << str << std::endl;
  }
else std::cout << str << std::endl;
} 




