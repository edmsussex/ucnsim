/*
======================================================================
STARucn MC software
File : InteractingMaterial.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/

//////////////////////////////////////////////////////////////
//                                                          //
// Class InteractingMaterial                                //
//                                                          //
// Simple container class for material properties           //
//   store :                                                //
//      * Decay constant     : double Lambda()              //
//      * FermiPotential     : double Fermi()               //
//      * Absorbtion coeff   : double Eta()                 //
//      * Diffusion fraction : double D()                   //
//                                                          //
//                                                          //
//////////////////////////////////////////////////////////////

#include "InteractingMaterial.h"
#include <iostream>

std::set<std::string, materialunsorter> InteractingMaterial::fPropertiesCatalog;
table* materialunsorter::fTable = 0;

InteractingMaterial::InteractingMaterial()
{
InteractingMaterial(-1,0,0,0);
}

InteractingMaterial::InteractingMaterial(double lifetime, double fermi, double eta, double d)
{
if(!materialunsorter::fTable)
  {
  materialunsorter::fTable = new table;
  materialunsorter::fTable->first=0;
  materialunsorter::fTable->second.clear();
  }
fProperties=0;
SetProperty("Lambda", lifetime==0 ? -1 :1/lifetime);
SetProperty("Fermi" , fermi);
SetProperty("Eta"   , eta);
SetProperty("D"     , d);
}

void InteractingMaterial::SetProperty(std::string name, double value)  
{
fPropertiesMap[name]=value; 
fPropertiesCatalog.insert(name);
}

void InteractingMaterial::CopyMap()
{
const int N = fPropertiesCatalog.size();
fProperties = new double[N];
std::set<std::string>::iterator itr = fPropertiesCatalog.begin();
std::set<std::string>::iterator itrE= fPropertiesCatalog.end();
int u=0;
for(;itr!=itrE;itr++) fProperties[u++] = fPropertiesMap[*itr];
}
