#include <string>
#include "Field.h"
#undef DEBUG
#undef DEBUG2

bool Field::Contains(Vector3L point)
{
	long double x, y, z, ox,oy,oz;
	
	return true; /* Remove at some point when fields boundaries have been implemented */
	x = point.x();
	y = point.y();
	z= point.z();
	
	ox = origin.x();
	oy = origin.y();
	oz = origin.z();
	return  (ox - halfX < x) && (x < ox + halfX) &&  (oy - halfY < y) && (y < oy + halfY) &&
			 (oz - halfZ < x) && (z < oz + halfZ);
}

bool Field::Contains(Vector3L point, double time)
{
	if (UseFieldBoundaries)
	{
		if (time < EndTime && time > StartTime){
			#ifdef DEBUG2
				std::cout << "within time boundaries - time = " << time << std::endl;
			#endif
			return true;
		}
		else {
			#ifdef DEBUG2
				std::cout << "outside time boundaries - time = " << time << std::endl;
			#endif
			return false;
		}
	}
	else
		return true;
}

void Field::SetFieldName(std::string& s)
{
	#ifdef DEBUG
		std::cout << "Old Field name is " << fieldName << std::endl;
		std::cout << "Setting field name = " << s << std::endl;
	#endif
	fieldName = s;
	#ifdef DEBUG
		std::cout << "Field name set" << std::endl;
	#endif
}

void Field::Configure(const char * file)
{
	std::string s;
	if(file!=0) SetParameterFile(file);
	
	std::cout << "Looking for " << fieldName + ".use_field_boundaries" << " in config file..." << std::endl;

	s = fieldName + ".use_field_boundaries";
	UseFieldBoundaries = fParam.GetBool(s.c_str());
	s = fieldName + ".start_time";
	StartTime = fParam.GetDouble(s.c_str());
	s = fieldName + ".end_time";
	EndTime = fParam.GetDouble(s.c_str());

	if (UseFieldBoundaries)
		std::cout << "Setting up Field Boundaries (time only) on " << fieldName << ", start time = " << StartTime << ", end time = " << EndTime << std::endl;
	
}

Field::Field()
{
}

Field::~Field()
{
}
