/*
======================================================================
STARucn MC software
File : Geometry.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class Geometry                                           //
//                                                          //
// Descritpion of detector geometry and materials           //
// based on ROOT TGeoVolume/TGeoManager                     //
//                                                          //
//                                                          //
//////////////////////////////////////////////////////////////

#include "ParameterReader.h"
#include "Geometry.h"
#include "Configurable.h"

#include "TGeoMaterial.h"
#include "TGeoMedium.h"
#include "TGeoCompositeShape.h"
#include "TGeoManager.h"
#include "TLine.h"
#include "TRandom3.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

/////////////////////////////////////////////////
Geometry::Geometry(const char* geometryfile) : TGeoManager("Geometry", "Geometry")
/////////////////////////////////////////////////
{
fEmpty = new TGeoMaterial("Nothing",0,0,0);
fMed   = new TGeoMedium("MED",1,fEmpty);
fCurrent = 0;
fCount=0;
fNodes=0;

ParameterReader pr;
pr.AddTypeFile(Configurable::TypeFile.c_str());
pr.AddParameterFile(geometryfile);
fInput = geometryfile; 
Configure(pr);
}


/////////////////////////////////////////////////
Geometry::Geometry(std::istream& geometryfile) : TGeoManager("Geometry", "Geometry")
/////////////////////////////////////////////////
{
fEmpty = new TGeoMaterial("Nothing",0,0,0);
fMed   = new TGeoMedium("MED",1,fEmpty);
fCurrent = 0;
fCount=0;
fNodes=0;

ParameterReader pr;
pr.AddTypeFile(Configurable::TypeFile.c_str());
pr.AddParameterFile(geometryfile);
fInput = ""; 
Configure(pr);
}

/////////////////////////////////////////////////
void Geometry::Configure(ParameterReader& pr) 
/////////////////////////////////////////////////
{
// material legend
fEmpty->SetTransparency(pr.GetInt("Transparency"));

// smearing parameters...

double size[3];
pr.itrInit("geometry");
pr.GetParameter(size[0],"geometry","","sizeX");
pr.GetParameter(size[1],"geometry","","sizeY");
pr.GetParameter(size[2],"geometry","","sizeZ");
//fMed = 0;
fWorld = this->MakeBox("World",fMed,size[0],size[1],size[2]);
fWorld->SetFillColor(0);
//this->SetTopVisible();
std::map<std::string, int> colors;
for(pr.itrInit("material");!pr.itrEnd("material"); pr.itrNext("material"))
  {
  double fermi     ; pr.GetParameter(fermi    ,"material","","fermi"    );
  double diffusion ; pr.GetParameter(diffusion,"material","","diffusion");
  double eta       ; pr.GetParameter(eta      ,"material","","eta"      );
  double lifetime  ; pr.GetParameter(lifetime ,"material","","lifetime" );
  int    color     ; pr.GetParameter(color    ,"material","","color"    );
//   std::cout << "material " << pr.itrName("material") << "  " << diffusion << std::endl;

  materials[pr.itrName("material")] = new InteractingMaterial(lifetime, fermi, eta, diffusion);
  colors[pr.itrName("material")] = color;
  }

// extra material parameters
std::vector<std::string> tmp;
for(pr.itrInit("materialproperty");!pr.itrEnd("materialproperty"); pr.itrNext("materialproperty"))
  {
  std::string name = pr.itrName("materialproperty");
  ParameterReader::ReadString(tmp,name,'.');
  if(tmp.size()!=2) continue;
  if(!materials[tmp[0]]) continue;
  materials[tmp[0]]->SetProperty(tmp[1], pr.GetParameter<double>("materialproperty","","property"));
  }
// freeze materials
std::map<std::string, InteractingMaterial*>::iterator itr  = materials.begin(); 
std::map<std::string, InteractingMaterial*>::iterator itrE = materials.end();
for(;itr!=itrE;itr++) itr->second->CopyMap(); 

// transformation matrices
for(pr.itrInit("matrix");!pr.itrEnd("matrix"); pr.itrNext("matrix"))
  {
  double tx;   pr.GetParameter(tx,"matrix","","transX");
  double ty;   pr.GetParameter(ty,"matrix","","transY");
  double tz;   pr.GetParameter(tz,"matrix","","transZ");  
  double rx;   pr.GetParameter(rx,"matrix","","rotX");
  double ry;   pr.GetParameter(ry,"matrix","","rotY");
  double rz;   pr.GetParameter(rz,"matrix","","rotZ");  
  TGeoTranslation *t1 = new TGeoTranslation(tx,ty,tz);

  std::string mname = pr.itrName("matrix");
  if(fabs(rx)<360 && fabs(ry)<360 && fabs(rz)<360 )
    {    
    TGeoRotation *r1 = new TGeoRotation("", rx, ry, rz);
    TGeoCombiTrans* c1 = new TGeoCombiTrans( *t1,*r1);  
    matrices[mname] = c1;
    }
  else matrices[mname] = t1;
  matrices[mname]->SetName(mname.c_str());
  matrices[mname]->RegisterYourself();
//  std::cout << "Adding matrix : " << mname << std::endl;
  }  

// All volumes
std::string vol;  pr.GetParameter(vol,"geometry","","volumes");
std::vector<std::string> volumes;
if(vol[0]=='|') vol=vol.substr(1,vol.size());
//std::cout << "List of volumes : " << vol << std::endl;
ParameterReader::ReadString(volumes,vol);
//add volumes
double* PARS = new double[20];

for(pr.itrInit("volume");!pr.itrEnd("volume"); pr.itrNext("volume"))
  {
  std::string material; pr.GetParameter(material,"volume","","material");
  std::string type;     pr.GetParameter(type,"volume","","type");
  std::string coord;    pr.GetParameter(coord,"volume","","coord");
  std::string matrix;   pr.GetParameter(matrix,"volume","","matrix");
  int    active;        pr.GetParameter(active,"volume","","active");
  std::string vname = pr.itrName("volume");
  bool toDeclare = std::find(volumes.begin(), volumes.end(), vname)!=volumes.end();
  std::vector<std::string> pars;
  ParameterReader::ReadString(pars,coord);
  for(unsigned int i=0; i<pars.size();++i)  PARS[i] = ParameterReader::Parse<double>(pars[i]);
  this->AddVolume(vname, type, PARS, matrices[matrix], materials[material], active, colors[material] ,toDeclare  );
  } 

for(pr.itrInit("composite");!pr.itrEnd("composite"); pr.itrNext("composite"))
  {
  std::string material; pr.GetParameter(material,"composite","","material");
  std::string def;      pr.GetParameter(def,"composite","","definition");
  std::string matrix;   pr.GetParameter(matrix,"composite","","matrix");
  int   active;        pr.GetParameter(active,"composite","","active");
  std::string vname = pr.itrName("composite");
  bool toDeclare = std::find(volumes.begin(), volumes.end(), vname)!=volumes.end();
  this->AddCompositeVolume(vname, def, matrices[matrix], materials[material], active, colors[material] ,toDeclare);
  }
//////////////////////////////////////////////////////////////////////////////////////////
this->SetTopVolume(fWorld);
this->CloseGeometry();
fNodes = fWorld->GetNodes();
kNodes = fNodes->GetEntries();
}  
  
///////////////////////////////////////////////// 
Geometry::~Geometry()
/////////////////////////////////////////////////
{
// // std::map<std::string, InteractingMaterial*>::iterator itr1 = materials.begin();
// // std::map<std::string, InteractingMaterial*>::iterator itr1E= materials.end();
// // for(;itr1!=itr1E;itr1++)
// //   {
// //   delete itr1->second;
// //   itr1->second = 0;
// //   }
// // 
// // std::map<std::string, TGeoMatrix*>::iterator  itr2 =  matrices.begin();
// // std::map<std::string, TGeoMatrix*>::iterator  itr2E=  matrices.end();
// // 
// // for(;itr2!=itr2E;itr2++)
// //   {
// //   delete itr2->second;
// //   itr2->second = 0;
// //   }
// // 

}
/////////////////////////////////////////////////  
void Geometry::AddCompositeVolume(std::string name, std::string& definition, TGeoMatrix* mov, InteractingMaterial* mat, int active, int color, bool declare)
/////////////////////////////////////////////////
{
TGeoCompositeShape *cs = new TGeoCompositeShape(name.c_str(), definition.c_str());
TGeoVolume *v = new TGeoVolume(name.c_str(),cs);

v->SetFillColor(color);
v->SetLineColor(color);
if(declare)
  {
  fWorld->AddNode(v, 1, mov);
  fMaterials[v] = mat;
  fActive[v] = active;
  fVolumes.push_back(v);
  }

}  


/////////////////////////////////////////////////  
void Geometry::AddVolume(std::string name, std::string& type, double* pars, TGeoMatrix* mov, InteractingMaterial* mat, int active, int color, bool declare)
/////////////////////////////////////////////////
{
TGeoVolume * v = 0;
//std::cout << "Adding volume " << name << "   " << type << "  " << mat << std::endl;
if(type=="Arb8")       v = this->MakeArb8(name.c_str(), fMed, pars[0],pars+1);
if(type=="Box")        v = this->MakeBox(name.c_str(), fMed, pars[0],pars[1],pars[2]);
if(type=="Cone")       v = this->MakeCone(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4]);
if(type=="Cons")       v = this->MakeCons(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4],pars[5],pars[6]);
if(type=="Ctub")       v = this->MakeCtub(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4],pars[5],pars[6],pars[7],pars[8],pars[9],pars[10]);

if(type=="Eltu")       v = this->MakeEltu(name.c_str(), fMed, pars[0],pars[1],pars[2]);
if(type=="Gtra")       v = this->MakeGtra(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4],pars[5],pars[6],pars[7],pars[8],pars[9],pars[10],pars[12]);

if(type=="Hype")       v = this->MakeHype(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4]);
if(type=="Para")       v = this->MakePara(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4],pars[5]);
if(type=="Paraboloid") v = this->MakeParaboloid(name.c_str(), fMed, pars[0],pars[1],pars[2]);
if(type=="Pcon")       v = this->MakePcon(name.c_str(), fMed, pars[0],pars[1],int(pars[2]));
if(type=="Pgon")       v = this->MakePgon(name.c_str(), fMed, pars[0],pars[1],int(pars[2]),int(pars[3]));
if(type=="Sphere")     v = this->MakeSphere(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4],pars[5]);
if(type=="Toru")       v = this->MakeTorus(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4]);
if(type=="Trap")       v = this->MakeTrap(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4],pars[5],pars[6],pars[7],pars[8],pars[9],pars[10]);
if(type=="Trd1")       v = this->MakeTrd1(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3]);
if(type=="Trd2")       v = this->MakeTrd2(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4]);
if(type=="Tube")       v = this->MakeTube(name.c_str(), fMed, pars[0],pars[1],pars[2]);
if(type=="Tubs")       v = this->MakeTubs(name.c_str(), fMed, pars[0],pars[1],pars[2],pars[3],pars[4]);

if(!v) return;
v->SetFillColor(color);
v->SetLineColor(color);
if(declare)
  {
  fWorld->AddNode(v, 1, mov);
  fMaterials[v] = mat;
  fActive[v] = active;
  fVolumes.push_back(v);
  }
}  

/////////////////////////////////////////////////
InteractingMaterial* Geometry::GetMaterial()
/////////////////////////////////////////////////
{
if(fCurrent) return fMaterials[fCurrent->GetVolume()];
return 0;
}

/////////////////////////////////////////////////
InteractingMaterial* Geometry::GetMaterial(TGeoVolume* v)
/////////////////////////////////////////////////
{
if(v) return fMaterials[v];
return 0;
}

/////////////////////////////////////////////////
int Geometry::SetCurrent(double* pos)
/////////////////////////////////////////////////
{
// Find in which volume a point belongs
// if none active is void (infinite propagation)
fCurrent = 0;
double localpos[3];
kNodes = fWorld->GetNodes()->GetEntriesFast();
for(int i=0; i<kNodes;++i)  
  {
  TGeoNode* node = static_cast<TGeoNode*>(fWorld->GetNodes()->At(i));
  node->MasterToLocal(pos, localpos);
  if(node->GetVolume()->Contains(localpos))
    {
    fCurrent = node;
    return fActive[node->GetVolume()];
    }
  }
return 0;
}

/////////////////////////////////////////////////
TGeoVolume* Geometry::FindVolume(double* pos)
/////////////////////////////////////////////////
{
TGeoVolume* res = 0;
double  tmp[3];
for(int i=0; i<kNodes;++i)
  {
  TGeoNode* node = (TGeoNode*) fNodes->At(i);
  node->MasterToLocal(pos, tmp);
//  std::cout << node->GetVolume()->GetName() << "  " << node->GetVolume()->Contains(tmp) << std::endl;
  if(node->GetVolume()->Contains(tmp)) 
    {
    res = node->GetVolume();
    break;
    }
  }
return res;
}

