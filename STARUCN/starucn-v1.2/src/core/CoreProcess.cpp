/*
======================================================================
STARucn MC software
File : CoreProcess.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/

#include "CoreProcess.h"

#include "Geometry.h"

#include "BaseGenerator.h"
#include "BasePropagator.h"
#include "BaseInteraction.h"
#include "BaseOutputInterface.h"

#include "TClass.h"

#include <iostream>

CoreProcess* CoreProcess::fInstance = 0;

///////////////////////////////////////////////////////
CoreProcess::CoreProcess()
///////////////////////////////////////////////////////
{
fGeo        = 0;
fGenerator  = 0;
fPropagator = 0;
fInteraction= 0;
fOutputInterface = 0;
RootFile = 0;
}

///////////////////////////////////////////////////////
CoreProcess::~CoreProcess()
///////////////////////////////////////////////////////
{
std::cout << "Running CoreProcess Deconstructor" << std::endl;
if(fGeo)             delete fGeo;             fGeo=0;
std::cout << "Deleted fGeo" << std::endl;
if(fGenerator)       delete fGenerator;       fGenerator=0;
std::cout << "Deleted fGenerator" << std::endl;
if(fPropagator)      delete fPropagator;      fPropagator=0;
std::cout << "Deleted fPropagator" << std::endl;
if(fInteraction)     delete fInteraction;     fInteraction=0;
std::cout << "Deleted fInteraction" << std::endl;
if(fOutputInterface) delete fOutputInterface; fOutputInterface=0;
std::cout << "Deleted fOutputInterface" << std::endl;
if(RootFile)         RootFile->Close();          RootFile=0;
std::cout << "Closed Root File" << std::endl << "Core Process Deconstructor complete" << std::endl;
}

///////////////////////////////////////////////////////
CoreProcess* CoreProcess::GetInstance()
///////////////////////////////////////////////////////
{
if(!fInstance) fInstance = new CoreProcess(); 
return fInstance;
}
  
///////////////////////////////////////////////////////
void CoreProcess::SetGeometry       (std::string& config)
///////////////////////////////////////////////////////
{
fGeo = new Geometry(config.c_str());
}

///////////////////////////////////////////////////////
void CoreProcess::SetGenerator      (std::string& type, std::string& config)
///////////////////////////////////////////////////////
{
fGenerator = static_cast<BaseGenerator*>(TClass::GetClass(TString(type.c_str()))->New());
fGenerator->Configure(config.c_str());
}

///////////////////////////////////////////////////////
void CoreProcess::SetPropagator     (std::string& type, std::string& config)
///////////////////////////////////////////////////////
{
fPropagator = static_cast<BasePropagator*>(TClass::GetClass(TString(type.c_str()))->New());
fPropagator->Configure(config.c_str());
}

///////////////////////////////////////////////////////
void CoreProcess::SetInteraction    (std::string& type, std::string& config)
///////////////////////////////////////////////////////
{
fInteraction = static_cast<BaseInteraction*>(TClass::GetClass(TString(type.c_str()))->New());
fInteraction->Configure(config.c_str());
}

///////////////////////////////////////////////////////
void CoreProcess::SetOutputInterface(std::string& type, std::string& config)
///////////////////////////////////////////////////////
{
fOutputInterface = static_cast<BaseOutputInterface*>(TClass::GetClass(TString(type.c_str()))->New());
fOutputInterface->Configure(config.c_str());
}

void CoreProcess::OpenRootFile(std::string filename){
	if (!RootFile)
		RootFile = new TFile(filename.c_str(),"update");
}
