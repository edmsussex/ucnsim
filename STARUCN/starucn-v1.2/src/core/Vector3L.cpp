#include <cmath>
#include <iomanip>
#include "Vector3L.h"

Vector3L::Vector3L(long double x, long double y, long double z)
{
	elements[0] = x;
	elements[1] = y;
	elements[2] = z;
}

Vector3L::Vector3L()
{
	elements[0] = 0;
	elements[1] = 0;
	elements[2] = 0;
}

Vector3L::~Vector3L()
{
}

long double Vector3L::x() const
{
	return elements[0];
}

long double Vector3L::y() const 
{
	return elements[1];
}


long double Vector3L::z() const 
{
	return elements[2];
}

Vector3L Vector3L::Cross(const Vector3L& v) const
{
long double x,y,z;
	x = elements[1]*v.elements[2] - elements[2]*v.elements[1];
	y = elements[2]*v.elements[0] - elements[0]*v.elements[2];
	z = elements[0]*v.elements[1] - elements[1]*v.elements[0];
	return Vector3L(x,y,z);
}


Vector3L Vector3L::ProjectOnPlane(const Vector3L& t, const Vector3L &u) const
{
	long double d1 = elements[0]*t.elements[0] + elements[1]*t.elements[1] + elements[2]*t.elements[2];
	long double d2 = elements[0]*u.elements[0] + elements[1]*u.elements[1] + elements[2]*u.elements[2];
	
	long double x = d1 * t.elements[0] + d2 * u.elements[0];
	long double y = d1 * t.elements[1] + d2 * u.elements[1];
	long double z = d1 * t.elements[2] + d2 * u.elements[2];
	
	return Vector3L(x,y,z);
}


long double Vector3L::Mag() const
{
	return sqrtl(elements[0]*elements[0] + elements[1]*elements[1] +elements[2]*elements[2]);
}

long double Vector3L::Mag2() const
{
	return elements[0]*elements[0] + elements[1]*elements[1]  +elements[2]*elements[2];
}

long double Vector3L::Dot(const Vector3L& v) const
{
	return elements[0]*v.elements[0] + elements[1]*v.elements[1] + elements[2]*v.elements[2];
}


Vector3L&	Vector3L::operator+=(const Vector3L& v)
{
	
	elements[0] += v.elements[0];
	elements[1] += v.elements[1];
	elements[2] += v.elements[2];
	return *this;
}


Vector3L&	Vector3L::operator-=(const Vector3L& v)
{
	elements[0] -= v.elements[0];
	elements[1] -= v.elements[1];
	elements[2] -= v.elements[2];
	return *this;
}


Vector3L&	Vector3L::operator*=(const long double a)
{
	elements[0] *= a;
	elements[1] *= a;
	elements[2] *= a;
	return *this;
}

Vector3L&	Vector3L::operator/=(const long double a)
{
	elements[0] /= a;
	elements[1] /= a;
	elements[2] /= a;
	return *this;
}



Vector3L	operator+(const Vector3L& a, const Vector3L& b)
{
	return Vector3L(a.x()+b.x(),a.y()+b.y(),a.z()+b.z());
}


Vector3L	operator-(const Vector3L& a, const Vector3L& b)
{
	return Vector3L(a.x()-b.x(),a.y()-b.y(),a.z()-b.z());
}
/*
Vector3L Vector3L::operator*(long double a){
	elements[0] *= a;
	elements[1] *= a;
	elements[2] *= a;
	return *this;
}

Vector3L Vector3L::operator/(long double a){
	elements[0] /= a;
	elements[1] /= a;
	elements[2] /= a;
	return *this;
}
*/

Vector3L	operator*(const long double a, const Vector3L& b)
{
	return Vector3L(a*b.x(),a*b.y(),a*b.z());
}

Vector3L	operator*(const Vector3L& b, const long double a)
{
	return Vector3L(a*b.x(),a*b.y(),a*b.z());
}

Vector3L	operator/(const Vector3L& a,const long double b)
{
	return Vector3L(a.x()/b,a.y()/b,a.z()/b);
}


long double Vector3L::AngleBetween(const Vector3L &v) const
{
	long double dot = elements[0] *v.elements[0] +elements[1] *v.elements[1] + elements[2] *v.elements[2];
	long double l1, l2;
	l1 = elements[0]*elements[0] + elements[1]*elements[1] + elements[2]*elements[2];
	l2 = v.elements[0]*v.elements[0] + v.elements[1]*v.elements[1] + v.elements[2]*v.elements[2];
	long double anglebetween = acosl(dot/sqrtl(std::abs(l1*l2)));
	
	if (anglebetween != anglebetween) {
		//if this occurs, it is because anglebetween = nan. This should only happen if the argument is >1 or <-1, giving an imaginary result
		//this is obviously not correct, so we write it off as a rounding error.
		#ifdef DEBUG
		std::cout << std::setprecision(25) << "angle between " << *this << " and " << v << " returned nan. " << std::endl;
		std::cout << "a^2 = "<< l1 << ", b^2 = " << l2 << " a dot b = " << dot << std::endl;
		std::cout << "argument of arccos = " << dot/sqrtl(std::abs(l1*l2)) << ", arccos returned " << anglebetween << std::endl;
		#endif
		anglebetween = 0;
	}
	
	return anglebetween;
}

void Vector3L::print(std::ostream &out)
{
    out << '(' << elements[0] << ',' << elements[1] << ',' << elements[2] << ')';   
}

std::ostream& operator<< (std::ostream &out, Vector3L &v)
{
	v.print(out);
    return out;
}


