/*
======================================================================
STARucn MC software
File : BasePropagator.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "BasePropagator.h"
#include "CoreProcess.h"

////////////////////////////////////////////////////////////////////////
BasePropagator::BasePropagator()
////////////////////////////////////////////////////////////////////////
{
fCore = CoreProcess::GetInstance();
fNodes = fCore->fGeo->GetNodes();
kNodes = fNodes->GetEntriesFast();
}

////////////////////////////////////////////////////////////////////////
BasePropagator::~BasePropagator()
////////////////////////////////////////////////////////////////////////
{
}

////////////////////////////////////////////////////////////////////////
void BasePropagator::SetCurrentVolume(TGeoVolume* vol)
////////////////////////////////////////////////////////////////////////
{
fCurrentVolume = vol;
fCurrentShape  = 0;
fCurrentNode   = 0;
if(!fCurrentVolume) return;
fCurrentShape = fCurrentVolume->GetShape();
// looking for corresponding node

for(int i=0; i<kNodes;++i)
  {
  TGeoNode* tmp = (TGeoNode*) fNodes->At(i);
  if(tmp->GetVolume() == fCurrentVolume) 
    {
  fCurrentNode = tmp;
  break;
    }
  }
}


