#include "FieldCollection.h"

#undef DEBUG

FieldCollection::~FieldCollection()
{
	for (std::vector<Field*>::iterator it = Fields.begin(); it != Fields.end(); ++it)
		delete *it;
}

void FieldCollection::AddField(Field *F)
{
	#ifdef DEBUG
		std::cout << "Field Collection is adding new field called " << F->fieldName << std::endl;
	#endif
	Fields.push_back(F);
}

Vector3L FieldCollection::GetField(Vector3L& point)
{
	Vector3L v(0,0,0), w;
	
	std::cout << "Warning! FieldCollection::GetField Time independant version was called!" << std::endl;
	
	#ifdef DEBUG
		std::cout << "FieldCollection is Getting Field at point " << point << " time independant" << std::endl;
	#endif
	
	for (std::vector<Field*>::iterator it = Fields.begin(); it != Fields.end(); ++it)
	{
		#ifdef DEBUG
			std::cout << "iterating through fields (time independent version)" << std::endl;
		#endif
		if ((*it)->Contains(point))
		{
			w = (*it)->GetField(point);
			v += w;
		}
	}
	return v;
}

Vector3L FieldCollection::GetField(Vector3L& point, double time)
{
	Vector3L v(0,0,0), w;
	
	#ifdef DEBUG
		std::cout << "FieldCollection is Getting Field at point " << point << " at time = " << time << std::endl;
	#endif
	
	for (std::vector<Field*>::iterator it = Fields.begin(); it != Fields.end(); ++it)
	{
		#ifdef DEBUG
			std::cout << "getting field called " << (*it)->fieldName << " time dependant version" << std::endl;
		#endif
		if ((*it)->Contains(point,time))
		{
			w = (*it)->GetField(point,time);
			v += w;
		}
	}
	return v;
}
