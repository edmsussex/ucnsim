/*
======================================================================
STARucn MC software
File : LoopCounter.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class LoopCounter                                        //
//                                                          //
// Display percentage of procesed events                    //
//                                                          //
// Author : B. Clement (bclement_AT_lpsc.in2p3.fr)             //
//                                                          //
//////////////////////////////////////////////////////////////

#include "LoopCounter.h"
#include "TString.h"
#include <iostream>

LoopCounter::LoopCounter(int max, int step, std::string s1, std::string s2, int cfg, int cbg)
{
fCurr=-1;
Init(max,step);
SetColors(cfg, cbg);
SetText(s1,s2);
Update();
}

LoopCounter::~LoopCounter()
{
}

void LoopCounter::Init(int max, int step)
{
kMax = max;
fStep = step<max ? step : max;
}
void LoopCounter::SetColors(int i, int j)
{
fColor   = TxtForm(1,i,j);
fRestore = TxtForm(); 
}

void LoopCounter::SetText(std::string txt, std::string fin)
{
fText = txt;
fFinal= fin;
}

void LoopCounter::Update()
{
fCurr++;
if(fCurr>kMax) std::cout << "\r\t" << fColor << fFinal << fRestore << "                              " << std::endl;  
else if(!(fCurr%(kMax/fStep))) 
  {
  std::string toto = Form("%.0lf", (100.*fCurr)/kMax);
  std::cout << "\r\t" << fColor << fText << "\t" << toto << "\% done\t\t\t";
  std::cout << fRestore << "\r" ; 
  std::cout.flush();     
  }
}
