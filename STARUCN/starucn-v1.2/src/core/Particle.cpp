/*
======================================================================
STARucn MC software
File : Particle.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "Particle.h"
#include "TMath.h"
#include "TRandom3.h"

Particle::Particle() : TObject()
{
for(int i=0; i<3;++i) position[i] = 0;
for(int i=0; i<3;++i) direction[i] = 0;
speed = 0;
time =0;
dead = false;
active = true;
kTran = 0;
kSpec = 0;
kDiff = 0;
}

Particle & Particle::operator=(Particle& par)
{
if(this!=&par)
  {
  this->Reset(par.GetPosition(), par.GetDirection());
  time   = par.GetTime();
  speed  = par.GetSpeed();
  active = par.Active();
  dead   = par.Dead();
  spin = par.spin;
  spinRevE = par.spinRevE;
  }
return *this;
} 

Particle::Particle(double v, double* pos) : TObject()
{
double phi   = Gen.Rndm()*TMath::Pi()*2;
dead = false;
active = true;
cth = Gen.Rndm()*2 - 1;
sth = sqrt(1-cth*cth);
cph = cos(phi);
sph = sin(phi);
direction[0] = sth*cph;
direction[1] = sth*sph;
direction[2] = cth;

speed = v;
time = 0;
for(int i=0; i<3;++i) position[i] = pos[i];
}

void Particle::SetDirection(double* dir)
{
double norm = 0;
for(int i=0; i<3;++i) norm += dir[i]*dir[i];
norm = 1./sqrt(norm);
for(int i=0; i<3;++i) direction[i] = dir[i]*norm;
cth = direction[2];
sth = sqrt(1-cth*cth);
cph = sth>0 ? direction[0]/sth : 0;
sph = sth>0 ? direction[1]/sth : 1;
}

void Particle::Scatter(double cdth, double dph)
{
double sdth = sqrt(1-cdth*cdth);
double cdph = cos(dph);
double sdph = sin(dph);
direction[0] = cth*cph*sdth*cdph - sph*sdth*sdph + sth*cph*cdth;
direction[1] = cth*sph*sdth*cdph + cph*sdth*sdph + sth*sph*cdth;
direction[2] = -sth*sdth*cdph + cth*cdth;
}


Vector3L Particle::GetVelocityV()
{
Vector3L v(direction[0], direction[1], direction[2]);
v *= this->speed;
return v;
}

Vector3L Particle::GetPositionV()
{
Vector3L v(position[0], position[1], position[2]);
return v;
}

void Particle::SetInit(){
  kV_init=this->GetSpeed();
  kVx_init=this->GetDirection(0);
  kVy_init=this->GetDirection(1);
  kVz_init=this->GetDirection(2);
  kX_init=this->GetPosition(0);
  kY_init=this->GetPosition(1);
  kZ_init=this->GetPosition(2);
  kT_init=this->GetTime();
  kDiff = 0;
  kSpec = 0;
  kTran = 0;
  spinInit=spin;
  spinRevEInit=spinRevE;
}

void Particle::SetParticleSeed(unsigned int seed) {
	ParticleSeed = seed;
	Gen.SetSeed(seed);
}
