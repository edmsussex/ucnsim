/*
 * Not the best code in this file... tidy up!
 */
#include <vector>

#include "TClass.h"

#include "FieldManager.h"

#undef DEBUG

static void _tokenize(std::vector<std::string> &L, std::string& s, char delim)
{
	const char *p, *q;	

	for (q = s.c_str();;)
	{
		for (; *q != 0 && *q != '\n' && (*q == delim); q++) ; 
		if (*q == 0)
			return;
		for (p = q; *p != 0 && *p != '\n' && *p != delim; p++) ; 
		L.push_back(std::string(q, p - q));
		q = p;
	}
}


void FieldManager::Configure(const char *file)
{
	
	ConfigureB(file);
	ConfigureE(file);
	ConfigureERev(file);
}

/*
 * hmmmmm -- sometimes copy and paste is faster?
 */
void FieldManager::ConfigureB(const char *file)
{
	std::vector<std::string> bfield_names;
	std::string sbnames;
	
	if(file!=0) 
		SetParameterFile(file);

	sbnames = fParam.GetString("FieldManager.BFields");
	_tokenize(bfield_names, sbnames,','); 
	if (verbosity)
		std::cout << "Configuring B fields:" << std::endl;
	for (unsigned int i = 0; i < bfield_names.size(); i++)
		{
			Field *f;
			std::vector<std::string> toks;
			std::string s = "FieldManager.Fields.", classstr, type, name;
			s += bfield_names[i];
			classstr = fParam.GetString(s);
			_tokenize(toks, classstr, ':');
			if (toks.size() != 2)
				{
					if (verbosity)
					{
						std::cout << "WARNING: skipping " << bfield_names[i]
							<< "-- no field structure specified!\n         Please define "
							<< s << " with the format <FieldName>:<FieldType> in script file: " 
							<<"\n         " << file << '.' <<  std::endl;
						continue; 
					}
				}
			name = "FieldManager."+toks[0];
			type = toks[1];
			f = static_cast<Field*>(TClass::GetClass(TString(type.c_str()))->New());
			std::cout << "Configuring "<< name << " with type " << type  << std::endl;
			f->SetFieldName(name);
			f->Configure(file);
			BFields.AddField(f);
		}
}

void FieldManager::ConfigureERev(const char *file)
{
	std::vector<std::string> bfield_names;
	std::string sbnames;
	
	if(file!=0) 
		SetParameterFile(file);

	sbnames = fParam.GetString("FieldManager.ERevFields");
	_tokenize(bfield_names, sbnames,','); 
	if (verbosity)
		std::cout << "Configuring reversed E fields:" << std::endl;
	for (unsigned int i = 0; i < bfield_names.size(); i++)
		{
			Field *f;
			std::vector<std::string> toks;
			std::string s = "FieldManager.Fields.", classstr, type, name;
			s += bfield_names[i];
			classstr = fParam.GetString(s);
			_tokenize(toks, classstr, ':');
			if (toks.size() != 2)
				{
					if (verbosity)
					{
						std::cout << "WARNING: skipping " << bfield_names[i]
							<< "-- no field structure specified!\n         Please define "
							<< s << " with the format <FieldName>:<FieldType> in script file: " 
							<<"\n         " << file << '.' <<  std::endl;
						continue; 
					}
				}
			name = "FieldManager."+toks[0];
			type = toks[1];
			f = static_cast<Field*>(TClass::GetClass(TString(type.c_str()))->New());
			std::cout << "Configuring "<< name << " with type " << type  << std::endl;
			f->SetFieldName(name);
			f->Configure(file);
			ERevFields.AddField(f);
		}
}

void FieldManager::ConfigureE(const char *file)
{
	std::vector<std::string> bfield_names;
	std::string sbnames;
	
	if(file!=0) 
		SetParameterFile(file);

	sbnames = fParam.GetString("FieldManager.EFields");
	_tokenize(bfield_names, sbnames,','); 
	if (verbosity)
		std::cout << "Configuring E fields:" << std::endl;
	for (unsigned int i = 0; i < bfield_names.size(); i++)
		{
			Field *f;
			std::vector<std::string> toks;
			std::string s = "FieldManager.Fields.", classstr, type, name;
			s += bfield_names[i];
			classstr = fParam.GetString(s);
			_tokenize(toks, classstr, ':');
			if (toks.size() != 2)
				{
					if (verbosity)
					{
						std::cout << "WARNING: skipping " << bfield_names[i]
							<< "-- no field structure specified!\n         Please define "
							<< s << " with the format <FieldName>:<FieldType> in script file: " 
							<<"\n         " << file << '.' <<  std::endl;
						continue; 
					}
				}
			name = "FieldManager."+toks[0];
			type = toks[1];
			f = static_cast<Field*>(TClass::GetClass(TString(type.c_str()))->New());
			std::cout << "Configuring "<< name << " with type " << type  << std::endl;
			f->SetFieldName(name);
			f->Configure(file);
			EFields.AddField(f);
		}
}

Vector3L FieldManager::GetEField(Vector3L& point)
{
	std::cout<< "Warning - using time independant version of FieldManager::GetEField" << std::endl;
	return EFields.GetField(point);
}


Vector3L FieldManager::GetERevField(Vector3L& point)
{
	std::cout<< "Warning - using time independant version of FieldManager::GetERevField" << std::endl;
	return ERevFields.GetField(point);
}



Vector3L FieldManager::GetEField(Vector3L& point, double time)
{
	return EFields.GetField(point, time);
}


Vector3L FieldManager::GetERevField(Vector3L& point, double time)
{
	return ERevFields.GetField(point, time);
}

Vector3L FieldManager::GetBField(Vector3L& point)
{
	std::cout<< "Warning - using time independant version of FieldManager::GetBField" << std::endl;
	#ifdef DEBUG
		std::cout << "FieldManager is Getting B at point " << point << "without specified time" << std::endl;
	#endif
	return BFields.GetField(point);
}

Vector3L FieldManager::GetBField(Vector3L& point, double time)
{
	#ifdef DEBUG
		std::cout << "FieldManager is Getting B at point " << point << " at time = " << time << std::endl;
	#endif
	return BFields.GetField(point, time);
}


FieldManager::FieldManager()
{
	verbosity = 1;
}

FieldManager::~FieldManager()
{
		
}
