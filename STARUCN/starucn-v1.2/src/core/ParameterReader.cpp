/*
======================================================================
STARucn MC software
File : ParameterReader.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/

#include "ParameterReader.h"

//////////////////////////////////////////////////
//
//              class Parameterreader
//		struct unsorter
//		class Parameter
//
//              Parameterreader.cpp
//
//////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

//=============================================
//=============================================                         
//					     //
//		PARAMETER TYPE		     //
//					     //
// a general type for int, string, double    //
// and bool				     //
//=============================================                         
//=============================================

//______________________________________________________________________________
/*
A general type for integer, string, double and bool.
______________________________________________________________________________*/


//ClassImp(Parameter); // Integrate this class into ROOT

// init static member of the unsorter structure
table* unsorter::tab = 0; 

//=============================================
Parameter::Parameter(const Parameter& param)
//=============================================
{
  // Copy constructor.
  ;

  // the map need Parameter::Parameter(const Parameter& param)
  // but getType and getValue need a Param not a const Param
  Parameter* a = const_cast<Parameter*>(&param);

  val = 0;
  type = -1;
  int u = a->getType();
  if(u==0)
    {
      int v = 0;
      a->getValue(v);
      setValue(v);
    }
  if(u==1)
    {
      std::string v = "";
      a->getValue(v);
      setValue(v);
    }
  if(u==2)
    {
      double v = 0.;
      a->getValue(v);
      setValue(v);
    }
  if(u==3)
    {
      bool v = 0;
      a->getValue(v);
      setValue(v);
    }
}

//=============================================
void Parameter::setValue(int t, std::string& name)
//=============================================
{
if     (t==0) setValue(ParameterReader::Parse<int>(name));// int 
else if(t==1) setValue(name);// string
else if(t==2) setValue(ParameterReader::Parse<double>(name)); // double
else if(t==3) setValue(ParameterReader::Parse<int>(name)==1);// bool  
}

//=============================================
void Parameter::setValue(int i)
  //=============================================
{
  // Set an integer parameter.
  this->Clear();
  type=0; val = static_cast<void*>(new int(i));
}
//=============================================
void Parameter::setValue(std::string i)
  //=============================================
{
  // Set a string parameter.
  this->Clear();
  type=1; val = static_cast<void*>(new std::string(i));
}
//=============================================
void Parameter::setValue(double i)
  //=============================================
{
  // Set a double parameter.
  this->Clear();
  type=2; val = static_cast<void*>(new double(i));
}
//=============================================
void Parameter::setValue(bool i)
  //=============================================
{
  // Set a boolean parameter.
  this->Clear();
  type=3; val = static_cast<void*>(new bool(i));
}

//=============================================
void Parameter::Clear()
  //=============================================
{
  // Delete the member pointer.
  if(type==0) {int* i = static_cast<int*>(val); delete i;}
  if(type==1) {std::string* i = static_cast<std::string*>(val); delete i;}
  if(type==2) {double* i = static_cast<double*>(val); delete i;}
  if(type==3) {bool* i = static_cast<bool*>(val); delete i;}
  type = -1;
}
//=============================================
Parameter Parameter::operator= (Parameter a)
  //=============================================
{
  // Operator =.
  int u = a.getType();
  if(u==0)
    {
      int v = 0;
      a.getValue(v);
      setValue(v);
    }
  if(u==1)
    {
      std::string v = "";
      a.getValue(v);
      setValue(v);
    }
  if(u==2)
    {
      double v = 0.;
      a.getValue(v);
      setValue(v);
    }
  if(u==3)
    {
      bool v = 0;
      a.getValue(v);
      setValue(v);
    }
  return a;
}


//=============================================
//=============================================                         
//					     //
//		PARAMETER READER	     //
//					     //
//=============================================                         
//=============================================

//______________________________________________________________________________
/*
Parameter reader.

See : 
 - AddTypeFile
 - AddParameterFile
 - GetParameter
 - itrNext

New parameter files could be added with 'include file' in your main parameters file.
______________________________________________________________________________*/

//=============================================
ParameterReader::ParameterReader()
  //=============================================
{
  // Constructor
}

//=============================================
ParameterReader::~ParameterReader()
  //=============================================
{
  // Destructor
}

//=============================================
void ParameterReader::AddTypeFile(const char* filename)
  //=============================================
{
  // Add a type file : a file that define types of objects used
  // by the parameter files. A type has a name and parameters (keys)
  // which can be int, string, double or boolean. This four types
  // are defined by default.
  //
  // The type name is following by its keys with format :
  // ' - key_type key_name' (1 parameter per line).
  // 
  // Black lines and lines begining with '#' are ignored.
  //
  // A parameter file looks like :
  //	# this is a comment
  //	point
  //	 - double X
  //	 - double Y
  //	 - int color
  //	 - string title
  //	 - bool draw
  // This create a type point wich has five keys.

  std::ifstream file(filename);
  if(file.eof() || file.bad() || !file.is_open()) {
	  std::cerr << "Error opening type file " << filename << std::endl;
	  return;
  }

  std::string string_buffer ;
  std::string name ;
  std::string type ;
  std::string line;

  // empty class
  std::string dummy_str = "";
  int         dummy_int = 0 ;
  double      dummy_dbl = 0.;
  bool        dummy_boo = false;

  // declare default base types
  m_Catalog["int"].first.first=1;
  m_Catalog["int"].first.second.clear();
  m_Catalog["int"].second.clear();
  unsorter::tab = &(m_Catalog["int"].first); // select the good (un)ordering catalog
  m_Catalog["int"].second[""].setValue(dummy_int);

  m_Catalog["string"].first.first=1;
  m_Catalog["string"].first.second.clear();
  m_Catalog["string"].second.clear();
  unsorter::tab = &(m_Catalog["string"].first); // select the good (un)ordering catalog
  m_Catalog["string"].second[""].setValue(dummy_str);

  m_Catalog["double"].first.first=1;
  m_Catalog["double"].first.second.clear();
  m_Catalog["double"].second.clear();
  unsorter::tab = &(m_Catalog["double"].first); // select the good (un)ordering catalog
  m_Catalog["double"].second[""].setValue(dummy_dbl);

  m_Catalog["bool"].first.first=1;
  m_Catalog["bool"].first.second.clear();
  m_Catalog["bool"].second.clear();
  unsorter::tab = &(m_Catalog["bool"].first); // select the good (un)ordering catalog
  m_Catalog["bool"].second[""].setValue(dummy_boo);

  while(file.good())
    {
      getline(file,line);
      if(line=="" || line[0]=='#') continue;   
      std::istringstream input_line(line);
      string_buffer="";
      input_line >> string_buffer;
      if (string_buffer=="") continue; // if the line begin with " " or "	"
      if(string_buffer!="-") 
	{
	  name = string_buffer; 
	  m_Catalog[name].first.first=1;	// the type is defined, this counter is incremented for each additional key
	  m_Catalog[name].first.second.clear();
	  m_Catalog[name].second.clear();
	  unsorter::tab = &(m_Catalog[name].first); // select the good (un)ordering catalog
	}
      else
	{
	  input_line >> type;
	  input_line >> string_buffer;
	  Parameter *p = &(m_Catalog[name].second[string_buffer]);
	  if(type=="int")    p->setValue(dummy_int);
	  if(type=="string") p->setValue(dummy_str);
	  if(type=="double") p->setValue(dummy_dbl);
	  if(type=="bool")   p->setValue(dummy_boo);
	}
    }
}

//=============================================
void ParameterReader::AddParameterFile(const char* filename)
  //=============================================
{
std::ifstream file(filename);
m_Constants["this"]    =filename;
m_Constants["thispath"]=GetPath(filename);
  if(file.eof() || file.bad() || !file.is_open()) {
	  std::cout << "Error opening Parameter File " << filename << std::endl;
	  return;
  }
this->AddParameterFile(file);
}

//=============================================
void ParameterReader::AddParameterFile(std::istream& file)
  //=============================================
{
  // Add a parameter file : a file that set parameters. Parameter 
  // types are defined with AddTypeFile
  //
  // Syntax :
  // type	name	key1	key2 ...  
  //
  // Black lines and lines begining with '#' are ignored.
  //
  // For example, to set parameters of a point (see AddTypeFile)
  // and an integer parameter which is the number of points :
  //	# comment:	name	X	Y	color	title	draw?
  //	point		P1	0.5	0.5	1	title1	1
  //	point		P2	0.3	0.8	1	title2	0
  //	point		P3	0.8	0.3	2	title3	1
  //	# how many points ?
  //	int NbOfPoint 3
  if(file.eof() || file.bad()) {
	  std::cout << "Error opening Parameter File " << std::endl;
	  return;
  }

  std::string string_buffer ;
  std::string name_buffer ;
  std::string line;

  bool inComment =false;
  while(file.good())
    {
      getline(file,line);
      if(line=="") continue; 
      SubstituteConstant(line);      
      std::istringstream input_line(line);
      input_line >> string_buffer;
      if(string_buffer[0]=='%' && string_buffer[1]=='%'&& string_buffer[2]=='%') // block comment
        {
        inComment = !inComment;
        continue;
        }
      else if(inComment) continue;
      else if(string_buffer[0]=='#') continue;
      else if(string_buffer=="include")
	      {
	      input_line >> string_buffer;
	      std::string current = m_Constants["this"];
        std::string currentp= m_Constants["thispath"];        
	      this->AddParameterFile(string_buffer.c_str());
	      m_Constants["this"]=current;
        m_Constants["thispath"]=currentp;         
	      }
      else if(string_buffer=="define") // constant definition ->Must be defined BEFORE reading  
  {
  input_line >> string_buffer;
  input_line >> name_buffer;
  m_Constants[string_buffer] = name_buffer;  
  }    
      else if(string_buffer==">>")
  {
   input_line >> string_buffer;
  if(string_buffer=="Catalog")   DisplayCatalog();
  if(string_buffer=="Variables") DisplayVariables();
  if(string_buffer=="Constants") DisplayConstants();


  }
      else 
	{
	  // check if the type exist within the catalog
	  std::string type = string_buffer;
	  int m = m_Catalog[type].first.first;
	  if(m==0) continue;
	  // get name of the type instance
	  input_line >> name_buffer;
	  // select ordering table
	  unsorter::tab = &(m_Catalog[type].first);
	  // loop on user type definition    
	  prtype::iterator itr  = m_Catalog[type].second.begin();
	  prtype::iterator itrE = m_Catalog[type].second.end();
	  for(;itr!=itrE; itr++)
	    {
      int typeidx =  (itr->second).getType();
      input_line >> string_buffer;
      m_Parameters[type][name_buffer][itr->first].setValue(typeidx, string_buffer);
	    }   
	  }
  }
}
  
//=============================================
int ParameterReader::GetParameter(int& i, std::string type, std::string name, std::string key)
  //=============================================
{
  // Get the integer key of the parameter. the variable i is set and is value
  // returned.
  //
  // Eg. (see AddParameterFile and AddTypeFile) :
  //	int val1, val2, dummy_int;
  //	ParameterReader.GetParameter(val1,"point","P1","color");
  //	val2 = ParameterReader.GetParameter(dummy_int,"point","P2","color");
  //	int n = ParameterReader.GetParameter(dummy_int,"int","NbOfPoint");
  int m = m_Catalog[type].first.first;
  if(m==0) return 0;
  // select ordering table
  unsorter::tab = &(m_Catalog[type].first);
  try {
	if(name!="") m_Parameters.at(type).at(name).at(key).getValue(i);
	else         m_Itr.at(type)->second.at(key).getValue(i);
  }
  catch (const std::out_of_range& oor) {
	  std::cerr << std::endl;
	  std::cerr << "Out of Range Error: " << oor.what() << std::endl;
	  std::cerr << "This occured while looking in parameter reader for: " << std::endl;
	  std::cerr << "Type: " << type;
	  std::cerr << ", Name: " << name;
	  std::cerr << ", Key: " << key << std::endl;
	  std::cerr << "Setting Parameter = 0" << std::endl << std::endl;
	  i = 0;
  }
  return i;
}

//=============================================
std::string ParameterReader::GetParameter(std::string& i, std::string type, std::string name, std::string key)
  //=============================================
{
  // Get the string key of the parameter. the variable i is set and is value
  // returned.
  //
  // Eg. (see AddParameterFile and AddTypeFile) :
  //	string val1, val2, dummy_string;
  //	ParameterReader.GetParameter(val1,"point","P1","title");
  //	val2 = ParameterReader.GetParameter(dummy_string,"point","P2","title");
  int m = m_Catalog[type].first.first;
  if(m==0) return "";
  // select ordering table
  unsorter::tab = &(m_Catalog[type].first);
  try {
	if(name!="") m_Parameters.at(type).at(name).at(key).getValue(i);
	else         m_Itr.at(type)->second.at(key).getValue(i);
  }
  catch (const std::out_of_range& oor) {
	  std::cerr << std::endl;
	  std::cerr << "Out of Range Error: " << oor.what() << std::endl;
	  std::cerr << "This occured while looking in parameter reader for: " << std::endl;
	  std::cerr << "Type: " << type;
	  std::cerr << ", Name: " << name;
	  std::cerr << ", Key: " << key << std::endl;
	  std::cerr << "Setting Parameter = \"\"" << std::endl;
	  i = "";
  }
  return i;
}

//=============================================
double      ParameterReader::GetParameter(double& i, std::string type, std::string name, std::string key)
  //=============================================
{
  // Get the double key of the parameter. the variable i is set and is value
  // returned.
  //
  // Eg. (see AddParameterFile and AddTypeFile) :
  //	double val1, val2, dummy_double;
  //	ParameterReader.GetParameter(val1,"point","P1","X");
  //	val2 = ParameterReader.GetParameter(dummy_double,"point","P2","Y");
  int m = m_Catalog[type].first.first;
  if(m==0) return 0.;
  // select ordering table
  unsorter::tab = &(m_Catalog[type].first);
  try {
	if(name!="") m_Parameters.at(type).at(name).at(key).getValue(i);
	else         m_Itr.at(type)->second.at(key).getValue(i);
  }
  catch (const std::out_of_range& oor) {
	  std::cerr << std::endl;
	  std::cerr << "Out of Range Error: " << oor.what() << std::endl;
	  std::cerr << "This occured while looking in parameter reader for: " << std::endl;
	  std::cerr << "Type: " << type;
	  std::cerr << ", Name: " << name;
	  std::cerr << ", Key: " << key << std::endl;
	  std::cerr << "Setting Parameter = 0." << std::endl;
	  i = 0.;
  }
  return i;
}

//=============================================
bool        ParameterReader::GetParameter(bool& i, std::string type, std::string name, std::string key)
  //=============================================
{
  // Get the bool key of the parameter. the variable i is set and is value
  // returned.
  //
  // Eg. (see AddParameterFile and AddTypeFile) :
  //	bool val1, val2, dummy_bool;
  //	ParameterReader.GetParameter(val1,"point","P1","draw");
  //	val2 = ParameterReader.GetParameter(dummy_bool,"point","P2","draw");
  int m = m_Catalog[type].first.first;
  if(m==0) return false;
  // select ordering table
  unsorter::tab = &(m_Catalog[type].first);
  try {
	if(name!="") m_Parameters.at(type).at(name).at(key).getValue(i);
	else         m_Itr.at(type)->second.at(key).getValue(i);
  }
  catch (const std::out_of_range& oor) {
	  std::cerr << std::endl;
	  std::cerr << "Out of Range Error: " << oor.what() << std::endl;
	  std::cerr << "This occured while looking in parameter reader for: " << std::endl;
	  std::cerr << "Type: " << type;
	  std::cerr << ", Name: " << name;
	  std::cerr << ", Key: " << key << std::endl;
	  std::cerr << "Setting Parameter = false" << std::endl;
	  i = false;
  }
  return i;

}

//=============================================
void ParameterReader::itrInit(std::string s)
  //=============================================
{
  // Initialize the member iterator for the parameter s.
  m_Itr[s] = m_Parameters[s].begin();
}

//=============================================
bool ParameterReader::itrEnd(std::string s)
  //=============================================
{
  // Is the member iterator for parameter s at the end of the list ?
  return (m_Itr[s] == m_Parameters[s].end());
}
//=============================================
void ParameterReader::itrNext(std::string s)
  //=============================================
{
  // Increment the member iterator.
  // To loop on a parameter, for exemple point (pr is the ParameterReader) :
  //	for(pr.itrInit("point"); !pr.itrEnd("point"); pr.itrNext("point")){
  m_Itr[s]++;
}

//=============================================
std::string ParameterReader::itrName(std::string s)
  //=============================================
{
  // Return the name of the current parameter pointed by the member iterator.
  return m_Itr[s]->first;
}

//=============================================
void ParameterReader::SubstituteConstant(std::string& line)
//=============================================
{
// // DisplayConstants();
std::string newline="";
int len = line.length();
bool foundcst=0;
std::string cstname="";
int cnt = 0;
for(int i=0; i<len;++i)
  {
  char c = line[i];
  if(!foundcst && c=='@' && line[i+1]=='{') // start
    {
    i++;
    cstname="";
    foundcst = true;
    cnt=0; // special counter for '{' caracters
    }
  else if(foundcst && c=='}' && cnt==0)  
    {
    SubstituteConstant(cstname);
    newline += m_Constants[cstname];// end of cst
    foundcst=false;
    } 
  else if(foundcst)
    {
    if(c=='@' && line[i+1]=='{') cnt++;
    if(c=='}') cnt--;
    cstname+=c;
    }
  else
    {
    newline+=c;
    }   
  }
// std::cout << "BEFORE : " << line << std::endl;  
line = newline;
// std::cout << "AFTER  : " << line << std::endl;  
}

//=============================================
void ParameterReader::ReadString(std::vector<std::string>& v, std::string s, char c)
  //=============================================
{
  // Decompose a string into substrings, eg. "a|b|c" -> {"a","b","c"}
  v.clear();
  int size = s.size();
  std::string res = "";
  for(int i=0; i<size;++i)
    {
      char ch = s[i];
      if(ch==c)
	{
	  v.push_back(res);
	  res = "";
	}
      else res += ch;
    }
  if(res!="") v.push_back(res);
}

//=============================================
std::string ParameterReader::Substitute(std::string s, char ci, char co)
  //=============================================
{
  // Substitute a char by another in string.
  int size = s.size();
  std::string res = "";
  for(int i=0; i<size;++i)
    {
      char ch = s[i];
      if(ch==ci) res += co;
      else res += ch;
    }
  return res;
}
   

//=============================================
void ParameterReader::DisplayCatalog()
  //=============================================
{
  // Display the type catalog.
  std::map< std::string, std::pair<table, prtype> >::iterator itr  = m_Catalog.begin();
  std::map< std::string, std::pair<table, prtype> >::iterator itrE = m_Catalog.end();

  for(;itr!=itrE;itr++)
    {
      prtype::iterator itr2 = (itr->second).second.begin();
      prtype::iterator itr2E= (itr->second).second.end();
      for(;itr2!=itr2E;itr2++)
	{
	  std::cout << itr->first << "   " << itr2->first << std::endl;
	}
    }
}

//=============================================
void ParameterReader::DisplayVariables()
  //=============================================
{
  // Display the existing parameters.
  std::map< std::string, std::map<std::string, prtype > >::iterator itr  = m_Parameters.begin();
  std::map< std::string, std::map<std::string, prtype > >::iterator itrE = m_Parameters.end();

  for(;itr!=itrE;itr++)
    {
      std::map<std::string, prtype >::iterator itr2 = (itr->second).begin();
      std::map<std::string, prtype >::iterator itr2E= (itr->second).end();
      for(;itr2!=itr2E;itr2++)
	{
	  std::cout << itr->first << "   " << itr2->first << std::endl;
	}
    }
}
   
//=============================================
void ParameterReader::DisplayConstants()
  //=============================================
{
  // Display the type catalog.
  std::map< std::string, std::string>::iterator itr  = m_Constants.begin();
  std::map< std::string, std::string>::iterator itrE = m_Constants.end();

  for(;itr!=itrE;itr++)
    {
    std::cout << itr->first << "   " << itr->second << std::endl;
    }
}
//=============================================
std::string ParameterReader::GetPath(std::string file)
//=============================================
{
int n = file.size();
for(int i=0; i<n;++i) if(file[n-i-1]=='/') return (file.substr(0,n-i-1)+"/");
return std::string(".");
}


// Parser of numerical expressions
//=============================================
template <class T> T  ParameterReader::Parse(std::string& expr, bool rem)
//=============================================
{
int n = expr.size();
int count = 0;
std::string st = expr;
T res = 0;
// Scan for isolated minus sign and remove spaces
char prev = ' ';   
std::string extra="";

if(rem)
  {
  for(int i=0;i<n;++i) 
    { 
    if(st[i]==' ') 
      {
      st = st.substr(0,i)+st.substr(i+1,n-i-1);
      i--;
      n = st.size();
      continue;
      } 
    else if(st[i]=='-') 
      {
      if(prev=='e') continue;
      if(prev==',' || prev=='*' || prev=='+' || prev=='/' || prev=='(' || prev==' ' ) extra = "";
      else extra = "+";  
      st = st.substr(0,i)+extra+"(-1)*"+st.substr(i+1,n-i-1);
      i += (extra+"(-1)*").size() -1;
      n = st.size();
      }  
    prev = st[i];
    }
  }
// Remove extra parenthesis
bool done = false;
while(!done) 
  {
  n = st.size();
  if(n==1) break;
  count = 0;
  for(int i=0;i<n;++i) 
    {
    if(st[i]=='(') count++;
    if(st[i]==')') count--;
    if(count==0) 
      {
      if(i==n-1) st = st.substr(1,n-2);
      else 
        {
        done = true; 
        break;
        }
      }
    }
  }
n = st.size();
count = 0;  
// Scan for binary operators
for(int i=0;i<n;++i) 
  {
  if(st[i]=='(') count++;
  else if(st[i]==')') count--;
  else if(count==0) 
    {
    if(st[i]=='+') 
      {
      std::string f1 = st.substr(0,i);
      std::string f2 = st.substr(i+1,n-i-1);
      return Parse<T>(f1,0)+Parse<T>(f2,0);
      }
    }
  }
count = 0;
for(int i=0;i<n;++i) 
  {
  if(st[i]=='(') count++;
  else if(st[i]==')') count--;  
  else if(count==0) 
    {
    if(st[i]=='*') 
      {
      std::string f1 = st.substr(0,i);
      std::string f2 = st.substr(i+1,n-i-1);
      return Parse<T>(f1,0)*Parse<T>(f2,0);
      }
    if(st[i]=='/') 
      {
      std::string f1 = st.substr(0,i);
      std::string f2 = st.substr(i+1,n-i-1);
      return Parse<T>(f1,0)/Parse<T>(f2,0);
      } 
    }
  }
// Scan for unary operators and var/const 
int start = -1;
int end   = 0;
count = 0;
char type = 0;
for(int i=0;i<n;++i) 
  { 
  if(start==-1) 
    {
    if(st[i]==' ') continue;
    else if(st[i]=='A' ||st[i]=='H') 
      {
      if(st[i+1]=='S') type= (st[i]=='H') ? 'X' : 'I';
      if(st[i+1]=='C') type= (st[i]=='H') ? 'Y' : 'J';
      if(st[i+1]=='T') type= (st[i]=='H') ? 'Z' : 'K';  
      i++;
      start = i+2;
      continue;
      }
    else if(st[i]=='E' || st[i]=='L' || st[i]=='P' || st[i]=='U' || st[i]=='S' || st[i]=='C' || st[i]=='T' || st[i]=='R') 
      {
      start = i+2;
      type = st[i];
      continue;
      }
    else if(st[i]=='-' || (st[i]>47 && st[i]<58)) 
      {
      start = i;
      type = '0';
      end = n;
      break;
      }
    }
  else if(type=='E' || type=='P' || type=='S' || type=='C' || type=='L' || type=='I' || type=='J' || type=='K' || type=='X' || type=='Y' || type=='Z' || type=='R') 
    {
    if(st[i]=='(') count++;
    else if(st[i]==')') count--; 
    if(count==0) 
      {
      end = i-1; 
      break;
      }
    }
  }
std::string arg = st.substr(start, end-start+1);
// Build unary operators and var/const   
if(type=='P') 
  {
  n = arg.size();
  std::string arg2 = "";
  std::string expo = "";
  for(int i=0; i<n; ++i) 
    {
    if(arg[i]==',') 
      {
      arg2 = arg.substr(0,i); 
      expo = arg.substr(i+1,n); 
      break;
      }
    }
  double f1 = Parse<T>(arg2,0);
  double f2 = Parse<T>(expo,0);
  return static_cast<T>((double)pow(f1,f2));
  }
else if(type=='E') return static_cast<T>(exp((double)Parse<T>(arg,0)));
else if(type=='L') return static_cast<T>(log((double)Parse<T>(arg,0)));
else if(type=='R') return static_cast<T>(sqrt((double)Parse<T>(arg,0)));
else if(type=='C') return static_cast<T>(cos(0.0174532925199432955*(double)Parse<T>(arg,0)));
else if(type=='S') return static_cast<T>(sin(0.0174532925199432955*(double)Parse<T>(arg,0)));
else if(type=='T') return static_cast<T>(tan(0.0174532925199432955*(double)Parse<T>(arg,0)));
else if(type=='I') return static_cast<T>(57.2957795130823229*asin((double)Parse<T>(arg,0)));
else if(type=='J') return static_cast<T>(57.2957795130823229*acos((double)Parse<T>(arg,0)));
else if(type=='K') return static_cast<T>(57.2957795130823229*atan((double)Parse<T>(arg,0)));
else if(type=='X') return static_cast<T>(sinh((double)Parse<T>(arg,0)));
else if(type=='Y') return static_cast<T>(cosh((double)Parse<T>(arg,0)));
else if(type=='Z') return static_cast<T>(tanh((double)Parse<T>(arg,0)));
else if(type=='0') return static_cast<T>(atof(arg.c_str()));
return res;
}


//

