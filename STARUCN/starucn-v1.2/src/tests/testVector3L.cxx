#include <iostream>
#include "Vector3L.h"

int main () {

	Vector3L A = Vector3L(1,2,3);
	Vector3L B = Vector3L(4,5,6);
	Vector3L C = A + B;
	Vector3L D = C;
	std::cout << "C = A + B = (1,2,3)+(4,5,6) - should be (5,7,9): " << C << std::endl;

	std::cout << "D = C should be (5,7,9): " << D << std::endl;
	
	D = D + A;
	
	std::cout << " D = D + A should be (6,9,12) " << D << std::endl;
	std::cout << "C Should still be (5,7,9) " << C << std::endl;
	return 0;

}
