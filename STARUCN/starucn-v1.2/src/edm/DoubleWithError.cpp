#include "DoubleWithError.h"

DoubleWithError::DoubleWithError() {
	number = 0;
	error = 0;
}

void DoubleWithError::print(std::ostream &out)
{
    out << number << "+/-" << error;
}

std::ostream& operator<< (std::ostream &out, DoubleWithError &d)
{
	d.print(out);
    return out;
}

