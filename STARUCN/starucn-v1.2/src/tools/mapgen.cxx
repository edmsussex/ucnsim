/* Nick Ayres June 2014
* na280@sussex.ac.uk 
* Generates field maps of constant field*/

#include <iostream>
#include <fstream>


int main()
{
	long double xfield, yfield, zfield;
	long double xmin, xmax, xstep;
	long double ymin, ymax, ystep;
	long double zmin, zmax, zstep;

	std::cout << "Enter Field in form F_x F_y F_z , each separated by whitespace:" << std::endl;
	std::cin >> xfield >> yfield >> zfield;

	std::cout << " Enter xmin xmax xstep" << std::endl;
	std::cin >> xmin >> xmax >> xstep;

	std::cout << "Enter ymin ymax ystep" << std::endl;
	std::cin >> ymin >> ymax >> ystep;

	std::cout << "Enter zmin zmax zstep" << std::endl;
	std::cin >> zmin >> zmax >> zstep;

	std::cout << "Creating mapgen.map..." << std::endl;

	std::ofstream mapfile;
	mapfile.open("mapgen.map",std::ios::out|std::ios::trunc); //creates or overwrites mapgen.map

	unsigned int numx = (xmax - xmin)/xstep;	//calculates number of steps - watch here if off-by-one error
	unsigned int numy = (ymax - ymin)/ystep;
	unsigned int numz = (zmax - zmin)/zstep;

	mapfile << numx << "\t" << numy << "\t" << numz << std::endl;

	for (unsigned int i = 0; i != numx; i++)
		for (unsigned int j = 0; j!= numy; j++)
			for (unsigned int k=0; k!= numz; k++)
				mapfile << xmin + i * xstep << "\t" << ymin + j * ystep << "\t" << zmin + k * zstep << "\t"	//writes one line to file
					<< xfield << "\t" << yfield << "\t" << zfield << std::endl;
	
		
	mapfile.close();
	std::cout << "File Created" << std::endl;
}
