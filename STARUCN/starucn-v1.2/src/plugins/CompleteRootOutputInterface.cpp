/*
======================================================================
STARucn MC software
File : CompleteRootOutputInterface.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class CompleteRootOutputInterface                        //
//                                                          //
// OutputInterface plugin for root file event output        //
//                                                          //
//                                                          //
//////////////////////////////////////////////////////////////

#include "CompleteRootOutputInterface.h"
#include "LoopCounter.h"
#include <algorithm>

////////////////////////////////////////////////////////////////////////
CompleteRootOutputInterface::CompleteRootOutputInterface() : BaseOutputInterface()
////////////////////////////////////////////////////////////////////////
{
LineRecord = 0;
fOutput = 0;
fTree = 0;
kTran=0;
kSpec=0;
kDiff=0;
MNEUTRON = 1.674927211e-027;
}

////////////////////////////////////////////////////////////////////////
CompleteRootOutputInterface::~CompleteRootOutputInterface() {;}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
void CompleteRootOutputInterface::Configure(const char* file)
////////////////////////////////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);

std::string outfile = fParam.GetString("CompleteRootOutputInterface.OutputFile");
LineRecord   = fParam.GetInt("CompleteRootOutputInterface.TrajectoryRecord");

std::string tmp = fParam.GetString("CompleteRootOutputInterface.TrajectoryRecordFinalVolume");
ParameterReader::ReadString(fFinalVolumes,tmp);
fOutput = new TFile(outfile.c_str(), "recreate");
fTree = new TNtupleD("starucn","starucn","id:nstep:NbSpecular:NbDiffuse:NbTransmitted:FinalStatus:x:y:z:vx:vy:vz:V:Ex:Ey:Ez:E:t:xinit:yinit:zinit:vxinit:vyinit:vzinit:Vinit:Exinit:Eyinit:Ezinit:Einit:Tinit:idLastVolume:idLastInterface");
if(LineRecord>0) {if(kId%LineRecord==0) {fLine3D.Reset(); Line3D = &fLine3D;}}
else {Line3D=0;}
std::string nor = LoopCounter::TxtForm(1,7,0);
std::string gre = LoopCounter::TxtForm(1,3,0);
std::string blu = LoopCounter::TxtForm(1,4,0);
std::cout << blu << "*********** CompleteRootOutputInterface, status documentation ***********" << nor << std::endl;
std::cout << gre << "      -1 : unknown error (should not happen)                             " << nor << std::endl;
std::cout << gre << "       0 : particle goes outside of defined volumes (should not happen)  " << nor << std::endl;
std::cout << gre << "       1 : particle decays in a volume                                   " << nor << std::endl;
std::cout << gre << "       2 : particle is absorbed on a surface                             " << nor << std::endl;
std::cout << gre << "       3 : particle is transmitted to null lifetime material             " << nor << std::endl;
std::cout << gre << "       4 : too many rebound                                              " << nor << std::endl;
std::cout << blu << "*************************************************************************" << nor << std::endl << std::endl;

}

////////////////////////////////////////////////////////////////////////
void CompleteRootOutputInterface::SaveStep(Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)
////////////////////////////////////////////////////////////////////////
{
vol       = 0;
previous  = 0;
interface = 0;
// only counting
if(sur==0) kTran++;
if(sur==2) kSpec++;
if(sur==3) kDiff++;               
kStep++;
// if(Line3D) fLine3D.SetNextPoint(N.GetPosition());
}


////////////////////////////////////////////////////////////////////////
void CompleteRootOutputInterface::BeginParticle (Particle& N)     
////////////////////////////////////////////////////////////////////////
{

  kV_init=N.GetSpeed();
  kVx_init=N.GetDirection(0);
  kVy_init=N.GetDirection(1);
  kVz_init=N.GetDirection(2);
  kX_init=N.GetPosition(0);
  kY_init=N.GetPosition(1);
  kZ_init=N.GetPosition(2);
  kT_init=N.GetTime();
}


////////////////////////////////////////////////////////////////////////
void CompleteRootOutputInterface::EndParticle (Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)     
////////////////////////////////////////////////////////////////////////
{
// previous  = 0;
// interface = 0;
if(N.Active())
  {
/*std::cout << "Active " << Line3D << std::endl;*/
if(Line3D&&interface) 
  {
  fLine3D.SetName(Form("Trajectory_%d",kId)); 
  fOutput->cd(); 
/*  std::cout << interface->GetName() << "  " << previous->GetName() << "  " << fFinalVolumes[0] << std::endl;*/
  if(std::find(fFinalVolumes.begin(), fFinalVolumes.end(), interface->GetName())!=fFinalVolumes.end() || fFinalVolumes.size()==0) fLine3D.Write(); 
  Line3D=0;
  }


int status = -1;
if(N.Dead())
  {
  if     (vol==2) status=0; // meet non existing volume : should never happen
  else if(vol==1) status=1; // decay
  else if(sur==1) status=2; // absorbtion
  else if(sur==4) status=3; // transmission to zero lifetime material
  else status = -1;// not possible
  }
else status = 4; // too many rebound...

double save_in_tree[32];
 int id1 = fCore->fGeo->GetVolumeId(previous);
 int id2 = fCore->fGeo->GetVolumeId(interface);

save_in_tree[0]=kId;
save_in_tree[1]=kStep;
save_in_tree[2]=kSpec;
save_in_tree[3]=kDiff;
save_in_tree[4]=kTran;
save_in_tree[5]=status;

save_in_tree[6]=N.GetPosition(0);
save_in_tree[7]=N.GetPosition(1);
save_in_tree[8]=N.GetPosition(2);

save_in_tree[9]=N.GetDirection(0);
save_in_tree[10]=N.GetDirection(1);
save_in_tree[11]=N.GetDirection(2);

save_in_tree[12]=N.GetSpeed();

save_in_tree[13]=0.5*pow(N.GetDirection(0)*N.GetSpeed()/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[14]=0.5*pow(N.GetDirection(1)*N.GetSpeed()/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[15]=0.5*pow(N.GetDirection(2)*N.GetSpeed()/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[16]=0.5*pow(N.GetSpeed()/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV

save_in_tree[17]=N.GetTime();


save_in_tree[18]=kX_init;
save_in_tree[19]=kY_init;
save_in_tree[20]=kZ_init;
save_in_tree[21]=kVx_init;
save_in_tree[22]=kVy_init;
save_in_tree[23]=kVz_init;
save_in_tree[24]=kV_init;

save_in_tree[25]=0.5*pow(kVx_init*kV_init/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[26]=0.5*pow(kVy_init*kV_init/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[27]=0.5*pow(kVz_init*kV_init/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[28]=0.5*pow(kV_init/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV

save_in_tree[29]=kT_init;

save_in_tree[30]=id1;
save_in_tree[31]=id2;

fTree->Fill(save_in_tree);
}
kTran=0;
kSpec=0;
kDiff=0;
kId++;
if(LineRecord>0) if(kId%LineRecord==0) {fLine3D.Reset(); Line3D = &fLine3D;}
kStep=0;
}

////////////////////////////////////////////////////////////////////////
void CompleteRootOutputInterface::Finalize()
////////////////////////////////////////////////////////////////////////            
{
fOutput->cd();
fTree->Write();
fOutput->Close();
}
