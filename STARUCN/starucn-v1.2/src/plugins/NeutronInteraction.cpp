/*
======================================================================
STARucn MC software
File : NeutronInteraction.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "NeutronInteraction.h"
#include "TMath.h"

///////////////////////////////////////////////////////
NeutronInteraction::NeutronInteraction() : BaseInteraction()
///////////////////////////////////////////////////////
{;}

///////////////////////////////////////////////////////
NeutronInteraction::~NeutronInteraction()
///////////////////////////////////////////////////////
{;}

////////////////////////////////////////////////////////////////////////
void NeutronInteraction::Configure(const char* file)
////////////////////////////////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);
UseSurfaceDepolarisation = fParam.GetBool("NeutronInteraction.UseSurfaceDepolarisation");
DepolarisationProbability = fParam.GetDouble("NeutronInteraction.DepolarisationProbability");

if (UseSurfaceDepolarisation) {
	std::cout << "Surface Depolarisation Enabled with Probability = " << DepolarisationProbability <<std::endl;
}
else {
	std::cout << "Surface Depolarisation Disabled" << std::endl;
}

NoDecay = fParam.GetBool("NeutronInteraction.NoDecay");
if (NoDecay) {
	std::cout << "Beta Decay Disabled" << std::endl;
}
else {
	std::cout << "Beta Decay Enabled" << std::endl;
}
}

///////////////////////////////////////////////////////
int NeutronInteraction::SurfaceInteraction(Particle& n, TGeoVolume* vin, TGeoVolume* vnext, double last, double* norm, TGeoVolume*& newvol)
///////////////////////////////////////////////////////
{

if (UseSurfaceDepolarisation){
	if (n.Gen.Rndm() < DepolarisationProbability){
		#ifdef DEBUG
			std::cout << "Neutron Depolaried at surface interaction!" << std::endl;
		#endif
		n.spin.SetRandomSpin();
		n.spinRevE.SetRandomSpin();
	}	
}	
	
int type = 0; // 0= transmission : 1 = absorbtion :  2= specular reflection : 3=diffuse reflection : 4= transmisson+abs (lifetime=0)
double* inc = n.GetDirection();
double ct = -(inc[0]*norm[0]+inc[1]*norm[1]+inc[2]*norm[2]);

InteractingMaterial* m1 = fCore->fGeo->GetMaterial(vin);
InteractingMaterial* m2 = fCore->fGeo->GetMaterial(vnext);

double V = n.GetSpeed();
double vorth = fabs(V*ct);
double Vfermi= m2->Fermi()-m1->Fermi();
double Eorth = VelocityToEnergy(vorth);
double E=VelocityToEnergy(V);
double p=0;
double rndm = n.Gen.Rndm();

if(m1==m2)
  {
  type=0;
  }
else if(Eorth>Vfermi)
  {
  double e1 = TMath::Sqrt(Eorth);
  double e2 = TMath::Sqrt(Eorth-Vfermi);
  p =  (e1-e2)/(e1+e2); // quantum reflection proba, no absorbtion 
  p = p*p;

////////////////////////////
//p=0;////BAD, for test only
/////////////////////////////
  if     (rndm < m2->D()*p) type=3; // diffuse relection
  else if(rndm < p        ) type=2; // specular reflection
  else if(m2->Lambda()<0  ) type=4;
  else                      type=0; // transmission
  }
else
  {
  p = 1-2*m2->Eta()*TMath::Sqrt(Eorth/(Vfermi-Eorth));
  if     (rndm < m2->D()*p) type=3; 
  else if(rndm < p        ) type=2;
  else                      type=1; // absobtion  
  }
double newdir[3];
newvol = vin;
if     (type==0)
  {
  fCore->fPropagator->Propagate(last, n); // very important to put the neutron into the new volume in case of transmission
  newvol = vnext;
  if(m1!=m2)
    {
    double dv = ct*(sqrt(1-Vfermi/E)-1);

    newdir[0] = inc[0]-dv*norm[0];
    newdir[1] = inc[1]-dv*norm[1];
    newdir[2] = inc[2]-dv*norm[2];
    //double newV = V*TMath::Sqrt(newdir[0]*newdir[0]+newdir[1]*newdir[1]+newdir[2]*newdir[2]);
    double newV=EnergyToVelocity(E-Vfermi);

    n.SetDirection(newdir);
    n.SetSpeed(newV);
/*  std::cout << "in propagation : " <<u << "  " <<  dv<< "   " << newV << std::endl; */
    }
  }
else if(type==1 || type==4) n.Kill();
else if(type==2) 
  {
  newdir[0] = 2*ct*norm[0]+inc[0];
  newdir[1] = 2*ct*norm[1]+inc[1];
  newdir[2] = 2*ct*norm[2]+inc[2];
  n.SetDirection(newdir);
  }
else if(type==3) 
  {
// Lambert cosine law...
//  std::cout << "incident direction        : " <<  inc[0]  << "  " << inc[1]  << "  " <<  inc[2]  << std::endl;
//  std::cout << "normal to reflecting surf : " <<  norm[0] << "  " << norm[1] << "  " <<  norm[2] << std::endl;  
  double nphi = n.Gen.Rndm()*TMath::TwoPi();
  double ncth = TMath::Sqrt(n.Gen.Rndm()); //lamberts law : f(phi, cth) ~ cth.dcth.dphi
  double nsth = TMath::Sqrt(1-ncth*ncth);
  double ncph = TMath::Cos(nphi);
  double nsph = TMath::Sin(nphi);
  double cth = -norm[2];
  double sth = TMath::Sqrt(norm[0]*norm[0] + norm[1]*norm[1]);
  double cph = sth!=0 ? -norm[0]/sth : 1;
  double sph = sth!=0 ? -norm[1]/sth : 0;
  newdir[0] = cph*cth*ncph*nsth-sph*nsph*nsth+cph*sth*ncth;
  newdir[1] = sph*cth*ncph*nsth+cph*nsph*nsth+sph*sth*ncth;
  newdir[2] = cth*ncth-sth*ncph*nsth;
//  std::cout << "reflected direction       : " <<  newdir[0] << "  " << newdir[1] << "  " <<  newdir[2] << std::endl;
  n.SetDirection(newdir);
  n.SetSpeed(V);
  }
return type;
}

///////////////////////////////////////////////////////
int NeutronInteraction::VolumeInteraction (Particle& n, TGeoVolume* vin, double Dt)
///////////////////////////////////////////////////////
{
if (NoDecay) return 0;
int type = 0;
double l = fCore->fGeo->GetMaterial(vin)->Lambda();
/*double prob = l<0 ? -1 : TMath::Exp(-Dt*l);*/
double dt = l<0 ? -1 : n.Gen.Exp(1./l);
if(dt<Dt) // back propagate the neutron to its end point
  {
  type = 1;
  fCore->fPropagator->Propagate(dt,n);
  n.Kill();
  }
return type;
}
