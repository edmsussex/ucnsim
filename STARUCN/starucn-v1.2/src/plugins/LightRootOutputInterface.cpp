/*
======================================================================
STARucn MC software
File : LightRootOutputInterface.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class LightRootOutputInterface                           //
//                                                          //
// OutputInterface plugin for root file event output        //
//                                                          //
//                                                          //
//////////////////////////////////////////////////////////////

#include "LightRootOutputInterface.h"
#include <algorithm>

////////////////////////////////////////////////////////////////////////
LightRootOutputInterface::LightRootOutputInterface() : BaseOutputInterface()
////////////////////////////////////////////////////////////////////////
{
LineRecord = 0;
fOutput = 0;
fTree = 0;
kTran=0;
kSpec=0;
kDiff=0;
}

////////////////////////////////////////////////////////////////////////
LightRootOutputInterface::~LightRootOutputInterface() {;}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
void LightRootOutputInterface::Configure(const char* file)
////////////////////////////////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);

std::string outfile = fParam.GetString("LightRootOutputInterface.OutputFile");
LineRecord   = fParam.GetInt("LightRootOutputInterface.TrajectoryRecord");

std::string tmp = fParam.GetString("LightRootOutputInterface.TrajectoryRecordFinalVolume");
ParameterReader::ReadString(fFinalVolumes,tmp);

fOutput = new TFile(outfile.c_str(), "recreate");
fTree = new TNtupleD("starucn","starucn","id:nstep:NbSpecular:NbDiffuse:NbTransmitted:FinalStatus:x:y:z:vx:vy:vz:V:t");
if(LineRecord>0){ if(kId%LineRecord==0) {fLine3D.Reset(); Line3D = &fLine3D;}}
else {Line3D=0;}
}

////////////////////////////////////////////////////////////////////////
void LightRootOutputInterface::SaveStep(Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)
////////////////////////////////////////////////////////////////////////
{
vol       = 0;
previous  = 0;
interface = 0;

// only counting
if(sur==0) kTran++;
if(sur==2) kSpec++;
if(sur==3) kDiff++;               
kStep++;
// if(Line3D) fLine3D.SetNextPoint(N.GetPosition());
}

////////////////////////////////////////////////////////////////////////
void LightRootOutputInterface::EndParticle (Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)     
////////////////////////////////////////////////////////////////////////
{
previous  = 0;
if(Line3D&&interface) 
  {
  fLine3D.SetName(Form("Trajectory_%d",kId)); 
  fOutput->cd(); 
//   std::cout << interface->GetName() << "  " << previous->GetName() << "  " << fFinalVolumes[0] << std::endl;
  if(std::find(fFinalVolumes.begin(), fFinalVolumes.end(), interface->GetName())!=fFinalVolumes.end() || fFinalVolumes.size()==0) fLine3D.Write(); 
  Line3D=0;
  }

int status = -1;
if(N.Dead())
  {
  if     (vol==2) status=0; // meet non existing volume : should never happen
  else if(vol==1) status=1; // decay
  else if(sur==1) status=2; // absorbtion
  else if(sur==4) status=3; // transmission to zero lifetime material 
  else status = -1;// not possible
  }
else status = 4; // too many rebound...

fTree->Fill(kId,kStep,kSpec,kDiff,kTran,status,N.GetPosition(0),N.GetPosition(1),N.GetPosition(2),N.GetDirection(0),N.GetDirection(1),N.GetDirection(2),N.GetSpeed(),N.GetTime());
kTran=0;
kSpec=0;
kDiff=0;
kId++;
if(LineRecord>0) if(kId%LineRecord==0) {fLine3D.Reset(); Line3D = &fLine3D;}
kStep=0;
}

////////////////////////////////////////////////////////////////////////
void LightRootOutputInterface::Finalize()
////////////////////////////////////////////////////////////////////////            
{
fOutput->cd();
fTree->Write();
fOutput->Close();
}
