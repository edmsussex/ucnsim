/*
======================================================================
STARucn MC software
File : FileGenerator.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "FileGenerator.h"
#include <sstream>
#include <string>
#include <iostream>

//////////////////////////////////////////////
FileGenerator::FileGenerator() : BaseGenerator()
//////////////////////////////////////////////
{
kGood=true;
}


//////////////////////////////////////////////
FileGenerator::~FileGenerator()
//////////////////////////////////////////////
{
}


//////////////////////////////////////////////
void FileGenerator::Configure(const char* file)
//////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);
std::string filename = fParam.GetString("FileGenerator.InputFile");
// std::cout << "in [FileGenerator], filename = " << filename << std::endl;
fFile.open(filename.c_str());
kGood = fFile.good() && !fFile.eof();
}


//////////////////////////////////////////////
void FileGenerator::Generate(Particle& n)
//////////////////////////////////////////////
{
if(kGood)
  {
  std::string line;
  if(GetNextLine(line))
    {
    std::istringstream str(line);
    double x[3],v[3],t;
    str >> x[0] >> x[1] >> x[2] >> v[0] >> v[1] >> v[2] >> t;
    double V = sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
    for(int i=0; i<3;i++) v[i]/=V;
    n.Reset(x,v);
    n.SetSpeed(V);
    n.SetTime(t);
    }
  else {n.Kill(); n.Switch();} 
  }
else {n.Kill(); n.Switch();}
kGood = fFile.good() && !fFile.eof();
n.SetParticleSeed(Gen.Rndm()* 4294967295);
}

//////////////////////////////////////////////
bool FileGenerator::GetNextLine(std::string& line)
//////////////////////////////////////////////
{
line="#";
while(fFile.good() && !fFile.eof() && (line=="" ? true : line[0]=='#' ? true : false)) 
  {
  getline(fFile,line);
  }
return (line=="" ? false : line[0]=='#' ? false : true);
}

