/*
======================================================================
STARucn MC software
File : VolumeGenerator.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "VolumeGenerator.h"
#include <sstream>
#include <string>
#include <fstream>
#include <cmath>

#undef DEBUG

////////////////////////////////////////////////////////////////////////
VolumeGenerator::VolumeGenerator() : BaseGenerator()
////////////////////////////////////////////////////////////////////////
{
fActiveVolume = 0;
fActiveBox    = 0;
fActiveNode   = 0;
fRate=-1;
fCurrTime=0;
UseAlphaMode = 0;
ParticleGyro = 0;
id = 0;
}

////////////////////////////////////////////////////////////////////////
VolumeGenerator::~VolumeGenerator()
////////////////////////////////////////////////////////////////////////
{;}

////////////////////////////////////////////////////////////////////////
void VolumeGenerator::Configure(const char* file)
////////////////////////////////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);

ParticleGyro = fParam.GetDouble("Particle.gyro");

std::cout << "Particle gyro = " << ParticleGyro << std::endl;

double x,y,z; //temporary variables

SpinGeneratorMode = fParam.GetString("VolumeGenerator.spin_generator_mode");
	
x = fParam.GetDouble("VolumeGenerator.spin_plane_ux");
y = fParam.GetDouble("VolumeGenerator.spin_plane_uy");
z = fParam.GetDouble("VolumeGenerator.spin_plane_uz");
plane_u = Vector3L(x,y,z);
plane_u = plane_u/plane_u.Mag();

x = fParam.GetDouble("VolumeGenerator.spin_plane_vx");
y = fParam.GetDouble("VolumeGenerator.spin_plane_vy");
z = fParam.GetDouble("VolumeGenerator.spin_plane_vz");
plane_v = Vector3L(x,y,z);
plane_v = plane_v/plane_v.Mag();

x = fParam.GetDouble("VolumeGenerator.spin_startphase_x");
y = fParam.GetDouble("VolumeGenerator.spin_startphase_y");
z = fParam.GetDouble("VolumeGenerator.spin_startphase_z");

NbParticles = fParam.GetInt("NbParticles");

StartPhase = Vector3L(x,y,z);
//Normalise spin vector to length 1
StartPhase = StartPhase / StartPhase.Mag();

std::cout << "Spin Generator Mode is " << SpinGeneratorMode << std::endl;

if (SpinGeneratorMode == "identical"){
	std::cout << "Starting Phase = " << StartPhase << std::endl;
}
else if (SpinGeneratorMode == "plane"){
	std::cout << "Plane is defined by u = " << plane_u << ", v = " << plane_v << std::endl;
}
else if (SpinGeneratorMode == "random"){
	;
}
else if (SpinGeneratorMode == "alpha") {
	std::cout << "Generating neutrons evenly spread through xy plane" << std::endl;
}
else {
	std::cout << "Error! spinVolumeGenerator Mode not set properly!" << std::endl; 
}

fRate  = 1./fParam.GetDouble("VolumeGenerator.Rate");
fMode  = fParam.GetString("VolumeGenerator.Mode");
UseVelocityHistogram = fParam.GetBool("VolumeGenerator.UseVelocityHistogram"); //overrides velocity setting
VelocityHistogramPath = fParam.GetString("VolumeGenerator.VelocityHistogramPath");
if (UseVelocityHistogram) std::cout << "Using Velocity Histogram: " << VelocityHistogramPath << std::endl;

//calculates proper starting height distribution for neutron
UseProperPositionDist = fParam.GetBool("VolumeGenerator.UseProperHeightDist");
if (UseProperPositionDist) std::cout << "Using proper start position distribution" << std::endl;
else std::cout << "Not using proper start position distribution" << std::endl;


UseAlphaMode = fParam.GetBool("VolumeGenerator.UseAlphaMode");
Alpha = fParam.GetDouble("VolumeGenerator.Alpha");
TrapRadius = fParam.GetDouble("VolumeGenerator.TrapRadius");

std::string type ="";
for(int i=0; i<3;i++)
  {
  std::string name = Form("VolumeGenerator.Density_%c",fMode[i]);
  fParam.GetParameter(type,"density",name,"formula");
  fParam.GetParameter(fMin[i],"density",name,"min");
  fParam.GetParameter(fMax[i],"density",name,"max");
  fSpeedDensity[i] = type == "Uniform" ? 0 : new TF1(Form("SpeedDensity_%c",fMode[i]),type.c_str(),fMin[i],fMax[i]);
  }
fCurrTime=0;

std::string volume = fParam.GetString("VolumeGenerator.Volume");
fActiveVolume = fCore->fGeo->GetVolume(volume.c_str());
fActiveBox = 0;
fActiveNode =0;
if(!fActiveVolume) return;
//box that contains whole of fActiveVolume (?)
fActiveBox = static_cast<TGeoBBox*>(fActiveVolume->GetShape());
//get half lengths
fDx = fActiveBox->GetDX();
fDy = fActiveBox->GetDY();
fDz = fActiveBox->GetDZ();
//get origin
fOx = (fActiveBox->GetOrigin())[0];
fOy = (fActiveBox->GetOrigin())[1];
fOz = (fActiveBox->GetOrigin())[2];
// looking for corresponding node

TObjArray* nodes =  fCore->fGeo->GetNodes();
int N = nodes->GetEntriesFast();
for(int i=0; i<N;++i)
  {
  TGeoNode* tmp = (TGeoNode*) nodes->At(i);
  if(tmp->GetVolume() == fActiveVolume)
    {
    fActiveNode = tmp;
    break;
    }
  }

if (UseVelocityHistogram){
	std::cout << "opening velocity histogram file " << VelocityHistogramPath << std::endl;
	std::cout << "note that this expects first line contains no data, and velocities are in m/s" << std::endl;
	std::cout << "for example, " << std::endl;
	std::cout << "Velocity (m/s)	Number of neutrons" << std::endl;
	std::cout << "5.000000e-02	24" << std::endl;
	std::cout << "1.500000e-01	26" << std::endl;
	std::cout << "2.500000e-01	23" << std::endl;
	std::cout << "et cetera..." << std::endl;
	
	std::ifstream file(VelocityHistogramPath.c_str());
	double velocity;
	int number;
	std::string stringy;
	unsigned int histtotal = 0;
	std::getline(file,stringy); //throw away first line
	while (std::getline(file,stringy))
	{
		std::stringstream stringystream;
		stringystream << stringy;
		stringystream >> velocity >> number;
		hist_velocitylist.push_back(velocity*1000); //convert velocity to m/s
		histtotal = histtotal + number;
		hist_velocityprob.push_back(histtotal);
	}
	
	file.close();
	
	for (unsigned int i = 0; i < hist_velocityprob.size();i++) {
		hist_velocityprob[i] /= histtotal; //cumulative probability
		#ifdef DEBUG
			std::cout << "culmulative probability of velocity bin " << hist_velocitylist[i] << " is " << hist_velocityprob[i] << std::endl;
		#endif
	}
	
}

}

////////////////////////////////////////////////////////////////////////
void VolumeGenerator::Generate(Particle& n)
////////////////////////////////////////////////////////////////////////
{
double pos[3],v[3], V=0;
if (!fActiveVolume) return;
if (!fActiveBox ) return;

n.SetParticleSeed(Gen.Rndm()* 4294967295);



// Generate neutron velocity
TRandom* toto = gRandom; // this is ugly
gRandom = &(n.Gen);          // this is uglier
if(fMode=="CPV")
  {
  double cth = fSpeedDensity[0] ? fSpeedDensity[0]->GetRandom()                  : n.Gen.Uniform(fMin[0], fMax[0]);
  double phi = fSpeedDensity[1] ? fSpeedDensity[1]->GetRandom()*TMath::Pi()/180. : n.Gen.Uniform(fMin[1], fMax[1])*TMath::Pi()/180.;
  V =          fSpeedDensity[2] ? fSpeedDensity[2]->GetRandom()                  : n.Gen.Uniform(fMin[2], fMax[2]); 
  v[0] = cth;
  v[1] = sqrt(1-cth*cth)*cos(phi);
  v[2] = sqrt(1-cth*cth)*sin(phi);
  }
else if(fMode=="CPVZ")
  {
  double cth = fSpeedDensity[0] ? fSpeedDensity[0]->GetRandom()                  : n.Gen.Uniform(fMin[0], fMax[0]);
  double phi = fSpeedDensity[1] ? fSpeedDensity[1]->GetRandom()*TMath::Pi()/180. : n.Gen.Uniform(fMin[1], fMax[1])*TMath::Pi()/180.;
  V =          fSpeedDensity[2] ? fSpeedDensity[2]->GetRandom()                  : n.Gen.Uniform(fMin[2], fMax[2]); 
  v[0] = sqrt(1-cth*cth)*sin(phi);
  v[1] = sqrt(1-cth*cth)*cos(phi);
  v[2] = cth;
  }
else if(fMode=="XYZ")
  {
  for(int i=0; i<3;i++) 
    {
    v[i] = fSpeedDensity[i] ? fSpeedDensity[i]->GetRandom() : n.Gen.Uniform(fMin[i], fMax[i]);
    V += v[i]*v[i];
    }
  V = TMath::Sqrt(V);  
  }
else if(fMode=="VVV")
  {
  bool stop = false;
  while(!stop)
    {
    for(int i=0; i<3;i++)
      {
      v[i] = fSpeedDensity[i] ? fSpeedDensity[i]->GetRandom() : n.Gen.Uniform(-fMin[0], fMin[0]);
      V += v[i]*v[i];
      }
    V = TMath::Sqrt(V);
    stop = V < fMin[0];
    }
  }

//override velocity
if (UseVelocityHistogram) {
	double prob = n.Gen.Rndm();
	int k = 0;
	
	while (prob>hist_velocityprob[k]) k++; //go up probability histogram until you find right bin
	while (prob<hist_velocityprob[k]) k--; //go back one
	//linear interpolation between (k, k+1)
	V =  hist_velocitylist[k]+(hist_velocitylist[k+1]-hist_velocitylist[k])*(prob-hist_velocityprob[k])/(hist_velocityprob[k+1]-hist_velocityprob[k]);
}

//Generate Starting Positions
if (UseProperPositionDist) {
  //Use proper start height distribution with z distribution according to
  // n(h) = (1 - h/E)^1/2 * n(0)
  //integration and inversion yields that heights can be generated by
  // h = E ( 1 - (1- kX) ^(2/3) )
  // with k = 1 for E < H and k = 1 - (1 - H/E)^(3/2) otherwise
  // (E = energy expressed as max attainable height above bottom of generation volume, H = height of vessel, X = uniform random variable in (0,1)
  // Source: Harris et. al. 2013 - Gravitationally enhanced depolarization of ultracold neutrons in magnetic fieldgradients, and implications for neutron electric dipole moment measurements
  double local[3];
  double E = V * V / (2 * 9806.);
  double k = E < 2 * fDz ? 1 : 1 - std::pow(1 - (2 * fDz / E),1.5);
  #ifdef DEBUG
    std::cout << "Generated Velocity = " << V << std::endl;
  #endif
  while (1)
    {
	//generate (uniform) random positions in x and y in box containing whole start volume
    local[0] = fOx-fDx+2*fDx*n.Gen.Rndm();
    local[1] = fOy-fDy+2*fDy*n.Gen.Rndm();
    local[2] = fOz-fDz+E*(1.-std::pow(1-k*n.Gen.Rndm(),2./3.)) ;
    
    #ifdef DEBUG
      std::cout << "trying (local) position (" << local[0] << "," << local[1] << "," << local[2] << ")" << std::endl;
    #endif
    
    //if generated position contained in volume
    if(fActiveVolume->Contains(local))
      {
	  // update velocity
	  // new V = sqrt( old v ^2 - 2 * g * height)
	  V = std::pow(V*V - 2*9806. * (local[2]-fOz + fDz),0.5);
      fActiveNode->LocalToMaster(local,pos);
      #ifdef DEBUG
        std::cout << "local[2]-fOz + fDz gives " << local[2]-fOz + fDz << std::endl;
        std::cout << "Using (local) position (" << local[0] << "," << local[1] << "," << local[2] << ")" << std::endl;
        std::cout << "Velocity = " << V << std::endl;
      #endif
      break;
      }
    }
}
else {
  //Random Position in start volume
  double local[3];
  while (1)
    {
	//generate (uniform) random positions in box containing whole start volume
    local[0] = fOx-fDx+2*fDx*n.Gen.Rndm();
    local[1] = fOy-fDy+2*fDy*n.Gen.Rndm();
    local[2] = fOz-fDz+2*fDz*n.Gen.Rndm();
    if(fActiveVolume->Contains(local))
      {
	  //if position 
      fActiveNode->LocalToMaster(local,pos);
      break;
      }
    }
}

//Override position, direction
if (UseAlphaMode) {

	//start all neutrons at same point - midpoint of chord
	double x = 0;
	double y = TrapRadius * std::cos(Alpha*M_PI/180); //set y to be correct height
	
	pos[0] = x;
	pos[1] = y;
	pos[2] = 0;
	
	//half particles go clockwise,half anticlockwise
	if (id%2 == 0) {
		v[0] = 1;
		v[1] = 0;
		v[2] = 0;
	}
	else {
		v[0] =-1;
		v[1] = 0;
		v[2] = 0;
	}
}


gRandom = toto;
// set new neutron parameters
n.Reset(pos,v);
n.SetSpeed(V);
if(fRate>0) fCurrTime += n.Gen.Exp(fRate);
n.SetTime(fCurrTime);

//set random seeds
	n.spin.SeedRNG(n.Gen.Rndm() * 4294967295); //generates random number in [0,1] then scales to sizeof(UInt_t), then casts to unsigned int
	n.spinRevE.SeedRNG(n.Gen.Rndm() * 4294967295);
	
	if (SpinGeneratorMode == "identical"){
		n.spin.SetSpin(StartPhase);
		n.spin.SetStartSpin();
		n.spinRevE.SetSpin(StartPhase);
		n.spinRevE.SetStartSpin();
	}
	else if (SpinGeneratorMode == "plane"){
		n.spin.SetRandomSpinPlane(plane_u,plane_v);
		n.spin.SetStartSpin();
		n.spinRevE.SetRandomSpinPlane(plane_u,plane_v);
		n.spinRevE.SetStartSpin();
	}
	else if (SpinGeneratorMode == "random"){
		n.spin.SetRandomSpin();
		n.spin.SetStartSpin();
		n.spinRevE.SetRandomSpin();
		n.spinRevE.SetStartSpin();
	}
	else if (SpinGeneratorMode == "alpha") {
		double startangle = (2 * M_PI / NbParticles) * id;
		Vector3L StartSpin(std::cos(startangle),std::sin(startangle),0);
		n.spin.SetSpin(StartSpin);
		n.spin.SetStartSpin();
		n.spinRevE.SetSpin(StartSpin);
		n.spinRevE.SetStartSpin();
	}
	else {
		std::cout << "Error! SpinGeneratorMode not set properly!" << std::endl; 
	}
	
	//Set Particle Gyromagnetic Ratio
	
	n.spin.SetGyro(ParticleGyro);
	n.spinRevE.SetGyro(ParticleGyro);

id++; //increment particle number

}
