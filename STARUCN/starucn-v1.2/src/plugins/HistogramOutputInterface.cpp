/*
======================================================================
STARucn MC software
File : HistogramOutputInterface.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class HistogramOutputInterface		                        //
//                                                          //
// OutputInterface plugin for root file event output        //
//                                                          //
//                                                          //
//////////////////////////////////////////////////////////////

#include "HistogramOutputInterface.h"
#include "TH1F.h"

////////////////////////////////////////////////////////////////////////
HistogramOutputInterface::HistogramOutputInterface() : BaseOutputInterface()
////////////////////////////////////////////////////////////////////////
{
fOutput   = 0;
fTstep    = 0;
fNstep    = 0;
fHistTime = 0; 
fBin      = 0;
}

////////////////////////////////////////////////////////////////////////
HistogramOutputInterface::~HistogramOutputInterface() {;}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
void HistogramOutputInterface::Configure(const char* file)
////////////////////////////////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);
std::string tmp;
tmp = fParam.GetString("HistogramOutputInterface.OutputFile");
fOutput = new TFile(tmp.c_str(), "recreate");
fTstep = fParam.GetDouble("HistogramOutputInterface.SnapShotTime");
fNstep = fParam.GetInt   ("HistogramOutputInterface.NbSnapShots");
fVol   = fParam.GetString("HistogramOutputInterface.VolumeName");
fHistTime = new TH1F("HistTime","HistTime",fNstep,fTstep*0.5,(fNstep+0.5)*fTstep);
fHistTime->Sumw2();
}

////////////////////////////////////////////////////////////////////////
void HistogramOutputInterface::SaveStep(Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)
////////////////////////////////////////////////////////////////////////
{
if(fVol!="" && interface->GetName()!=fVol) return;
if(N.GetTime()>fBin*fTstep) 
  {
  fHistTime->Fill(fBin*fTstep);
  fBin++;
  }
}


////////////////////////////////////////////////////////////////////////
void HistogramOutputInterface::BeginParticle (Particle& N)     
////////////////////////////////////////////////////////////////////////
{
fBin = int(N.GetTime()/fTstep);
}


////////////////////////////////////////////////////////////////////////
void HistogramOutputInterface::EndParticle (Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)     
////////////////////////////////////////////////////////////////////////
{
}

////////////////////////////////////////////////////////////////////////
void HistogramOutputInterface::Finalize()
////////////////////////////////////////////////////////////////////////            
{
fOutput->cd();
fHistTime->Write();
fOutput->Close();
}
