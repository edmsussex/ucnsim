#include "SpinPropagator.h"

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

SpinPropagator::SpinPropagator() {
	fTolerance = 1e-6;
	backwardstime = 0;
	} 

SpinPropagator::~SpinPropagator() {
}

double SpinPropagator::PropagateToSurface(Particle& n, double& Dt, double* Norm, TGeoVolume* &next, TPolyLine3D* poly)
{
double Lstep  = fStep;  //set step size = configured step size
if(!fCurrentNode) 
  {
  next=0;
  return 0;
  }
if(fStep<=0) // try adaptative step
  {
  double locP[3];
  double locD[3];
  fCurrentNode->MasterToLocal(n.GetPosition() ,locP);
  fCurrentNode->MasterToLocalVect(n.GetDirection(),locD);
  Lstep = -fStep*0.66*fCurrentShape->DistFromInside(locP, locD, 3, 0, 0);
  if(Lstep==0)  Lstep = -fStep*0.1;
  }
Dt = 0;
double tstep =0;
Vector3L endofstep_pos;
double iniStep = Lstep;
double tmp[3];
double tmp2[3];
//step position forward until we are within fTolerance of edge of volume
do{
	tstep = Lstep/n.GetSpeed();


	endofstep_pos = GetPositionV(tstep,n);
	tmp2[0]=endofstep_pos.x();
	tmp2[1]=endofstep_pos.y();
	tmp2[2]=endofstep_pos.z();
	fCurrentNode->MasterToLocal(tmp2, tmp); //convert neutron coordinates into those for fCurrentVolume
	//check if calculated neutron position is in volume
	if (!fCurrentVolume->Contains(tmp)){
		//if not, halve step
		Lstep = 0.5*Lstep;
	}
	else {
		//if it is, add the timestep to Dt and save the position in the Trajectory Line
		//if (tstep < (Dt*1e-15) ) tstep=Dt*1e-15; // to insure that the step is not out of the 15 digit range of double
		
		Propagate(tstep,n);
		if(poly) if(Lstep==iniStep) poly->SetNextPoint(endofstep_pos.x(),endofstep_pos.y(),endofstep_pos.z());
	}

} while(2*Lstep>fTolerance && tstep>MinTimeStep);

//std::cout << "end of step radius is " << std::pow(endofstep_pos.x()*endofstep_pos.x()+endofstep_pos.y()*endofstep_pos.y(),0.5) << std::endl;
//std::cout << "here"<< std::endl;
//Advance neutron state forward by calculated time
//Vector3L start_pos = GetPositionV(0.,n);
//Vector3L exp_end_pos = GetPositionV(Dt,n);
//std::cout << "doing propagate for " << Dt << "secs, start pos = " << start_pos << ", expected end pos = " << exp_end_pos << std::endl;


//Propagate(Dt,n);


//Vector3L real_end_pos = GetPositionV(0.,n);
//std::cout << "real end pos = " << real_end_pos << std::endl;

// move inside the next volume
double timetonextvolume = 0;
bool InVolume = 1;
tstep = Lstep/n.GetSpeed();
do {
	timetonextvolume += tstep; 
	endofstep_pos = GetPositionV(timetonextvolume,n);
	
	tmp2[0]=endofstep_pos.x();
	tmp2[1]=endofstep_pos.y();
	tmp2[2]=endofstep_pos.z();
	Vector3L startofsteppos = GetPositionV(0.,n);
	//std::cout << "start of step position = " << startofsteppos << ", tstep = " << tstep << "timetonextvolume = " << timetonextvolume << std::endl;
	//std::cout << "end of step position = " << endofstep_pos << std::endl;
	
	fCurrentNode->MasterToLocal(tmp2, tmp); //convert neutron coordinates into those for fCurrentVolume
	InVolume = fCurrentVolume->Contains(tmp);
	//if (!InVolume) std::cout << "not ";
	//std::cout << "in volume" << std::endl;
	
} while(InVolume);

// identify next volume
next = fCore->fGeo->FindVolume(tmp2);

// determine normal direction...
if(Norm) 
  {
  double locP[3];
  double locD[3];
  double locN[3];
  fCurrentNode->MasterToLocal(n.GetPosition() ,locP);  
  fCurrentNode->MasterToLocalVect(n.GetDirection(),locD);
  fCurrentShape->ComputeNormal(locP,locD,locN);
  fCurrentNode->LocalToMasterVect(locN, Norm);   
  }
return timetonextvolume;
}// neutron is inside the volume close to the interface, a step of timetonextvolume will bring it ot the other side



bool SpinPropagator::Propagate(double dt, Particle &n){
	//This is called first, then execution is sent to the appropriate algorithm
	//(this kind of switch statement isn't proper OOP so restructure at some point)

	if (PropagatorMode == "RungeKuttaCashKarp")
		return RungeKuttaCashKarpPropagate(dt, n);
	else if (PropagatorMode == "NonAdaptiveRungeKuttaCashKarp")
		return NonAdaptiveRungeKuttaCashKarpPropagate(dt,n);
	else if (PropagatorMode == "MinimalTestPropagate")
		return MinimalTestPropagate(dt,n);
	else if (PropagatorMode == "SingleStepNonAdaptiveRungeKuttaCashKarp")
		return SingleStepNonAdaptiveRungeKuttaCashKarpPropagate(dt,n);
	else {
		std::cout << "Error! PropgatorMode not set properly" << std::endl;
		return 0;
	}
}

bool SpinPropagator::IsInVolume(double t, Particle &n) {
	double tmp[3];
	double tmp2[3];
	Vector3L endofstep_pos = GetPositionV(t,n);
	tmp2[0]=endofstep_pos.x();
	tmp2[1]=endofstep_pos.y();
	tmp2[2]=endofstep_pos.z();
	if (!fCurrentNode) {
		Vector3L pos = n.GetPositionV();
		std::cout << "warning! fCurrentNode is null, this could indicate a problem with your geometry - neutron is at " << pos << std::endl;
		return 0;
	}
	fCurrentNode->MasterToLocal(tmp2, tmp); //convert neutron coordinates into those for fCurrentVolume
	return fCurrentVolume->Contains(tmp);
}

void SpinPropagator::UpdateNeutron(double dt, Particle &n){
	Vector3L pos = GetPositionV(dt,n);
	Vector3L velocity = GetVelocityV(dt,n);
	
	double tmp[3];
	tmp[0]=velocity.x();
	tmp[1]=velocity.y();
	tmp[2]=velocity.z();
	
	n.SetDirection(tmp);
	n.SetSpeed(velocity.Mag());
	
	n.SetPosition(0,pos.x());
	n.SetPosition(1,pos.y());
	n.SetPosition(2,pos.z());
	
	n.AddTime(dt);
	return;
}

bool SpinPropagator::MinimalTestPropagate(double dt,Particle &n){
	
	Vector3L B = GetEffectiveB(dt*0.5,n,plane_u,plane_v);
	Vector3L BRevE = GetEffectiveBRevE(dt*0.5,n,plane_u,plane_v);
	
	n.spin.Evolve(dt,B,plane_u,plane_v);
	n.spinRevE.Evolve(dt,BRevE,plane_u,plane_v);
	
	UpdateNeutron(dt,n);
	return IsInVolume(dt,n);
	
}

bool SpinPropagator::SingleStepNonAdaptiveRungeKuttaCashKarpPropagate(double dt,Particle &n){
	Vector3L k1 = GetEffectiveB(0,n,plane_u,plane_v); //Set starting position B
	Vector3L k1RevE = GetEffectiveBRevE(0,n,plane_u,plane_v);
	Vector3L k3,k4,k6,B5avg,B4avg,k3RevE,k4RevE,k6RevE,B5avgRevE;
	
	k3 = GetEffectiveB(dt*0.3,n,plane_u,plane_v);
	k4 = GetEffectiveB(dt*0.6,n,plane_u,plane_v);
	k6 = GetEffectiveB(dt*0.875,n,plane_u,plane_v);
	B5avg =  k1 * 37. / 378. + k3 * 250. / 621. + k4 * 125. / 594. + k6 * 512. / 1771.; //5th order average B
	
	//calculate average BRevE
	k3RevE = GetEffectiveBRevE(dt*0.3,n,plane_u,plane_v);
	k4RevE = GetEffectiveBRevE(dt*0.6,n,plane_u,plane_v);
	k6RevE = GetEffectiveBRevE(dt*0.875,n,plane_u,plane_v);
	B5avgRevE = k1RevE * 37. / 378. + k3RevE * 250. / 621. + k4RevE * 125. / 594. +  k6RevE * 512. / 1771.;
	
	#ifdef DEBUG
	std::cout << "avg B = " << B5avg << ", avg BRevE = " << B5avgRevE;
	#endif
	
	n.spin.Evolve(dt,B5avg,plane_u,plane_v);
	n.spinRevE.Evolve(dt,B5avgRevE,plane_u,plane_v);
	
	UpdateNeutron(dt,n);
	return IsInVolume(dt,n);
	
}

bool SpinPropagator::NonAdaptiveRungeKuttaCashKarpPropagate(double Dt,Particle &n){
	// timeleft tracks how much propagation is left to do, dt is size of individual step
	double timeleft = Dt;
	// timedone tracks how much we have done so far
	double timedone = 0;
	
	while (timeleft > 0.0) {
		double dt = timeleft > MaxTimeStep ? MaxTimeStep : timeleft; //stops us from overstepping
		Vector3L k1 = GetEffectiveB(0,n,plane_u,plane_v); //Set starting position B
		Vector3L k1RevE = GetEffectiveBRevE(0,n,plane_u,plane_v);
		Vector3L k3,k4,k6,B5avg,B4avg,k3RevE,k4RevE,k6RevE,B5avgRevE;
		
		k3 = GetEffectiveB(timedone + dt*0.3,n,plane_u,plane_v);
		k4 = GetEffectiveB(timedone + dt*0.6,n,plane_u,plane_v);
		k6 = GetEffectiveB(timedone + dt*0.875,n,plane_u,plane_v);
		B5avg = k1 * 37. / 378. + k3 * 250. / 621. + k4 * 125. / 594. + k6 * 512. / 1771.; //5th order average B
		
		//calculate average BRevE
		k3RevE = GetEffectiveBRevE(timedone + dt*0.3,n,plane_u,plane_v);
		k4RevE = GetEffectiveBRevE(timedone + dt*0.6,n,plane_u,plane_v);
		k6RevE = GetEffectiveBRevE(timedone + dt*0.875,n,plane_u,plane_v);
		B5avgRevE = k1RevE * 37. / 378. + k3RevE * 250. / 621. + k4RevE * 125. / 594. + k6RevE * 512. / 1771.;
		
		#ifdef DEBUG
		std::cout << "avg B = " << B5avg << ", avg BRevE = " << B5avgRevE;
		#endif
		
		n.spin.Evolve(dt,B5avg,plane_u,plane_v);
		n.spinRevE.Evolve(dt,B5avgRevE,plane_u,plane_v);
		
		timedone += dt;
		timeleft -= dt;
		
	}
	UpdateNeutron(Dt,n);
	return IsInVolume(Dt,n);
	
}

bool SpinPropagator::RungeKuttaCashKarpPropagate(double timestep,Particle &n){
	/* We are using an adaptive step size Runge-Kutta-Fehlberg style algorithm. We take 
	 * It requires 4 evaluations of the B field per step
	 * 
	 *The various magic numbers are the Cash-Karp Parameters for embedded runge kutta method
	 * They allow us to construct a both a fourth order and fifth order runge kutta for each step
	 * and the difference between them is taken to be the error.
	 * 
	 * This code is based on section 16.2 of Numerical Recipes in C 2nd edition by Press, Teukolsky, Vetterling and Flannery
	 * 
	 * One limitation is that the error estimation does not take account of BRevE, it is possible there will be rare situations where
	 * a fluctuation in B is hidden by an opposite fluctuation in the particle velocity, causing a motional magnetic field.
	 * I assume that this effect will be negligable.
	 **/
	#ifdef DEBUG
	std::cout << "Calling Runge Kutta Cash Karp Propagate, timestep = " << timestep << std::endl;
	#endif
	
	double Dt = timestep;
	
	double timedone = 0; //counts how long we have been so far
	
	#ifdef debug
	//std::cout << "n.GetTime() < storageTime" << std::endl;
	#endif
	//If dt will take us over the storage time, set dt so we finish
	//at exactly the storage time
	//Dt = Dt > storageTime - n.GetTime() ? storageTime - n.GetTime() : Dt;
	
	double dt = Dt; 
	//dt represents the step size
	//We choose initially dt = Dt, then if that is too big we'll decrease it.
	//Dt will be the time left, after each successful step of time dt
	//we will subtract dt from Dt
	dt = dt < MaxTimeStep ? dt : MaxTimeStep;
	
	
	Vector3L k1 = GetEffectiveB(timedone,n,plane_u,plane_v); //Set starting position B
	Vector3L k1RevE = GetEffectiveBRevE(timedone,n,plane_u,plane_v);
	Vector3L k3,k4,k5,k6,B5avg,B4avg,k3RevE,k4RevE,k5RevE,k6RevE,B5avgRevE,BErr;
	long double xerr,yerr,zerr, errmax; //holds errors
	
	bool MinTimeStepReached = 0;
	//Dt= std::abs(Dt);
	//Main Loop	
	while (Dt > MinTimeStep){
		
		if (std::abs(dt) < std::abs(MinTimeStep)) {
			dt = MinTimeStep * (sgn(dt) == 0 ? 1 : sgn(dt));
			MinTimeStepReached = 1;
			//std::cout << "MinTimeStep Reached  - neutron time = " << n.GetTime() << std::endl;
		}
		else {
			MinTimeStepReached = 0;
		}
		//if dt too big, make = maxtimestep * sign of dt
		dt = std::abs( dt) < MaxTimeStep ? dt : MaxTimeStep * sgn(dt);
		
		if(dt<(Dt*1e-15)) dt=Dt*1e-15;
		
		#ifdef DEBUG
			std::cout << "trying step dt = " << dt << std::endl;
		#endif
		
		dt = std::abs(dt) > std::abs(Dt)  ? Dt : dt; //ensure we do not overstep
		if (dt != dt){
			std::cout << "Error! dt is nan" << std::endl;
			std::string stringy2;
			std::getline(std::cin,stringy2);	
		}
		//As k2 doesn't appear in c in the tableau we don't need to evaluate it - b coefficents are irrelevant as we're integrating dy/dx = f(x) rather than dy/dx = f(x,y)
		k3 = GetEffectiveB(timedone+dt*0.3  ,n,plane_u,plane_v);
		k4 = GetEffectiveB(timedone+dt*0.6  ,n,plane_u,plane_v);
		k5 = GetEffectiveB(timedone+dt      ,n,plane_u,plane_v);
		k6 = GetEffectiveB(timedone+dt*0.875,n,plane_u,plane_v);
		B5avg =  k1 * 37. / 378. + k3 * 250. / 621. + k4 * 125. / 594. + k6 * 512. / 1771.; //5th order average B
		B4avg = k1 * 2825. / 27648. + k3 * 18575. / 48384. + k4 * 13525. /55296. + k5 * 277. / 14336. + k6 / 4.; //4th order average B
		BErr = B5avg - B4avg; //estimator of truncation error
		#ifdef DEBUG
		std::cout << "average B field was " << B5avg;
		std::cout << "k's are " << k1 << ", " << k3  << ", " << k4  << ", " << k5  << ", " << k6 << std::endl;
		#endif
		xerr = std::abs(BErr.x());
		yerr = std::abs(BErr.y());
		zerr = std::abs(BErr.z());
		#ifdef DEBUG
		std::cout << "x error is " << xerr << " y error is " << yerr << "z error is " << zerr << std::endl;
		#endif
		errmax = std::max(std::max(xerr,yerr),zerr); //largest error
		//here we multiply the error by the gyromagnetic ratio so that we can scale a magnetic field error to an
		//error in angular frequency
		double angerr = errmax*n.spin.GetGyro();
		
		#ifdef DEBUG
			std::cout << "errmax was " << errmax << " angular freq. error was " << errmax * n.spin.GetGyro() << std::endl;
		#endif
		
		if (std::abs(angerr)<TargetError || MinTimeStepReached) {
			//if error is sufficiently small, advance the neutron
			#ifdef DEBUG
				std::cout << "step successful" << std::endl;
			#endif
			//dt= std::abs(dt);
			//calculate average BRevE
			k3RevE = GetEffectiveBRevE(timedone+dt*0.3  ,n,plane_u,plane_v);
			k4RevE = GetEffectiveBRevE(timedone+dt*0.6  ,n,plane_u,plane_v);
			k5RevE = GetEffectiveBRevE(timedone+dt      ,n,plane_u,plane_v);
			k6RevE = GetEffectiveBRevE(timedone+dt*0.875,n,plane_u,plane_v);
			B5avgRevE =  k1RevE * 37. / 378. + k3RevE * 250. / 621. + k4RevE * 125. / 594. + k6RevE * 512. / 1771.;
			
			#ifdef DEBUG3
				Vector3L initialpos = GetPositionV(0,n);
				Vector3L finalpos   = GetPositionV(dt,n);
				std::cout << "Step accepted with dt = " << dt << ", from pos " << initialpos << " to " << finalpos << std::endl;
				std::cout << "B = " <<B5avg << ", BRevE = " << B5avgRevE << std::endl;
			#endif

			//advance spin vectors
			n.spin.Evolve(dt,B5avg,plane_u,plane_v);
			n.spinRevE.Evolve(dt,B5avgRevE,plane_u,plane_v);
			
			n.spin.AccumulateError(angerr*dt);
			
			k1 = k5; //update starting position B
			k1RevE = k5RevE;
			//dt= std::abs(dt);
			Dt = Dt - dt; //update Dt
			timedone += dt;
			
		//std::cout <<" dt " << dt << " Dt " << Dt  << std::endl;
			//grow step size
			#ifdef DEBUG
				std::cout << "growing step size from " << dt;
			#endif
			
			double newdt = dt * 0.9  * std::pow(std::abs(TargetError/angerr),0.2);
			
			#ifdef MOREDEBUG
				std::cout << "formula says new stepsize ought to be " << newdt << std::endl;
				std::cout << "std::abs(TargetError/angerr) = " << std::abs(TargetError/angerr) << ", std::pow(std::abs(TargetError/angerr),0.2) = " << std::pow(std::abs(TargetError/angerr),0.2) << ", dt * 0.9  * std::pow(std::abs(TargetError/angerr),0.2) = " << dt * 0.9  * std::pow(std::abs(TargetError/angerr),0.2) << std::endl;
				std::cout << "sgn(dt) returns " << sgn(dt) << std::endl;
			#endif
			
			dt = std::abs( newdt) < std::abs( dt * 5) ? newdt : dt * 5; //new dt should be no more than 5* previous dt
			dt = std::abs( dt) < MinTimeStep ? MinTimeStep * sgn(dt) : dt; //if dt < mintimestep, 
			#ifdef DEBUG
				std::cout << "to " << dt << std::endl;
			#endif
		}
		else {
			//shrinking step
			
			#ifdef DEBUG
				std::cout << "shrinking step size from " << dt;
			#endif
			//dt= std::abs(dt);
			double newdt = dt * 0.9  * std::pow(std::abs(TargetError/angerr),0.25);
			dt =  std::abs(newdt) > std::abs(dt/5) ? newdt : dt/5; //new dt should be no less than a fifth of previous dt
			#ifdef DEBUG
				std::cout << "to " << dt << std::endl;
			#endif
			
		}

	}
	UpdateNeutron(timestep,n);
	return IsInVolume(timestep,n);

}	

//Applies vxE effect, returns effective B
Vector3L SpinPropagator::GetEffectiveB(double t, Particle &n, Vector3L& u, Vector3L &v){
	Vector3L B0 = GetBField(t,n,u,v);
	if (DisablevxE)
		return B0;
	else {
		Vector3L E = GetEField(t,n);
		Vector3L velocity = GetVelocityV(t,n)/1000;  //STARucn gives velocities in mm/s, we want SI
		Vector3L vxE = velocity.Cross(E)/EDM::csquared();
		Vector3L B = B0 - vxE;
		#ifdef DEBUG2
			std::cout << "E = " << E << "vxE = " << vxE << "B0 = " << B0 << "B = " << B << std::endl;
		#endif
		return B;
	}
}

//This is the effective B field when E is reversed!
Vector3L SpinPropagator::GetEffectiveBRevE(double t, Particle &n, Vector3L& u, Vector3L &v){
	Vector3L B0 = GetBField(t,n,u,v);
	if (DisablevxE)
		return B0;
	else {
		Vector3L E = GetERevField(t,n);
		Vector3L velocity = GetVelocityV(t,n)/1000;  //STARucn gives velocities in mm/s, we want SI
		Vector3L vxE = velocity.Cross(E)/EDM::csquared();
		Vector3L B = B0 - vxE;
		#ifdef DEBUG2
			std::cout << "ERev = " << E << "vxERev = " << vxE << "B0 = " << B0 << "B = " << B << std::endl;
		#endif
		return B;
	}
}

Vector3L SpinPropagator::GetVelocityV(double t, Particle &n){
	Vector3L initialVelocity;
	initialVelocity = n.GetVelocityV();
	if (kUseGravity)
		return  Vector3L(0.,0.,-9806.*t) + initialVelocity;
	else
		return initialVelocity;
}

Vector3L SpinPropagator::GetPositionV(double t, Particle &n){
	Vector3L initialVelocity, initialPosition;
	initialVelocity = n.GetVelocityV();
	initialPosition = n.GetPositionV();
	if (kUseGravity)
		return initialPosition + initialVelocity * t + Vector3L(0.,0.,-4903.*t*t);
	else 
		return initialPosition + initialVelocity * t;
}

Vector3L SpinPropagator::GetBField(double t, Particle& n, Vector3L &u, Vector3L &v)
{
	Vector3L pos = GetPositionV(t,n);
	u = plane_u;
	v = plane_v;
	Vector3L B = fieldManager.GetBField(pos,t+n.GetTime());
	#ifdef MOREDEBUG
		std::cout << "GetBField returned " << B << std::endl;
		std::cout << "pos = " << pos << ", u = " << u << ", v = " << v << ", t = " << t << std::endl;
	#endif
	return B;
}

Vector3L SpinPropagator::GetEField(double t, Particle& n)
{
	Vector3L pos = GetPositionV(t,n);
	Vector3L E = fieldManager.GetEField(pos,t+n.GetTime());
	return E;
}

Vector3L SpinPropagator::GetERevField(double t, Particle& n)
{
	Vector3L pos = GetPositionV(t,n);
	Vector3L ERev = fieldManager.GetERevField(pos,t+n.GetTime());
	return ERev;
}

inline Vector3L SpinPropagator::AddVector3L(Vector3L A, Vector3L B) {
	Vector3L& C = B;
	return A + C;
}

void SpinPropagator::Configure(const char* file)
{
double x,y,z;

if(file!=0) SetParameterFile(file);

std::cout << "SetParameterFile(file)" << std::endl;

fieldManager.Configure(file);

std::cout << "fieldManager.Configure(file);" << std::endl;

fTolerance = fParam.GetDouble("SpinPropagator.Tolerance");
fStep      = fParam.GetDouble("SpinPropagator.StepSize");

kUseGravity= fParam.GetBool  ("SpinPropagator.UseGravity");

x = fParam.GetDouble("SpinPropagator.plane_ux");
y = fParam.GetDouble("SpinPropagator.plane_uy");
z = fParam.GetDouble("SpinPropagator.plane_uz");
plane_u = Vector3L(x, y, z);

x = fParam.GetDouble("SpinPropagator.plane_vx");
y = fParam.GetDouble("SpinPropagator.plane_vy");
z = fParam.GetDouble("SpinPropagator.plane_vz");
plane_v = Vector3L(x, y, z);

MaxTimeStep = fParam.GetDouble("SpinPropagator.max_time_step");
MinTimeStep = fParam.GetDouble("SpinPropagator.min_time_step");
TargetError = fParam.GetDouble("SpinPropagator.target_error");

DisablevxE = fParam.GetBool("SpinPropagator.disable_vxE");

PropagatorMode = fParam.GetString("SpinPropagator.PropagatorMode");

std::cout << "Setting up SpinPropagator: MaxTimeStep = " << MaxTimeStep <<std::endl;
std::cout << "MinTimeStep = " << MinTimeStep << ", TargetError = " << TargetError << std::endl;
std::cout << "Propagator Mode = " << PropagatorMode << std::endl;

return;

} 

