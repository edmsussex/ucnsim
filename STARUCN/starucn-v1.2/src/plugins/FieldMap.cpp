#include "FieldMap.h"

#if 0

/**
 * Get the points surrounding the vector using the FieldMap::GetBoundingPoints()
 * function then do linear  interpolation on the points (code from
 * Phils Monte Carlo simulation).
 */
Vector3L FieldMap::GetField(Vector3L& v)
{
  FieldMapPoint *points[8];
  double x_refpoint, y_refpoint, z_refpoint, xfrac, yfrac, zfrac;
  double next_x_gridpoint, next_y_gridpoint, next_z_gridpoint;
 long double B_A, B_B, B_C, B_D, B_E, B_F, Fx,Fy,Fz;

  GetBoundingPoints(v, points);
  x_refpoint = points[0]->pos.x();
  next_x_gridpoint = points[1]->pos.x();
  y_refpoint = points[0]->pos.y();
  next_y_gridpoint = points[2]->pos.y();
  z_refpoint = points[0]->pos.z();
  next_z_gridpoint = points[4]->pos.z();
  
  
  xfrac = (v.x() - x_refpoint)/(next_x_gridpoint-x_refpoint);
  yfrac = (v.y() - y_refpoint)/(next_y_gridpoint-y_refpoint);
  zfrac = (v.z() - z_refpoint)/(next_z_gridpoint-z_refpoint);

  B_A = (1-xfrac)*points[0]->val.x()  +  xfrac *points[1]->val.x(); 
  B_B = (1-xfrac)*points[2]->val.x()  +  xfrac *points[3]->val.x(); 
  B_C = (1-xfrac)*points[4]->val.x()  +  xfrac *points[5]->val.x(); 
  B_D = (1-xfrac)*points[6]->val.x()  +  xfrac *points[7]->val.x(); 
  
  B_E = (1-yfrac)*B_A + yfrac*B_B;
  B_F = (1-yfrac)*B_C + yfrac*B_D;
  
  Fx  = (1-zfrac)*B_E + zfrac*B_F;

  B_A = (1-xfrac)*points[0]->val.y()  +  xfrac *points[1]->val.y(); 
  B_B = (1-xfrac)*points[2]->val.y()  +  xfrac *points[3]->val.y(); 
  B_C = (1-xfrac)*points[4]->val.y()  +  xfrac *points[5]->val.y(); 
  B_D = (1-xfrac)*points[6]->val.y()  +  xfrac *points[7]->val.y(); 
  
  B_E = (1-yfrac)*B_A + yfrac*B_B;
  B_F = (1-yfrac)*B_C + yfrac*B_D;
  
  Fy = (1-zfrac)*B_E + zfrac*B_F;  
    
  B_A = (1-xfrac)*points[0]->val.z()  +  xfrac *points[1]->val.z(); 
  B_B = (1-xfrac)*points[2]->val.z()  +  xfrac *points[3]->val.z(); 
  B_C = (1-xfrac)*points[4]->val.z()  +  xfrac *points[5]->val.z(); 
  B_D = (1-xfrac)*points[6]->val.z()  +  xfrac *points[7]->val.z(); 
  
  B_E = (1-yfrac)*B_A + yfrac*B_B;
  B_F = (1-yfrac)*B_C + yfrac*B_D;
  
  Fz  = (1-zfrac)*B_E + zfrac*B_F;  
  
	return Vector3L(Fx,Fy,Fz);
}

#endif
Vector3L FieldMap::GetField(Vector3L& v)
{
	//std::cout << "Getting field at " << v << std::endl;

    double d = 0;               // Denominator of field at position
    Vector3L D(0,0,0);          // Vector 'numerator' of field at position
    Vector3L dpart(0,0,0);      // Contribution of point to the vector 'numerator' of field at position
    FieldMapPoint *points[8];
    GetBoundingPoints(v, points);
    
    /*for (int i = 0; i < 8; i++)
		{
			std::cout << "Point " << i << " is " << points[i]->val << " at " << points[i]->pos << std::endl;
		}
    */
    
    for (int k = 0; k<8; k++) {
        if ((v-points[k]->pos).Mag()==0) {
            // If position is at a point, then its field value is the same as the field at the point
            // Otherwise would have zero distance between position and point => inf
            D = points[k]->val;
            d = 1;
            break;
        }
        d += 1/((v-points[k]->pos).Mag());
        dpart = points[k]->val/((v-points[k]->pos).Mag());
        D += dpart;
    }
    //std::cout << D << " " << d <<std::endl;
	return D/d;
}
