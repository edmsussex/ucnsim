/*
======================================================================
STARucn MC software
File : TextOutputInterface.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class TextOutputInterface                           //
//                                                          //
// OutputInterface plugin for root file event output        //
//                                                          //
//                                                          //
//////////////////////////////////////////////////////////////

#include "TextOutputInterface.h"
#include <algorithm>
#include <iomanip>
#include "TMath.h"
////////////////////////////////////////////////////////////////////////
TextOutputInterface::TextOutputInterface() : BaseOutputInterface()
////////////////////////////////////////////////////////////////////////
{
fTree=0;
fFile=0;
}

////////////////////////////////////////////////////////////////////////
TextOutputInterface::~TextOutputInterface() {;}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
void TextOutputInterface::Configure(const char* file)
////////////////////////////////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);

std::string outfile = fParam.GetString("TextOutputInterface.OutputFile");
std::string mode    = fParam.GetString("TextOutputInterface.StopMode");
// std::cout << "in [TextOutputInterface], filename = " << outfile << std::endl;

if(mode=="Time")   
  {
  kMode = 1;
  fStopTime = fParam.GetDouble("TextOutputInterface.StopTime");
  }
if(mode=="Volumes") 
  {
  kMode = 2;
  std::string tmp = fParam.GetString("TextOutputInterface.FinalVolumes");
  ParameterReader::ReadString(fFinalVolumes,tmp);
  }
if(mode=="Interact") 
  {
  kMode = 3;
  kStopInteract = fParam.GetInt("TextOutputInterface.NbInteract");
  }
if(fParam.GetBool("TextOutputInterface.DoRootTree")) 
  {
  fFile = new TFile((outfile+".root").c_str(),"recreate");
  fTree=new TNtupleD("starucn","starucn","x:y:z:vx:vy:vz:t");
  }

fTextFile.open(outfile.c_str());
fTextFile << std::setprecision(15);
}

////////////////////////////////////////////////////////////////////////
void TextOutputInterface::SaveStep(Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)
////////////////////////////////////////////////////////////////////////
{
vol       = 0;
previous  = 0;
bool end = false;
if(N.Dead()) return;
if     (kMode==1)
  { 
  if(N.GetTime()>fStopTime) 
     {
 // repropagate neutron
     N = fOld;
     double dt = fStopTime-fOld.GetTime();
     fCore->fPropagator->Propagate(dt,N);
     end = true; 
     }
  }
else if(kMode==2){ if(std::find(fFinalVolumes.begin(), fFinalVolumes.end(), interface->GetName())!=fFinalVolumes.end() || fFinalVolumes.size()==0) end = true;}
else if(kMode==3){ if(kCnt==kStopInteract) end = true; kCnt++;} 
if(end)
  {
  double v = N.GetSpeed();
  if(!TMath::IsNaN(N.GetPosition(0))&&!TMath::IsNaN(v))
    {
    for(int i=0; i<3;i++) fTextFile << N.GetPosition(i) << " \t " ;
    for(int i=0; i<3;i++) fTextFile << N.GetDirection(i)*v << " \t " ;
    fTextFile << N.GetTime() <<  "\t " << int(N.Active())  << std::endl;
    if(fTree && N.Active()) fTree->Fill(N.GetPosition(0),N.GetPosition(1),N.GetPosition(2),N.GetDirection(0)*v,N.GetDirection(1)*v,N.GetDirection(2)*v,N.GetTime());
    }
  N.Kill();
  }
fOld = N;
}

////////////////////////////////////////////////////////////////////////
void TextOutputInterface::EndParticle (Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)     
////////////////////////////////////////////////////////////////////////
{
if(kMode==1 && !N.Dead() && !N.Active()) // dump (unused) inactive particles (init time > current time) for later use
  {
  double v = N.GetSpeed();
  for(int i=0; i<3;i++) fTextFile << N.GetPosition(i) << " \t " ;
  for(int i=0; i<3;i++) fTextFile << N.GetDirection(i)*v << " \t " ;
  fTextFile << N.GetTime() <<  "\t " << int(N.Active()) << std::endl;
//   if(fTree) fTree->Fill(N.GetPosition(0),N.GetPosition(1),N.GetPosition(2),N.GetDirection(0)*v,N.GetDirection(1)*v,N.GetDirection(2)*v,N.GetTime());
  N.Kill();
  }
previous  = 0;
interface=0;
vol = 0;
sur=0;
}

////////////////////////////////////////////////////////////////////////
void TextOutputInterface::Finalize()
////////////////////////////////////////////////////////////////////////            
{
if(fTree) {fFile->cd(); fTree->Write();fFile->Close();}
fTextFile.close();
}
