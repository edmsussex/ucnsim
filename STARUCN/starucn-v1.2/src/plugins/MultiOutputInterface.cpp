/*
======================================================================
STARucn MC software
File : MultiOutputInterface.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
//////////////////////////////////////////////////////////////
//                                                          //
// Class MultiOutputInterface		                        //
//                                                          //
// OutputInterface plugin for root file event output        //
//                                                          //
//                                                          //
//////////////////////////////////////////////////////////////

#include "MultiOutputInterface.h"
#include "TH1F.h"
#include "TClass.h"

////////////////////////////////////////////////////////////////////////
MultiOutputInterface::MultiOutputInterface() : BaseOutputInterface()
////////////////////////////////////////////////////////////////////////
{
fInterfaces  = 0;
kSize =0;
}

////////////////////////////////////////////////////////////////////////
MultiOutputInterface::~MultiOutputInterface() {;}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
void MultiOutputInterface::Configure(const char* file)
////////////////////////////////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);
std::string outitype, outiconfig;
kSize = fParam.GetInt("MultiOutputInterface.NbOutputs");
fInterfaces = new BaseOutputInterface*[kSize];
for(int i=0; i<kSize; i++)
  {
  fParam.GetParameter(outitype  ,"mcsetup",Form("MultiOutputInterface.Output_%d",i+1),"class");
  fParam.GetParameter(outiconfig,"mcsetup",Form("MultiOutputInterface.Output_%d",i+1),"config");
  fInterfaces[i] = static_cast<BaseOutputInterface*>(TClass::GetClass(TString(outitype.c_str()))->New());
  fInterfaces[i]->Configure(outiconfig.c_str());
  }
}

////////////////////////////////////////////////////////////////////////
void MultiOutputInterface::SaveStep(Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)
////////////////////////////////////////////////////////////////////////
{
for(int i=0; i<kSize;i++) fInterfaces[i]->SaveStep( N,vol,sur,previous,  interface);
}


////////////////////////////////////////////////////////////////////////
void MultiOutputInterface::BeginParticle (Particle& N)     
////////////////////////////////////////////////////////////////////////
{
for(int i=0; i<kSize;i++) fInterfaces[i]->BeginParticle(N);
}


////////////////////////////////////////////////////////////////////////
void MultiOutputInterface::EndParticle (Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)     
////////////////////////////////////////////////////////////////////////
{
for(int i=0; i<kSize;i++) fInterfaces[i]->EndParticle(N,vol, sur, previous, interface);
}

////////////////////////////////////////////////////////////////////////
void MultiOutputInterface::Finalize()
////////////////////////////////////////////////////////////////////////            
{
for(int i=0; i<kSize;i++) fInterfaces[i]->Finalize();
}

          
          
