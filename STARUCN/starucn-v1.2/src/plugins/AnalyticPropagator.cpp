/*
======================================================================
STARucn MC software
File : AnalyticPropagator.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "AnalyticPropagator.h"
#include <sstream>
#include <string>

///////////////////////////////////////////////////////
AnalyticPropagator::AnalyticPropagator() : StepPropagator()
///////////////////////////////////////////////////////
{
fShadow = new ShadowGeometry(fCore->fGeo);
}

///////////////////////////////////////////////////////
AnalyticPropagator::~AnalyticPropagator()
///////////////////////////////////////////////////////
{;}


////////////////////////////////////////////////////////////////////////
void AnalyticPropagator::Configure(const char* file)
////////////////////////////////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);
std::ostringstream str;
fTolerance   = fParam.GetDouble("AnalyticPropagator.Tolerance");
fStep        = fParam.GetDouble("AnalyticPropagator.StepSize");
kUseGravity  = fParam.GetBool  ("AnalyticPropagator.UseGravity");
}

////////////////////////////////////////////////////////////////////////
double AnalyticPropagator::PropagateToSurface(Particle& n, double& Dt, double* Norm, TGeoVolume* &next, TPolyLine3D* poly)
////////////////////////////////////////////////////////////////////////
{
// Check if the volume pocess a shadow
ShadowVolume* vol = fShadow->GetShadow(fCurrentVolume);
//no shadow, fall back to step propagator
if(!vol) return StepPropagator::PropagateToSurface(n, Dt, Norm, next, 0);
poly = 0;
//shadow found : use propation into shadow geometry
double * pos = n.GetPosition();
double * dir = n.GetDirection();
double v     = n.GetSpeed();
fX[0] = 0;
fX[1] = dir[0]*v; 
fX[2] = pos[0]  ; 
fY[0] = 0;
fY[1] = dir[1]*v; 
fY[2] = pos[1]  ; 
fZ[0] = kUseGravity ? -4903. : 0.;
fZ[1] = dir[2]*v;
fZ[2] = pos[2];
Dt = vol->ClosestIntersect(fX,fY,fZ);
Propagate(Dt, n);
double t = 1e-1*fTolerance/n.GetSpeed();
if(Norm) 
  {
  Norm[0]=fX[4];
  Norm[1]=fY[4];
  Norm[2]=fZ[4];
  }
//std::cout << "pos interf : " ;n.PrintPosition(std::cout); std::cout << "   *** " << fX[3] << " " << fY[3] << " " << fZ[3] << " " <<std::endl;
Propagate(t, n);
next = fCore->fGeo->FindVolume(n.GetPosition());
//std::cout << "pos beyond : " ;n.PrintPosition(std::cout); std::cout << std::endl;
Propagate(-2*t, n);
//std::cout << "pos inside : " ;n.PrintPosition(std::cout); std::cout << std::endl;
return 2*t;
}// Particle is inside the volume close to the interface, a step of timetonextvolume will bring it ot the other side
