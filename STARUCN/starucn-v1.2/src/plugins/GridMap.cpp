#include <cstdio>
#include "GridMap.h"


GridMap::GridMap()
{
}

GridMap::~GridMap()
{

}

void GridMap::Configure(const char *file)
{
std::string par;
Vector3L origin(0,0,0);

if(file!=0) SetParameterFile(file);

par = fieldName;
par += ".SourceFile";

std::cout << "Looking for parameter " << par;

sourceFile = fParam.GetString(par);
std::cout << ", found " << sourceFile << std::endl;
/* probably read the origin of the field map from the configure
 * file at some point
 */
Init(origin);

Field::Configure(file);
}


int GridMap::Init(Vector3L origin)
{
	std::cout << "Initialising Grid Map" << std::endl;
	int number_x_points=0, number_y_points=0, number_z_points = 0;
	double x=0,y=0,z=0, Fx=0,Fy=0,Fz=0;
	FILE *map_file;
	if (!(map_file = fopen(sourceFile.c_str(),"r"))) 
    {
	  std::cout << "Error opening map file named " << sourceFile << std::endl;
      return 1;
    }
  
	if (!fscanf(map_file,"%d\t%d\t%d\r",&number_x_points,&number_y_points,&number_z_points))
		std::cout << "Error reading field map file" << std::endl;  
	
	std::cout << "Grid should be " << number_x_points << "x" << number_y_points << "x" << number_z_points << std::endl;
	
	grid.resize(number_x_points);
	
	for (int i=0; i<number_x_points; i++){
    grid[i].resize(number_y_points);
    for (int j=0; j<number_y_points; j++){
      grid[i][j].resize(number_z_points);
      for (int k=0; k<number_z_points; k++){
		   if (!fscanf(map_file,"%lf\t%lf\t%lf\t%le\t%le\t%le\r",
						&x,&y,&z,&Fx,&Fy,&Fz))
				std::cout << "Error reading field map file" << std::endl;
	       grid[i][j][k].pos = Vector3L(x,y,z) + origin;
	       grid[i][j][k].val = Vector3L(Fx,Fy,Fz);
	       //std::cout << "pos is: " << grid[i][j][k].pos << ", field is " << grid[i][j][k].val << std::endl;
		}
    }
  }
  fclose(map_file);	
  return 0;
}


int GridMap::GetBoundingPoints(Vector3L& pos, FieldMapPoint **points)
{
	
	int cache_i = 0, cache_j = 0, cache_k = 0;
	long double x,y,z;
	x = pos.x();
	y = pos.y();
	z = pos.z();
	
	try {
		while(x<grid.at(cache_i)[0][0].pos.x()) cache_i--; 
		while(x>grid.at(cache_i+1)[0][0].pos.x())	cache_i++;
		
		while(y<grid[0].at(cache_j)[0].pos.y())cache_j--;
		while(y>grid[0].at(cache_j+1)[0].pos.y())cache_j++;

		while(z<grid[0][0].at(cache_k).pos.z())cache_k--;
		while(z>grid[0][0].at(cache_k+1).pos.z())cache_k++;

		int c = 0;
		for (int k = 0; k < 2; k++)
			for (int j = 0; j < 2; j++)
				for (int i = 0; i < 2; i++)
					points[c++] = &grid.at(cache_i + i).at(cache_j + j).at(cache_k + k);
		return c; /* return number of surrounding points stored */
	}

	catch (const std::out_of_range& oor) {
		std::cerr << "Out of Range Error " << oor.what() << std::endl;
		std::cerr << "This is probably because the neutron was outside of the specified grid map" << std::endl;
		std::cerr << "Neutron was at " << pos << ", Field map range is: " << grid.front().front().front().pos << " to " << grid.back().back().back().pos << std::endl;
		return 0;
	}

}

