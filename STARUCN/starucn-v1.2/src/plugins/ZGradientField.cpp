#include "ZGradientField.h"
#undef DEBUG

#ifdef DEBUG
	//#include <string>
#endif

ZGradientField::ZGradientField()
{
	fieldName = "ZGradientField";
}

ZGradientField::~ZGradientField()
{
}

Vector3L ZGradientField::GetField(Vector3L& pos)
{
	#ifdef DEBUG
		Vector3L position = pos;
		Vector3L field = Vector3L(F0.x()-((pos.x()/2)*dFdz),F0.y()-((pos.y()/2)*dFdz),F0.z()+(pos.z()*dFdz));
		std::cout << "got field of " << field << " at position " << position << std::endl;
		//std::string dummystring;
		//std::getline(std::cin,dummystring);
	#endif
	return Vector3L(F0.x()-((pos.x()/2)*dFdz),F0.y()-((pos.y()/2)*dFdz),F0.z()+(pos.z()*dFdz)); //Evaluates Fx,Fy,Fz at pos
}


void ZGradientField::Configure(const char *file)
{
std::cout << "Setting up ZGradientField" << std::endl;
double Fx0,Fy0,Fz0;
std::string s;
if(file!=0) SetParameterFile(file);

s = fieldName + ".Fx0";
Fx0 = fParam.GetDouble(s.c_str());
s = fieldName + ".Fy0";
Fy0 = fParam.GetDouble(s.c_str());
s = fieldName + ".Fz0";
Fz0 = fParam.GetDouble(s.c_str());

F0=Vector3L(Fx0,Fy0,Fz0);

s = fieldName + ".dFdz";
dFdz = fParam.GetDouble(s.c_str());

std::cout << "Fx0 = " << Fx0 << "Fy0 = " << Fy0 << "Fz0 = " << Fz0 << "dF/dz" << dFdz << std::endl;
Field::Configure(file);
}

