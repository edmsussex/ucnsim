#include "TimeVaryingField.h"

#undef DEBUG

TimeVaryingField::TimeVaryingField()
{
	fieldName = "TimeVaryingField";
}

TimeVaryingField::~TimeVaryingField()
{
}

Vector3L TimeVaryingField::GetField(Vector3L& pos, double time)
{
	#ifdef DEBUG
		std::cout << "TimeVaryingField is getting field at " << pos << ", t=" << time << std::endl;
	#endif
	
	Vector3L F(
		xformula->Eval(pos.x(),pos.y(),pos.z(),time),
		yformula->Eval(pos.x(),pos.y(),pos.z(),time),
		zformula->Eval(pos.x(),pos.y(),pos.z(),time));
	
	#ifdef DEBUG
		std::cout << "TimeVaryingField " << fieldName << " is returning field = " << F << std::endl;
	#endif
	
	
	return F;
}

Vector3L TimeVaryingField::GetField(Vector3L& pos)
{
	std::cout << "Warning! Time independant version of TimeVaryingField::GetField called!" << std::endl;
	return fieldValue;
}

void TimeVaryingField::Configure(const char *file)
{

#ifdef DEBUG

	std::cout << "looking for things called " << fieldName << ".x/.y/.z in the parameters file" << std::endl;

#endif	

double x,y,z;
std::string s;
if(file!=0) SetParameterFile(file);

s = fieldName + ".x";
x = fParam.GetDouble(s.c_str());
s = fieldName + ".y";
y = fParam.GetDouble(s.c_str());
s = fieldName + ".z";
z = fParam.GetDouble(s.c_str());

fieldValue = Vector3L(x,y,z);

s = fieldName + ".xformula";
xformulastring = fParam.GetString(s.c_str());
s = fieldName + ".yformula";
yformulastring = fParam.GetString(s.c_str());
s = fieldName + ".zformula";
zformulastring = fParam.GetString(s.c_str());

std::cout << "Setting up TimeVaryingField called " << fieldName << std::endl;
std::cout << "x formula = " << xformulastring << std::endl;
std::cout << "y formula = " << yformulastring << std::endl;
std::cout << "z formula = " << zformulastring << std::endl;

xformula = new TFormula("xformula",xformulastring.c_str());
yformula = new TFormula("yformula",yformulastring.c_str());
zformula = new TFormula("zformula",zformulastring.c_str());

Field::Configure(file);

}
