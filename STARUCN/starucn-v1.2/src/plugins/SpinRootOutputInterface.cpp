#include "SpinRootOutputInterface.h"
#include "LoopCounter.h"
#include <vector>
#include <algorithm>
#include <fstream>
#include "DoubleWithError.h"
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cstring>
#include "TKey.h"
#include "TTree.h"
#include "TBranch.h"
#include "CoreProcess.h"

#undef DEBUG
#undef KILL_ON_FIRST_COLLISION

SpinRootOutputInterface::SpinRootOutputInterface() : BaseOutputInterface()
{
	LineRecord = 0;
	fOutput = 0;
	fTree = 0;
	MNEUTRON = 1.674927211e-027;
	UseBootstrapErrors = 0;
	nBootstrap = 0;
}

SpinRootOutputInterface::~SpinRootOutputInterface()
{
	//std::cout << "Closing fOutput (SpinRootOutputInterface class deconstructor) " << std::endl;
	//fOutput->Close();
}


void SpinRootOutputInterface::SaveStep(Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface)
{
bool end = 0;
if(N.Dead()) return;
#ifdef KILL_ON_FIRST_COLLISION
  N.Kill();
  SpinRootOutputInterface::SaveParticle (N, vol, sur, previous, interface, end);
#endif
//vol       = 0;
//previous  = 0;
//interface = 0;
// only counting
if(sur==0) N.kTran++;
if(sur==2) N.kSpec++;
if(sur==3) N.kDiff++;               
N.kStep++;
// if(Line3D) fLine3D.SetNextPoint(N.GetPosition());
//std::cout << "neutron elapsed time = " << N.GetTime() << std::endl;
if(N.GetTime()>fStopTime) 
	{
 // repropagate neutron
	#ifdef DEBUG
		std::cout << "unpropagating neutron to finish time" << std::endl;
	#endif
	//N = fOld;
	double dt = fStopTime-N.GetTime();
	fCore->fPropagator->Propagate(dt,N);
	end = true;
	N.Kill();
	}
#ifndef ALWAYS_SAVE_NEUTRON
	if (N.Dead()) SpinRootOutputInterface::SaveParticle (N, vol, sur, previous, interface, end);
#endif

#ifdef ALWAYS_SAVE_NEUTRON
	SpinRootOutputInterface::SaveParticle (N, vol, sur, previous, interface, end);
#endif

return;
}


void SpinRootOutputInterface::EndParticle (Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface){
	if(!N.Dead())SaveParticle (N, vol, sur, previous, interface, 1);
}


void SpinRootOutputInterface::Configure(const char* file)
{
	std::cout << "Configuring SpinRootOutputInterface..." << std::endl;
	if(file!=0) SetParameterFile(file);
	
	UseBootstrapErrors = fParam.GetBool("SpinRootOutputInterface.UseBootstrapErrors");
	nBootstrap         = fParam.GetInt ("SpinRootOutputInterface.nBootstrap");
	if (UseBootstrapErrors) std::cout << "Using " << nBootstrap << " Bootstrap Samples to calculate errors" << std::endl;
	else                    std::cout << "Bootstrap error calculation disabled" << std::endl;
	
	//used to name output file + TNtupleD
	std::string baseoutfile = fParam.GetString("SpinRootOutputInterface.BaseOutputFile");
	runid = fParam.GetString("SpinRootOutputInterface.RunID");
	runsection = fParam.GetInt("SpinRootOutputInterface.RunSection");
	runtimeslice = fParam.GetInt("SpinRootOutputInterface.TimeSlice");
	
	
	std::cout << "run id = " << runid << ", run section = " << runsection << ", time slice = " << runtimeslice << std::endl;
	
	
	//convert int to string
	char runsection_c_str[50];
	char runtimeslice_c_str[50];
	std::sprintf (runsection_c_str,"%d",runsection);
	std::sprintf (runtimeslice_c_str,"%d",runtimeslice);
	std::string runsectionstring = runsection_c_str;
	std::string runtimeslicestring = runtimeslice_c_str;
	
	//calculate filenames
	std::string outfile = baseoutfile + "_" + runid + "_" + runsectionstring + ".root";
	std::string tuplename = "rawdata_" + runid + "_" + runsectionstring + "_" + runtimeslicestring;
	
	//These two are used to tag output for easier display
	OuterLoopVariableName = fParam.GetString("SpinRootOutputInterface.OuterLoopVariableName");
	OuterLoopVariableValue = fParam.GetDouble("SpinRootOutputInterface.OuterLoopVariableValue");
	
	LineRecord   = fParam.GetInt("SpinRootOutputInterface.TrajectoryRecord");
	fStopTime = fParam.GetDouble("SpinRootOutputInterface.StorageTime");
	std::string tmp = fParam.GetString("SpinRootOutputInterface.TrajectoryRecordFinalVolume");
	ParameterReader::ReadString(fFinalVolumes,tmp);
	
	
	std::cout << "Output file name = " << outfile << ", Output Tree Name = " << tuplename << std::endl;
	
	
	//Create Output file
	//fOutput = new TFile(outfile.c_str(), "update");
	fCore->OpenRootFile(outfile);
	fTree = new TNtupleD(tuplename.c_str(),tuplename.c_str(),"id:nstep:NbSpecular:NbDiffuse:NbTransmitted:FinalStatus:x:y:z:vx:vy:vz:V:Sx:Sy:Sz:SxRevE:SyRevE:SzRevE:Phase:PhaseRevE:Theta:ThetaErr:ThetaRevE:ThetaErrRevE:TrueFreq:TrueFreqRevE:FreqDiff:FalseEDM:Ex:Ey:Ez:E:t:xinit:yinit:zinit:vxinit:vyinit:vzinit:Vinit:Sxinit:Syinit:Szinit:Thetainit:Phaseinit:SxRevEinit:SyRevEinit:SzRevEinit:ThetaRevEinit:PhaseRevEinit:Exinit:Eyinit:Ezinit:Einit:Tinit:idLastVolume:idLastInterface:Runsection:ParticleSeed");
	if(LineRecord>0)
	{if(kId%LineRecord==0) {fLine3D.Reset(); Line3D = &fLine3D;}
	else {Line3D=0;}}
	
	#ifdef DEBUG
		std::cout << "output tree pointer = " << fTree << std::endl;
	#endif
	
	cfgpath = file;
	
	fieldManager.Configure(file);
	Vector3L origin(0.,0.,0.);
	AverageEField = fieldManager.GetEField(origin); //Assumes E Field is Homogeneous!!!
	
	std::string nor = LoopCounter::TxtForm(1,7,0);
	std::string gre = LoopCounter::TxtForm(1,3,0);
	std::string blu = LoopCounter::TxtForm(1,4,0);
	std::cout << blu << "************* SpinRootOutputInterface, status documentation **************" << nor << std::endl;
	std::cout << gre << "      -1 : unknown error (should not happen)                             " << nor << std::endl;
	std::cout << gre << "       0 : particle goes outside of defined volumes (should not happen)  " << nor << std::endl;
	std::cout << gre << "       1 : particle decays in a volume                                   " << nor << std::endl;
	std::cout << gre << "       2 : particle is absorbed on a surface                             " << nor << std::endl;
	std::cout << gre << "       3 : particle is transmitted to null lifetime material             " << nor << std::endl;
	std::cout << gre << "       4 : too many rebound                                              " << nor << std::endl;
	std::cout << gre << "       5 : survived to end of storage time                               " << nor << std::endl;
	std::cout << blu << "*************************************************************************" << nor << std::endl << std::endl;

}


void SpinRootOutputInterface::BeginParticle (Particle& N)
{
	#ifdef DEBUG
		std::cout << "calling SpinRootOutputInterface::BeginParticle" << std::endl;
	#endif
	N.SetInit();
}

void SpinRootOutputInterface::SaveParticle (Particle& N, int vol, int sur, TGeoVolume* previous, TGeoVolume* interface, bool end)
{
#ifdef DEBUG
	std::cout << "calling SpinRootOutputInterface::SaveParticle" << std::endl;
#endif

/*std::cout << "Active " << Line3D << std::endl;*/
if(Line3D&&interface) 
  {
  fLine3D.SetName(Form("Trajectory_%d",kId)); 
  //fOutput->cd(); 
/*  std::cout << interface->GetName() << "  " << previous->GetName() << "  " << fFinalVolumes[0] << std::endl;*/
  if(std::find(fFinalVolumes.begin(), fFinalVolumes.end(), interface->GetName())!=fFinalVolumes.end() || fFinalVolumes.size()==0) fLine3D.Write(); 
  Line3D=0;
  }
#ifdef DEBUG
std::cout << "vol = " << vol << ", sur = " << sur << std::endl;
#endif

int status = -1;
if(N.Dead())
  {
  if     (vol==2) status=0; // meet non existing volume : should never happen
  else if(vol==1) status=1; // decay
  else if(sur==1) status=2; // absorbtion
  else if(sur==4) status=3; // transmission to zero lifetime material
  else if(end) status=5; // end of time
  else status = -1;// not possible
  }
else status = 4; // too many rebound...

double save_in_tree[60];
 int id1 = fCore->fGeo->GetVolumeId(previous);
 int id2 = fCore->fGeo->GetVolumeId(interface);

save_in_tree[0]=kId;
save_in_tree[1]=N.kStep;
save_in_tree[2]=N.kSpec;
save_in_tree[3]=N.kDiff;
save_in_tree[4]=N.kTran;
save_in_tree[5]=status;

save_in_tree[6]=N.GetPosition(0);
save_in_tree[7]=N.GetPosition(1);
save_in_tree[8]=N.GetPosition(2);

save_in_tree[9]=N.GetDirection(0);
save_in_tree[10]=N.GetDirection(1);
save_in_tree[11]=N.GetDirection(2);

save_in_tree[12]=N.GetSpeed();

save_in_tree[13]=N.spin.GetSigma().x();
save_in_tree[14]=N.spin.GetSigma().y();
save_in_tree[15]=N.spin.GetSigma().z();
save_in_tree[16]=N.spinRevE.GetSigma().x();
save_in_tree[17]=N.spinRevE.GetSigma().y();
save_in_tree[18]=N.spinRevE.GetSigma().z();

save_in_tree[19]=N.spin.GetPhase();
save_in_tree[20]=N.spinRevE.GetPhase();
save_in_tree[21]=N.spin.GetTheta();
save_in_tree[22]=N.spin.GetError();
save_in_tree[23]=N.spinRevE.GetTheta();
save_in_tree[24]=N.spinRevE.GetError();

save_in_tree[25]=(N.spin.GetPhase() - N.spinInit.GetPhase()) / (2 * M_PI * N.GetTime());
save_in_tree[26]=(N.spinRevE.GetPhase() - N.spinRevEInit.GetPhase()) / (2 * M_PI * N.GetTime());
save_in_tree[27]=save_in_tree[25] - save_in_tree[26]; //frequency shift
save_in_tree[28]=GetTrueFalseEDM(N);

save_in_tree[29]=0.5*pow(N.GetDirection(0)*N.GetSpeed()/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[30]=0.5*pow(N.GetDirection(1)*N.GetSpeed()/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[31]=0.5*pow(N.GetDirection(2)*N.GetSpeed()/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[32]=0.5*pow(N.GetSpeed()/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV

save_in_tree[33]=N.GetTime();


save_in_tree[34]=N.kX_init;
save_in_tree[35]=N.kY_init;
save_in_tree[36]=N.kZ_init;
save_in_tree[37]=N.kVx_init;
save_in_tree[38]=N.kVy_init;
save_in_tree[39]=N.kVz_init;
save_in_tree[40]=N.kV_init;

save_in_tree[41] = N.spinInit.GetSigma().x();
save_in_tree[42] = N.spinInit.GetSigma().y();
save_in_tree[43] = N.spinInit.GetSigma().z();
save_in_tree[44] = N.spinInit.GetTheta();
save_in_tree[45] = N.spinInit.GetPhase();

save_in_tree[46] = N.spinRevEInit.GetSigma().x();
save_in_tree[47] = N.spinRevEInit.GetSigma().y();
save_in_tree[48] = N.spinRevEInit.GetSigma().z();
save_in_tree[49] = N.spinRevEInit.GetTheta();
save_in_tree[50] = N.spinRevEInit.GetPhase();

save_in_tree[51]=0.5*pow(N.kVx_init*N.kV_init/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[52]=0.5*pow(N.kVy_init*N.kV_init/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[53]=0.5*pow(N.kVz_init*N.kV_init/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV
save_in_tree[54]=0.5*pow(N.kV_init/1000,2)*MNEUTRON/1.6e-19*1e9;//energy in neV

save_in_tree[55]=N.kT_init;

save_in_tree[56]=id1;
save_in_tree[57]=id2;

save_in_tree[58]=runsection;

save_in_tree[59]=N.ParticleSeed;

fTree->Fill(save_in_tree);

#ifdef DEBUG
	std::cout << "fTree pointer is " << fTree << std::endl;
#endif
N.kTran=0;
N.kSpec=0;
N.kDiff=0;
kId++;
if(LineRecord>0) if(kId%LineRecord==0) {fLine3D.Reset(); Line3D = &fLine3D;}
N.kStep=0;
}

void SpinRootOutputInterface::Finalize(){
	#ifdef DEBUG
		std::cout << "Finalize Called, " << std::endl;
	#endif
	fTree->Write();
	
	//get data from file to a vector we can pass around
	std::vector<std::vector<double>> particledata;
	
	int entries = fTree->GetEntries();
	double* row_content;
	//std::cout << "data table is " << std::endl;
	for (int i = 0; i < entries; i++) {
		fTree->GetEntry(i);
        row_content = fTree->GetArgs();
        std::vector<double> row(row_content, row_content + 60);
        particledata.push_back(row);
        //for (auto i : row) std::cout << i << " ";
        //std::cout << std::endl;
	}

	//calculate run parameters
	DoubleWithError referencephase, referencephaseRevE, alpha, alphaRevE, TrueAvgAngFreq, TrueAvgAngFreqRevE, TrueAvgAngFreqDiff, EnsembleTrueFalseEDM,
			MeasuredAvgAngFreq, MeasuredAvgAngFreqRevE, MeasuredAngFreqDiff, EnsembleMeasuredFalseEDM;

	referencephase.number           = CalculateReferencePhase(particledata);
	referencephaseRevE.number       = CalculateReferencePhaseRevE(particledata);
	alpha.number                    = GetAlpha(particledata,referencephase.number);
	alphaRevE.number                = GetAlphaRevE(particledata,referencephase.number);
	TrueAvgAngFreq.number           = GetTrueAvgAngFreq(particledata);
	TrueAvgAngFreqRevE.number       = GetTrueAvgAngFreqRevE(particledata);
	TrueAvgAngFreqDiff.number       = GetAvgAngFreqDiff(particledata);
	EnsembleTrueFalseEDM.number     = GetEnsembleFalseEDM(TrueAvgAngFreqDiff.number);
	MeasuredAvgAngFreq.number       = GetMeasuredAngFreq(TrueAvgAngFreq.number,referencephase.number);
	MeasuredAvgAngFreqRevE.number   = GetMeasuredAngFreq(TrueAvgAngFreqRevE.number,referencephaseRevE.number);
	MeasuredAngFreqDiff.number      = MeasuredAvgAngFreq.number  - MeasuredAvgAngFreqRevE.number ;
	EnsembleMeasuredFalseEDM.number = GetEnsembleFalseEDM(MeasuredAngFreqDiff.number);
	
	if (UseBootstrapErrors) {
		double bootreferencephase, bootreferencephaseRevE, bootalpha, bootalphaRevE, bootTrueAvgAngFreq, bootTrueAvgAngFreqRevE, bootTrueAvgAngFreqDiff, bootEnsembleTrueFalseEDM
				,bootMeasuredAvgAngFreq, bootMeasuredAvgAngFreqRevE, bootMeasuredAngFreqDiff, bootEnsembleMeasuredFalseEDM;
		for (int i = 0; i < nBootstrap; ++i) {
			//draw bootstrap sample
			std::vector<std::vector<double>> sample= Bootstrap(particledata);
			
			//calculate parameters for bootstrap sample
			bootreferencephase           = CalculateReferencePhase(sample);
			bootreferencephaseRevE       = CalculateReferencePhaseRevE(sample);
			bootalpha                    = GetAlpha(sample,bootreferencephase);
			bootalphaRevE                = GetAlphaRevE(particledata,bootreferencephase);
			bootTrueAvgAngFreq           = GetTrueAvgAngFreq(sample);
			bootTrueAvgAngFreqRevE       = GetTrueAvgAngFreqRevE(sample);
			bootTrueAvgAngFreqDiff       = GetAvgAngFreqDiff(sample);
			bootEnsembleTrueFalseEDM     = GetEnsembleFalseEDM(bootTrueAvgAngFreqDiff);
			bootMeasuredAvgAngFreq       = GetMeasuredAngFreq(bootTrueAvgAngFreq,bootreferencephase);
			bootMeasuredAvgAngFreqRevE   = GetMeasuredAngFreq(bootTrueAvgAngFreqRevE,bootreferencephaseRevE);
			bootMeasuredAngFreqDiff      = bootMeasuredAvgAngFreq - bootMeasuredAvgAngFreqRevE;
			bootEnsembleMeasuredFalseEDM = GetEnsembleFalseEDM(bootMeasuredAngFreqDiff);
			
			//calculate standard error on each parameter, divide by n-1 to scale and accumulate
			referencephase.error           += (bootreferencephase - referencephase.number) * (bootreferencephase - referencephase.number) / (nBootstrap-1);
			referencephaseRevE.error       += (bootreferencephaseRevE - referencephaseRevE.number) * (bootreferencephaseRevE - referencephaseRevE.number) / (nBootstrap-1);
			alpha.error                    += (bootalpha - alpha.number) * (bootalpha - alpha.number) / (nBootstrap-1);
			alphaRevE.error                += (bootalphaRevE - alphaRevE.number) * (bootalphaRevE - alphaRevE.number) / (nBootstrap-1);
			TrueAvgAngFreq.error           += (bootTrueAvgAngFreq - TrueAvgAngFreq.number) * (bootTrueAvgAngFreq - TrueAvgAngFreq.number) / (nBootstrap-1);
			TrueAvgAngFreqRevE.error       += (bootTrueAvgAngFreqRevE - TrueAvgAngFreqRevE.number) * (bootTrueAvgAngFreqRevE - TrueAvgAngFreqRevE.number) / (nBootstrap-1);
			TrueAvgAngFreqDiff.error       += (bootTrueAvgAngFreqDiff - TrueAvgAngFreqDiff.number) * (bootTrueAvgAngFreqDiff - TrueAvgAngFreqDiff.number) / (nBootstrap-1);
			EnsembleTrueFalseEDM.error     += (bootEnsembleTrueFalseEDM - EnsembleTrueFalseEDM.number) * (bootEnsembleTrueFalseEDM - EnsembleTrueFalseEDM.number) / (nBootstrap-1);
			MeasuredAvgAngFreq.error       += (bootMeasuredAvgAngFreq - MeasuredAvgAngFreq.number) * (bootMeasuredAvgAngFreq - MeasuredAvgAngFreq.number) / (nBootstrap-1);
			MeasuredAvgAngFreqRevE.error   += (bootMeasuredAvgAngFreqRevE - MeasuredAvgAngFreqRevE.number) * (bootMeasuredAvgAngFreqRevE - MeasuredAvgAngFreqRevE.number) / (nBootstrap-1);
			MeasuredAngFreqDiff.error      += (bootMeasuredAngFreqDiff - MeasuredAngFreqDiff.number) * (bootMeasuredAngFreqDiff - MeasuredAngFreqDiff.number) / (nBootstrap-1);
			EnsembleMeasuredFalseEDM.error += (bootEnsembleMeasuredFalseEDM - EnsembleMeasuredFalseEDM.number) * (bootEnsembleMeasuredFalseEDM - EnsembleMeasuredFalseEDM.number) / (nBootstrap-1);
		}
		//take sqrt of each number to get actual S.E.
		referencephase.error           = sqrt(referencephase.error);
		referencephaseRevE.error       = sqrt(referencephaseRevE.error);
		alpha.error                    = sqrt(alpha.error);
		alphaRevE.error                = sqrt(alphaRevE.error);
		TrueAvgAngFreq.error           = sqrt(TrueAvgAngFreq.error);
		TrueAvgAngFreqRevE.error       = sqrt(TrueAvgAngFreqRevE.error);
		TrueAvgAngFreqDiff.error       = sqrt(TrueAvgAngFreqDiff.error);
		EnsembleTrueFalseEDM.error     = sqrt(EnsembleTrueFalseEDM.error);
		MeasuredAvgAngFreq.error       = sqrt(MeasuredAvgAngFreq.error);
		MeasuredAvgAngFreqRevE.error   = sqrt(MeasuredAvgAngFreqRevE.error);
		MeasuredAngFreqDiff.error      = sqrt(MeasuredAngFreqDiff.error);
		EnsembleMeasuredFalseEDM.error = sqrt(EnsembleMeasuredFalseEDM.error);
	}
	
	//print info about run
	std::cout << "Reference Phase = " << referencephase << ", Reverence Phase RevE = " << referencephaseRevE << std::endl;
	std::cout << "alpha = " << alpha << ", alpha RevE = " << alphaRevE << std::endl;
	std::cout << "True Average Angular Frequency = " << TrueAvgAngFreq << ", RevE = " << TrueAvgAngFreqRevE << std::endl;
	std::cout << "True Average Angular Frequency difference = " << TrueAvgAngFreqDiff << std::endl;
	std::cout << "Ensemble True False EDM = " << EnsembleTrueFalseEDM << std::endl;
	std::cout << "Measured Average Angular Frequency = " << MeasuredAvgAngFreq << "RevE = " << MeasuredAvgAngFreqRevE << std::endl;
	std::cout << "Ensemble Measured False EDM = " << EnsembleMeasuredFalseEDM << std::endl;
	
	#ifdef DEBUG
			std::cout << "writing results to file" << std::endl;
	#endif

	TTree* ResultsTree;
	if (runtimeslice == 0 ){
		ResultsTree = new TTree("starucn_summary","Summary of STARucn output");
		std::cout << "Creating new root tree starucn_summary" << std::endl;
	}
	else{
		ResultsTree = (TTree*)(fCore->RootFile)->Get("starucn_summary");
		std::cout << "Opening root tree starucn_summary" << std::endl;

	}
	
	
	//casts runid to Char_t* so we can save it
	const Char_t* runidcstrconst = new char[runid.size()+1]; //+1 for the null terminator
	runidcstrconst = runid.c_str();
	Char_t* runidcstr = new char[runid.size()+1]; //+1 for the null terminator
	runidcstr = const_cast<char *>(runidcstrconst);
	
	//casts outer loop variable name to Char_t so it can be saved
	const Char_t* OuterLoopVariableName_cstr_const = new char[OuterLoopVariableName.size()+1];
	OuterLoopVariableName_cstr_const = OuterLoopVariableName.c_str();
	Char_t* OuterLoopVariableName_cstr = new char[OuterLoopVariableName.size()+1];
	OuterLoopVariableName_cstr = const_cast<char *>(OuterLoopVariableName_cstr_const);
	
	
	//loads configuation file into char* so we can save it.
	std::ifstream cfgfile(cfgpath);
	cfgfile.seekg (0,cfgfile.end);
	int length = cfgfile.tellg();
	cfgfile.seekg (0,cfgfile.beg);
	Char_t* filecontents = new Char_t[length];
	cfgfile.read(filecontents,length);
	cfgfile.close();
	
	if (runtimeslice == 0) {
		//Create new branches in the Tree is this is first run time slice
		ResultsTree->Branch("RunID",runidcstr,"RunID/C");
		ResultsTree->Branch("Runsection",&runsection,"Runsection/I");
		ResultsTree->Branch("ReferencePhase",&referencephase.number,"number/D:error/D");
		ResultsTree->Branch("ReferencePhaseRevE",&referencephaseRevE.number,"number/D:error/D");
		ResultsTree->Branch("Alpha",&alpha.number,"number/D:error/D");
		ResultsTree->Branch("AlphaRevE",&alphaRevE.number,"number/D:error/D");
		ResultsTree->Branch("TrueAverageAngularFrequency",&TrueAvgAngFreq.number,"number/D:error/D");
		ResultsTree->Branch("TrueAverageAngularFrequencyRevE",&TrueAvgAngFreqRevE.number,"number/D:error/D");
		ResultsTree->Branch("EnsembleTrueFalseEDM",&EnsembleTrueFalseEDM.number,"number/D:error/D");
		ResultsTree->Branch("MeasuredAvgAngFreq",&MeasuredAvgAngFreq.number,"number/D:error/D");
		ResultsTree->Branch("MeasuredAvgAngFreqRevE",&MeasuredAvgAngFreqRevE.number,"number/D:error/D");
		ResultsTree->Branch("EnsembleMeasuredFalseEDM",&EnsembleMeasuredFalseEDM.number,"number/D:error/D");
		ResultsTree->Branch("TrueAverageAngularFrequencyDifference",&TrueAvgAngFreqDiff,"number/D:error/D");
		ResultsTree->Branch("OuterLoopVariableName",OuterLoopVariableName_cstr,"OuterLoopVariableName/C");
		ResultsTree->Branch("OuterLoopVariableValue",&OuterLoopVariableValue,"OuterLoopVariableValue/D");
		ResultsTree->Branch("ConfigFile",filecontents,"ConfigFile/C");
	}
	else {
		//set branch addresses - open existing branches
		TBranch* tmp;
		tmp =  ResultsTree->GetBranch("RunID");
		tmp->SetAddress(runidcstr);
		tmp =  ResultsTree->GetBranch("Runsection");
		tmp->SetAddress(&runsection);
		tmp =  ResultsTree->GetBranch("ReferencePhase");
		tmp->SetAddress(&referencephase);
		tmp =  ResultsTree->GetBranch("ReferencePhaseRevE");
		tmp->SetAddress(&referencephaseRevE);
		tmp =  ResultsTree->GetBranch("Alpha");
		tmp->SetAddress(&alpha.number);
		tmp =  ResultsTree->GetBranch("AlphaRevE");
		tmp->SetAddress(&alphaRevE.number);
		tmp =  ResultsTree->GetBranch("TrueAverageAngularFrequency");
		tmp->SetAddress(&TrueAvgAngFreq.number);
		tmp =  ResultsTree->GetBranch("TrueAverageAngularFrequencyRevE");
		tmp->SetAddress(&TrueAvgAngFreqRevE.number);
		tmp =  ResultsTree->GetBranch("EnsembleTrueFalseEDM");
		tmp->SetAddress(&EnsembleTrueFalseEDM.number);
		tmp =  ResultsTree->GetBranch("MeasuredAvgAngFreq");
		tmp->SetAddress(&MeasuredAvgAngFreq.number);
		tmp =  ResultsTree->GetBranch("MeasuredAvgAngFreqRevE");
		tmp->SetAddress(&MeasuredAvgAngFreqRevE.number);
		tmp =  ResultsTree->GetBranch("EnsembleMeasuredFalseEDM");
		tmp->SetAddress(&EnsembleMeasuredFalseEDM.number);
		tmp =  ResultsTree->GetBranch("TrueAverageAngularFrequencyDifference");
		tmp->SetAddress(&TrueAvgAngFreqDiff.number);
		tmp =  ResultsTree->GetBranch("OuterLoopVariableValue");
		tmp->SetAddress(&OuterLoopVariableValue);
		tmp =  ResultsTree->GetBranch("ConfigFile");
		tmp->SetAddress(filecontents);
	}
	ResultsTree->Fill();

	//write results to file, overwrite old tree
	ResultsTree->Write("", TObject::kOverwrite);
	
	#ifdef DEBUG
	std::cout << "runidcstrconst = " << runidcstrconst << ", runidcstr = " << runidcstr 
					<< ", OuterLoopVariableName_cstr = " << OuterLoopVariableName_cstr << ", OuterLoopVariableName_cstr_const = "
					<< OuterLoopVariableName_cstr_const << std::endl;
	#endif
	delete [] filecontents;
	#ifdef DEBUG
		std::cout << "Finalize Finished" << std::endl;
	#endif
	return;
}

double SpinRootOutputInterface::CalculateReferencePhase(std::vector<std::vector<double>> particledata){
	double sintot = 0, costot = 0;
	double Sx = 0, Sy = 0 , Sxy_len = 0;
	
	//fill arrays
	for (auto row : particledata){
        Sx = row[13];
        Sy = row[14];
        Sxy_len = sqrt(Sx * Sx + Sy * Sy);
		sintot += Sy/Sxy_len; //sum sin+ cos to calculate average
		costot += Sx/Sxy_len;
    }
	double referencephase = atan2(sintot,costot); //calculate reference phase
	#ifdef DEBUG
		std::cout << "reference phase is" << referencephase << std::endl;
	#endif
	return referencephase;
}
double SpinRootOutputInterface::CalculateReferencePhaseRevE(std::vector<std::vector<double>> particledata){
	double sintot = 0, costot = 0;
	double Sx, Sy, Sxy_len = 0;
	
	//fill arrays
	for (auto row : particledata){
        Sx = row[16];
        Sy = row[17];
        Sxy_len = sqrt(Sx * Sx + Sy * Sy);
		sintot += Sy/Sxy_len; //sum sin+ cos to calculate average
		costot += Sx/Sxy_len;
		//cout << sintot << ' ' << costot << ' ' << sintot/costot << ' ' << atan(sintot/costot) << std::endl;
    }
	double referencephase = atan2(sintot,costot); //calculate reference phase
	#ifdef DEBUG
		std::cout << "reference phase RevE is" << referencephase << std::endl;
	#endif
	return referencephase;
}

//calculates the false EDM of an individual particle based on phase difference - in e cm
double SpinRootOutputInterface::GetTrueFalseEDM(Particle &n){
	
	double phasediff = n.spin.GetPhase() - n.spinRevE.GetPhase();
	double phasediffovertime = phasediff / n.GetTime();
	return 100 * phasediffovertime * EDM::hbar() / (4* AverageEField.Mag() * EDM::echarge());

}

//called by Finalize() , calculates polarisation
double SpinRootOutputInterface::GetAlpha(std::vector<std::vector<double>> particledata,double referencephase){
	//Alpha Calculation detailed in Gravitational Depolarisation Paper
	double alpha = 0;
	double Sx, Sy, Sxy_len;
	double xref = std::cos(referencephase);
	double yref = std::sin(referencephase);
	int entries = particledata.size();
	
	for (auto row : particledata){
        Sx = row[13];
        Sy = row[14];
        Sxy_len = sqrt(Sx * Sx + Sy * Sy);
		alpha += (xref * Sx + yref * Sy)/Sxy_len;
    }
	//calculate average 
	alpha /= entries;
	#ifdef DEBUG
		std::cout << "alpha = " << alpha << std::endl;
	#endif
	return alpha;
}

double SpinRootOutputInterface::GetAlphaRevE(std::vector<std::vector<double>> particledata,double referencephase){
	//Alpha Calculation detailed in Gravitational Depolarisation Paper
	double alpha = 0;
	double Sx, Sy, Sxy_len;
	double xref = std::cos(referencephase);
	double yref = std::sin(referencephase);
	int entries = particledata.size();
	
	for (auto row : particledata){
        Sx = row[16];
        Sy = row[17];
        Sxy_len = sqrt(Sx * Sx + Sy * Sy);
        alpha += (xref * Sx + yref * Sy)/Sxy_len;
    }
	//calculate average 
	alpha /= entries;
	#ifdef DEBUG
		std::cout << "alpha = " << alpha << std::endl;
	#endif
	return alpha;
}

double SpinRootOutputInterface::GetTrueAvgAngFreq(std::vector<std::vector<double>> particledata){
	double avgangfreq = 0;
	double phase, time;
	int entries = particledata.size();
	for (auto row : particledata){      
		phase = row[19];
		time  = row[33];
		//std::cout << "phase = " << phase << ", time = " << time << std::endl;
		avgangfreq += phase/time;
    }
	avgangfreq /= entries;
	
	#ifdef DEBUG
		std::cout << "average ang freq = " << avgangfreq << std::endl;
	#endif
	return avgangfreq;
}

double SpinRootOutputInterface::GetTrueAvgAngFreqRevE(std::vector<std::vector<double>> particledata){
	double avgangfreq = 0;
	double phase, time;
	int entries = particledata.size();
	
	for (auto row : particledata){
		phase = row[20]; //phaseRevE
		time  = row[33]; //n.GetTime()
		avgangfreq += phase/time;
    }
	avgangfreq /= entries;
	
	#ifdef DEBUG
		std::cout << "average ang freq RevE = " << avgangfreq << std::endl;
	#endif
	
	return avgangfreq;
	
}

double SpinRootOutputInterface::GetAvgAngFreqDiff(std::vector<std::vector<double>> particledata){
	double avgangfreqdiff = 0;
	double phase, phaseRevE, time;
	int entries = particledata.size();
	
	for (auto row:particledata){
        phase = row[19];
		phaseRevE = row[20];
		time  = row[33];
		avgangfreqdiff += (phase-phaseRevE)/time;
    }
	avgangfreqdiff /= entries;
	
	#ifdef DEBUG
		std::cout << "average ang freq diff = " << avgangfreqdiff << std::endl;
	#endif

	return avgangfreqdiff;
}

double SpinRootOutputInterface::GetEnsembleFalseEDM(double AvgAngFreqDiff){
	double result;
	result = 100 * AvgAngFreqDiff * EDM::hbar() / (4* AverageEField.Mag() * EDM::echarge());
	
	#ifdef DEBUG
		std::cout << "GetEnsembleFalseEDM returned " << result << std::endl;
	#endif
	return result;
}

//Returns Average Measured Frequency of Ensemble - see grav depolarisation paper
double SpinRootOutputInterface::GetMeasuredAngFreq(double TrueAvgFreq, double referencephase){
	double result;
	long double integrated_avg_phase, rads_turned;
	long int n_turns;

	integrated_avg_phase = TrueAvgFreq * fStopTime;
	n_turns = integrated_avg_phase/2/M_PI; // this is an int, hence truncated
	rads_turned = n_turns * 2 * M_PI + referencephase;  //
	if ((rads_turned - integrated_avg_phase)< -M_PI) rads_turned += 2*M_PI;
	if ((rads_turned - integrated_avg_phase)> M_PI) rads_turned -= 2*M_PI;

	result = (rads_turned /fStopTime) ;
	
	#ifdef DEBUG
		std::cout << "measured ang freq = " << result.number << " +/- " << result.error << " rad / s" << std::endl;
	#endif
	
	return result;
}
//Returns a bootstrapped dataset
std::vector<std::vector<double>> SpinRootOutputInterface::Bootstrap(std::vector<std::vector<double>> data) {
	std::vector<std::vector<double>> new_sample;
	int n = data.size();
	
	for (int i = 0; i < int(n/2); i++) {
		int a = Gen.Rndm() * int(n/2)*2;  //casting to int rounds down, returns int in (0,n-1)
		new_sample.push_back(data[a]);
		a = Gen.Rndm() * (int(n/2)*2-1);  //casting to int rounds down, returns int in (0,n-1)
		new_sample.push_back(data[a]);
	}
	return new_sample;
}
