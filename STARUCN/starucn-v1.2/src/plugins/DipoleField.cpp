#include <iostream>
#include <string>
#include "DipoleField.h"
#include "EDM.h"

DipoleField::DipoleField()
{
	fieldName = "DipoleField";
}

DipoleField::~DipoleField()
{
}

Vector3L DipoleField::GetField(Vector3L& position)
{
	Vector3L r = (position - pos)/1000; //convert from mm to m
	EDM::real R = r.Mag();
	Vector3L secondterm = mdm/(R*R*R); // you can't do Vector3L + Vector3L for some silly reason
	Vector3L &ref = secondterm; //this is a workaround
	Vector3L B = ((r*mdm.Dot(r)*3./(R*R*R*R*R)) - ref) * (EDM::mu0()/(4.*M_PI)); //standard dipole formula
	//std::cout << "at pos " << position << ",B = " << B << std::endl;
	return B;
}


void DipoleField::Configure(const char *file)
{
#define DEBUG
#ifdef DEBUG

	std::cout << "looking for things called " << fieldName << ".whatever" << std::endl;

#endif	

double x,y,z;
std::string s;
if(file!=0) SetParameterFile(file);

s = fieldName + ".posx";
x = fParam.GetDouble(s.c_str());
s = fieldName + ".posy";
y = fParam.GetDouble(s.c_str());
s = fieldName + ".posz";
z = fParam.GetDouble(s.c_str());

pos = Vector3L(x,y,z);

s = fieldName + ".mdmx";
x = fParam.GetDouble(s.c_str());
s = fieldName + ".mdmy";
y = fParam.GetDouble(s.c_str());
s = fieldName + ".mdmz";
z = fParam.GetDouble(s.c_str());

mdm = Vector3L(x,y,z);

std::cout << "Position = " << pos << ", mdm = " << mdm << std::endl;

Field::Configure(file);
}


