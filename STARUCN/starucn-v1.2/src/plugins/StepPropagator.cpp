/*
======================================================================
STARucn MC software
File : StepPropagator.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "StepPropagator.h"
#include <iomanip>
////////////////////////////////////////////////////////////////////////
StepPropagator::StepPropagator()
////////////////////////////////////////////////////////////////////////
{
fTolerance = 1e-6;
}

////////////////////////////////////////////////////////////////////////
StepPropagator::~StepPropagator()
////////////////////////////////////////////////////////////////////////
{
}

////////////////////////////////////////////////////////////////////////
void StepPropagator::Configure(const char* file)
////////////////////////////////////////////////////////////////////////
{
if(file!=0) SetParameterFile(file);
fTolerance = fParam.GetDouble("StepPropagator.Tolerance");
fStep      = fParam.GetDouble("StepPropagator.StepSize");
kUseGravity= fParam.GetBool  ("StepPropagator.UseGravity");
}

////////////////////////////////////////////////////////////////////////
bool StepPropagator::Propagate(double dt, Particle& n)
////////////////////////////////////////////////////////////////////////
{
double V = n.GetSpeed();
double* gloD = n.GetDirection();
double* gloP = n.GetPosition();
double  tmp[3];
if(kUseGravity)
  {
  double nV=0;
  for(int i=0; i<3;i++)
    {
    double x0 = gloP[i];
    double v0 = gloD[i]*V;
    gloP[i] = x0+v0*dt-(i==2 ? 4903.*dt*dt : 0);
    tmp [i] = v0-(i==2 ? 9806.*dt : 0);
    nV+=tmp[i]*tmp[i];
    }
  nV = sqrt(nV);
  n.SetDirection(tmp);
  n.SetSpeed(nV);
  }
else
  {
  for(int i=0; i<3;i++)
    {
    double x0 = n.GetPosition(i);
    double v0 = n.GetDirection(i)*V;
    gloP[i] = x0+v0*dt;
    } 
  }
n.AddTime(dt);
Evolve(dt, n);
fCurrentNode->MasterToLocal(gloP, tmp);
return fCurrentVolume->Contains(tmp);
}
    
////////////////////////////////////////////////////////////////////////
double StepPropagator::PropagateToSurface(Particle& n, double& Dt, double* Norm, TGeoVolume* &next, TPolyLine3D* poly)
////////////////////////////////////////////////////////////////////////
{
double Lstep  = fStep;  // try something based on fCurrentShape->DistFromInside()*0.51;
if(!fCurrentNode) 
  {
  next=0;
  return 0;
  }
if(fStep<=0) // try adaptative step
  {
  double locP[3];
  double locD[3];

  fCurrentNode->MasterToLocal(n.GetPosition() ,locP);
  fCurrentNode->MasterToLocalVect(n.GetDirection(),locD);
  Lstep = -fStep*0.66*fCurrentShape->DistFromInside(locP, locD, 3, 0, 0);
  if(Lstep==0)  Lstep = -fStep*0.1;
  }
Dt = 0;
double tstep =0;
double iniStep = Lstep;
do{
  tstep = Lstep/n.GetSpeed();
  if(!Propagate(tstep, n)) 
    {
    Propagate(-tstep, n);//return to previous 
    Lstep = 0.5*Lstep; // reduce step
    }
  else Dt+=tstep;
  if(poly) if(Lstep==iniStep) poly->SetNextPoint(n.GetPosition(0),n.GetPosition(1),n.GetPosition(2));
  } while(2*Lstep>fTolerance);
  
if(poly) poly->SetNextPoint(n.GetPosition(0),n.GetPosition(1),n.GetPosition(2));

// move inside the next volume
double timetonextvolume = 0;
do timetonextvolume += tstep; while(Propagate(tstep, n));
// identify next volume
next = fCore->fGeo->FindVolume(n.GetPosition());
// go back
Propagate(-timetonextvolume, n);

// determine normal direction...
if(Norm) 
  {
  double locP[3];
  double locD[3];
  double locN[3];
  fCurrentNode->MasterToLocal(n.GetPosition() ,locP);  
  fCurrentNode->MasterToLocalVect(n.GetDirection(),locD);
  fCurrentShape->ComputeNormal(locP,locD,locN);
  fCurrentNode->LocalToMasterVect(locN, Norm);   
  }
return timetonextvolume;
}// neutron is inside the volume close to the interface, a step of timetonextvolume will bring it ot the other side


