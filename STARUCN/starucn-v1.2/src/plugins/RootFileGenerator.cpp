
#include "RootFileGenerator.h"
#undef DEBUG

RootFileGenerator::RootFileGenerator(){
	particleid = 0;
}

RootFileGenerator::~RootFileGenerator(){
	//std::cout << "closing InputFile" << std::endl;
	//InputFile->Close();
}

void RootFileGenerator::Configure(const char* file){
	std::cout << "Configuring RootFileGenerator..." << std::endl;
	
	if(file!=0) SetParameterFile(file);
	
	//load settings
	std::string baseinputfile = fParam.GetString("RootFileGenerator.base_input_file");
	std::string inputrunid = fParam.GetString("RootFileGenerator.inputrunid");
	std::string inputrunsection = fParam.GetString("RootFileGenerator.inputrunsection");
	std::string inputruntimeslice = fParam.GetString("RootFileGenerator.inputruntimeslice");
	
	//calculate names of things
	std::string filename = baseinputfile + "_" + inputrunid + "_" + inputrunsection + ".root";
	std::string tuplename = "rawdata_" + inputrunid + "_" + inputrunsection + "_" + inputruntimeslice;
	
	std::cout << "Looking for tuple " << tuplename << " in file " << filename << std::endl;
	
	//open root tree
	fCore->OpenRootFile(filename);
	//InputFile = new TFile(filename.c_str(),"update");
	//std::cout << "input file ptr is " << InputFile << std::endl;
	InputTree = (TNtupleD*)fCore->RootFile->Get(tuplename.c_str());
	std::cout << "input tree ptr is " << InputTree << std::endl;
	entries = InputTree->GetEntries();

	ParticleGyro = fParam.GetDouble("Particle.gyro");

	return;
}

void RootFileGenerator::Generate(Particle& n){
	double* row_content; //pointer to data about current neutron
	
	//if we've asked for more neutrons than are contained in the file
	if (particleid > entries){
		n.Kill();
		n.Switch();
		return;
	}
	//Get neutron data from file
	
	InputTree->GetEntry(particleid);
	row_content = InputTree->GetArgs();
	#ifdef DEBUG
		std::cout << "input file has " << entries << " entries, each with " << InputTree->GetNvar() << " variables";
		
		for (int i = 0; i!= entries; i++)
			std::cout << row_content[i] << " ";
			
		std::cout << std::endl;
		
	#endif
	n.Reset((row_content+6),(row_content+9));//pointers to position + direction vector
    n.SetSpeed(row_content[12]);
    n.SetTime(row_content[33]);
    n.SetParticleSeed(Gen.Rndm()* 4294967295);
    
    Vector3L sigma(row_content[13],row_content[14],row_content[15]);
	n.spin.SetSigma(sigma);
	sigma = Vector3L(row_content[16],row_content[17],row_content[18]);
	n.spinRevE.SetSigma(sigma);
	
	n.spin.SetTheta(row_content[21]);
	n.spin.SetPhase(row_content[19]);
	n.spin.SetThetaError(row_content[22]);
	sigma = Vector3L(row_content[41],row_content[42],row_content[43]);
	n.spin.SetSigma0(sigma);
	n.spin.SetRefPhase(0);
	n.spin.SetTime(row_content[33]);
	n.spin.SeedRNG(n.Gen.Rndm()* 4294967295); // generate random double in [0,1] then stretch to lengthof(int) then cast to int
	
	n.spinRevE.SetTheta(row_content[20]);
	n.spinRevE.SetPhase(row_content[23]);
	n.spinRevE.SetThetaError(row_content[24]);
	sigma = Vector3L(row_content[46],row_content[47],row_content[48]);
	n.spinRevE.SetSigma0(sigma);
	n.spinRevE.SetRefPhase(0);
	n.spinRevE.SetTime(row_content[33]);
	n.spinRevE.SeedRNG(n.Gen.Rndm()* 4294967295);
	
	n.id = row_content[59];
	
	particleid++;
	
	n.spin.SetGyro(ParticleGyro);
	n.spinRevE.SetGyro(ParticleGyro);
	
	return;
}
