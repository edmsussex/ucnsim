#include <string>
#include "UniformField.h"

UniformField::UniformField()
{
	fieldName = "UniformField";
}

UniformField::~UniformField()
{
}

Vector3L UniformField::GetField(Vector3L& pos)
{
	return fieldValue;
}


void UniformField::Configure(const char *file)
{

#ifdef DEBUG

	std::cout << "looking for things called " << fieldName << ".x/.y/.z in the parameters file" << std::endl;

#endif	

double x,y,z;
std::string s;
if(file!=0) SetParameterFile(file);

s = fieldName + ".x";
x = fParam.GetDouble(s.c_str());
s = fieldName + ".y";
y = fParam.GetDouble(s.c_str());
s = fieldName + ".z";
z = fParam.GetDouble(s.c_str());

fieldValue = Vector3L(x,y,z);
std::cout << "Setting up Uniform Field " << fieldName << " of " << fieldValue << std::endl;

Field::Configure(file);
}
