/*
======================================================================
STARucn MC software
File : STARucn.cxx
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "ParameterReader.h"
#include "TreeReader.h"
#include "TPolyLine3DWrap.h"
#include "CoreProcess.h"
#include "BaseGenerator.h"
#include "LoopCounter.h"
#include "Animator.h"
#include "Particle.h"

#include "TApplication.h"
#include "TCanvas.h"
#include "TError.h"
#include "TClass.h"
#include "TSystem.h"
#include "TNtupleD.h"
#include "TFile.h"
#include "TControlBar.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TMath.h"
#include "TGLViewer.h"

#include <iomanip>
#include <iostream>
#include <map>
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <ctime>
#include <thread>
#include <mutex>
#include <type_traits>
#include <functional>

//declarations
std::vector<CoreProcess*> CoreInit(ParameterReader& pr, std::string runmode);
void Display   (Geometry* geo);
void VisuField   ();
void Generate  (std::vector<CoreProcess*> core, ParameterReader& pr);
void Trajectory(Geometry*geo, ParameterReader& pr);
void Animate   (std::vector<CoreProcess*> core2, ParameterReader& pr);
void AnimatePic(std::string file);
void GenerateThread(const int threadno,const int Nthread, CoreProcess *core,int verbose, int Nmax);

//globals
std::mutex rootlock;
std::mutex printlock;

// colors
std::string red = LoopCounter::TxtForm(1,1,0);
std::string ird = LoopCounter::TxtForm(0,0,1);
std::string nor = LoopCounter::TxtForm(1,7,0);
std::string blu = LoopCounter::TxtForm(1,4,0);
std::string cya = LoopCounter::TxtForm(1,6,0);


int main(int argv, char* argc[])
{
std::string file = argv>1 ? argc[1] : "scripts/STARucn.par";
//int verbose = argv>2 ? atoi(argc[2]) : 0;
//verbose=2;
TApplication app("STARucn",&argv,argc);

gErrorIgnoreLevel=10000;

ParameterReader pr;
pr.AddTypeFile("scripts/parameters.typ");
pr.AddParameterFile(file.c_str());

std::string runmode =  pr.GetString("RunMode");
std::string geoconfig = pr.GetString("Geometry");;

std::cout << std::setw(50);
std::cout << std::setprecision(12) << nor << std::endl;
std::cout << red << "****************************************************" << nor << std::endl;
std::cout << red << "       Starting "   << ird << "STAR ucn - multihreaded Version"        << nor << std::endl;
std::cout << red << "   Runmode  : " << blu << runmode           << nor << std::endl;
std::cout << red << "   Geometry : " << blu << geoconfig         << nor << std::endl;
std::cout << red << "****************************************************"  << nor << std::endl << std::endl;

std::vector<CoreProcess*> core = CoreInit(pr,runmode);


if     (runmode=="Display")    Display(core[0]->fGeo);
else if (runmode=="VisuField") VisuField();
else if(runmode=="Trajectory") Trajectory(core[0]->fGeo, pr);
else if(runmode=="Generate")   Generate(core, pr);
else if(runmode=="Animate")     Animate(core,pr);
else if(runmode=="AnimateGraphics") {delete core[0]->fGeo; core[0]->fGeo=0;  AnimatePic(file);}
delete core[0]; core[0] = 0;
std::cout << LoopCounter::TxtForm() << std::endl;
std::cout << "STARucn Finished" << std::endl;
return 0;
}

/////////////////////////////////////////////
std::vector<CoreProcess*> CoreInit(ParameterReader& pr, std::string mode)
/////////////////////////////////////////////
{
// Main set of processing classes
std::string geoconfig = pr.GetString("Geometry");;

CoreProcess *core = CoreProcess::GetInstance();
// Read geometry info and instantiate object
core->SetGeometry(geoconfig);
core->fGeo->CheckOverlaps(0.0001);
core->fGeo->PrintOverlaps();

std::vector<CoreProcess*> coreVec;

if(mode=="Test" || mode=="Generate" || mode=="Animate")
	{
// Init seed for random generators
	int genseed =  pr.GetInt("GenerationSeed");
	BaseGenerator::Gen.SetSeed(genseed);
	std::string genetype, geneconfig;
	std::string proptype, propconfig;
	std::string intrtype, intrconfig;
	std::string outitype, outiconfig;
	pr.GetParameter(genetype  ,"mcsetup","Generator","class");
	pr.GetParameter(geneconfig,"mcsetup","Generator","config");
	pr.GetParameter(proptype  ,"mcsetup","Propagator","class");
	pr.GetParameter(propconfig,"mcsetup","Propagator","config");
	pr.GetParameter(intrtype  ,"mcsetup","Interaction","class");
	pr.GetParameter(intrconfig,"mcsetup","Interaction","config");
	pr.GetParameter(outitype  ,"mcsetup","Output","class");
	pr.GetParameter(outiconfig,"mcsetup","Output","config");

	std::cout << std::endl;
	std::cout << red << "****************************************************" << nor << std::endl;
	std::cout << red << "   Generator   : " << blu << genetype << "   \t" << cya << geneconfig << nor << std::endl;
	std::cout << red << "   Propagator  : " << blu << proptype << "   \t" << cya << propconfig << nor << std::endl;
	std::cout << red << "   Interaction : " << blu << intrtype << "   \t" << cya << intrconfig << nor << std::endl;
	if(mode!="Animate") std::cout << red << "   Output      : " << blu << outitype << "   \t" << cya << outiconfig << nor << std::endl;
	std::cout << red << "****************************************************"  << nor << std::endl << std::endl;

	//automatically choose number of threads
  int threads = std::thread::hardware_concurrency();
  std::cout << "Hardware detected supported threads = " << threads << std::endl;

//overrides automatic thread detection
  if (pr.GetBool("OverrideDefaultThreads"))
	threads = pr.GetInt("NbThreadsOverride");
  std::cout << "running with " << threads << " threads" << std::endl;

// Generator
	if(genetype!="") core->SetGenerator(genetype,geneconfig);
// Propagation algorithm within the geometry
	if(proptype!="") core->SetPropagator(proptype,propconfig);
// Interaction processes
	if(intrtype!="") core->SetInteraction(intrtype,intrconfig);
// Output interface
	if(outitype!="") if(mode!="Animate") core->SetOutputInterface(outitype,outiconfig);

  coreVec.push_back(core);
  std::cout << "length of coreVec = " << coreVec.size() << std::endl;
  std::cout << "pointer to core = " << core << std::endl;
  //each thread's CoreProcess gets its own Propagator, but shares the generator, outputinterface and interaction with the rest.
  for (int i = 1; i < threads; i++) {
	  CoreProcess *newcore = new CoreProcess;
	  newcore->fGeo = core->fGeo;
	  newcore->fGenerator = core->fGenerator;
	  newcore->fOutputInterface = core->fOutputInterface;
	  newcore->fInteraction = core->fInteraction;
	  if(proptype!="") newcore->SetPropagator(proptype,propconfig);
	  coreVec.push_back(newcore);
	    }
}
return coreVec;
}

/////////////////////////////////////////////
void Generate  (std::vector<CoreProcess*> core, ParameterReader& pr)
/////////////////////////////////////////////
{
//automatically choose number of threads
int threads = std::thread::hardware_concurrency();
std::cout << "Hardware detected supported threads = " << threads << std::endl;
// Generator of neutrons

int Niter = pr.GetInt("NbParticles");
int verbose=pr.GetInt("Verbose");
int Nmax  = pr.GetInt("MaxBounce");

//overrides automatic thread detection
if (pr.GetBool("OverrideDefaultThreads"))
	threads = pr.GetInt("NbThreadsOverride");
std::cout << "running with " << threads << " threads" << std::endl;

//calculate
std::vector<int> neutronsPerThread(threads, Niter / threads); //split neutrons between threads
Niter -= (Niter/threads) * threads; //calculate remainder

//allocate leftover neutrons
int i = 0;
while (Niter > 0) {
	neutronsPerThread[i]++;
	Niter--;
	i++;
}

//spawn threads
std::vector<std::thread> threadVector;
for (int threadno = 0; threadno < threads; threadno++)
	threadVector.push_back(std::thread(GenerateThread,threadno,neutronsPerThread[threadno],core[threadno],verbose,Nmax));
	//GenerateThread(threadno,neutronsPerThread[threadno],core[threadno],verbose,Nmax);
	
for (auto& th : threadVector) th.join();

core[0]->fOutputInterface->Finalize();
}


void GenerateThread(const int threadno, const int Nthread, CoreProcess *core,int verbose, int Nmax) {
	std::cout << "Starting Thread " << threadno << " with " << Nthread << " neutrons" << std::endl;
	
	if (Nthread == 0) return;
	
	double Dt=0;
	TGeoVolume* old = 0;
	TGeoVolume* next = 0;
	TGeoVolume* curr = 0;
	double norm[3]= {1,1,1};
	std::cout << LoopCounter::TxtForm() << std::endl;  
	LoopCounter counter(Nthread, 100);
	int vol = 0, sur = 0;
	Particle N;

for(int i=0; i<Nthread; i++) 
	{
	if(verbose==0) counter.Update();
	rootlock.lock();
	core->fGenerator->Generate(N); //Generate
	rootlock.unlock();
	if(verbose>0)
		{
		printlock.lock();
		std::cout << "*************Thread " << threadno << " spawning New neutron " << i << "****************************** " << std::endl;
		N.PrintPosition(std::cout); std::cout << "  " ;
		N.PrintDirection(std::cout); std::cout << "  ";
		N.PrintSpinVector(std::cout); std::cout << "  ";
		N.PrintSpinRevEVector(std::cout); std::cout << "  ";
		std::cout << " Speed = " << N.GetSpeed();
		std::cout << " Time = " << N.GetTime() << " Phase = " << N.spin.GetTheta() << " PhaseRevE = " << N.spinRevE.GetTheta() << std::endl ;                 
		printlock.unlock();
		}
	int r=0;
	rootlock.lock();
	core->fOutputInterface->BeginParticle(N); //Begin Particle Outpuut Interface
	rootlock.unlock();
	while(r<Nmax && !N.Dead()&& N.Active())
		{
		curr = core->fGeo->FindVolume(N.GetPosition());
		core->fPropagator->SetCurrentVolume(curr);
		if(verbose>1) {printlock.lock(); std::cout << "Current volume is : "<<curr->GetName() << " , step = " << r <<std::endl; printlock.unlock();}
		double last = core->fPropagator->PropagateToSurface(N, Dt, norm, next, core->fOutputInterface->Line3D ? &(core->fOutputInterface->Line3D->pl) : 0);
		if(next==0)
			{
			N.Kill();
			vol=2;
			} 
		else
			{
			rootlock.lock();
			core->fOutputInterface->SaveStep(N,vol,sur,old,curr);
			rootlock.unlock();
			old = curr;
			vol = core->fInteraction->VolumeInteraction(N, curr, Dt);    
			sur = vol==0 ? core->fInteraction->SurfaceInteraction(N, curr, next, last, norm, curr) : -1;
			if(verbose>1)        
				 {
				 printlock.lock();
				 std::cout << "  ->  interaction : " << (sur==-1 ? "Decay" : sur==0 ? "Trans" : sur==1 ? "Abs" : sur==2 ? "Spec" : sur==3 ? "Diff" : "Blurp"); 
				 std::cout << " on " << next->GetName()<< " at time t= " << N.GetTime() << std::endl;
				 std::cout << "  ->  position : " ;
				 N.PrintPosition(std::cout);
				 std::cout << std::endl;
				 printlock.unlock();
				 }
			if(verbose>2)
				 {
				 printlock.lock();
				 double cth = norm[0]*N.GetDirection(0)+norm[1]*N.GetDirection(1)+norm[2]*N.GetDirection(2);
				 std::cout << "         normal : " << norm[0] << "  " << norm[1] << "  " << norm[2] << std::endl;
				 std::cout << "         speed  : " << N.GetSpeed() << "  " << cth << std::endl;
				 N.PrintPosition(std::cout); std::cout << "  " ;
				 N.PrintDirection(std::cout); std::cout << std::endl ;
				 N.PrintSpinVector(std::cout); std::cout << "  ";
				 N.PrintSpinRevEVector(std::cout); std::cout << "  ";
				 printlock.unlock();
				 } 
			}

		r++;
		}
	if(verbose>0)    
		{
		printlock.lock();
		std::cout << "Particle id : " <<i << ", Nb Steps : " << r << ", Time : " << N.GetTime() << std::endl;
		std::cout << "Last volume interaction : "  << (vol==0 ? "Trans" : vol==1 ? "Decay" : vol==2 ? "Exit" : "Undef") << "\t\t "; 
		std::cout << "Last surface interaction : " << (sur==0 ? "Trans" : sur==1 ? "Abs" : sur==2 ? "Spec" : sur==3 ? "Diff" : sur==4 ? "TrAbs" : "Blurp") << std::endl;

	std::cout << "Spin Time = " << N.spin.GetTime() << ", SpinRevE Time = " << N.spinRevE.GetTime() << std::endl;

		N.PrintPosition(std::cout); std::cout << "  " ;
		N.PrintDirection(std::cout); std::cout << "  ";    
		N.PrintSpinVector(std::cout); std::cout << "  ";
		N.PrintSpinRevEVector(std::cout); std::cout << "  ";
		std::cout << " Speed = " << N.GetSpeed();
		std::cout << "mm/s Time = " << N.GetTime() << "s Theta = " << N.spin.GetTheta() << " ThetaRevE = " << N.spinRevE.GetTheta() << " Estd Err on each " << N.spin.GetError() << " Phase " << N.spin.GetPhase() << " Phase RevE " << N.spinRevE.GetPhase() << std::endl;
		std::cout << "Frequency = " << N.spin.GetPhase() / (N.GetTime() * 2 * M_PI) << "Hz FreqRevE = " << N.spinRevE.GetPhase() / (N.GetTime() * 2 * M_PI) << "FreqDiff = " << ( N.spin.GetPhase() - N.spinRevE.GetPhase() )/ (N.GetTime() * 2 * M_PI)<<std::endl;
		printlock.unlock();
		}
	rootlock.lock();
	core->fOutputInterface->EndParticle(N,vol,sur,old,next);
	rootlock.unlock();
	}

if(verbose==0) counter.Update();  

}
///////////////////////////////////////////////////////////
void VisuField()
//////////////////////////////////////////////////////////
{ 

std::cout << red <<"Visualisation of the fields is not working with the threaded version . Please use the single thread version." <<nor << std::endl; 

}



/////////////////////////////////////////////
void Display(Geometry* geo)
/////////////////////////////////////////////
{
std::cout << LoopCounter::TxtForm() << std::endl;  
int kNext = 1;
TControlBar *bar = new TControlBar("vertical","Control",620,50);
const char * out = Form("*((int*)0x%lx)=0;" ,long(&kNext)); 
bar->AddButton("     Quit     ",out);
TCanvas* gui = 0;
gui = new TCanvas("c","c",800,800);
//geo->GetWorld()->Raytrace();
geo->GetWorld()->Draw();
static_cast<TGLViewer*>(gui->GetViewer3D("ogl"))->SetCurrentCamera(TGLViewer::kCameraPerspXOY)  ;
bar->Show();

while(kNext) gSystem->ProcessEvents();
delete gui; gui = 0;
delete bar; bar =0;
}

/////////////////////////////////////////////
void Trajectory(Geometry*geo, ParameterReader& pr)
/////////////////////////////////////////////
{
std::cout << LoopCounter::TxtForm() << std::endl;  

std::string file = pr.GetString("Trajectory.InputFile") ;
std::string tree = pr.GetString("Trajectory.InputTree") ;
if(file=="") file="generator.root";

int kNext = 0;
int kQuit = 0;
int kNew  = 1;
TControlBar *bar = new TControlBar("vertical","Control",620,60);
const char * out1 = Form("*((int*)0x%lx)=1;" ,long(&kNext)); 
const char * out2 = Form("*((int*)0x%lx)=1;" ,long(&kQuit)); 
const char * out3 = Form("*((int*)0x%lx)=1;*((int*)0x%lx)=1" ,long(&kNext),long(&kNew)); 
bar->AddButton("     Next     ",out1);
bar->AddButton("     New      ",out3);
bar->AddButton("     Quit     ",out2);

TCanvas* gui = 0;

TFile rootfile(file.c_str(),"read");
rootfile.ls();
TTree* atree = static_cast<TTree*>(rootfile.Get(tree.c_str()));
int n = atree->GetEntries();

for(int i=0;i<n;i++)
	{
	TPolyLine3DWrap* line = static_cast<TPolyLine3DWrap*>(rootfile.Get(Form("Trajectory_%d",i)));
	if(!line) continue;
	std::cout << "Display neutron trajectory number : " << i << std::endl;

	if(kNew)
	 {
		 if(gui) delete gui;
		 gui = new TCanvas("c","c",800,800);
		 geo->GetWorld()->Draw();
		 gui->FeedbackMode(1);
		 static_cast<TGLViewer*>(gui->GetViewer3D("ogl"))->SetCurrentCamera(TGLViewer::kCameraPerspXOY)  ;
		 kNew=0;
		 }
	line->pl.Draw();
	gStyle->SetMarkerColor(1);
	bar->Show();

	while(!kNext&&!kQuit) gSystem->ProcessEvents();
	kNext = 1; 
	if(kQuit) break;
	}

delete bar; bar =0;
}

/////////////////////////////////////////////
void Animate   (std::vector<CoreProcess*> core2, ParameterReader& pr)
/////////////////////////////////////////////
{
// retrive animation parameters
CoreProcess* core = core2[0];
int  frames = pr.GetInt("Animate.NbFrames");
double step = pr.GetDouble("Animate.TimeStep");
std::string name = pr.GetString("Animate.BaseName");
std::string out  = pr.GetString("Animate.OutputDir");

std::cout << blu << "****************************************************" << nor << std::endl;
std::cout << blu << "   Building animation with " << cya << frames << blu << " frames of "<< cya << step << blu << " sec." << nor << std::endl;
std::cout << blu << "****************************************************" << nor << std::endl;

//proverride 
for(int i=1; i<=frames;i++)
	{
	std::cout << blu << "Frame : " << red << i << blu << " / " <<  frames;
	std::string override = "";
	override += Form("string    TextOutputInterface.OutputFile    %s_%d.frame\n",(out+"/"+name).c_str(),i);
	override += "bool           TextOutputInterface.DoRootTree      0\n";
	override += "string         TextOutputInterface.StopMode        Time\n";
	override += Form("double    TextOutputInterface.StopTime        %lf     #seconds\n",step*i);
	std::istringstream outOveride(override);
	if(core->fOutputInterface) delete core->fOutputInterface;
	core->fOutputInterface = static_cast<BaseOutputInterface*>(TClass::GetClass("TextOutputInterface")->New());
	core->fOutputInterface->SetParameterFile(outOveride);
	core->fOutputInterface->Configure(0);
	if(i>1)
		{
		std::istringstream genOveride(Form("string     FileGenerator.InputFile       %s_%d.frame\n",(out+"/"+name).c_str(),i-1));
		if(core->fGenerator) delete core->fGenerator;
		core->fGenerator = static_cast<BaseGenerator*>(TClass::GetClass("FileGenerator")->New());
		core->fGenerator->SetParameterFile(genOveride);
		core->fGenerator->Configure(0);
		}
	Generate(core2,pr);
	}
}

void AnimatePic (std::string file) 
{
std::cout << LoopCounter::TxtForm() << std::endl;  
Animator ani(file);
ani.MakeMovie();
}
