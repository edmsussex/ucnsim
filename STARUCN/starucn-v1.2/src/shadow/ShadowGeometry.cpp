/*
======================================================================
STARucn MC software
File : ShadowGeometry.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "ShadowGeometry.h"
#include "ShadowQuadric.h"
#include "ShadowTorus.h"
#include "ShadowCorner.h"

#include "TMath.h"
#include "ParameterReader.h"
#include "LoopCounter.h"
#include "Configurable.h"

#include <string>

/// List of root volumes from Geometry class
///  "Arb8", "Box", "Cone", "Cons", "Ctub", 
///  "Eltu", "Gtra", "Hype", "Para", "Paraboloid", 
///  "Pcon", "Pgon", "Sphere", "Toru", "Trap", 
///   "Trd1", "Trd2", "Tube", "Tubs"
/// List of corresponding shadow volumes implemented
///  "Box" , "Tube", "Tubs", "Toru"
///
///


///////////////////////////////////////////////////////
ShadowGeometry::ShadowGeometry(Geometry* geo)
///////////////////////////////////////////////////////
{
kDebug=false;
std::string red = LoopCounter::TxtForm(1,1,0);
std::string nor = LoopCounter::TxtForm(1,7,0);
std::string blu = LoopCounter::TxtForm(1,4,0);
std::string cya = LoopCounter::TxtForm(1,6,0);
std::cout << red << "****************************************************" << nor << std::endl;
std::cout << red << "             Building shadow geometry" << std::endl;
std::cout << red << "   Geometry file : " << blu << geo->GetInput().c_str() << nor << std::endl;
ParameterReader pr;
pr.AddTypeFile(Configurable::TypeFile.c_str());
pr.AddParameterFile(geo->GetInput().c_str());
fGeometry=geo;
// build transformation for shadows
double t[3], r[3];
double* PARS = new double[20];
pr.itrInit("geometry");
std::string vol;  pr.GetParameter(vol,"geometry","","volumes");
std::vector<std::string> volumes;
if(vol[0]=='|') vol=vol.substr(1,vol.size());
ParameterReader::ReadString(volumes,vol);

for(pr.itrInit("matrix");!pr.itrEnd("matrix"); pr.itrNext("matrix"))
  {
  pr.GetParameter(t[0],"matrix","","transX");
  pr.GetParameter(t[1],"matrix","","transY");
  pr.GetParameter(t[2],"matrix","","transZ");  
  pr.GetParameter(r[0],"matrix","","rotX");
  pr.GetParameter(r[1],"matrix","","rotY");
  pr.GetParameter(r[2],"matrix","","rotZ");  
  if(r[0]==1000 ||r[1]==1000 || r[2]==1000)
    {
    r[0] = r[1] = r[2] = 0;
    }
  ShadowTransform* tr = new ShadowTransform(t,r);
  fTransforms[pr.itrName("matrix")] = tr;
  }
for(pr.itrInit("volume");!pr.itrEnd("volume"); pr.itrNext("volume"))
  {
  std::string type;     pr.GetParameter(type,"volume","","type");
  if(type!="Box" && type!= "Tube" && type!="Tubs" && type!="Toru") continue; // no other geometry available

  std::string vname = pr.itrName("volume");
  TGeoVolume* vol = fGeometry->GetVolume(vname.c_str());
  bool toDeclare = std::find(volumes.begin(), volumes.end(), vname)!=volumes.end();
  if(vol==0 || !toDeclare) continue;

  std::string coord;    pr.GetParameter(coord,"volume","","coord");
  std::string matrix;   pr.GetParameter(matrix,"volume","","matrix");
  std::vector<std::string> pars;
  ParameterReader::ReadString(pars,coord);

  for(unsigned int i=0; i<pars.size();++i)  PARS[i] = ParameterReader::Parse<double>(pars[i]);
  if(type=="Box")    fVolumes[vname] = this->MakeBox   (PARS[0],PARS[1],PARS[2], fTransforms[matrix]);
  if(type=="Tube")   fVolumes[vname] = this->MakeTube  (PARS[0],PARS[1],PARS[2], fTransforms[matrix]);
  if(type=="Tubs")   fVolumes[vname] = this->MakeTubs  (PARS[0],PARS[1],PARS[2],PARS[3],PARS[4], fTransforms[matrix]);
  if(type=="Toru")   fVolumes[vname] = this->MakeTorus (PARS[0],PARS[1],PARS[2],PARS[3],PARS[4], fTransforms[matrix]);

  fShadows[vol] = fVolumes[vname];
  std::cout << red << "   Adding to shadow geometry : " << cya << vname <<red <<" as " <<cya << type << nor <<std::endl;
  }
delete[] PARS;

std::cout << red << "   Shadow geometry contains " << blu << fShadows.size() << red << " shadow volumes" << nor <<std::endl;
std::cout << red <<"*****************************************************" << nor << std::endl << std::endl;
}


///////////////////////////////////////////////////////
ShadowGeometry::~ShadowGeometry()
///////////////////////////////////////////////////////
{
}


///////////////////////////////////////////////////////
ShadowVolume* ShadowGeometry::MakeBox (double dx, double dy, double dz, ShadowTransform* sdw)
///////////////////////////////////////////////////////
{
if(kDebug) std::cout << "in [ShadowGeometry::MakeBox] : " << dx << " " << dy << " " << dz << "  " << sdw << std::endl;
double pars[10]={0};
pars[6]=1;
pars[9]=dx;
ShadowQuadric* s1 = new ShadowQuadric(pars);
pars[9]=-dx;
ShadowQuadric* s2 = new ShadowQuadric(pars);
pars[6]=0;
pars[7]=1;
pars[9]=dy;
ShadowQuadric* s3 = new ShadowQuadric(pars);
pars[9]=-dy;
ShadowQuadric* s4 = new ShadowQuadric(pars);
pars[7]=0;
pars[8]=1;
pars[9]=dz;
ShadowQuadric* s5 = new ShadowQuadric(pars);
pars[9]=-dz;
ShadowQuadric* s6 = new ShadowQuadric(pars);
pars[8]=0;
s1->SetTransform(sdw);
s2->SetTransform(sdw);
s3->SetTransform(sdw);
s4->SetTransform(sdw);
s5->SetTransform(sdw);
s6->SetTransform(sdw);
ShadowVolume* vol = new ShadowVolume;
vol->AddSurface(s1,-1);
vol->AddSurface(s2,1);
vol->AddSurface(s3,-1);
vol->AddSurface(s4,1);
vol->AddSurface(s5,-1);
vol->AddSurface(s6,1);
return vol;
}


///////////////////////////////////////////////////////
ShadowVolume* ShadowGeometry::MakeTube(double rmin, double rmax, double dz, ShadowTransform* sdw)
///////////////////////////////////////////////////////
{
if(kDebug) std::cout << "in [ShadowGeometry::MakeTube]" << std::endl;

ShadowVolume* vol = new ShadowVolume;

double pars[10]={0};
pars[8]=1;
pars[9]=dz;
ShadowQuadric* s1 = new ShadowQuadric(pars);
pars[9]=-dz;
s1->SetTransform(sdw);
ShadowQuadric* s2 = new ShadowQuadric(pars);
pars[8]=0;
s2->SetTransform(sdw);

ShadowQuadric* s3 = new ShadowQuadric();
s3->SetCylinder(rmax);
s3->SetTransform(sdw);

vol->AddSurface(s1,-1);
vol->AddSurface(s2,1);
vol->AddSurface(s3,1);

if(rmin>0)
  {
  ShadowQuadric* s4 = new ShadowQuadric();
  s4->SetCylinder(rmin);
  s4->SetTransform(sdw);
  vol->AddSurface(s4,-1);
  }
return vol;
}

///////////////////////////////////////////////////////
ShadowVolume* ShadowGeometry::MakeTubs  (double rmin, double rmax, double dz, double phi1, double phi2, ShadowTransform* sdw)
///////////////////////////////////////////////////////
{
if(kDebug) std::cout << "in [ShadowGeometry::MakeTubs]" << std::endl;
ShadowVolume* vol = MakeTube(rmin,rmax,dz, sdw);
// half planes
ShadowCorner* surf = new ShadowCorner(phi1,phi2);
surf->SetTransform(sdw);
vol->AddSurface(surf,1);

return vol;
}

///////////////////////////////////////////////////////
ShadowVolume* ShadowGeometry::MakeTorus (double R, double rmin, double rmax, double phi1, double phi2, ShadowTransform* sdw)
///////////////////////////////////////////////////////
{
if(kDebug) std::cout << "in [ShadowGeometry::MakeTorus]" << std::endl;
ShadowVolume* vol = new ShadowVolume;

ShadowTorus * tor1 = new ShadowTorus(R,rmin);
ShadowTorus * tor2 = new ShadowTorus(R,rmax);
tor1->SetTransform(sdw);
vol->AddSurface(tor1,-1);
tor2->SetTransform(sdw);
vol->AddSurface(tor2,1);
// // half planes
ShadowCorner* surf = new ShadowCorner(phi1,phi2);
surf->SetTransform(sdw);
vol->AddSurface(surf,1); 
return vol;
}

///////////////////////////////////////////////////////
bool ShadowGeometry::Contains(double* x)
///////////////////////////////////////////////////////
{
std::map<TGeoVolume*, ShadowVolume*>::iterator itr = fShadows.begin();
std::map<TGeoVolume*, ShadowVolume*>::iterator itrE= fShadows.end();
for(;itr!=itrE;itr++)
  {
  if(itr->second->Contains(x)) return true;
  }
return false;
}




