/*
======================================================================
STARucn MC software
File : ShadowSurface.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "ShadowSurface.h"
#include "TRandom3.h"
#include "TMath.h"

///////////////////////////////////////////////////////
ShadowSurface::ShadowSurface()
///////////////////////////////////////////////////////
{
fT=0;
}

///////////////////////////////////////////////////////
ShadowSurface::~ShadowSurface()
///////////////////////////////////////////////////////
{
}

///////////////////////////////////////////////////////
void ShadowSurface::DrawRandoms(int n, double bX, double bY, double bZ,  std::vector<double>& x,  std::vector<double>& y,  std::vector<double>& z)
///////////////////////////////////////////////////////
{
int cnt = 0;
std::vector<double> tmp;
int u;
while(cnt<n)
  {
  double X = gRandom->Uniform(-bX,bX);
  double Y = gRandom->Uniform(-bY,bY);
  double Z = gRandom->Uniform(-bZ,bZ);
  EvalXY(X,Y,tmp);
  u= tmp.size();
  for(int i=0; i<u;i++) if(fabs(tmp[i])<bZ)
    {
    x.push_back(X);
    y.push_back(Y);
    z.push_back(tmp[i]);
    cnt++;
    }

  EvalYZ(Y,Z,tmp);
  u= tmp.size();
  for(int i=0; i<u;i++) if(fabs(tmp[i])<bX)
    {
    x.push_back(tmp[i]);
    y.push_back(Y);
	z.push_back(Z);
    cnt++;
    }  
  EvalZX(Z,X,tmp);
  u= tmp.size();
  for(int i=0; i<u;i++) if(fabs(tmp[i])<bY)
    { 
    x.push_back(X);
    y.push_back(tmp[i]);
    z.push_back(Z);
    cnt++;
    }
  }

}


///////////////////////////////////////////////////////
void ShadowSurface::Normal(double x, double y, double z, double vx, double vy, double vz, double* n)
///////////////////////////////////////////////////////
{
// check only sign and normalization
double cth = n[0]*vx+n[1]*vy+n[2]*vz;
double N = n[0]*n[0]+n[1]*n[1]+n[2]*n[2];
N = cth>=0 ? 1./TMath::Sqrt(N) : -1/TMath::Sqrt(N);
n[0]*=N;
n[1]*=N;
n[2]*=N;
}
          
