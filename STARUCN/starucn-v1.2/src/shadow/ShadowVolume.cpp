/*
======================================================================
STARucn MC software
File : ShadowVolume.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "ShadowVolume.h"

///////////////////////////////////////////////////////
ShadowVolume::ShadowVolume()
///////////////////////////////////////////////////////
{
kN=0;
}

///////////////////////////////////////////////////////
ShadowVolume::~ShadowVolume()
///////////////////////////////////////////////////////
{;}

///////////////////////////////////////////////////////
bool ShadowVolume::Contains(double* p) 
///////////////////////////////////////////////////////
{
bool res=true;
for(int i=0; i<kN && res; i++)
  {
  int side = fSurfaces[i].first->Side(p[0],p[1],p[2]);
  res = res && (side==fSurfaces[i].second || side==0);
  }
return res;
}

///////////////////////////////////////////////////////
double ShadowVolume::ClosestIntersect(double* x, double* y, double* z) 
///////////////////////////////////////////////////////
{
double res=-1;
double goodx=0, goody=0, goodz=0;
double normx=0, normy=0, normz=0;
double n[3];
for(int i=0; i<kN; i++)
  {
  double t = fSurfaces[i].first->ClosestIntersect(x,y,z,n);
/*std::cout << i << "   " << t << std::endl;*/
  if(t<0) continue;
  if(res<0 || t<res)
    {
    res = t;
  	goodx = x[3];
	  goody = y[3];
	  goodz = z[3];
	  normx = n[0];
	  normy = n[1];
	  normz = n[2];
    }
  }
x[3] = goodx;
y[3] = goody;
z[3] = goodz;
x[4] = normx;
y[4] = normy;
z[4] = normz;
return res;
}

