/*
======================================================================
STARucn MC software
File : ShadowQuadric.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "ShadowQuadric.h"
#include "TRandom.h"
#include "TMath.h"
#include "PolynomialSolver.h"
#include <gsl/gsl_poly.h>

///////////////////////////////////////////////////////
ShadowQuadric::ShadowQuadric(double* pars)
///////////////////////////////////////////////////////
{
if(pars)
  {
  fQuad [0] = pars[0];
  fQuad [1] = pars[1];
  fQuad [2] = pars[2];
  fCross[0] = pars[3];
  fCross[1] = pars[4];
  fCross[2] = pars[5];
  fLin  [0] = pars[6];
  fLin  [1] = pars[7];
  fLin  [2] = pars[8];
  fConst    = pars[9];
  IsPlane = (fQuad [0]==0 && fQuad [1]==0 && fQuad [2]==0 && fCross[0]==0 && fCross[1]==0 && fCross[2]==0 );
  }
else
  {
  fQuad [0] = 0.;
  fQuad [1] = 0.;
  fQuad [2] = 0.;
  fCross[0] = 0.;
  fCross[1] = 0.;
  fCross[2] = 0.;
  fLin  [0] = 0.;
  fLin  [1] = 0.;
  fLin  [2] = 0.;
  fConst    = 0.;  
  }
}

///////////////////////////////////////////////////////
ShadowQuadric::~ShadowQuadric()
///////////////////////////////////////////////////////
{;}

///////////////////////////////////////////////////////
void ShadowQuadric::SetEllipsoide(double r1, double r2, double r3)
///////////////////////////////////////////////////////
{
fQuad [0] = 1./(r1*r1);
fQuad [1] = 1./(r2*r2);
fQuad [2] = 1./(r3*r3);
fCross[0] = 0.;
fCross[1] = 0.;
fCross[2] = 0.;
fLin  [0] = 0.;
fLin  [1] = 0.;
fLin  [2] = 0.;
fConst    = -1.;
IsPlane  = false;
}

///////////////////////////////////////////////////////
void ShadowQuadric::SetCylinder(double r)
///////////////////////////////////////////////////////
{
fQuad [0] = 1.;
fQuad [1] = 1.;
fQuad [2] = 0.;
fCross[0] = 0.;
fCross[1] = 0.;
fCross[2] = 0.;
fLin  [0] = 0.;
fLin  [1] = 0.;
fLin  [2] = 0.;
fConst    = -r*r;
IsPlane  = false;

}

///////////////////////////////////////////////////////
void ShadowQuadric::SetPlane(double phi) // horzontal plane at z=0
///////////////////////////////////////////////////////
{
fQuad [0] = 0.;
fQuad [1] = 0.;
fQuad [2] = 0.;
fCross[0] = 0.;
fCross[1] = 0.;
fCross[2] = 0.;
fLin  [0] = TMath::Sin(phi*TMath::Pi()/180);
fLin  [1] = -TMath::Cos(phi*TMath::Pi()/180);
fLin  [2] = 0.;
fConst    = 0.;
IsPlane  = true;

}

///////////////////////////////////////////////////////
void ShadowQuadric::SetTwistedPlane(double a, double dz)
///////////////////////////////////////////////////////
{// needs modification
double s = TMath::Tan(a*TMath::Pi()/360.)/dz;
fQuad [0] = 0.;
fQuad [1] = 0.;
fQuad [2] = 0.;
fCross[0] = 0.;
fCross[1] = 0.;
fCross[2] = s;
fLin  [0] = 0.;
fLin  [1] = -1.;
fLin  [2] = 0.;
fConst    = 0.;
IsPlane  = false;

}

///////////////////////////////////////////////////////
void ShadowQuadric::Transform()   // Change coordinates 
///////////////////////////////////////////////////////
{
double j = -fT->M(0,0)*fT->T(0) - fT->M(1,0)*fT->T(1) - fT->M(2,0)*fT->T(2);
double k = -fT->M(0,1)*fT->T(0) - fT->M(1,1)*fT->T(1) - fT->M(2,1)*fT->T(2);
double l = -fT->M(0,2)*fT->T(0) - fT->M(1,2)*fT->T(1) - fT->M(2,2)*fT->T(2);

double A = fT->M(0,0)*fT->M(0,0)*fQuad [0] + fT->M(0,1)*fT->M(0,1)*fQuad [1] + fT->M(0,2)*fT->M(0,2)*fQuad [2];
       A+= fT->M(0,0)*fT->M(0,1)*fCross[0] + fT->M(0,1)*fT->M(0,2)*fCross[1] + fT->M(0,2)*fT->M(0,0)*fCross[2]; 
double B = fT->M(1,0)*fT->M(1,0)*fQuad [0] + fT->M(1,1)*fT->M(1,1)*fQuad [1] + fT->M(1,2)*fT->M(1,2)*fQuad [2];
       B+= fT->M(1,0)*fT->M(1,1)*fCross[0] + fT->M(1,1)*fT->M(1,2)*fCross[1] + fT->M(1,2)*fT->M(1,0)*fCross[2]; 
double C = fT->M(2,0)*fT->M(2,0)*fQuad [0] + fT->M(2,1)*fT->M(2,1)*fQuad [1] + fT->M(2,2)*fT->M(2,2)*fQuad [2];
       C+= fT->M(2,0)*fT->M(2,1)*fCross[0] + fT->M(2,1)*fT->M(2,2)*fCross[1] + fT->M(2,2)*fT->M(2,0)*fCross[2]; 
    
double D = 2*fT->M(0,0)*fT->M(1,0)*fQuad [0] + 2*fT->M(0,1)*fT->M(1,1)*fQuad [1] + 2*fT->M(0,2)*fT->M(1,2)*fQuad [2];
       D+= (fT->M(0,0)*fT->M(1,1)+fT->M(0,1)*fT->M(1,0))*fCross[0]; 
       D+= (fT->M(0,1)*fT->M(1,2)+fT->M(0,2)*fT->M(1,1))*fCross[1]; 
       D+= (fT->M(0,2)*fT->M(1,0)+fT->M(0,0)*fT->M(1,2))*fCross[2]; 
double E = 2*fT->M(1,0)*fT->M(2,0)*fQuad [0] + 2*fT->M(1,1)*fT->M(2,1)*fQuad [1] + 2*fT->M(1,2)*fT->M(2,2)*fQuad [2];
       E+= (fT->M(1,0)*fT->M(2,1)+fT->M(1,1)*fT->M(2,0))*fCross[0]; 
       E+= (fT->M(1,1)*fT->M(2,2)+fT->M(1,2)*fT->M(2,1))*fCross[1]; 
       E+= (fT->M(1,2)*fT->M(2,0)+fT->M(1,0)*fT->M(2,2))*fCross[2]; 
double F = 2*fT->M(2,0)*fT->M(0,0)*fQuad [0] + 2*fT->M(2,1)*fT->M(0,1)*fQuad [1] + 2*fT->M(2,2)*fT->M(0,2)*fQuad [2];
       F+= (fT->M(2,0)*fT->M(0,1)+fT->M(2,1)*fT->M(0,0))*fCross[0]; 
       F+= (fT->M(2,1)*fT->M(0,2)+fT->M(2,2)*fT->M(0,1))*fCross[1];
       F+= (fT->M(2,2)*fT->M(0,0)+fT->M(2,0)*fT->M(0,2))*fCross[2]; 

double G = 2*fT->M(0,0)*j*fQuad [0] + 2*fT->M(0,1)*k*fQuad [1] + 2*fT->M(0,2)*l*fQuad [2];
       G+= (fT->M(0,0)*k+fT->M(0,1)*j)*fCross[0]; 
       G+= (fT->M(0,1)*l+fT->M(0,2)*k)*fCross[1]; 
       G+= (fT->M(0,2)*j+fT->M(0,0)*l)*fCross[2]; 
       G+= fT->M(0,0)*fLin[0]+fT->M(0,1)*fLin[1]+fT->M(0,2)*fLin[2];

double H = 2*fT->M(1,0)*j*fQuad [0] + 2*fT->M(1,1)*k*fQuad [1] + 2*fT->M(1,2)*l*fQuad [2];
       H+= (fT->M(1,0)*k+fT->M(1,1)*j)*fCross[0]; 
       H+= (fT->M(1,1)*l+fT->M(1,2)*k)*fCross[1]; 
       H+= (fT->M(1,2)*j+fT->M(1,0)*l)*fCross[2]; 
       H+= fT->M(1,0)*fLin[0]+fT->M(1,1)*fLin[1]+fT->M(1,2)*fLin[2];

double I = 2*fT->M(2,0)*j*fQuad [0] + 2*fT->M(2,1)*k*fQuad [1] + 2*fT->M(2,2)*l*fQuad [2];
       I+= (fT->M(2,0)*k+fT->M(2,1)*j)*fCross[0]; 
       I+= (fT->M(2,1)*l+fT->M(2,2)*k)*fCross[1]; 
       I+= (fT->M(2,2)*j+fT->M(2,0)*l)*fCross[2]; 
       I+= fT->M(2,0)*fLin[0]+fT->M(2,1)*fLin[1]+fT->M(2,2)*fLin[2];
double J = j*j*fQuad [0] + k*k*fQuad [1] + l*l*fQuad [2];
       J+= j*k*fCross[0] + k*l*fCross[1] + l*j*fCross[2];
       J+= j*fLin[0]+k*fLin[1]+l*fLin[2]+fConst;
if(!IsPlane)
  {
  fQuad [0] = A;
  fQuad [1] = B;
  fQuad [2] = C;
  fCross[0] = D;
  fCross[1] = E;
  fCross[2] = F;
  }
fLin  [0] = G;
fLin  [1] = H;
fLin  [2] = I;
fConst    = J;
}

///////////////////////////////////////////////////////
void ShadowQuadric::EvalIJ(int u0, double a, double b, std::vector<double>& r) 
///////////////////////////////////////////////////////
{
r.clear();

int u1 = (u0+1)%3;
int u2 = (u0+2)%3;

double A = fQuad [u0];
double B = fCross[u0]*a   +fCross[u2]*b    +fLin[u0];
double C = fQuad [u1]*a*a + fQuad[u2]*b*b + fCross[u1]*a*b + fLin[u1]*a + fLin[u2]*b + fConst;

if(A==0.)// no quadratic term
  {
  if(B==0.) return;
  r.push_back(-C/B);
  }
else
  {
  double Delta = B*B-4*A*C;
  if(Delta<0) return;
  r.push_back( 0.5*(-B+TMath::Sqrt(Delta))/A);
  r.push_back( 0.5*(-B-TMath::Sqrt(Delta))/A);
  }
}

///////////////////////////////////////////////////////
int ShadowQuadric::Side(double x, double y, double z)
///////////////////////////////////////////////////////
{
double u = fQuad[0]*x*x+fQuad[1]*y*y+fQuad[2]*z*z+fCross[0]*x*y+fCross[1]*y*z+fCross[2]*z*x+fLin[0]*x+fLin[1]*y+fLin[2]*z+fConst;
return u<=0 ? 1 : -1; // 1 unter, -1 under
}


///////////////////////////////////////////////////////
double ShadowQuadric::ClosestIntersect(double* x, double* y, double* z, double* n) // x=x[0]*t*t+x[1]*t+x[2]  x[3]=result;
///////////////////////////////////////////////////////
{
double par[5] = {0};
if(!IsPlane)
  {
  par[4] = x[0]*x[0]*fQuad [0]+y[0]*y[0]*fQuad [1]+z[0]*z[0]*fQuad [2];
  par[4]+= x[0]*y[0]*fCross[0]+y[0]*z[0]*fCross[1]+z[0]*x[0]*fCross[2];

  par[3] = 2*x[0]*x[1]          *fQuad [0]+2*y[0]*y[1]          *fQuad [1]+2*z[0]*z[1]          *fQuad [2];
  par[3]+= (x[0]*y[1]+x[1]*y[0])*fCross[0]+(y[0]*z[1]+y[1]*z[0])*fCross[1]+(z[0]*x[1]+z[1]*x[0])*fCross[2];
  }
par[2] = (x[1]*x[1]+2*x[0]*x[2])        *fQuad [0]+(y[1]*y[1]+2*y[0]*y[2])        *fQuad [1]+(z[1]*z[1]+2*z[0]*z[2])        *fQuad [2];
par[2]+= (x[1]*y[1]+x[0]*y[2]+x[2]*y[0])*fCross[0]+(y[1]*z[1]+y[0]*z[2]+y[2]*z[0])*fCross[1]+(z[1]*x[1]+z[0]*x[2]+z[2]*x[0])*fCross[2];
par[2]+= x[0]                           *fLin  [0]+y[0]                           *fLin  [1]+z[0]                           *fLin  [2];

par[1] = 2*x[1]*x[2]          *fQuad [0]+2*y[1]*y[2]          *fQuad [1]+2*z[1]*z[2]          *fQuad [2];
par[1]+= (x[1]*y[2]+x[2]*y[1])*fCross[0]+(y[1]*z[2]+y[2]*z[1])*fCross[1]+(z[1]*x[2]+z[2]*x[1])*fCross[2];
par[1]+= x[1]                 *fLin  [0]+y[1]                 *fLin  [1]+z[1]                 *fLin  [2];

par[0] = x[2]*x[2]*fQuad [0]+y[2]*y[2]*fQuad [1]+z[2]*z[2]*fQuad [2];
par[0]+= x[2]*y[2]*fCross[0]+y[2]*z[2]*fCross[1]+z[2]*x[2]*fCross[2];
par[0]+= x[2]     *fLin  [0]+y[2]     *fLin  [1]+z[2]     *fLin  [2]    +fConst;

double t = -1;
double S = 0;
for(int i=0;i<5;i++) S+=fabs(par[i]);
for(int i=0;i<5;i++) par[i]/=S;
int o=4;
for(int i=4; i>=2;--i)
  {
  if(fabs(par[i])<1e-12) o--;
  else break;
  }

double sol[8];
gsl_poly_complex_workspace * w  = gsl_poly_complex_workspace_alloc (o+1);
gsl_poly_complex_solve (par, o+1, w, sol);
gsl_poly_complex_workspace_free (w);
for(int i=0; i<o;i++)
  {
  if(sol[2*i+1]!=0) continue;
  if(sol[2*i]<0)    continue;
  if(sol[2*i]<t || t<0) t=sol[2*i];
  }
if(t>0)
  {
  x[3]=x[0]*t*t+x[1]*t+x[2];
  y[3]=y[0]*t*t+y[1]*t+y[2];
  z[3]=z[0]*t*t+z[1]*t+z[2];
  if(n) Normal(x[3],y[3],z[3],2*x[0]*t+x[1],2*y[0]*t+y[1],2*z[0]*t+z[1], n);
  }
return t;
}

///////////////////////////////////////////////////////
void ShadowQuadric::Normal(double x, double y, double z, double vx, double vy, double vz, double* n)
///////////////////////////////////////////////////////
{
n[0] = 2*fQuad[0]*x+fCross[0]*y+fCross[2]*z+fLin[0];
n[1] = 2*fQuad[1]*y+fCross[1]*z+fCross[0]*x+fLin[1];
n[2] = 2*fQuad[2]*z+fCross[2]*x+fCross[1]*y+fLin[2];
ShadowSurface::Normal(x,y,z,vx,vy,vz,n);
}






//PolynomialSolver::Solve(o,par,fTmp);
//std::sort(fTmp.begin(), fTmp.end());
// 
//   int k = fTmp.size();
//   for(int i=0; i<k; i++)
//     {
//     if(fTmp[i]>0) 
//       {
//       t=fTmp[i]; 
//       x[3]=x[0]*t*t+x[1]*t+x[2];
//       y[3]=y[0]*t*t+y[1]*t+y[2];
//       z[3]=z[0]*t*t+z[1]*t+z[2];
//      if(n) 
//        {
//        Normal(x[3],y[3],z[3],2*x[0]*t+x[1],2*y[0]*t+y[1],2*z[0]*t+z[1], n);
//        }
//      break;
//       }
//     } 
//   }
// else
//   {
// use gsl here
//   std::cout << o<< std::endl;

//   int k = fTmp.size();
//   for(int i=0; i<k; i++) std::cout <<fTmp[i] << " , " << (par2[0]+par2[1]*fTmp[i]+par2[2]*fTmp[i]*fTmp[i]+par2[3]*fTmp[i]*fTmp[i]*fTmp[i]+par2[4]*fTmp[i]*fTmp[i]*fTmp[i]*fTmp[i]) << "\t|||\t";
//   std::cout << std::endl;
