/*
======================================================================
STARucn MC software
File : PolynomialSolver.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "PolynomialSolver.h"
#include <iostream>
#include <cmath>
#include <algorithm>

///////////////////////////////////////////////////////
PolynomialSolver::PolynomialSolver() {;}
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
PolynomialSolver::~PolynomialSolver() {;}
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
void PolynomialSolver::Solve(int order, double* par, std::vector <double>& sol, bool clear, bool scale)
///////////////////////////////////////////////////////
{
// par[0] + par[1]*x + par[2]*x**2 + ... + par[order]*x**order = 0
if(clear) sol.clear();
// double S=0;
// if(scale)
//   {
//   for(int i=0;i<=order;i++) S+=fabs(par[i]);
//   for(int i=0;i<=order;i++) par[i]/=S;
//   }
for(int i=order; i>=0;--i)
  {
  if(fabs(par[i])<1e-10) order--;
  else break;
  }
for(int i=0; i<=order;++i) par[i] /= par[order];

if(fabs(par[0])<1e-15)
  {
  if(sol.size()==0) sol.push_back(0);
  order--;
  for(int i=0; i<=order;++i) par[i]=par[i+1];
  Solve(order,par,sol,0,0);
  return;
  }
  
if(order==1) 
  {
  sol.push_back(-par[0]);
  return;
  }
else if(order==2)
  {               
  double D = par[1]*par[1]-4*par[0];
  if(fabs(D)<1e-15) sol.push_back( -0.5 * par[1] );
  else if(D<0) return;
  else 
    {
    double k = sqrt(D);
    sol.push_back( - 0.5*(par[1]+k) );
    sol.push_back( - 0.5*(par[1]-k) );           
    } 
  return;   
  }
else if(order==3) // Caran formulae
  {
  double val = 0.;
  double p = par[1]-par[2]*par[2]/3.;
  double q = par[0] - par[2]*par[1]/3. + 2./27.*par[2]*par[2]*par[2];
  double n1 = 4./27.*p*p*p+q*q;
  // Case with real discriminant
  if(n1>=0)
    {
    n1 = sqrt(n1);
    double n2 = -0.5*(q+n1);
    double n3 = -0.5*(q-n1);
    double nn2 = fabs(n2);
    double nn3 = fabs(n3);
    double sn2 = 0., sn3 = 0.;
    if(nn2>0) sn2 = n2/nn2;
    if(nn3>0) sn3 = n3/nn3;
    val = sn2*pow(nn2,1./3.)+sn3*pow(nn3,1./3.)-par[2]/3.;
    }
  else// Case with imaginary discriminant
    {
    double r = -0.5*q ;
    double i = -0.5*sqrt(-n1) ;
    double k = 0.;
    if(r<0) k = 4*atan(1.);
    double pp = 1./3.*(atan(i/r)+k);
    double A = pow(r*r+i*i,1./6.);
    double rsq3 = 2.*A*cos(pp);
    double t1 = rsq3;
    val = t1-par[2]/3.;
    }
  sol.push_back(val);  
  double v = par[2]+val;
  double w = par[1]+val*v;
    
  double eq[3] = {w, v, 1};
  Solve(2,eq,sol,0,0);
  return;
  }
else if(order == 4) // Ferrari's method...
  {
  // start with reduced quartic
  // x -> u - par[3]/4 (par[4]=1)   ;  u**4 + alpha[2] u**2 + alpha[1] u + alpha[0] = 0;    
  double alpha[3];
  double b  = par[3];
  double b2 = b*b;   
  alpha[0] = -0.01171875*b2*b2 + 0.0625*par[2]*b2 - 0.25*par[1]*b + par[0] ; 
  alpha[1] = 0.125*b2*b - 0.5*par[2]*b + par[1];  
  alpha[2] = -0.375*b2 + par[2]; 
  // and now...
  if(fabs(alpha[1])<1e-15)// biquartic equation
    {
    double par2[3]={alpha[0],alpha[2],1};
    std::vector<double> sol2;
    Solve(2, par2, sol2,1,0);
    std::vector<double>::iterator itr  = sol2.begin();
    std::vector<double>::iterator itrE = sol2.end();
    for(;itr!=itrE;++itr)
      {
      if(fabs(*itr) < 1e-15) sol.push_back( -0.25 * b );
      else if(*itr < 0) continue;
      else 
        {
        sol.push_back( -0.25*b + sqrt(*itr)); 
        sol.push_back( -0.25*b - sqrt(*itr));
        } 
      }         
    }
  else // general case, factorise into (x*x+p*x+q)(x*x+r*x+s)...
    {
    double par3[4] = {-alpha[1]*alpha[1],alpha[2]*alpha[2]-4*alpha[0],2*alpha[2],1};
    std::vector<double> sol2;
    Solve(3,par3,sol2,1,0);
    double val = -1;
    std::vector<double>::iterator itr  = sol2.begin();
    std::vector<double>::iterator itrE = sol2.end();
    for(;itr!=itrE;itr++)
      {
      if(*itr>0) 
        {
        val = *itr;
        break;
        }                       
      }
    if(val<=0) return;
    double p = sqrt(val);
    double r = -p;
    double s = 0.5*(alpha[2]+val+alpha[1]/p);
    double q = 0.5*(alpha[2]+val-alpha[1]/p);
    double par4[3]={s,r,1};
    
    Solve(2,par4,sol2,1,0);
    itr  = sol2.begin();
    itrE = sol2.end();
    for(;itr!=itrE;++itr)
      {
      if(fabs(*itr) < 1e-15) sol.push_back( -0.25 * b );
      else sol.push_back( -0.25*b + *itr); 
      }         
    double par5[3]={q,p,1};
    Solve(2,par5,sol2,1,0); 
    itr  = sol2.begin();
    itrE = sol2.end(); 
    for(;itr!=itrE;++itr)
      {
      if(fabs(*itr) < 1e-15) sol.push_back( -0.25 * b );
      else sol.push_back( -0.25*b + *itr); 
      }         
    }            
  }                                     
}
