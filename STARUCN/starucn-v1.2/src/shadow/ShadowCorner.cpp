/*
======================================================================
STARucn MC software
File : ShadowCorner.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "ShadowCorner.h"
#include "TMath.h"
#include "PolynomialSolver.h"

///////////////////////////////////////////////////////
ShadowCorner::ShadowCorner(double phi1, double phi2) // vertical corner 
///////////////////////////////////////////////////////
{
int s1 = int(TMath::Sin((phi2-phi1)*TMath::Pi()/180.)/fabs(TMath::Sin((phi2-phi1)*TMath::Pi()/180.)));

fCoeffs[0][0]= TMath::Sin(phi1*TMath::Pi()/180.);
fCoeffs[0][1]= -TMath::Cos(phi1*TMath::Pi()/180.); 
fCoeffs[0][2]= 0;
fCoeffs[0][3]= 0;
fCoeffs[1][0]= TMath::Sin(phi2*TMath::Pi()/180.);
fCoeffs[1][1]= -TMath::Cos(phi2*TMath::Pi()/180.);
fCoeffs[1][2]= 0;
fCoeffs[1][3]= 0;
fPlaneSide[0]= s1;
fPlaneSide[1]= -s1;
// compute orthogonal plane coord
double chi = (phi1+phi2)*0.5*TMath::Pi()/180.;

double s = TMath::Sin(chi);
double c = TMath::Cos(chi);
int i1 = Side(c,s,0);
int i2 = Side(s,-c,0);
fCoeffs[2][0]= c;
fCoeffs[2][1]= s;
fCoeffs[2][2]= 0;
fCoeffs[2][3]= 0;
if(i2<0)  fPlaneSide[2]=i1;
else      fPlaneSide[2]=-i1;
}

///////////////////////////////////////////////////////
ShadowCorner::~ShadowCorner()
///////////////////////////////////////////////////////
{;}


///////////////////////////////////////////////////////
void ShadowCorner::Transform()  // Change coordinates 
///////////////////////////////////////////////////////
{
double j = -fT->M(0,0)*fT->T(0) - fT->M(1,0)*fT->T(1) - fT->M(2,0)*fT->T(2);
double k = -fT->M(0,1)*fT->T(0) - fT->M(1,1)*fT->T(1) - fT->M(2,1)*fT->T(2);
double l = -fT->M(0,2)*fT->T(0) - fT->M(1,2)*fT->T(1) - fT->M(2,2)*fT->T(2);
for(int i=0; i<3;i++)
  {
  double G = fT->M(0,0)*fCoeffs[i][0]+fT->M(0,1)*fCoeffs[i][1]+fT->M(0,2)*fCoeffs[i][2];
  double H = fT->M(1,0)*fCoeffs[i][0]+fT->M(1,1)*fCoeffs[i][1]+fT->M(1,2)*fCoeffs[i][2];
  double I = fT->M(2,0)*fCoeffs[i][0]+fT->M(2,1)*fCoeffs[i][1]+fT->M(2,2)*fCoeffs[i][2];
  double J = j*fCoeffs[i][0]+k*fCoeffs[i][1]+l*fCoeffs[i][2]+fCoeffs[i][3];
  fCoeffs[i][0] = G;
  fCoeffs[i][1] = H;
  fCoeffs[i][2] = I;
  fCoeffs[i][3] = J;
  }
}

///////////////////////////////////////////////////////
void ShadowCorner::EvalIJ(int u, double a, double b, std::vector<double>& r)
///////////////////////////////////////////////////////
{
// not implemented yet;
}

///////////////////////////////////////////////////////
int ShadowCorner::Side(double x, double y, double z)
///////////////////////////////////////////////////////
{
double u1 = fCoeffs[0][0]*x+fCoeffs[0][1]*y+fCoeffs[0][2]*z+fCoeffs[0][3];
double u2 = fCoeffs[1][0]*x+fCoeffs[1][1]*y+fCoeffs[1][2]*z+fCoeffs[1][3];
return (fPlaneSide[0]*u1<=0&&fPlaneSide[1]*u2<=0 ? fPlaneSide[0] : -fPlaneSide[0]);
}


///////////////////////////////////////////////////////
double ShadowCorner::ClosestIntersect(double* x, double* y, double* z, double*n) // x=x[0]*t*t+x[1]*t+x[2]  x[3]=result;
///////////////////////////////////////////////////////
{
double par[3];
double t = -1;
double X,Y,Z;
int k;

par[2] = x[0]*fCoeffs[0][0]+y[0]*fCoeffs[0][1]+z[0]*fCoeffs[0][2];
par[1] = x[1]*fCoeffs[0][0]+y[1]*fCoeffs[0][1]+z[1]*fCoeffs[0][2];
par[0] = x[2]*fCoeffs[0][0]+y[2]*fCoeffs[0][1]+z[2]*fCoeffs[0][2]+fCoeffs[0][3];
double S = 0;
for(int i=0;i<3;i++) S+=fabs(par[i]);
for(int i=0;i<3;i++) par[i]/=S;

PolynomialSolver::Solve(2,par,fTmp,true);
std::sort(fTmp.begin(), fTmp.end());
k = fTmp.size();
for(int i=0; i<k; i++)
  {
  if(fTmp[i]>0) 
    {
    X=x[0]*fTmp[i]*fTmp[i]+x[1]*fTmp[i]+x[2];
    Y=y[0]*fTmp[i]*fTmp[i]+y[1]*fTmp[i]+y[2];
    Z=z[0]*fTmp[i]*fTmp[i]+z[1]*fTmp[i]+z[2];
     double u = fCoeffs[2][0]*X+fCoeffs[2][1]*Y+fCoeffs[2][2]*Z+fCoeffs[2][3];
     if(fPlaneSide[2]*u>=0)
       {
      t=fTmp[i]; 
      x[3]=X;
      y[3]=Y;
      z[3]=Z;
      if(n) {n[0]=1; Normal(x[3],y[3],z[3],2*x[0]*t+x[1],2*z[0]*t+z[1],2*z[0]*t+z[1], n);}
      break;
      }
    }
  }

par[2] = x[0]*fCoeffs[1][0]+y[0]*fCoeffs[1][1]+z[0]*fCoeffs[1][2];
par[1] = x[1]*fCoeffs[1][0]+y[1]*fCoeffs[1][1]+z[1]*fCoeffs[1][2];
par[0] = x[2]*fCoeffs[1][0]+y[2]*fCoeffs[1][1]+z[2]*fCoeffs[1][2]+fCoeffs[1][3];
S = 0;
for(int i=0;i<3;i++) S+=fabs(par[i]);
for(int i=0;i<3;i++) par[i]/=S;

PolynomialSolver::Solve(2,par,fTmp,true);
std::sort(fTmp.begin(), fTmp.end());
k = fTmp.size();
for(int i=0; i<k; i++)
  {
  if(fTmp[i]>0&&(fTmp[i]<t||t<0)) 
    {
    X=x[0]*fTmp[i]*fTmp[i]+x[1]*fTmp[i]+x[2];
    Y=y[0]*fTmp[i]*fTmp[i]+y[1]*fTmp[i]+y[2];
    Z=z[0]*fTmp[i]*fTmp[i]+z[1]*fTmp[i]+z[2];
     double u = fCoeffs[2][0]*X+fCoeffs[2][1]*Y+fCoeffs[2][2]*Z+fCoeffs[2][3];
     if(fPlaneSide[2]*u>=0)
       {
      t=fTmp[i]; 
      x[3]=X;
      y[3]=Y;
      z[3]=Z;
      if(n) {n[0]=2;Normal(x[3],y[3],z[3],2*x[0]*t+x[1],2*y[0]*t+y[1],2*z[0]*t+z[1], n);}
      break;
      }
    } 
  }
return t;
}


//////////////////////////////////////////////////////
void ShadowCorner::Normal(double x, double y, double z, double vx, double vy, double vz, double* n)
///////////////////////////////////////////////////////
{
int u= n[0]==1 ? 0 : 1;
n[0] = fCoeffs[u][0];
n[1] = fCoeffs[u][1];
n[2] = fCoeffs[u][2];

ShadowSurface::Normal(x,y,z,vx,vy,vz,n);
}



