/*
======================================================================
STARucn MC software
File : ShadowTorus.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "ShadowTorus.h"
#include "PolynomialSolver.h"
#include <stdio.h>
#include <gsl/gsl_poly.h>
#include "TMath.h"
///////////////////////////////////////////////////////
ShadowTorus::ShadowTorus(double R, double r)
///////////////////////////////////////////////////////
{;
fRadius[0]=R;
fRadius[1]=r;

fQuad [0][0] = 1.;
fQuad [0][1] = 1.;
fQuad [0][2] = 1.;
fCross[0][0] = 0.;
fCross[0][1] = 0.;
fCross[0][2] = 0.;
fLin  [0][0] = 0.;
fLin  [0][1] = 0.;
fLin  [0][2] = 0.;
fConst[0]    = R*R-r*r;
fQuad [1][0] = 4.*R*R;
fQuad [1][1] = 4.*R*R;
fQuad [1][2] = 0.;
fCross[1][0] = 0.;
fCross[1][1] = 0.;
fCross[1][2] = 0.;
fLin  [1][0] = 0.;
fLin  [1][1] = 0.;
fLin  [1][2] = 0.;
fConst[1]    = 0.;
Convert();
}

///////////////////////////////////////////////////////
ShadowTorus::~ShadowTorus()
///////////////////////////////////////////////////////
{;}

///////////////////////////////////////////////////////
void ShadowTorus::Transform()   // Change coordinates 
///////////////////////////////////////////////////////
{
double j = -fT->M(0,0)*fT->T(0) - fT->M(1,0)*fT->T(1) - fT->M(2,0)*fT->T(2);
double k = -fT->M(0,1)*fT->T(0) - fT->M(1,1)*fT->T(1) - fT->M(2,1)*fT->T(2);
double l = -fT->M(0,2)*fT->T(0) - fT->M(1,2)*fT->T(1) - fT->M(2,2)*fT->T(2);

for(int i=0; i<2;i++)
  {
  double A = fT->M(0,0)*fT->M(0,0)*fQuad[i] [0] + fT->M(0,1)*fT->M(0,1)*fQuad[i] [1] + fT->M(0,2)*fT->M(0,2)*fQuad[i] [2];
	     A+= fT->M(0,0)*fT->M(0,1)*fCross[i][0] + fT->M(0,1)*fT->M(0,2)*fCross[i][1] + fT->M(0,2)*fT->M(0,0)*fCross[i][2]; 
  double B = fT->M(1,0)*fT->M(1,0)*fQuad[i] [0] + fT->M(1,1)*fT->M(1,1)*fQuad[i] [1] + fT->M(1,2)*fT->M(1,2)*fQuad[i] [2];
	     B+= fT->M(1,0)*fT->M(1,1)*fCross[i][0] + fT->M(1,1)*fT->M(1,2)*fCross[i][1] + fT->M(1,2)*fT->M(1,0)*fCross[i][2]; 
  double C = fT->M(2,0)*fT->M(2,0)*fQuad[i] [0] + fT->M(2,1)*fT->M(2,1)*fQuad[i] [1] + fT->M(2,2)*fT->M(2,2)*fQuad[i] [2];
	     C+= fT->M(2,0)*fT->M(2,1)*fCross[i][0] + fT->M(2,1)*fT->M(2,2)*fCross[i][1] + fT->M(2,2)*fT->M(2,0)*fCross[i][2]; 
	   
  double D = 2*fT->M(0,0)*fT->M(1,0)*fQuad[i] [0] + 2*fT->M(0,1)*fT->M(1,1)*fQuad[i] [1] + 2*fT->M(0,2)*fT->M(1,2)*fQuad[i] [2];
	     D+= (fT->M(0,0)*fT->M(1,1)+fT->M(0,1)*fT->M(1,0))*fCross[i][0]; 
	     D+= (fT->M(0,1)*fT->M(1,2)+fT->M(0,2)*fT->M(1,1))*fCross[i][1]; 
	     D+= (fT->M(0,2)*fT->M(1,0)+fT->M(0,0)*fT->M(1,2))*fCross[i][2]; 
  double E = 2*fT->M(1,0)*fT->M(2,0)*fQuad[i] [0] + 2*fT->M(1,1)*fT->M(2,1)*fQuad[i] [1] + 2*fT->M(1,2)*fT->M(2,2)*fQuad[i] [2];
	     E+= (fT->M(1,0)*fT->M(2,1)+fT->M(1,1)*fT->M(2,0))*fCross[i][0]; 
	     E+= (fT->M(1,1)*fT->M(2,2)+fT->M(1,2)*fT->M(2,1))*fCross[i][1]; 
	     E+= (fT->M(1,2)*fT->M(2,0)+fT->M(1,0)*fT->M(2,2))*fCross[i][2]; 
  double F = 2*fT->M(2,0)*fT->M(0,0)*fQuad[i] [0] + 2*fT->M(2,1)*fT->M(0,1)*fQuad[i] [1] + 2*fT->M(2,2)*fT->M(0,2)*fQuad[i] [2];
	     F+= (fT->M(2,0)*fT->M(0,1)+fT->M(2,1)*fT->M(0,0))*fCross[i][0]; 
	     F+= (fT->M(2,1)*fT->M(0,2)+fT->M(2,2)*fT->M(0,1))*fCross[i][1];
	     F+= (fT->M(2,2)*fT->M(0,0)+fT->M(2,0)*fT->M(0,2))*fCross[i][2]; 
		 
  double G = 2*fT->M(0,0)*j*fQuad[i] [0] + 2*fT->M(0,1)*k*fQuad[i] [1] + 2*fT->M(0,2)*l*fQuad[i] [2];
	     G+= (fT->M(0,0)*k+fT->M(0,1)*j)*fCross[i][0]; 
	     G+= (fT->M(0,1)*l+fT->M(0,2)*k)*fCross[i][1]; 
	     G+= (fT->M(0,2)*j+fT->M(0,0)*l)*fCross[i][2]; 
	     G+= fT->M(0,0)*fLin[i][0]+fT->M(0,1)*fLin[i][1]+fT->M(0,2)*fLin[i][2];
  double H = 2*fT->M(1,0)*j*fQuad[i] [0] + 2*fT->M(1,1)*k*fQuad[i] [1] + 2*fT->M(1,2)*l*fQuad[i] [2];
	     H+= (fT->M(1,0)*k+fT->M(1,1)*j)*fCross[i][0]; 
	     H+= (fT->M(1,1)*l+fT->M(1,2)*k)*fCross[i][1]; 
	     H+= (fT->M(1,2)*j+fT->M(1,0)*l)*fCross[i][2]; 
	     H+= fT->M(1,0)*fLin[i][0]+fT->M(1,1)*fLin[i][1]+fT->M(1,2)*fLin[i][2];
  double I = 2*fT->M(2,0)*j*fQuad[i] [0] + 2*fT->M(2,1)*k*fQuad[i] [1] + 2*fT->M(2,2)*l*fQuad[i] [2];
	     I+= (fT->M(2,0)*k+fT->M(2,1)*j)*fCross[i][0]; 
	     I+= (fT->M(2,1)*l+fT->M(2,2)*k)*fCross[i][1]; 
	     I+= (fT->M(2,2)*j+fT->M(2,0)*l)*fCross[i][2]; 
	     I+= fT->M(2,0)*fLin[i][0]+fT->M(2,1)*fLin[i][1]+fT->M(2,2)*fLin[i][2];
  double J = j*j*fQuad[i] [0] + k*k*fQuad[i] [1] + l*l*fQuad[i] [2];
         J+= j*k*fCross[i][0] + k*l*fCross[i][1] + l*j*fCross[i][2];
	     J+= j*fLin[i][0]+k*fLin[i][1]+l*fLin[i][2]+fConst[i];
  fQuad [i][0] = A;
  fQuad [i][1] = B;
  fQuad [i][2] = C;
  fCross[i][0] = D;
  fCross[i][1] = E;
  fCross[i][2] = F;
  fLin  [i][0] = G;
  fLin  [i][1] = H;
  fLin  [i][2] = I;
  fConst[i]    = J;
  }
Convert();
}

///////////////////////////////////////////////////////
void ShadowTorus::Convert()
///////////////////////////////////////////////////////
{
  double q =  0.25/(fRadius[0]*fRadius[0]);
  fQuad [2][0] = q*fQuad [1][0];
  fQuad [2][1] = q*fQuad [1][1];
  fQuad [2][2] = q*fQuad [1][2];
  fCross[2][0] = q*fCross[1][0];
  fCross[2][1] = q*fCross[1][1];
  fCross[2][2] = q*fCross[1][2];
  fLin  [2][0] = q*fLin  [1][0];
  fLin  [2][1] = q*fLin  [1][1];
  fLin  [2][2] = q*fLin  [1][2];
  fConst[2]    = q*fConst[1];
  double dr = fRadius[1]*fRadius[1]-fRadius[0]*fRadius[0];
  fQuad [3][0] = fQuad [0][0] - fQuad [2][0];
  fQuad [3][1] = fQuad [0][1] - fQuad [2][1];
  fQuad [3][2] = fQuad [0][2] - fQuad [2][2];
  fCross[3][0] = fCross[0][0] - fCross[2][0];
  fCross[3][1] = fCross[0][1] - fCross[2][1];
  fCross[3][2] = fCross[0][2] - fCross[2][2];
  fLin  [3][0] = fLin  [0][0] - fLin  [2][0];
  fLin  [3][1] = fLin  [0][1] - fLin  [2][1];
  fLin  [3][2] = fLin  [0][2] - fLin  [2][2];
  fConst[3]    = dr + fConst [0] - fConst [2];
}

///////////////////////////////////////////////////////
void ShadowTorus::EvalIJ(int u0, double a, double b, std::vector<double>& r)
///////////////////////////////////////////////////////
{
int u1 = (u0+1)%3;
int u2 = (u0+2)%3;

double A1 = fQuad[0][u0];
double B1 = fCross[0][u0]*a+fCross[0][u2]*b+fLin[0][u0];
double C1 = fQuad[0][u1]*a*a + fQuad[0][u2]*b*b + fCross[0][u1]*a*b + fLin[0][u1]*a + fLin[0][u2]*b + fConst[0];
double A2 = fQuad[1][u0];
double B2 = fCross[1][u0]*a+fCross[1][u2]*b+fLin[1][u0];
double C2 = fQuad[1][u1]*a*a + fQuad[1][u2]*b*b + fCross[1][u1]*a*b + fLin[1][u1]*a + fLin[1][u2]*b + fConst[1];

double par[5];
par[0] = C1*C1-C2;
par[1] = 2*B1*C1 - B2;
par[2] = B1*B1 + 2*A1*C1 - A2;
par[3] = 2*A1*B1;
par[4] = A1*A1  ;
PolynomialSolver::Solve(4, par, r);
}

///////////////////////////////////////////////////////
int ShadowTorus::Side(double x, double y, double z) 
///////////////////////////////////////////////////////
{
double u1 = fQuad[2][0]*x*x+fQuad[2][1]*y*y+fQuad[2][2]*z*z+fCross[2][0]*x*y+fCross[2][1]*y*z+fCross[2][2]*z*x+fLin[2][0]*x+fLin[2][1]*y+fLin[2][2]*z+fConst[2];
double u2 = fQuad[3][0]*x*x+fQuad[3][1]*y*y+fQuad[3][2]*z*z+fCross[3][0]*x*y+fCross[3][1]*y*z+fCross[3][2]*z*x+fLin[3][0]*x+fLin[3][1]*y+fLin[3][2]*z+fConst[3];
double k = (fRadius[0]-TMath::Sqrt(u1))*(fRadius[0]-TMath::Sqrt(u1)) + u2 - fRadius[1]*fRadius[1];
return (k<=0 ? 1 : -1);
}

///////////////////////////////////////////////////////
double ShadowTorus::ClosestIntersect(double* x, double* y, double* z, double* n) // x=x[0]*t*t+x[1]*t+x[2]*t  x[3]=result;
///////////////////////////////////////////////////////
{
// Torus equation as Q1^2 = Q2,  Q1, Q2 quadrics
//
double par[9];
double A[2],B[2],C[2],D[2],E[2];
for(int i=0; i<2;i++)
  {
  A[i] = x[0]*x[0]*fQuad[i][0]+y[0]*y[0]*fQuad[i][1]+z[0]*z[0]*fQuad[i][2];
  A[i]+= x[0]*y[0]*fCross[i][0]+y[0]*z[0]*fCross[i][1]+z[0]*x[0]*fCross[i][2];
  B[i] = 2*x[0]*x[1]*fQuad[i][0]+2*y[0]*y[1]*fQuad[i][1]+2*z[0]*z[1]*fQuad[i][2];
  B[i]+= (x[0]*y[1]+x[1]*y[0])*fCross[i][0]+(y[0]*z[1]+y[1]*z[0])*fCross[i][1]+(z[0]*x[1]+z[1]*x[0])*fCross[i][2];
  C[i] = (x[1]*x[1]+2*x[0]*x[2])*fQuad[i][0]+(y[1]*y[1]+2*y[0]*y[2])*fQuad[i][1]+(z[1]*z[1]+2*z[0]*z[2])*fQuad[i][2];
  C[i]+= (x[1]*y[1]+x[0]*y[2]+x[2]*y[0])*fCross[i][0]+(y[1]*z[1]+y[0]*z[2]+y[2]*z[0])*fCross[i][1]+(z[1]*x[1]+z[0]*x[2]+z[2]*x[0])*fCross[i][2];
  C[i]+= x[0]*fLin[i][0]+y[0]*fLin[i][1]+z[0]*fLin[i][2];
  D[i] = 2*x[1]*x[2]*fQuad[i][0]+2*y[1]*y[2]*fQuad[i][1]+2*z[1]*z[2]*fQuad[i][2];
  D[i]+= (x[1]*y[2]+x[2]*y[1])*fCross[i][0]+(y[1]*z[2]+y[2]*z[1])*fCross[i][1]+(z[1]*x[2]+z[2]*x[1])*fCross[i][2];
  D[i]+= x[1]*fLin[i][0]+y[1]*fLin[i][1]+z[1]*fLin[i][2];
  E[i] = x[2]*x[2]*fQuad[i][0]+y[2]*y[2]*fQuad[i][1]+z[2]*z[2]*fQuad[i][2];
  E[i]+= x[2]*y[2]*fCross[i][0]+y[2]*z[2]*fCross[i][1]+z[2]*x[2]*fCross[i][2];
  E[i]+= x[2]*fLin[i][0]+y[2]*fLin[i][1]+z[2]*fLin[i][2]+fConst[i];
  }

par[8] = A[0]*A[0];                              // t^8;
par[7] = 2*A[0]*B[0];                            // t^7;
par[6] = B[0]*B[0]+2*A[0]*C[0];                  // t^6;
par[5] = 2*(B[0]*C[0]+A[0]*D[0]);                // t^5;
par[4] = C[0]*C[0]+2*(A[0]*E[0]+B[0]*D[0])+A[1]; // t^4;
par[3] = 2*(B[0]*E[0]+C[0]*D[0])          +B[1]; // t^3;
par[2] = D[0]*D[0]+2*C[0]*E[0]            +C[1]; // t^2;
par[1] = 2*D[0]*E[0]                      +D[1]; // t^1;
par[0] = E[0]*E[0]                        +E[1]; // t^0;

double S = 0;
for(int i=0;i<9;i++) S+=fabs(par[i]);
for(int i=0;i<9;i++) par[i]/=S;
int o=8;
for(int i=8; i>=0;--i)
  {
  if(fabs(par[i])<1e-12) o--;
  else break;
  }
// use gsl here
double sol[16];
gsl_poly_complex_workspace * w  = gsl_poly_complex_workspace_alloc (o+1);
gsl_poly_complex_solve (par, o+1, w, sol);
gsl_poly_complex_workspace_free (w);

double t = -1;
for(int i=0; i<o;i++)
  {
  if(sol[2*i+1]!=0) continue;
  if(sol[2*i]<0)    continue;
  if(sol[2*i]<t || t<0) t=sol[2*i];
  }
if(t>=0)
  {
  x[3]=x[0]*t*t+x[1]*t+x[2];
  y[3]=y[0]*t*t+y[1]*t+y[2];
  z[3]=z[0]*t*t+z[1]*t+z[2];
  if(n) Normal(x[3],y[3],z[3],2*x[0]*t+x[1],2*y[0]*t+y[1],2*z[0]*t+z[1], n);
  }

return t;
}

///////////////////////////////////////////////////////
void ShadowTorus::Normal(double x, double y, double z, double vx, double vy, double vz, double* n)
///////////////////////////////////////////////////////
{
for(int i=0; i<3;i++)
  {
  n[i] = fQuad [0][0]*x*x+fQuad [0][1]*y*y+fQuad [0][2]*z*z;
  n[i]+= fCross[0][0]*x*y+fCross[0][1]*y*z+fCross[0][2]*z*z;
  n[i]+= fLin[0][0]*x+fLin[0][1]*y+fLin[0][2]*z+fConst[0];
  }
n[0]*= 2*(2*fQuad[0][0]*x+fCross[0][0]*y+fCross[0][2]*z+fLin[0][0]);
n[0]-= 2*fQuad[1][0]*x+fCross[1][0]*y+fCross[1][2]*z+fLin[1][0];
n[1]*= 2*(2*fQuad[0][1]*x+fCross[0][1]*y+fCross[0][0]*z+fLin[0][1]);
n[1]-= 2*fQuad[1][1]*y+fCross[1][1]*z+fCross[1][0]*x+fLin[1][1];
n[2]*= 2*(2*fQuad[0][2]*x+fCross[0][2]*y+fCross[0][1]*z+fLin[0][2]);
n[2]-= 2*fQuad[1][2]*z+fCross[1][2]*x+fCross[1][1]*y+fLin[1][2];
ShadowSurface::Normal(x,y,z,vx,vy,vz,n);
}

