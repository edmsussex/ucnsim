/*
======================================================================
STARucn MC software
File : ShadowTransform.cpp
Copyright 2013 Benoit Cl�ment
mail : bclement_AT_lpsc.in2p3.fr
======================================================================
This file is part of STARucn. 
STARucn is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
======================================================================
*/
#include "ShadowTransform.h"
#include "TMath.h"

///////////////////////////////////////////////////////
ShadowTransform::ShadowTransform(double* trans, double* rot)
///////////////////////////////////////////////////////
{
SetTranslation(trans);	
SetRotation(rot);
}

///////////////////////////////////////////////////////
ShadowTransform::~ShadowTransform() {;}
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
void ShadowTransform::SetTranslation(double * t)
///////////////////////////////////////////////////////
{
SetTranslation(t[0],t[1],t[2]);
}

///////////////////////////////////////////////////////
void ShadowTransform::SetTranslation(double tx,double ty,double tz)
///////////////////////////////////////////////////////
{
fDx = tx;
fDy = ty;
fDz = tz;
}

///////////////////////////////////////////////////////
void ShadowTransform::SetRotation(double *r)
///////////////////////////////////////////////////////
{
SetRotation(r[0],r[1],r[2]);
}
///////////////////////////////////////////////////////
void ShadowTransform::SetRotation(double a,double b,double c)
///////////////////////////////////////////////////////
{
fCa = TMath::Cos(a/180.*TMath::Pi());
fSa = TMath::Sin(a/180.*TMath::Pi());
fCb = TMath::Cos(b/180.*TMath::Pi());
fSb = TMath::Sin(b/180.*TMath::Pi());
fCc = TMath::Cos(c/180.*TMath::Pi());
fSc = TMath::Sin(c/180.*TMath::Pi());

fMatrix[0][0] =  fCc*fCa - fCb*fSa*fSc;
fMatrix[0][1] = -fSc*fCa - fCb*fSa*fCc;
fMatrix[0][2] =  fSb*fSa;
fMatrix[1][0] =  fCc*fSa + fCb*fCa*fSc;
fMatrix[1][1] = -fSc*fSa + fCb*fCa*fCc;
fMatrix[1][2] = -fSb*fCa;
fMatrix[2][0] =  fSc*fSb;
fMatrix[2][1] =  fCc*fSb;
fMatrix[2][2] =  fCb;
}
///////////////////////////////////////////////////////
void ShadowTransform::Transform(double* in , double* out)
///////////////////////////////////////////////////////
{
for(int i=0; i<3;i++) out[i]=in[i];
Transform(out[0],out[1],out[2]);
}

///////////////////////////////////////////////////////
void ShadowTransform::Transform(double& x,double& y,double& z)
///////////////////////////////////////////////////////
{
double X = fMatrix[0][0]*x+fMatrix[0][1]*y+fMatrix[0][2]*z;
double Y = fMatrix[1][0]*x+fMatrix[1][1]*y+fMatrix[1][2]*z;
double Z = fMatrix[2][0]*x+fMatrix[2][1]*y+fMatrix[2][2]*z;
x=X+fDx; y=Y+fDy; z=Z+fDz;
}

///////////////////////////////////////////////////////
void ShadowTransform::InvTransform(double* in , double* out)
///////////////////////////////////////////////////////
{
for(int i=0; i<3;i++) out[i]=in[i];
InvTransform(out[0],out[1],out[2]);
}

///////////////////////////////////////////////////////
void ShadowTransform::InvTransform(double& x,double& y,double& z)
///////////////////////////////////////////////////////
{
x-=fDx; y-=fDy; z-=fDz;
double X = fMatrix[0][0]*x+fMatrix[0][1]*y+fMatrix[0][2]*z;
double Y = fMatrix[1][0]*x+fMatrix[1][1]*y+fMatrix[1][2]*z;
double Z = fMatrix[2][0]*x+fMatrix[2][1]*y+fMatrix[2][2]*z;
x=X; y=Y; z=Z;
}

