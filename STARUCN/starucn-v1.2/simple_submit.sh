#!/bin/bash
#$ -q mps.q
#$ -S /bin/bash
#$ -j y
#$ -pe openmp 64-1


export PATH
module load sge
module load gcc
module add python
module add gsl/gcc/1.16
source ~/src/root/bin/thisroot.sh

cd ~/src/ucnsim/STARUCN/starucn-v1.2
source bin/thisstarucn.sh
cd

source /home/na280/.bash_profile
source /home/na280/.bashrc
cd /home/na280/src/ucnsim/STARUCN/starucn-v1.2
echo "starting starucn with arguments"
echo $*
./starucn_threaded $*
