#!/usr/bin/env python

import os
import numpy
import shutil
import random
import math
from fractions import Fraction
import math

print "STARucn T2 Script - Nicholas Ayres 2014 na280@sussex.ac.uk"
print "This generates several base scripts, then qsubs several to run starucn for each value"

NbParticles = 128 #number of neutrons to start with for each alpha value, must be even
random.seed(624889041) #seed for RNG, in turn seeds STARucn RNGs
runid = "alpha" #so we know how to name everything...
minstoragetime = 15.0
Vxy = 5.0 #starting velocity in xy plane - need to change manually in basescript as well
trap_radius = 0.250 #[m] - need to change manually in basescript as well

runscriptpath = os.getcwd() + "/alpha_distributed_submit.sh"

alphaArray = numpy.linspace(1,90,90)
#alphaArray = numpy.array([90])


def starucnthread(runsection):
	#base script
	basescriptpath = os.getcwd() + '/scripts/false_edm_vs_alpha_base.par'
	
	#folder in which to place automatically generated starucn scripts
	outscriptfolder = os.getcwd() + '/scripts/autogenscripts'
	
	#output file
	outputfile = "output_" + runid + "_" + str(runsection) + ".root"
	
	
	
	print "target alpha = ", alphaArray[runsection]
	
	if alphaArray[runsection] == 0:
		print "alpha == 0, skipping"
		return
	
	alpha = alphaArray[runsection]

	#round up so storage time is integer number of chord traversals-> starting position same as final
	#position, except rotated (doesn't matter in isotropic volume)
	chordtime = 2.0 * trap_radius * math.sin(math.pi*alpha/180.0) / Vxy
	#we want to have an even integer of chordtime
	storage_time = math.ceil( minstoragetime / (chordtime*2)) * chordtime * 2
	
	print "storage time = " , storage_time
	
	print "chordtime = " , chordtime
	
	print "no chords = " , storage_time/chordtime
	
	
	
	scriptpath = outscriptfolder + '/' + runid + "_" + str(runsection) + ".par"
	print "copying ", basescriptpath, " to " , scriptpath
	shutil.copy(basescriptpath,scriptpath) #copy base script
	print "Opening File ", scriptpath
	
	f = open(scriptpath,'a') #open for appending
	
	print "Writing to File"
	#write lines to file
	
	f.write("int NbParticles " + str(NbParticles) + "\n")
	f.write("string SpinRootOutputInterface.RunID " + runid + "\n")
	f.write("int SpinRootOutputInterface.RunSection " + str(runsection) + "\n")
	f.write("double SpinRootOutputInterface.StorageTime " + str(storage_time) + "\n")
	f.write("double VolumeGenerator.Alpha " + str(alpha) + "\n")
	f.write("double SpinRootOutputInterface.OuterLoopVariableValue " + str(alpha) + "\n")
	f.write("int GenerationSeed " + str(random.randint(0,2**31)) + "\n")
	print "closing file"
	f.close()
	
	os.system("rm " +outputfile)
	
	#command = "./starucn_threaded " + scriptpath
	command = "qsub -q mps.q -N " + runid + "_" + str(runsection) + " -o $HOME/out/out_"+ runid + "_" + str(runsection)+ " " + runscriptpath + " " + scriptpath
	print "queueing alpha with command " , command
	os.system(command)
	


for runsection in range (len (alphaArray)):
	starucnthread(runsection)

	
