#!/usr/bin/env python

import glob
import numpy
import math
import matplotlib
from rootpy.tree import Tree
from rootpy.io import root_open
from root_numpy import tree2array
from matplotlib import pyplot

filename = "fedm_vs_dBdz.root"

hbar = 1.05457173e-34
B0z = 1e-6 #T
#dBdz = 5e-9 #T/m
R = 0.235 #m
gyro = -1.83247179e8
J = 0.5
c = 2.998e8
w0 = -gyro * B0z
e_charge = 1.602e-19
V=5

def eq29(dBdz):
	dBdz = dBdz
	Vxy = numpy.sqrt(V**2 * 2.0 /3.0)
	wr_star_squared = math.pi**2 /6 * (Vxy/R)**2
	bracket = 1/(1-(wr_star_squared/w0**2))
	falseedm = - (J*hbar/2) * (dBdz/B0z**2) * (Vxy**2/c**2) * bracket
	falseedm_ecm = falseedm * 100 / e_charge
	#print falseedm_ecm
	return falseedm_ecm

def eq20_lazy(dBdz):
	dBdz = dBdz
	Vxy = numpy.sqrt(V**2 * 2.0 /3.0)
	#wr_star_squared = math.pi**2 /6 * (Vxy/R)**2
	bracket = 1
	falseedm = - (J*hbar/2) * (dBdz/B0z**2) * (Vxy**2/c**2) * bracket
	falseedm_ecm = falseedm * 100 / e_charge
	#print falseedm_ecm
	return falseedm_ecm


def getresults(outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	resultsarray = tree2array(resultstree)
	falseedm = []
	error = []
	dBdz = []	
	
	for row in resultsarray:
		falseedm.append(row[14])
		error.append(row[15])
		dBdz.append(row[25]*1000)
	return falseedm,error,dBdz

fedm_mc, fedm_error_mc, dBdz_mc = getresults(filename)

dBdz_analytic = numpy.linspace(-35*10**-9,35*10**-9,10000)
fedm_analytic  = eq29(dBdz_analytic)

pyplot.plot(dBdz_analytic,fedm_analytic,label="eq29")

pyplot.errorbar(dBdz_mc,fedm_mc,yerr=fedm_error_mc,fmt='+',label="STARucn")

pyplot.legend()
pyplot.xlabel("dBz/dz")
pyplot.ylabel("False EDM (ecm)")

pyplot.figure(2)

discrepancy = numpy.zeros(len(dBdz_mc))
for i in range(len(dBdz_mc)):
	theory = eq29(dBdz_mc[i])
	discrepancy[i] = theory - fedm_mc[i]

pyplot.scatter(dBdz_mc,discrepancy,label="residual")
pyplot.legend()
#pyplot.xlim(min(dBdz_mc)*1.1,max(dBdz_mc)*1.1)
#pyplot.ylim(max(fedm_mc)*1.1,max(fedm_mc)*1.1)
pyplot.xlabel("dBz/dz")
pyplot.ylabel("False EDM residual (ecm)")
pyplot.show()
