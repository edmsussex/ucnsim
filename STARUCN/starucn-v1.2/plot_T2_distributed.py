#!/usr/bin/env python

import csv
from matplotlib import pyplot
import numpy

csvfile = open("T2_T2_dBzdx.txt",'rb')
reader = csv.reader(csvfile,delimiter='\t')
dBdz = []
T2 = []
T2_err = []
for row in reader:
	#print row
	dBdz.append(float(row[1]))
	T2.append(float(row[2]))
	T2_err.append(float(row[3]))

dBdz = numpy.array(dBdz)
T2 = numpy.array(T2)
T2_err = numpy.array(T2_err)

#print dBdz, T2, T2_err

pyplot.errorbar(dBdz,T2,yerr=T2_err,label="STARucn MC",fmt='+')

pyplot.ylim(0,1.3*T2[1])

pyplot.xlabel("dBdz - T/m")
pyplot.ylabel("T2 - s")
pyplot.savefig("T2_dBdz.png")
pyplot.show()
