#!/usr/bin/env python

#before running, do pip install --user rootpy
#pip install --user root_numpy

import os
from scipy import optimize
import numpy
import shutil
import random
import math
import sys
from rootpy.tree import Tree
from rootpy.io import root_open
from root_numpy import tree2array
import csv

def getalpha(i,outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	resultsarray = tree2array(resultstree)
	alpha = resultsarray[i][6]
	alphaerror = resultsarray[i][7]
	return alpha,alphaerror
	
def gettruefalseedm(i,outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	resultsarray = tree2array(resultstree)
	falseedm = resultsarray[i][14]
	error = resultsarray[i][15]
	return falseedm,error
	
def getmeasuredfalseedm(i,outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	resultsarray = tree2array(resultstree)
	falseedm = resultsarray[i][20]
	error = resultsarray[i][21]
	return falseedm,error

print "STARucn T2 Script - Nicholas Ayres 2014 na280@sussex.ac.uk"
print "This generates a run file, runs STARucn on it, reads alpha from file, runs until alpha < endalpha then calculates T2"

print "arguments passed = "
for arg in sys.argv:
	print arg

endalpha = float(sys.argv[5]) #finish when alpha gets below this number
timestep = float(sys.argv[6]) #timestep in seconds
random.seed(sys.argv[4]) #seed for RNG, in turn seeds STARucn RNGs
runid = sys.argv[1]
runsection = sys.argv[2]
killtime = float(sys.argv[7])
basescriptpath = sys.argv[8]
dBdz = float(sys.argv[3])

print "runid = ", runid
print "runsection = " , runsection
print "dBdz = " , dBdz
print "random seed = " , sys.argv[4]
print "endalpha = ", endalpha
print "timestep = ", timestep
print "killtime = ", killtime
print "basescriptpath = " , basescriptpath

#folder in which to place automatically generated starucn scripts
outscriptfolder = os.getcwd() + '/scripts/autogenscripts'

#output file
outputfile = "output_" + runid + "_" + str(runsection) + ".root"

os.system("rm " + outputfile)

	
	
#for starting parameters alpha = 1 exactly at t = 0
alphalist = numpy.array([1])
alphaerrorlist = numpy.array([0])
storagetimelist = numpy.array([0])

dBdzArray = numpy.linspace(-20e-12,20e-12,5)
#dBdzPointFitArray = numpy.linspace(-20e-12,20e-12,100)
T2Array = numpy.zeros(len(dBdzArray))
T2ErrorArray = numpy.zeros(len(dBdzArray))
true_fedm_array = numpy.zeros(len(dBdzArray))
true_fedm_errarray = numpy.zeros(len(dBdzArray))
meas_fedm_array = numpy.zeros(len(dBdzArray))
meas_fedm_errarray = numpy.zeros(len(dBdzArray))

i = 0

print "timestep = " , timestep ,", killtime = " , killtime

while (alphalist[i] > endalpha and (i * timestep) < killtime):
#while (i<10):
#while (i * timestep) < killtime:
	print "i * timestep = " , i * timestep, ", killtime = " , killtime
	scriptpath = outscriptfolder + '/' + runid + "_" + str(runsection) + "_" + str(i) + '.par'
	shutil.copy(basescriptpath,scriptpath) #copy base script
	print "Opening File"
	f = open(scriptpath,'a') #open for appending
	#first time use EDMVolumeGenerator, then use EDMRootFileGenerator
	print "Writing to File"
	if (i == 0):
		f.write("mcsetup Generator VolumeGenerator  @{this} \n")
	else:
		f.write("mcsetup Generator RootFileGenerator  @{this} \n")
	#write lines to file
	f.write("string RootFileGenerator.inputruntimeslice " + str(i-1)+ "\n")
	f.write("int SpinRootOutputInterface.TimeSlice " + str(i)+ "\n")
	f.write("double SpinRootOutputInterface.StorageTime " + str ((i+1) * timestep)+ "\n")
	f.write("int GenerationSeed "  + str(random.randint(0,2147483647))+ "\n") #scale random number to largest integer
	
	print "closing file"
	f.close()
	
	print "running starucn with command \"./starucn_threaded " + scriptpath + "\" "
	os.system("./starucn_threaded " + scriptpath + " > /dev/null")
	
	alpha,alphaerror = getalpha(i,outputfile)
	
	storagetimelist = numpy.append(storagetimelist,(i+1)*timestep)
	alphalist = numpy.append(alphalist,alpha)
	alphaerrorlist = numpy.append(alphaerrorlist,alphaerror)
	i=i+1
	print "at time " + str(i)+"alpha = " + str(alpha) + "+/-" + str(alphaerror)
	print alphalist[i], endalpha, 

print alphalist
print alphaerrorlist
	
#function to fit
def expdecay(t,A,T):
	return A*numpy.exp(-t/T)

popt, pcov = optimize.curve_fit(expdecay,storagetimelist,alphalist)#,None,alphaerrorlist,True)
print "found popt, pcov ", popt, pcov
try:
	perr = numpy.sqrt(numpy.diag(pcov))
except:
	perr= [0,0]
print "T2 = ", popt[1], "+-" , perr[1]

true_fedm, true_fedm_err = gettruefalseedm(i-1,outputfile)
meas_fedm, meas_fedm_err = getmeasuredfalseedm(i-1,outputfile)

print "True False EDM = " , true_fedm, "+-", true_fedm_err
print "Measured False EDM = ", meas_fedm , "+-", meas_fedm_err

#form row to write to csv: runsection | dBdz | T2 | T2_error
row = (runsection, dBdz, popt[1], perr[1])
csvfile = open("T2_"+runid+".txt",'ab')
csvwriter =  csv.writer(csvfile,delimiter = '\t')
csvwriter.writerow(row)
csvfile.close()

