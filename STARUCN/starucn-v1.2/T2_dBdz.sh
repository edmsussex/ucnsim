#!/bin/bash
#$ -pe openmp 10 -q mps.q 
#$ -S /bin/bash
#$ -N T2_dBdz
export JOB_DIR=$0
export JOB_PARAM1=$1
#$ -o /home/na280/outdBdz
#$ -e /home/na280/err
#$ -j y

export PATH

module load sge #job queueing system
module load gcc

module add python #things for starucn
module add cmake
module add gcc
module add gsl/gcc/1.16 # load gsl compiled with gcc (defaults to intel otherwise)
source ~/src/root/bin/thisroot.sh # source the root file just compiled


cd ~/src/ucnsim/STARUCN/starucn-v1.2
source bin/thisstarucn.sh
cd

source /home/na280/.bash_profile
cd /home/na280/src/ucnsim/STARUCN/starucn-v1.2
./T2_dBdz.py
