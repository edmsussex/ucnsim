#!/usr/bin/env python

import glob
import numpy
import math
import matplotlib
import csv
from rootpy.tree import Tree
from rootpy.io import root_open
from root_numpy import tree2array
from matplotlib import pyplot

hbar = 1.05457173e-34 #[Js]
B0z = 1e-6 #[T]
dBdz = 1e-9 #[T/m]
R = 0.250 #[m]
Vxy = 5 #[m/s]
gyro = -1.83247179e8 #[rad s^-1 T^-1]
J = 0.5 
c = 2.998e8 #[m/s]
w0 = -gyro * B0z
e_charge = 1.602e-19 #[C]


def eq78(alpha):
	delta=w0*R*numpy.sin(alpha*math.pi/180)/Vxy #delta is amount by which the phase increases per half-chord travelled
	#print delta
	bracket = 1+ ((numpy.sin(alpha*math.pi/180)**2 * numpy.sin(2*delta))/(2*delta*numpy.sin(delta - alpha*math.pi/180)*numpy.sin(delta + alpha*math.pi/180)))
	#print bracket
	falseedm = - (J*hbar/2) * (dBdz/B0z**2) * (Vxy**2/c**2) * bracket
	falseedm_ecm = falseedm * 100 / e_charge
	#print falseedm_ecm
	return falseedm_ecm

def eq29():
	wr_star_squared = math.pi**2 /6 * (Vxy/R)**2
	bracket = 1/(1-(wr_star_squared/w0**2))
	falseedm = - (J*hbar/2) * (dBdz/B0z**2) * (Vxy**2/c**2) * bracket
	falseedm_ecm = falseedm * 100 / e_charge
	#print falseedm_ecm
	return falseedm_ecm


def gettruefalseedm(i,outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	resultsarray = tree2array(resultstree)
	falseedm = resultsarray[i][14]
	error = resultsarray[i][15]
	alpha = resultsarray[i][25]
	#print alpha, " " , falseedm, "+/- " , error
	return falseedm,error,alpha
	
filelist = glob.glob("output_alpha_*.root")
print filelist
print len(filelist)

falseedmlist = []
falseedmerrorlist=[]
alphalist = []
tuplelist = []

for i in range ( len(filelist) ):
	try:
		falseedm, error, alpha = gettruefalseedm(0,filelist[i])
		falseedmlist.append(falseedm)
		falseedmerrorlist.append(error)
		alphalist.append (alpha)
	except:
		print "error in ",filelist[i]

chi_squared = 0
for i in range(len(alphalist)):
	chi_squared += (falseedmlist[i] - eq78(alphalist[i]))**2 / falseedmerrorlist[i]**2

print "chi squared is ", chi_squared, "reduced chi sq = ", chi_squared/len(alphalist)

pyplot.ylim(-3,5) #keep axis sensible despite resonances
pyplot.xlim(0,90)

alpha_analytic = numpy.linspace(0.00001,90,10000)
fedm_analytic  = eq78(alpha_analytic)

print eq78(90)

normalised_fedm_analytic = fedm_analytic / eq29()
normalised_fedm_mc = numpy.array(falseedmlist) / eq29()
normalised_fedm_error_mc = numpy.array(falseedmerrorlist) / eq29()

file_fedm_analytic = eq78(numpy.array(alphalist))
difference = numpy.array(falseedmlist) - file_fedm_analytic
file_normalised_fedm_analytic = file_fedm_analytic/eq29()

outputfile = open("out.txt",'w')
csvwriter = csv.writer(outputfile,dialect='excel-tab')

toprow = ["alpha (degrees)","STARucn False EDM (ecm)","eq78 theoretical False EDM (ecm)", "deviation (ecm)" , "STARucn normalised false edm","eq78 normalsed false edm"]
csvwriter.writerow(toprow)

for i in range(len(alphalist)):
	row = [alphalist[i],falseedmlist[i],file_fedm_analytic[i],difference[i],normalised_fedm_mc[i],file_normalised_fedm_analytic[i]]
	csvwriter.writerow(row)

pyplot.plot(alpha_analytic,normalised_fedm_analytic,label="eq13", color="green")

pyplot.scatter(alphalist,normalised_fedm_mc,marker='x',label ="STARucn", color = "red")
pyplot.errorbar(alphalist,normalised_fedm_mc,yerr=normalised_fedm_error_mc,fmt='+',label="STARucn")

pyplot.legend()
pyplot.xlabel("alpha (degrees)")
pyplot.ylabel("Normalised False EDM")

pyplot.show()

