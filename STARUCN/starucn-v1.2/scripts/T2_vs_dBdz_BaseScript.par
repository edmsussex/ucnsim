#======================================================================
#STARucn MC software
#File : STARucn.par
#Copyright 2013 Benoit Cl�ment
#mail : bclement_AT_lpsc.in2p3.fr
#======================================================================
#This file is part of STARucn. 
#STARucn is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================


#############################################################################################""
### Main script for STARucn simulation
#############################################################################################""

#####################
### Running mode
#####################
#string RunMode  Display
#string RunMode  Animate
#string RunMode  AnimateGraphics
string RunMode  Generate
#string RunMode  Trajectory
#string RunMode  Test

#####################
int Verbose  0
#####################

#####################
### Geometry file (include material descritpion)
#####################

#string   Geometry    @{thispath}geo/Cryo.geo
#string   Geometry    @{thispath}geo/TestBox.geo
string   Geometry    @{thispath}geo/RoomTemp.geo
#string   Geometry    @{thispath}geo/STARucn.geo


#############################################################################################
### Generation parameters

### Number of generated particless
#int NbParticles    1000
int MaxBounce      50000

### Seeds of the random generators
#int GenerationSeed      236412312
#int InteractionSeed     65433213
#####################
### Generation tool
#####################
#mcsetup  Generator   VolumeGenerator      @{this}
#mcsetup  Generator   FileGenerator       @{this}
#mcsetup Generator    VolumeGenerator  @{this}
#mcsetup Generator    RootFileGenerator  @{this}

#####################
### Interaction tool
#####################
#mcsetup  Interaction NeutronInteraction             @{this}
mcsetup Interaction NeutronInteraction		@{this}

#####################
### Propagation tool
#####################
mcsetup Propagator SpinPropagator       @{this}
#mcsetup  Propagator  EDMPropagator       @{this}
#mcsetup  Propagator  StepPropagator       @{this}

#####################
### Output tool
#####################

#mcsetup  Output     TextOutputInterface    @{this}
#mcsetup  Output      CompleteRootOutputInterface    @{this}
#mcsetup Output      MultiOutputInterface    @{this}
mcsetup  Output      SpinRootOutputInterface    @{this}


#############################################################################################
### Only used for Trajectory runmode

string Trajectory.InputFile  output.root
string Trajectory.InputTree  starucn

#############################################################################################
### Only used for Animate runmode
int    Animate.NbFrames     1000
double Animate.TimeStep     0.1
string Animate.BaseName     animation
string Animate.OutputDir    anim
camera  Animate.ThetaAngle   const   12.6 
camera  Animate.PhiAngle     function   -80+360*x/20.  

#camera  Animate.OpenGLFov     const   45
#camera  Animate.OpenGLDolly   const   0
#camera  Animate.OpenGLCenterX const   500
#camera  Animate.OpenGLCenterY const   0
#camera  Animate.OpenGLCenterZ const   0

double  Animate.Timefactor   1  
string  Animate.Display      StdROOT  ##    OpenGL           
int     Animate.StartFrame   1
int     Animate.StopFrame    200
double  Animate.MarkerSize   0.5
animset  Animate.Set1    [v]>4000   2
animset  Animate.Blah    [v]<=4000  8

######################################################################################
######################################################################################
######################################################################################
#################                 PLUGINS SETUPS                     #################
######################################################################################
######################################################################################
######################################################################################

#####################
#Particle Setup
#####################
#gyromagnetic ratio of particle in s-1 T-1

#this value is 2010 CODATA recommended values
double Particle.gyro -1.83247179e8

#####################
#[FileGenerator]
#####################
string     FileGenerator.InputFile        Input.dat

#####################
#[VolumeGenerator]
#####################
### angular distribution according to geometry master frame,  cos theta with respect to X, phi in transverse plane

#string    VolumeGenerator.Volume        BigBox
string 	VolumeGenerator.Volume 	BigTube
double    VolumeGenerator.Rate          -5
string    VolumeGenerator.Mode    CPV
density   VolumeGenerator.Density_C    Uniform   -1  1
density   VolumeGenerator.Density_P    Uniform   0   180
density   VolumeGenerator.Density_V    Uniform   5000   5000

#####################
#[StepPropagator]
#####################
double    StepPropagator.StepSize         1        ## [mm], if negative, use adaptative step
double    StepPropagator.Tolerance        0.0001   ## [mm]
bool      StepPropagator.UseGravity       1
#if you change UseGravity here, also change it for SpinPropagator


#####################
#[AnalyticPropagator]
#####################
double    AnalyticPropagator.StepSize         -1        ## [mm], if negative, use adaptative step
double    AnalyticPropagator.Tolerance        0.0001   ## [mm]
bool      AnalyticPropagator.UseGravity       1

#####################
#[Light/CompleteRootOutputInterface]
#####################
int       CompleteRootOutputInterface.TrajectoryRecord   0
string    CompleteRootOutputInterface.OutputFile      output.root

#####################
#[MultiOutputInterface]
#####################
int       MultiOutputInterface.NbOutputs    2
mcsetup   MultiOutputInterface.Output_1  CompleteRootOutputInterface  @{this}
mcsetup   MultiOutputInterface.Output_2  TextOutputInterface        @{this}

#####################
#[TextOutputInterface]
#####################
string    TextOutputInterface.OutputFile      output.txt
bool      TextOutputInterface.DoRootTree      0
string    TextOutputInterface.StopMode        Time
double    TextOutputInterface.StopTime        10     #seconds


################
####EDM stuff###
################

#############################
###Fixed field propagator###
#############################
double EDMPropagator.plane_ux 1
double EDMPropagator.plane_uy 0
double EDMPropagator.plane_uz 0

double EDMPropagator.plane_vx 0
double EDMPropagator.plane_vy 1
double EDMPropagator.plane_vz 0

##################
###Field System###
##################
#These can be a list of several fields to sum. Fields must be separated by , and NO SPACES
string FieldManager.BFields BGrad,BDip
string FieldManager.EFields E
string FieldManager.ERevFields Erev

string FieldManager.Fields.E  FixedE:UniformField
string FieldManager.Fields.RamseyFlip RamseyFlip:TimeVaryingField
string FieldManager.Fields.BGrad BGrad:ZGradientField
string FieldManager.Fields.BDip BDip:DipoleField
string FieldManager.Fields.Erev  FixedErev:UniformField

#string FieldManager.RamseyFlip.xformula 2e-6*sin(t*183.247137)
#string FieldManager.RamseyFlip.yformula 2e-6*cos(t*183.247137)
#string FieldManager.RamseyFlip.zformula 0
#bool   FieldManager.RamseyFlip.use_field_boundaries 1
#double FieldManager.RamseyFlip.start_time 0
#double FieldManager.RamseyFlip.end_time 0.001364278  #for pi/2 flip

double FieldManager.FixedE.x 0
double FieldManager.FixedE.y 0
double FieldManager.FixedE.z 5e6

double FieldManager.FixedErev.x 0
double FieldManager.FixedErev.y 0
double FieldManager.FixedErev.z -5e6

double FieldManager.BGrad.Fx0 0
double FieldManager.BGrad.Fy0 0
double FieldManager.BGrad.Fz0 1e-6
bool   FieldManager.BGrad.use_field_boundaries 1
double FieldManager.BGrad.start_time 0
double FieldManager.BGrad.end_time 10000

#this is Tesla/mm!
double FieldManager.BGrad.dFdz 5e-12


double FieldManager.BDip.posx 0
double FieldManager.BDip.posy 0
double FieldManager.BDip.posz -10

double FieldManager.BDip.mdmx 0
double FieldManager.BDip.mdmy 0
double FieldManager.BDip.mdmz 6.283e-7

#############################
###Adaptive EDM Propagator###
#############################

string  SpinPropagator.PropagatorMode RungeKuttaCashKarp
#string  SpinPropagator.PropagatorMode StepDoubling

bool	SpinPropagator.UseGravity 1
#if you set UseGravity here, it must be matched by StepPropagator.UseGravity

double	SpinPropagator.plane_ux 1
double	SpinPropagator.plane_uy 0
double	SpinPropagator.plane_uz 0

double	SpinPropagator.plane_vx 0
double	SpinPropagator.plane_vy 1
double	SpinPropagator.plane_vz 0

double	SpinPropagator.max_time_step 1e-2
double	SpinPropagator.min_time_step 1e-20
double	SpinPropagator.target_error 1e-10

bool SpinPropagator.disable_vxE 0


###############################
###Depolarisation at Surface###
###############################

#same for all types of surface and surface interaction including trans...
bool NeutronInteraction.UseSurfaceDepolarisation 0
double NeutronInteraction.DepolarisationProbability 0.0001 #always between 0 and 

############################
###SpinRootOutputInterface###
############################
int       SpinRootOutputInterface.TrajectoryRecord   0
string    SpinRootOutputInterface.BaseOutputFile      output
#double    SpinRootOutputInterface.StorageTime 2
#string    SpinRootOutputInterface.RunID test
#int		  SpinRootOutputInterface.RunSection 0
#int       SpinRootOutputInterface.TimeSlice 1
string    SpinRootOutputInterface.OuterLoopVariableName outerloopvar
double    SpinRootOutputInterface.OuterLoopVariableValue 1

bool      SpinRootOutputInterface.UseBootstrapErrors 1
int       SpinRootOutputInterface.nBootstrap 50
########################
###VolumeGenerator###
########################

#start all neutrons with identical spin vectors
string VolumeGenerator.spin_generator_mode identical

#start all neutrons with spin vectors in a plane. Note starting positions of spin and spinRevE will be different
#string VolumeGenerator.spin_generator_mode plane

#start all neutrons with spin vectors in random direction. starting values of spin and spinRevE will again be different
#string VolumeGenerator.spin_generator_mode random

# for plane generator mode, spins all start in plane containing u.v
double VolumeGenerator.spin_plane_ux 1
double VolumeGenerator.spin_plane_uy 0
double VolumeGenerator.spin_plane_uz 0
double VolumeGenerator.spin_plane_vx 0
double VolumeGenerator.spin_plane_vy 1
double VolumeGenerator.spin_plane_vz 0

#for identical generator mode, starting spin vector for all particles
double VolumeGenerator.spin_startphase_x 1
double VolumeGenerator.spin_startphase_y 0
double VolumeGenerator.spin_startphase_z 0

##########################
###RootFileGenerator###
##########################

string RootFileGenerator.base_input_file output
#string RootFileGenerator.inputrunid test
#string RootFileGenerator.inputrunsection 0
#string RootFileGenerator.inputruntimeslice 0

###################################
###Automatically Generated Lines###
###################################

