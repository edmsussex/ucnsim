#======================================================================
#STARucn MC software
#File : STARucn.par
#Copyright 2013 Benoit Clement
#mail : bclement_AT_lpsc.in2p3.fr
#======================================================================
#This file is part of STARucn. 
#STARucn is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================


mcsetup
  - string class
  - string config

material
  - double fermi
  - double diffusion
  - double eta
  - double lifetime
  - int    color

materialproperty
  - double property

matrix
  - double transX
  - double transY
  - double transZ
  - double rotX
  - double rotY
  - double rotZ  

volume
  - string material
  - string type
  - string coord
  - string matrix
  - int    active

composite
  - string material
  - string definition
  - string matrix
  - bool   active

geometry
  - double sizeX
  - double sizeY
  - double sizeZ
  - string volumes

density
  - string formula
  - double min
  - double max

camera
  - string type
  - string value

animset
  - string def
  - int color
