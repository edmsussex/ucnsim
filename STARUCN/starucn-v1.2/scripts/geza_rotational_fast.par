#======================================================================
#STARucn MC software
#File : STARucn.par
#Copyright 2013 Benoit Cl�ment
#mail : bclement_AT_lpsc.in2p3.fr
#======================================================================
#This file is part of STARucn. 
#STARucn is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================


#############################################################################################""
### Main script for STARucn simulation
#############################################################################################""

#####################
### Running mode
#####################
#string RunMode  Display
#string RunMode  Animate
#string RunMode  AnimateGraphics
string RunMode  Generate
#string RunMode  Trajectory
#string RunMode  Test

#####################
int Verbose  0
#####################

#####################
### Geometry file (include material descritpion)
#####################

#string   Geometry    @{thispath}geo/Cryo.geo
#string   Geometry    @{thispath}geo/TestBox.geo
#string   Geometry    @{thispath}geo/RoomTemp.geo
#string   Geometry    @{thispath}geo/STARucn.geo
#string   Geometry @{thispath}geo/Alpha.geo

string   Geometry    @{thispath}geo/geza.geo

#################################################
### Threading Parameters

#for these to have any effect, you must execute starucn_threaded

bool OverrideDefaultThreads 0
int NbThreadsOverride 1 #if OverrideDefaultThreads 1, how many threads to run.

#############################################################################################
### Generation parameters

### Number of generated particless
int NbParticles    24000 #24000 in final version
int MaxBounce      50000

### Seeds of the random generators
int GenerationSeed      2362222
#####################
### Generation tool
#####################
#mcsetup  Generator   FileGenerator       @{this}
mcsetup Generator    VolumeGenerator  @{this}
#mcsetup Generator    RootFileGenerator  @{this}

#####################
### Interaction tool
#####################
mcsetup Interaction NeutronInteraction		@{this}

#####################
### Propagation tool
#####################
mcsetup Propagator SpinPropagator       @{this}
#mcsetup Propagator  StepPropagator       @{this}
#mcsetup Propagator  AnalyticPropagator   @{this}

#####################
### Output tool
#####################
#only known threadsafe output interface
mcsetup  Output      SpinRootOutputInterface    @{this}


#############################################################################################
### Only used for Trajectory runmode

string Trajectory.InputFile  output.root
string Trajectory.InputTree  starucn

#############################################################################################
### Only used for Animate runmode
int    Animate.NbFrames     1000
double Animate.TimeStep     0.1
string Animate.BaseName     animation
string Animate.OutputDir    anim
camera  Animate.ThetaAngle   const   12.6 
camera  Animate.PhiAngle     function   -80+360*x/20.  

#camera  Animate.OpenGLFov     const   45
#camera  Animate.OpenGLDolly   const   0
#camera  Animate.OpenGLCenterX const   500
#camera  Animate.OpenGLCenterY const   0
#camera  Animate.OpenGLCenterZ const   0

double  Animate.Timefactor   1  
string  Animate.Display      StdROOT  ##    OpenGL           
int     Animate.StartFrame   1
int     Animate.StopFrame    200
double  Animate.MarkerSize   0.5
animset  Animate.Set1    [v]>4000   2
animset  Animate.Blah    [v]<=4000  8

######################################################################################
######################################################################################
######################################################################################
#################                 PLUGINS SETUPS                     #################
######################################################################################
######################################################################################
######################################################################################

#####################
#Particle Setup
#####################
#gyromagnetic ratio of particle in s-1 T-1

#this value is for a neutron - source 2010 CODATA recommended values
double Particle.gyro -1.83247179e8

#####################
#[FileGenerator]
#####################
string     FileGenerator.InputFile        Input.dat

#####################
#[VolumeGenerator]
#####################
### angular distribution according to geometry master frame,  cos theta with respect to X, phi in transverse plane

string 	VolumeGenerator.Volume 	StartSlice
double    VolumeGenerator.Rate          -5
string    VolumeGenerator.Mode    CPV
density   VolumeGenerator.Density_C    Uniform   -1  1
density   VolumeGenerator.Density_P    Uniform   0   180
density   VolumeGenerator.Density_V    Uniform   3912.1652017   3912.1652017 ## = 80 neV

#overrides any other velocity formula
bool VolumeGenerator.UseVelocityHistogram 0
string VolumeGenerator.VelocityHistogramPath velocity_histogram.txt


#Alpha is the half-angle between wall collisions
#this mode starts neutrons at x=0, z=0, vx=+/-V, vy=0, vz=0, and y at appropriate distance 
#See "Geometric-phase-induced false electric dipole moment signals for particles in traps"
# Pendelbury et. al. 2004
#overrides above position/direction settings
bool   VolumeGenerator.UseAlphaMode 0
double VolumeGenerator.Alpha 10 #in degrees
double VolumeGenerator.TrapRadius 235 #in mm

#start all neutrons with identical spin vectors
string VolumeGenerator.spin_generator_mode identical

#start all neutrons with spin vectors in a plane. Note starting positions of spin and spinRevE will be different
#string VolumeGenerator.spin_generator_mode plane

#start all neutrons with spin vectors in random direction. starting values of spin and spinRevE will again be different
#string VolumeGenerator.spin_generator_mode random

# for plane generator mode, spins all start in plane containing u.v
double VolumeGenerator.spin_plane_ux 1
double VolumeGenerator.spin_plane_uy 0
double VolumeGenerator.spin_plane_uz 0
double VolumeGenerator.spin_plane_vx 0
double VolumeGenerator.spin_plane_vy 1
double VolumeGenerator.spin_plane_vz 0

#for identical generator mode, starting spin vector for all particles
double VolumeGenerator.spin_startphase_x 0
double VolumeGenerator.spin_startphase_y 0
double VolumeGenerator.spin_startphase_z 1

#######################
###RootFileGenerator###
#######################

#This opens a file named in the format [base_input_file]_[inputrunsection].root
#and looks for a TNtupleD called [base_input_file]_[inputrunsection]_[inputruntimeslice]
#it is intended to open the output of SpinRootOutputInterface

#Note that Input and Output files must be the same or ROOT doesn't work...

string RootFileGenerator.base_input_file output
string RootFileGenerator.inputrunid alpha
string RootFileGenerator.inputrunsection 0
string RootFileGenerator.inputruntimeslice 0

#####################
#[StepPropagator]
#####################
double    StepPropagator.StepSize         -1        ## [mm], if negative, use adaptative step
double    StepPropagator.Tolerance        0.0001   ## [mm]
bool      StepPropagator.UseGravity       1
#if you change UseGravity here, also change it for SpinPropagator


#####################
#[AnalyticPropagator]
#####################
double    AnalyticPropagator.StepSize         -1        ## [mm], if negative, use adaptative step
double    AnalyticPropagator.Tolerance        0.0001   ## [mm]
bool      AnalyticPropagator.UseGravity       0


####################
#[SpinPropagator]
####################

double    SpinPropagator.StepSize         -1        ## [mm], if negative, use adaptative step
double    SpinPropagator.Tolerance        1e-8   ## [mm]

#This Determines how the field is integrated for averaging

string  SpinPropagator.PropagatorMode RungeKuttaCashKarp
#string  SpinPropagator.PropagatorMode NonAdaptiveRungeKuttaCashKarp
#string  SpinPropagator.PropagatorMode MinimalTestPropagate

bool	SpinPropagator.UseGravity 1

#u and v are vectors defining the plane in which to track the phase - usually choose
# to be perpendicular to start plane
double	SpinPropagator.plane_ux 1
double	SpinPropagator.plane_uy 0
double	SpinPropagator.plane_uz 0

double	SpinPropagator.plane_vx 0
double	SpinPropagator.plane_vy 1
double	SpinPropagator.plane_vz 0

double	SpinPropagator.max_time_step 1e-4
double	SpinPropagator.min_time_step 1e-9
double	SpinPropagator.target_error 1e-7

bool SpinPropagator.disable_vxE 0

#####################
#[Light/CompleteRootOutputInterface]
#####################
int       CompleteRootOutputInterface.TrajectoryRecord   0
string    CompleteRootOutputInterface.OutputFile      output.root

#############################
###SpinRootOutputInterface###
#############################

#This opens (or creates) a file named in the format [base_input_file]_[inputrunsection].root
#and creates a TNtupleD called [BaseOutputFile]_[RunSection]_[TimeSlice]

#Note that Input and Output files must be the same or ROOT doesn't work...

int       SpinRootOutputInterface.TrajectoryRecord   0
string    SpinRootOutputInterface.BaseOutputFile      output
double    SpinRootOutputInterface.StorageTime 12
string    SpinRootOutputInterface.RunID geza_rotational_fast
int		  SpinRootOutputInterface.RunSection 0
int       SpinRootOutputInterface.TimeSlice 0
string    SpinRootOutputInterface.OuterLoopVariableName test
double    SpinRootOutputInterface.OuterLoopVariableValue 0

bool      SpinRootOutputInterface.UseBootstrapErrors 1
int       SpinRootOutputInterface.nBootstrap 50

#####################
#[MultiOutputInterface]
#####################
int       MultiOutputInterface.NbOutputs    2
mcsetup   MultiOutputInterface.Output_1  CompleteRootOutputInterface  @{this}
mcsetup   MultiOutputInterface.Output_2  TextOutputInterface        @{this}

#####################
#[TextOutputInterface]
#####################
string    TextOutputInterface.OutputFile      output.txt
bool      TextOutputInterface.DoRootTree      0
string    TextOutputInterface.StopMode        Time
double    TextOutputInterface.StopTime        10     #seconds

##################
###Field System###
##################
#These can be a list of several fields to sum. Fields must be separated by , and NO SPACES
#Example: string FieldManager.BFields BGrad,RamseyFlip

string FieldManager.BFields B0,B1
string FieldManager.EFields E
string FieldManager.ERevFields Erev


#This tells STARucn how each field is defined
#Valid types are UniformField, TimeVaryingField, ZGradientField, DipoleField and FieldMap
string FieldManager.Fields.B1 B1:TimeVaryingField
string FieldManager.Fields.B0 B0:TimeVaryingField
#string FieldManager.Fields.B0 B0:ZGradientField


#Set up our Fields- units are Tesla for B fields or V/m for E fields

string FieldManager.B1.xformula 4.286004116e-9*sin(t*183.247179) #ang freq = neutron gyro * 1uT
string FieldManager.B1.yformula 4.286004116e-9*cos(t*183.247179)
string FieldManager.B1.zformula 0

#if you want your fields to turn on and off, use these lines. Omit if you don't care
bool   FieldManager.B1.use_field_boundaries 1
double FieldManager.B1.start_time 10
double FieldManager.B1.end_time 12

#double FieldManager.B0.Fx0 0
#double FieldManager.B0.Fy0 0
#double FieldManager.B0.Fz0 1e-6

#this is Tesla/mm!
#double FieldManager.B0.dFdz 40e-12

string FieldManager.B0.xformula 0
string FieldManager.B0.yformula 0
string FieldManager.B0.zformula 1e-6+z*40e-12

##########################
###[NeutronInteraction]###
##########################

#if bool NeutronInteraction.UseSurfaceDepolarisation 0, the probability that the Spin Vector will be set to
#a random (isotropic) value
#same for all types of surface and surface interaction including trans...

bool NeutronInteraction.UseSurfaceDepolarisation 0
double NeutronInteraction.DepolarisationProbability 0.0001 #always between 0 and 1

#if 1, disable beta decay of UCN
bool NeutronInteraction.NoDecay 1

###################################
###Automatically Generated Lines###
###################################
#This space is left for the included python run scripts to add lines to
