#======================================================================
#STARucn MC software
#File : STARucn.par
#Copyright 2013 Benoit Clément
#mail : bclement_AT_lpsc.in2p3.fr
#======================================================================
#This file is part of STARucn. 
#STARucn is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================
#
# The cell might be too fat


int Transparency 70
matrix Id  0  0  0     1000 1000 1000 # identity

%%%
material NAME      Fermi(neV)   D      Eta     Lifetime(s)  Color
%%%
material MaxAbs        250      0        0.5        0            51
material TotDiff       250      1        0          0            61
material Mixed         250      0.99     0          0            61

material Vacuum      0          0        0          888          2
material Vacuum2     0          0        0          888          3
material Steel       184        0.05     1e-4       0            60
material Abs	     0          0        0          0            6

material Al        54.1         0.1    0.001       3.3e-4        3
material Ti        -51.1        0.1    0.001       1.3e-5        5


### test materials
#define mat MaxAbs
#define mat TotDiff
define mat Mixed

define rad 250   
define hei 120 
define thi 10
define drad 15
define dhei 60
define thinslice 1e-3

### modules placing
volume BigTube Vacuum  Tube  0|@{rad}|@{hei}/2  Id  0 


matrix M2   0    0    (@{hei}/2+@{thi})                1000 1000 1000 
matrix M3   0    0   -(@{hei}/2+@{thi}+@{thinslice})   1000 1000 1000 
matrix M8   0    0   -(@{hei}/2+@{thinslice}/2)        1000 1000 1000 

volume Top            Mixed   Tube   0|@{rad}+@{thi}|@{thi} 	 	M2 0 
volume Side           Mixed   Tube   @{rad}|@{rad}+@{thi}|@{hei}/2 	Id 0
volume Bottom 	      Mixed   Tube   0|@{rad}+@{thi}|@{thi} 	M3 0
volume StartSlice     Vacuum2 Tube   0|@{rad}|@{thinslice}/2 	M8 0
volume StartSliceEdge Mixed   Tube   @{rad}|@{rad}+@{thi}|@{thinslice}/2 	M8 0

geometry Geo 200 200 200 BigTube|Top|Bottom|Side|StartSlice|StartSliceEdge


