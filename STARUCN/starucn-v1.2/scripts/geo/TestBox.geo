#======================================================================
#STARucn MC software
#File : STARucn.par
#Copyright 2013 Benoit Cl�ment
#mail : bclement_AT_lpsc.in2p3.fr
#======================================================================
#This file is part of STARucn. 
#STARucn is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================



int Transparency 70
matrix Id  0  0  0     1000 1000 1000 # identity

material Vacuum        0            0        0             10000000          87
material MaxAbs        250          0        0.5           0            51
material TotDiff       250          1        0             0            61
material Mixed         250          0.2      0          0            61

### test materials
#define mat MaxAbs
#define mat TotDiff
define mat Mixed


### modules placing
volume BigBox Vacuum  Box  100|100|100  Id  0 

matrix M2   0    0    105   1000 1000 1000 
matrix M3   0    0   -105   1000 1000 1000 
matrix M4   0    105  0     1000 1000 1000 
matrix M5   0   -105  0     1000 1000 1000 
matrix M6   105  0    0     1000 1000 1000 
matrix M7  -105  0    0     1000 1000 1000 

volume Top    @{mat}   Box  100|100|5  M2 0 
volume Bottom @{mat}   Box  100|100|5  M3 0 
volume Front  @{mat}   Box  100|5|110  M4 0 
volume Back   @{mat}   Box  100|5|110  M5 0 
volume Right  @{mat}   Box  5|110|110  M6 0 
volume Left   @{mat}   Box  5|110|110  M7 0 

geometry Geo  200 200 200  BigBox|Top|Bottom|Front|Back|Right|Left



