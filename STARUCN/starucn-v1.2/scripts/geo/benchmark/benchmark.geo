#======================================================================
#STARucn MC software
#File : STARucn.par
#Copyright 2013 Benoit Cl�ment
#mail : bclement_AT_lpsc.in2p3.fr
#======================================================================
#This file is part of STARucn. 
#STARucn is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================


%%%
material NAME      Fermi(neV)   D      Eta     Lifetime(s)  Color
%%%

material Vacuum      0            0       0       888            2
material Steel       184          0.05    1e-4       0              60
material Abs	     0           0        0         0               6

material Al        54.1         0.1    0.001     3.3e-4            3
material Ti        -51.1        0.1    0.001   1.3e-5            5

define detlist V1c|V1b|Lv2c|Lv2b|Rv2c|Rv2b|Lv3c|Lv3b|Rv3c|Rv3b|Lv4c|Lv4b|Rv4c|Rv4b|Lv5c|Lv5b|Rv5c|Rv5b|Lwin1|Lwin2|Rwin1|Rwin2

define U_hgt  700*0.5 
define U_len  300*0.5 
define P_len  100*0.5 
define sec    50*0.5
define rad    60
define thk    10

define U_angle  20
matrix Id  0 0 0 1000 1000 1000

### U central part
matrix m1 0 -S(@{U_angle})*@{U_hgt}*2 C(@{U_angle})*@{U_hgt}*2    0 @{U_angle} 0 
volume V1a Steel  Box  @{U_len}|@{sec}+@{thk}|@{sec}+@{thk}  m1 0
volume V1b Vacuum  Box  @{U_len}|@{sec}|@{sec}  m1 0
composite V1c Steel V1a:m1-V1b:m1  Id  0  

### U upper elbow
matrix Lm2 -@{U_len} -S(@{U_angle})*(@{U_hgt}*2-@{rad}) C(@{U_angle})*(@{U_hgt}*2-@{rad})    0 90+@{U_angle} 0 
volume Lv2a Steel  Tubs @{rad}-@{sec}-@{thk}|@{rad}+@{sec}+@{thk}|@{sec}+@{thk}|90|180  Lm2 0 
volume Lv2b Vacuum  Tubs @{rad}-@{sec}|@{rad}+@{sec}|@{sec}|90|180 Lm2 0 
composite Lv2c Steel Lv2a:Lm2-Lv2b:Lm2  Id  0

matrix Rm2 @{U_len} -S(@{U_angle})*(@{U_hgt}*2-@{rad}) C(@{U_angle})*(@{U_hgt}*2-@{rad})    0 90+@{U_angle} 0 
volume Rv2a Steel  Tubs @{rad}-@{sec}-@{thk}|@{rad}+@{sec}+@{thk}|@{sec}+@{thk}|0|90  Rm2 0 
volume Rv2b Vacuum  Tubs @{rad}-@{sec}|@{rad}+@{sec}|@{sec}|0|90   Rm2 0 
composite Rv2c Steel Rv2a:Rm2-Rv2b:Rm2  Id  0

### U vertical

matrix Lm3 -@{U_len}-@{rad} -S(@{U_angle})*@{U_hgt}  C(@{U_angle})*@{U_hgt}    0 @{U_angle} 0 
volume Lv3a Steel  Box  @{sec}+@{thk}|@{sec}+@{thk}|@{U_hgt}-@{rad}}  Lm3 0 
volume Lv3b Vacuum  Box  @{sec}|@{sec}|@{U_hgt}-@{rad}}  Lm3 0 
composite Lv3c Steel Lv3a:Lm3-Lv3b:Lm3  Id  0

matrix Rm3 @{U_len}+@{rad} -S(@{U_angle})*@{U_hgt}  C(@{U_angle})*@{U_hgt}    0 @{U_angle} 0 
volume Rv3a Steel  Box  @{sec}+@{thk}|@{sec}+@{thk}|@{U_hgt}-@{rad}}  Rm3 0 
volume Rv3b Vacuum  Box  @{sec}|@{sec}|@{U_hgt}-@{rad}}  Rm3 0 
composite Rv3c Steel Rv3a:Rm3-Rv3b:Rm3  Id  0

### U lower elbow

matrix Lm4 -@{U_len}-2*@{rad} -S(@{U_angle})*@{rad} C(@{U_angle})*@{rad}    0 90+@{U_angle} 0 
volume Lv4a Steel  Tubs @{rad}-@{sec}-@{thk}|@{rad}+@{sec}+@{thk}|@{sec}+@{thk}|270|360  Lm4 0 
volume Lv4b Vacuum  Tubs @{rad}-@{sec}|@{rad}+@{sec}|@{sec}|270|360  Lm4 0 
composite Lv4c Steel Lv4a:Lm4-Lv4b:Lm4  Id  0

matrix Rm4 @{U_len}+2*@{rad} -S(@{U_angle})*@{rad} C(@{U_angle})*@{rad}    0 90+@{U_angle} 0 
volume Rv4a Steel  Tubs @{rad}-@{sec}-@{thk}|@{rad}+@{sec}+@{thk}|@{sec}+@{thk}|180|270  Rm4 0 
volume Rv4b Vacuum  Tubs @{rad}-@{sec}|@{rad}+@{sec}|@{sec}|180|270  Rm4 0 
composite Rv4c Steel Rv4a:Rm4-Rv4b:Rm4  Id  0

### U low horizontal

matrix Lm5 -@{U_len}-2*@{rad}-@{P_len} 0  0     0 @{U_angle} 0 
volume Lv5a Steel  Box  @{P_len}|@{sec}+@{thk}|@{sec}+@{thk}  Lm5 0 
volume Lv5b Vacuum  Box  @{P_len}|@{sec}|@{sec}   Lm5 0 
composite Lv5c Steel Lv5a:Lm5-Lv5b:Lm5  Id  0

matrix Rm5 @{U_len}+2*@{rad}+@{P_len} 0  0     0 @{U_angle} 0 
volume Rv5a Steel  Box  @{P_len}|@{sec}+@{thk}|@{sec}+@{thk}  Rm5 0 
volume Rv5b Vacuum  Box  @{P_len}|@{sec}|@{sec}   Rm5 0 
composite Rv5c Steel Rv5a:Rm5-Rv5b:Rm5  Id  0


### Generation window
matrix Lm6 -@{U_len}-2*@{rad}-2*@{P_len}-0.1 0  0     0 @{U_angle} 0 
volume Lwin0 Steel  Box  0.1|@{sec}|@{sec}   Lm6 0 
volume Lwin1 Vacuum  Box  0.1|@{sec}|@{sec}   Lm6 1 
composite Lwin2 Steel Lwin0:Lm6-Lwin1:Lm6  Id  0



### Detection window
matrix Rm6 @{U_len}+2*@{rad}+2*@{P_len}+0.1 0  0     0 @{U_angle} 0 
volume Rwin0 Steel  Box  0.1|@{sec}|@{sec}   Rm6 0 
volume Rwin1 Abs  Box  0.1|@{sec}|@{sec}   Rm6 2 
composite Rwin2 Steel Rwin0:Rm6-Rwin1:Rm6  Id  0

geometry Geo  1500 500 800 @{detlist}

