int Transparency 50
matrix Id  0  0  0     1000 1000 1000 # identity

material Media1         0          0      0          200           63
material Media2         0          0      0          200           91
material Media3         3000        0      0          -1            91

### test materials
define H   200
define L   150
define h   150
define l   120
define w   30
### modules placing

##S
matrix M1   @{L}-0.25*(@{H}+@{w})   0   @{w}   1000 1000 1000 
volume S1 Media1 Box @{L}-0.25*(@{H}+@{w})|@{w}|@{w} M1  0
matrix M2   2*@{L}-(@{H}+@{w})*0.5    0   0.5*(@{H}+@{w})   0  90  0
volume S2 Media1 Tubs 0.5*(@{H}-3*@{w})|0.5*(@{H}+@{w})|@{w}|-90|90 M2  0
matrix M3   (@{H}+@{w})*0.5    0   1.5*@{H}-0.5*@{w}   0  90  0
volume S3 Media1 Tubs 0.5*(@{H}-3*@{w})|0.5*(@{H}+@{w})|@{w}|90|270 M3  0
matrix M4   @{L}    0   @{H}    1000 1000 1000 
volume S4 Media1 Box @{L}-0.5*(@{H}+@{w})|@{w}|@{w} M4  0

## S and T
matrix M5   2*@{L}-(@{H}+@{w})*0.25+(@{H}+@{w})*0.5    0   2*@{H}-@{w}   1000 1000 1000 
volume ST1 Media1 Box 2*@{L}-(@{H}+@{w})*0.25|@{w}|@{w} M5  0

## T
matrix M6   3*@{L}    0   @{H}-@{w}   1000 1000 1000 
volume T1 Media1 Box @{w}|@{w}|@{H}-@{w} M6  0

## u
matrix M7   3*@{L}  -@{h}+@{l}*0.5+@{w} -@{w}  1000 1000 1000 
volume u1 Media2 Box @{w}|@{h}-@{l}*0.5|@{w} M7  0

matrix M8   3*@{L}+@{l}-@{w}   -2*@{h}+@{l}+@{w}  -@{w} 1000 1000 1000 
volume u2 Media2 Tubs @{l}-2*@{w}|@{l}|@{w}|180|0 M8  0

matrix M9   3*@{L}+2*@{l}-@{w}*2    -@{h}+@{l}*0.5+@{w} -@{w}  1000 1000 1000 
volume u3 Media2 Box @{w}|@{h}-@{l}*0.5|@{w} M9  0

## A
matrix M10   3*@{L}+2*@{l}-@{w}*2    0    @{H}*(1-@{w}*2/@{L})  1000 1000 1000 
volume A1 Media1 Arb8 @{H}*(1-2*@{w}/@{L})|-@{w}|-@{w}|-@{w}|@{w}|@{w}|@{w}|@{w}|-@{w}|-@{w}+@{L}-2*@{w}|-@{w}|-@{w}+@{L}-2*@{w}|@{w}|@{w}+@{L}-2*@{w}|@{w}|@{w}+@{L}-2*@{w}|-@{w}         M10  0

matrix M11   4*@{L}+2*@{l}-3*@{w}    0    @{H}*(2-2*@{w}/@{L})  1000 1000 1000 
volume A2 Media1 Trd1  @{w}*2|0|@{w}|2*@{H}*@{w}/@{L} M11  0


matrix M12   5*@{L}+2*@{l}-4*@{w}    0    @{H}*(1-2*@{w}/@{L})  1000 1000 1000 
volume A3 Media1 Arb8 @{H}*(1-2*@{w}/@{L})|-@{w}|-@{w}|-@{w}|@{w}|@{w}|@{w}|@{w}|-@{w}|-@{w}-@{L}+2*@{w}|-@{w}|-@{w}-@{L}+2*@{w}|@{w}|@{w}-@{L}+2*@{w}|@{w}|@{w}-@{L}+2*@{w}|-@{w}         M12  0
## c
matrix M13   5*@{L}+@{l}-3*@{w}+0.5*@{h}  0   -@{w} 1000 1000 1000 
volume c1 Media2 Box @{l}-0.5*@{h}|@{w}|@{w} M13  0

matrix M14   5*@{L}+@{h}-3*@{w}   -@{h}+@{w}  -@{w} 1000 1000 1000 
volume c2 Media2 Tubs @{h}-2*@{w}|@{h}|@{w}|90|270 M14  0

matrix M15   6*@{L}-3*@{w}+0.5*@{h}   -2*@{h}+@{w}*2  -@{w} 1000 1000 1000 
volume c3 Media2  Box @{L}-0.5*@{h}|@{w}|@{w} M15  0

## n
matrix M16   7*@{L}-2*@{w}  -@{h}+@{w}   -@{w} 1000 1000 1000 
volume n1 Media2 Box @{w}|@{h}|@{w} M16  0

define W (@{w}*1.5)
matrix M17   7*@{L}+@{l}-3*@{w} -@{W}+@{w} -@{w}  -90 -90 0 
volume n2 Media2 Arb8 @{l}-2*@{w}|-@{W}|-@{w}|-@{W}|@{w}|@{W}|@{w}|@{W}|-@{w}|-@{W}+2*@{h}-2*@{W}|-@{w}|-@{W}+2*@{h}-2*@{W}|@{w}|@{W}+2*@{h}-2*@{W}|@{w}|@{W}+2*@{h}-2*@{W}|-@{w}         M17  0

matrix M18  7*@{L}+2*@{l}-4*@{w} -@{h}+@{w}   -@{w} 1000 1000 1000 
volume n3 Media2  Box @{w}|@{h}|@{w} M18  0

## R
matrix M19   7*@{L}+2*@{l}-3*@{w}-(@{H}+@{w})*0.5    0   1.5*@{H}-0.5*@{w}    0  90  0
volume R1 Media1 Tubs 0.5*(@{H}-3*@{w})|0.5*(@{H}+@{w})|@{w}|-90|90 M19  0

matrix M20   5*@{L}+2*@{l}-2*@{w}    0   @{H}   1000 1000 1000 
volume R2 Media1 Box @{w}|@{w}|@{H} M20  0

matrix M21   6*@{L}+2*@{l}-2*@{w}-(@{H}+@{w})*0.25     0   2*@{H}-@{w}   1000 1000 1000 
volume R3 Media1 Box @{L}-0.25*(@{H}+@{w})-@{w}|@{w}|@{w} M21  0

matrix M22   6*@{L}+2*@{l}-2*@{w}-(@{H}+@{w})*0.25   0   @{H}   1000 1000 1000 
volume R4 Media1 Box @{L}-0.25*(@{H}+@{w})-@{w}|@{w}|@{w} M22  0

matrix M23   7*@{L}+@{l}   0 0.5*(@{H}-@{w})   1000 1000 1000 
volume R5 Media1 Arb8 0.5*(@{H}-@{w})|-@{w}|-@{w}|-@{w}|@{w}|@{w}|@{w}|@{w}|-@{w}|-@{w}-2*@{L}+5*@{w}|-@{w}|-@{w}-2*@{L}+5*@{w}|@{w}|@{w}-2*@{L}+5*@{w}|@{w}|@{w}-2*@{L}+5*@{w}|-@{w}         M23  0

matrix M25   4*@{L}  -@{h}  1.5*@{H}    1000 1000 1000 
volume MasterBox  Media3 Box   5*@{L}|2*@{h}|2*@{H}  M25 0
composite Ext  Media3  MasterBox:M25-S1:M1-S2:M2-S3:M3-S4:M4-ST1:M5-T1:M6-u1:M7-u2:M8-u3:M9-A1:M10-A2:M11-A3:M12-c1:M13-c2:M14-c3:M15-n1:M16-n2:M17-n3:M18-R1:M19-R2:M20-R3:M21-R4:M22-R5:M23 Id 0                            
geometry Geo  10000 2000 4000    S1|S2|S3|S4|ST1|T1|u1|u2|u3|A1|A2|A3|c1|c2|c3|n1|n2|n3|R1|R2|R3|R4|R5 |Ext



