\documentclass[12pt,fleqn]{article}

\usepackage{graphicx}
\usepackage{mathptmx}
\usepackage{helvet}
\usepackage{epstopdf}
\usepackage{slashed}
\usepackage{lscape}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[switch]{lineno}
\usepackage[scriptsize,normalsize]{caption}
\usepackage{multicol}


\textwidth175mm
\textheight247mm
\topmargin-10mm
\oddsidemargin-10mm
\evensidemargin-10mm
\parindent0mm

%%%%%%%%%%%%%% End of Commands %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

%%%%%%%%%%%%%% Begin Main Body %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
\begin{LARGE}
STARucn\\
\end{LARGE}
\begin{Large}
a software for {\bf S}imulation of {\bf T}ransmission, {\bf A}bsoption and {\bf R}eflection of {\bf u}ltra-{\bf c}old {\bf n}eutrons\\
\end{Large}
\end{center}
\section{Installation of STARucn}
Download the {\tt STARucn.zip} file at {\tt http://lpsc.in2p3.fr/atlas/bclement/starucn/STARucn.zip} and compile it with {\tt make all}
ROOT needs to be installed as well as the GSL library. You might have to change the path of the GLS library in the Makefile.

\section{The script language}
STARucn relies on specific ascii files to configure the various parts of the software.
These scripts use a specific language.

A script consists in a series of single line instructions. Empty lines and line beginning with {\tt \#} are ignored.
A script line can either~:
\begin{itemize}
\item \textbf{include another script}~: \texttt{include <script>}
\item \textbf{define a constant}~: \texttt{define <name> <value>}. The value can be any chain of characters, not including spaces. 
Afterwards, the constant can be referred to in the script with \texttt{@\{<name>\}}. 
Constants can also be nested. In addition two constants are automatically defined~: \texttt{@\{this\}} and \texttt{@\{thispath\}} which refers to the absolute filname and path of the script itself.
\item \textbf{define a value}~: \texttt{<type> <name> <value>}. Each value is caracterized by a type, name and associated value. Default types are either \texttt{string}, \texttt{double},\texttt{int} or \texttt{bool}.
More complex types can be defined by the user in the \texttt{script/parameters.typ} file~:
\begin{itemize}
\ttfamily
\item[]<usertype>:
\item[]  - <type 1>  <key 1>
\item[]  - <type 2>  <key 2>
\item[]$\dots$
\end{itemize}
A user type consists of several keys of basic types. It is used in the script with the syntax~:
\begin{itemize}
\ttfamily
\item[]<user type> <name> <value key 1> <value key 2> \dots
\end{itemize}

Standard and user defined variables will be accessible to the code.
\end{itemize}

When entering an integer of double value, one can also include simple arithmetic as well as standard functions with a specific syntax. Available functions are~:

\begin{table}[!h!tbp]
\begin{center}
\ttfamily
\label{tab-boson}
\begin{tabular}{|cc|cc|cc|}
\hline
$\sin(x)$ & S(x) &$\cos(x)$ & C(x) & $\tan(x)$ & T(x) \\
$\arcsin(x)$ & AS(x) &$\arccos(x)$ & AC(x) & $\arctan(x)$ & AT(x) \\
$\sinh(x)$ & HS(x) &$\cosh(x)$ & HC(x) & $\tanh(x)$ & HT(x) \\
$\exp(x)$ & E(x) &$\ln(x)$ & L(x) & $\sqrt{x}$ \textrm{or} $x^y$ & R(x) \textrm{or} P(x,y) \\
\hline
\end{tabular}
\end{center}
\end{table} 

\section{The main script}
To run the code, call~:
\begin{itemize}
\ttfamily
\item[]starucn <script>
\end{itemize}
By default, {\tt <script>} is \texttt{scripts\slash STARucn.par}. This script controls the main function of the simulation. One simple, minimal exemple is given here~:

\begin{footnotesize}
\begin{itemize}
\ttfamily
\item[]string RunMode  Generate
\item[]string   Geometry    @$\{$thispath$\}$/geo/TestBox.geo
\item[]
\item[]mcsetup  Generator   VolumeGenerator      @\{thispath\}/generator.par
\item[]mcsetup  Propagator  StepPropagator       @\{thispath\}/propagator.par
\item[]mcsetup  Interaction NeutronInteraction  @\{thispath\}/interaction.par
\item[]mcsetup  Output      TextOutputInterface @\{thispath\}/output.par
\item[]
\item[]int Verbose 0
\item[]int NbParticles    1000
\item[]int MaxBounce      10000
\item[]int GenerationSeed      21235521
\item[]int InteractionSeed     76756243
\end{itemize}
\end{footnotesize}

The first mandatory element is the run mode of the simulation~:
\begin{itemize}
\ttfamily
\item[]string RunMode  <runmode>
\end{itemize}
Three modes are available~:
\begin{itemize}
\item\texttt{Display}~: display the geometry. In this mode the only mandatory information is the geometry setup.
\item\texttt{Generate}~: run the actual full simulation.
\item\texttt{Test}~: instantiate geometries and plug-ins but do not run the particle loop. 
\end{itemize}
A fourth mode allows to display neutron trajectories within the geometry, if such trajectories have been previously generated (see CompleteRootOutputInterface for details)

The second mandatory line defines the input files describing the geometry.
\begin{itemize}
\ttfamily
\item[]string Geometry  <geometry file> 
\end{itemize}

The other parameters are only mandatory when running a simulation. They consists in the setup of the four plugins~: \texttt{Generator}, \texttt{Propagator}, \texttt{Interaction} and \texttt{Output}.
\begin{itemize}
\ttfamily
\item[]mcsetup  <plugin type>   <class name>  <config. file>
\end{itemize}
as well as several numeric constants
\begin{itemize}
\item \texttt{int Verbose <value>}~: level of verbosity, from 0 (nothing) to 2.
\item \texttt{int NbParticles  <value>}~: number of neutrons to generate.
\item \texttt{int MaxBounce      <value>}~: maximal number of bounces for each neutron. This is numerical safety to avoid infinite looping neutrons. One must ensure that number of neutrons reaching this limit stays marginal. 
\item \texttt{int GenerationSeed      <value>} and \texttt{int InteractionSeed     <value>}~: the seeds of the random number generators used for generation and interaction respectively.
\end{itemize}

\section{Geometry}
A geometry is a set of volumes, included in larger world box. Each volume is defined from a shape, a material and a placement matrix.
Here is a simple example of an empty cubic box~:
\begin{footnotesize}
\begin{itemize}
\ttfamily
\item[]int Transparency 70
\item[]geometry Geo  200 200 200  Inside|Top|Bottom|Front|Back|Right|Left
\item[]
\item[]material Vacuum        0            0        0              880          2
\item[]material BeO           250          0.5      0.001          0            4
\item[]
\item[]matrix M2   0    0    105   1000 1000 1000 
\item[]matrix M3   0    0   -105   1000 1000 1000 
\item[]matrix M4   0    105  0     1000 1000 1000 
\item[]matrix M5   0   -105  0     1000 1000 1000 
\item[]matrix M6   105  0    0     1000 1000 1000 
\item[]matrix M7  -105  0    0     1000 1000 1000 
\item[]matrix Id  0  0  0     1000 1000 1000
\item[]
\item[]volume Top    BeO   Box  100|100|5  M2 0 
\item[]volume Bottom BeO   Box  100|100|5  M3 0 
\item[]volume Front  BeO   Box  100|5|110  M4 0 
\item[]volume Back   BeO   Box  100|5|110  M5 0 
\item[]volume Right  BeO   Box  5|120|110  M6 0 
\item[]volume Left   BeO   Box  5|120|110  M7 0 
\item[]volume Inside Vacuum  Box  100|100|100  Id  0
\end{itemize}
\end{footnotesize}
The overall geometry is defined by
\begin{equation*}
\texttt{geometry  Geometry  <WorldX> <WorldY> <WorldZ>  <volumes>}
\end{equation*}
with~:
\begin{itemize}
\item \texttt{ <WorldX> <WorldY> <WorldZ> } are half length in x, y, and z direction of the world box
\item \texttt{ <volumes> } is the list of the volumes constituting the geometry. It consists of the name of the volumes separated by a vertical bar,$|$.
\end{itemize}
For display purposes one can also specify a fraction of transparency of the volumes for OpenGL, with \texttt{int Transparency <value>}

\subsection{Material}
\noindent Materials are defined with the command~:
\begin{itemize}
\ttfamily
\item[]material  <name>  <Vf> <eta> <d>  <tau> <color>
\end{itemize}
where~:
\begin{itemize}
\item \texttt{<name>}  is the name under which the material will be referred to afterwards,
\item \texttt{<Vf>}    is the Fermi potential, in neV,
\item \texttt{<eta>}   is the absorption coefficient upon reflection, defined as the ration of the imaginary and real parts of the Fermi potential, $\eta=\frac{W}{V}$,
\item \texttt{<d>}     is the fraction of diffuse reflection,
\item \texttt{<tau>}   is the effective lifetime, in seconds, of the neutron in the medium. If this value is zero or negative, neutrons will be killed upon entering the material. 
\item \texttt{<color>} is the color used for displaying any volume build from this material. The color scheme uses the default ROOT color code.
\end{itemize}
Extra properties can be added, for use in user-defined plugins. For any material one can add a property as~:
\begin{itemize}
\ttfamily
\item[]materialpropery  <materialname>.<propertyname>  <value>
\end{itemize}
\subsection{Volume and placement}
Volumes are build following the volume definition from the ROOT geometry package (see chapter 18 of the ROOT user's guide).
Distances are given in millimetres and angles in degrees. Shapes are always centred around the (0,0,0) point and along the vertical $z$-axis and therefore need a transformation matrix to place them within the geometry. 
\begin{itemize}
\ttfamily
\item[]volume  <name> <material> <type>  <def> <matrix> <flag>
\end{itemize}
where~:

\begin{itemize}
\item \texttt{<name>}  is the name under which the volume will be referred to afterwards,
\item \texttt{<material>} is the material constituing the volume (if is used in the final geometry),
\item \texttt{<type>}  is the type of shape. The available shapes, corresponding to the ROOT shapes classes \texttt{TGeo<type>} are ~: \texttt{Arb8}, \texttt{Box}, \texttt{Cone}, \texttt{Cons}, \texttt{Ctub}, \texttt{Eltu}, \texttt{Gtra}, \texttt{Hype}, \texttt{Para}, \texttt{Pcon}, \texttt{Pgon}, \texttt{Sphere}, \texttt{Toru}, \texttt{Trap}, \texttt{Trd1}, \texttt{Trd2}, \texttt{Tube} and \texttt{Tubs}.

\item \texttt{<def>}      is the list of parameters defining the shape. They are given in the same order as in the ROOT volume definition, separated by vertical bars $|$.
\item \texttt{<matrix>}   is the placement matrix.
\item \texttt{<flag>} is an additional integer flag that can be used for various purposes.
\end{itemize}
Alternatively, one can also define composite volumes, 
\begin{equation*}
\texttt{composite  <name>  <material> <def> <matrix> <flag>}
\end{equation*}
where \texttt{<def>} follow the syntax of composite volumes in the ROOT geometry package.
As an example the external part of the empty box shown previously could have been defined as a single composite volume instead of six parts~:
\begin{itemize}
\ttfamily
\item[]volume ExternalBox vacuum   Box  120|120|120  Id 0 
\item[]composite Outside  BeO      ExternalBox-Inside  Id 0
\end{itemize}

Volume placement is done through a combination of a rotation around the (0,0,0) center followed by a translation of the center.
\begin{equation*}
\texttt{matrix <name>  <Tx> <Ty> <Tz> <A1> <A2> <A3>}
\end{equation*}
with
\begin{itemize}
\item \texttt{<name>}  is the name under which the matrix will be referred to,
\item \texttt{<Tx> <Ty> <Tz>} are the translation along the three axes,
\item \texttt{<A1> <A2> <A3>} are the Euler angles of the rotation. If at least one of these is set to -1000, the rotation part will be skipped.
\end{itemize}
One must be very careful that there is no overlap nor gap between volumes constituting the geometry. When instantiating the geometry, STARucn checks for volume overlaps (but not gaps) and will print a warning.

%
\section{Plugins}
Plug-ins are modules that perform specific parts of the simulation. There are four kind of plug-ins~: 
\begin{itemize}
\item\texttt{Generator}~: creates the initial coordinates, direction, velocity and time of the simulated neutron 
\item\texttt{Propagator}~: handles the propagation of the neutron within the geometry from the current position to the next boundary of the active volume.
\item\texttt{Interaction}~: handles the interaction of the neutron during its propagation within the volume and at the interface.
\item\texttt{Output}~: handles the information to be saved at each simulation step and at the end of each neutron.
\end{itemize}
Each plug-in is configured with its specific parameter script, with specific keywords.
\subsection{Generators}
Two default generators are available~:
\begin{itemize}
\item\texttt{VolumeGenerator}~: Neutrons are uniformly distributed within one volume of the geometry with a user specified velocity distribution and production rate~:
Coordinates are provided in mm, time in s and velocities in mm/s.
\begin{itemize}
\item\texttt{string   VolumeGenerator.Volume  <name>}~: name of the volume used for generation\\
\item\texttt{double	 VolumeGenerator.Rate	 <rate>}~: production rate, in Hz. If negative, all neutrons are produced at time 0.\\
\item\texttt{string   VolumeGenerator.Mode    <mode>}~: the mode define the way the velocity density is provided~: 
\texttt{XYZ} cartesian coordinates ($x$,$y$,$z$), \texttt{CPV} spherical coordinates along the $x$-axis ($\cos\theta$, $\varphi$, $V$), \texttt{CPVZ}Spherical coordinates along the $z$-axis.\\
\item\texttt{density  VolumeGenerator.Density\_<X>    <fun>   <min>   <max> }~: Specify the probability density for a given variable {\tt <X>=X,Y,Z,C,P,V}. The function {\tt <fun>} is given using ROOT TF1 syntax or by the specific keyword {\tt Uniform} for uniform density. Than range of the pdf is given by {\tt <min>} and {\tt <max>}.\\
\end{itemize} 
\item\texttt{TextGenerator}~: Neutrons are read from a simple ascii file where each neutron is described in one line of 8 numbers ~: the position (cartesian vector, in mm), direction (cartesian vector), velocity (in mm/s) and time (s). Note that the $TextOutput$ output plug-in can produce such a file.
\begin{itemize}
\item\texttt{string   FileGenerator.InputFile <name>}~: name of the input file to read.\\
\end{itemize} 
\end{itemize}

\section{Propagators}
This plug-in computes the time and interaction point where the neutron is going out of its current containing volume. Two techniques are used in STARucn. Both have similar configuration.
\begin{itemize}
\item\texttt{StepPropagator}~: The next surface is reached through time steps following is trajectory. When the neutron goes out of the volume, the last step is cancelled and the time step is reduced, until the distance to surface is smaller than some tolerance.
\item\texttt{AnalyticPropagator}~: For the simpler volumes (box, cylinder, tube and torus), the analytic solution of the intersection is computed. For more complex volumes, the \texttt{StepPropagator} will be automatically used, hence the set-up of this plug-in require also the inputs for the step propagation. 
\end{itemize}
The configuration script must contain the following keywords~:
\begin{itemize}
\item\texttt{double   StepPropagator.StepSize     <length>}~: initial size (in mm) of the step. If set to -1, an adaptative step will be used; this is the recommended mode. Fixed step is used if one need to save the particle trajectory with many points along the trajectory.
\item\texttt{double   StepPropagator.Tolerance    <length>}~: minimal distance from the surface to which we assume we reached the surface.
\item\texttt{bool     StepPropagator.UseGravity   <bool>}~: switch on (1) or off (0) the gravity.
\end{itemize}
For the \texttt{AnalyticPropagator}, the keyword are identical, replacing \texttt{StepPropagator.<X>} by  \texttt{AnalyticPropagator.<X>}

\section{Interaction}
There is only one interaction plugin available, \texttt{NeutronInteraction}, that does not need any external configuration.

\section{Output}
Outputs of the simulation can be produced after each step and at the end of each neutron. Three default output plug-ins are available~:
\begin{itemize}
\item\texttt{CompleteRootOutputInterface}~: Information on each neutron are saved into a ROOT tree named \texttt{starucn}. This plug-in allow also to save trajectories of some of the generated neutrons.
\begin{itemize}
\item\texttt{string  CompleteRootOutputInterface.OutputFile         <file>}~: name of the output file.
\item\texttt{int     CompleteRootOutputInterface.TrajectoryRecord   <nb>}~: record the trajectory every \texttt{<nb>} neutron. If off \texttt{<nb>}=0, this feature is switched off.
\item\texttt{string  CompleteRootOutputInterface.TrajectoryRecordFinalVolume   <volumes>}~: extra flag to record only neutrons that end up in one of the listed volumes (volumes name separated by vertical bars). If not defined, all recorded trajectories are saved.
\end{itemize} 

\item\texttt{HistogramOutputInterface}~: This plug-in fills an histogram recording the number of neutrons within a given volume as a function of time.
\begin{itemize}
\item\texttt{string       HistogramOutputInterface.OutputFile   <file>}~: name of the output file.
\item\texttt{double       HistogramOutputInterface.SnapShotTime  <time>}~: duration of each time bin.
\item\texttt{int          HistogramOutputInterface.NbSnapShots   <nb>}~: number of bins.
\item\texttt{string       HistogramOutputInterface.VolumeName    <volume>}~: volume to study. If not specified the full geometry is used. It is not possible at the moment to specify several volumes.
\end{itemize} 


\item\texttt{TextOutputInterface}~: dump the information for each neutron into a text file for later use. Neutron data can be recoreded after a fixed time (\texttt{Time} mode), when they enter a specific volume (\texttt{Volume} mode) or after a fixed number of interactions (\texttt{Interaction} mode, mostly used for debugging purpose). The generated text file can be used as an input of a second simulation, through the \texttt{TextGenerator} plug-in.
\begin{itemize}
\item\texttt{string    TextOutputInterface.OutputFile   <file>}~: name of the output file.
\item\texttt{bool      TextOutputInterface.DoRootTree   <bool>}~: if 1, a ROOT tree will also be created mirroring the output file.
\item\texttt{string    TextOutputInterface.StopMode     <mode>}~: saving mode.
\item\texttt{double    TextOutputInterface.StopTime     <time>}~: for \texttt{Time} mode only, specify the time in seconds after which data are dumped.
\item\texttt{string    TextOutputInterface.FinalVolumes <volumes>}~: for \texttt{Volume} mode only, list of volumes to reach. 
\item\texttt{int       TextOutputInterface.NbInteract   <nb>}~: for \texttt{Interaction} mode only, number of allowed interactions. 
\end{itemize} 

\end{itemize}

An additional fourth output plug-in allow to run simultaneously several output plugins~: \texttt{MultiOutputInterface}. The configuration script is as follow~:
\begin{itemize}
\item\texttt{int       MultiOutputInterface.NbOutputs    <nbmax>}~: number of output plug-ins to use.
\item\texttt{mcsetup  MultiOutputInterface.Output\_<n>  <class name>  <config. file>}~: type and configuration file of the \texttt{<n>}-th plug-in (\texttt{<n>}=1 to <nbmax>)
\end{itemize} 

\section{User defined plug-ins}

All plug-ins have a mandatory \texttt{void Configure(const char*)} methods, that reads the input file and initialize the plug-in. 
This method is called at the beginning of the simulation

\subsection{Script interpreter}


\end{document}