#!/usr/bin/env python

#before running, do:
#pip install --user rootpy
#pip install --user root_numpy

import os
from scipy import optimize
import numpy
import shutil
import random
import math
from rootpy.tree import Tree
from rootpy.io import root_open
from root_numpy import tree2array
from matplotlib import pyplot
import threading

def getalpha(i,outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	resultsarray = tree2array(resultstree)
	alpha = resultsarray[i][4]
	alphaerror = resultsarray[i][5]
	return alpha,alphaerror
	
def gettruefalseedm(i,outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	resultsarray = tree2array(resultstree)
	falseedm = resultsarray[i][12]
	error = resultsarray[i][13]
	return falseedm,error
	
def getmeasuredfalseedm(i,outputfile):
	f = root_open(outputfile,"open")
	resultstree = f.Get("starucn_summary") #gets the results tree
	resultsarray = tree2array(resultstree)
	falseedm = resultsarray[i][18]
	error = resultsarray[i][19]
	return falseedm,error

print "STARucn T2 Script - Nicholas Ayres 2014 na280@sussex.ac.uk"
print "This generates a run file, runs STARucn on it, reads alpha from file, runs until alpha < endalpha then calculates T2"


#initialise some stuff
endalpha = 0.9 #finish when alpha gets below this number
timestep = 1 #timestep in seconds
NbParticles = 1000 #number of neutrons to start with
random.seed(1) #seed for RNG, in turn seeds STARucn RNGs
runid = "T2" #so we know how to name everything...
killtime = 5

dBdzArray = numpy.linspace(-20e-12,20e-12,5)
dBdzPointFitArray = numpy.linspace(-20e-12,20e-12,100)
T2Array = numpy.zeros(len(dBdzArray))
T2ErrorArray = numpy.zeros(len(dBdzArray))
true_fedm_array = numpy.zeros(len(dBdzArray))
true_fedm_errarray = numpy.zeros(len(dBdzArray))
meas_fedm_array = numpy.zeros(len(dBdzArray))
meas_fedm_errarray = numpy.zeros(len(dBdzArray))

#tell each thread how to do its job
def starucnthread(runsection):
	#base script 
	basescriptpath = os.getcwd() + '/scripts/T2_vs_dBdz_BaseScript.par'
	
	#folder in which to place automatically generated starucn scripts
	outscriptfolder = os.getcwd() + '/scripts/autogenscripts'
	
	#output file
	outputfile = "output_" + runid + "_" + str(runsection) + ".root"
	
	os.system("rm " +outputfile) #should really do with shutils...
	
	#for starting parameters alpha = 1 exactly at t = 0
	alphalist = numpy.array([1])
	alphaerrorlist = numpy.array([0])
	storagetimelist = numpy.array([0])
	
	i = 0
	while (alphalist[i] > endalpha and ((i * timestep) < killtime)):
		scriptpath = outscriptfolder + '/' + runid + "_" + str(runsection) + "_" + str(i) + '.par'
		shutil.copy(basescriptpath,scriptpath) #copy base script
		print "Opening File"
		f = open(scriptpath,'a') #open for appending
		#first time use EDMVolumeGenerator, then use EDMRootFileGenerator
		print "Writing to File"
		if (i == 0):
			f.write("mcsetup Generator VolumeGenerator  @{this} \n")
		else:
			f.write("mcsetup Generator RootFileGenerator  @{this} \n")
		#write lines to file
		f.write("int NbParticles " + str(NbParticles) + "\n")
		f.write("string RootFileGenerator.inputrunid " + runid + "\n")
		f.write("string RootFileGenerator.inputrunsection " + str(runsection) + "\n")
		f.write("string RootFileGenerator.inputruntimeslice " + str(i-1)+ "\n")
		f.write("string RootOutputInterface.RunID " + runid+ "\n") 
		f.write("int SpinRootOutputInterface.TimeSlice " + str(i)+ "\n")
		f.write("int SpinRootOutputInterface.RunSection " + str(runsection) + "\n")
		f.write("double SpinRootOutputInterface.StorageTime " + str ((i+1) * timestep)+ "\n")
		f.write("int GenerationSeed "  + str(random.randint(0,2147483647))+ "\n") #scale random number to largest integer
		f.write("double FieldManager.BGrad.dFdz " + str(dBdzArray[runsection])+ "\n")
		print "closing file"
		f.close()
		
		print "running starucn with command \"./starucn " + scriptpath + "\" "
		os.system("./starucn " + scriptpath + ">/dev/null")
		
		alpha,alphaerror = getalpha(i,outputfile)
		
		storagetimelist = numpy.append(storagetimelist,(i+1)*timestep)
		alphalist = numpy.append(alphalist,alpha)
		alphaerrorlist = numpy.append(alphaerrorlist,alphaerror)
		i=i+1
		print "alpha = ", alpha
	
	print alphalist
	print alphaerrorlist
		
	#function to fit
	def expdecay(t,A,T):
		return A*numpy.exp(-t/T)
	
	popt, pcov = optimize.curve_fit(expdecay,storagetimelist,alphalist)#,None,alphaerrorlist,True)
	print "found popt, pcov ", popt, pcov
	perr = numpy.sqrt(numpy.diag(pcov))
	
	print "T2 = ", popt[1], "+-" , perr[1]
	
	T2Array[runsection]= popt[1]
	T2ErrorArray[runsection]=perr[1]
	
	true_fedm, true_fedm_err = gettruefalseedm(i-1,outputfile)
	meas_fedm, meas_fedm_err = getmeasuredfalseedm(i-1,outputfile)
	
	print "True False EDM = " , true_fedm, "+-", true_fedm_err
	print "Measured False EDM = ", meas_fedm , "+-", meas_fedm_err
	
	true_fedm_array[runsection] = true_fedm
	true_fedm_errarray[runsection] = true_fedm_err
	
	meas_fedm_array[runsection] = meas_fedm
	meas_fedm_errarray[runsection] = meas_fedm_err
	
	return popt[1],perr[1]

class starucnthreadobject (threading.Thread):
    def __init__(self,runsection):
		threading.Thread.__init__(self)
		self.runsection = runsection
		return
    def run(self):
        print "Starting runsection " , self.runsection
        starucnthread(self.runsection)
        print "Exiting runsection ", self.runsection
        return

threads = []

print "starting threads"

for i in range(len(dBdzArray)):
	 thread1 = starucnthreadobject(i)
	 thread1.start()
	 threads.append(thread1)

print "threads all started, running t.join until they all terminate"

#wait for all threads to finish
for t in threads:
	t.join()

print "True False EDM Array = "
print true_fedm_array
print "Measured False EDM Array = "
print meas_fedm_array

print "about to fit to "
print dBdzArray
print T2Array

def straightline(x,a,b):
	return a*x+b

popt, pcov = optimize.curve_fit(straightline,dBdzArray,T2Array)

#T2Fitted = straightline(dBdzPointFitArray,*popt)

print "plotting..."

pyplot.plot(dBdzArray,T2Array,'r+',label="MC data")
#pyplot.plot(dBdzPointFitArray,T2Fitted,'-',label="fit")
pyplot.legend()
pyplot.savefig("image.png")
